"use strict";

// Class definition

var dataKaryawan = (function() {
    return {
        // Init demos
        init: function() {
            $('#grade').select2({
                placeholder: "Grade",
            });
            $('#sub_grade').select2({
                placeholder: "Sub Grade",
            });
            $('#tabelUjiKelayakan').DataTable();
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });
        }
    };
})();
$('#file').on("change", function() {
    $('#labelFile').text('');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});

$('#btnEksport').click(function() {
    $.ajax({
        type: "GET",
        url: 'uji_kelayakan/eksport_exel',
        data: {
            grade: $('#grade').val(),
            sub_grade: $('#sub_grade').val(),
            tahun: $('#txt_tahun').val(),
        },
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
})

function tabelUjiKelayakan(data) {
    var table = $('#tabelUjiKelayakan').DataTable({
        "orderCellsTop": true,
        "processing": true,
        "destroy": true,
        responsive: true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [2, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        data: data,
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                data: null,
                "orderable": false
            },
            {
                data: "nik"
            },
            {
                data: "nama"
            },
            {
                data: "grade"
            },
            {
                data: "unit_name"
            },
            {
                data: "nm_direktorat"
            },
            {
                data: "asesmen"
            },
            {
                data: "kat_asesmen"
            },
            {
                data: "satu"
            },
            {
                data: "dua"
            },
            {
                data: "tiga"
            },
            {
                data: "potensi"
            },
            {
                data: "kat_potensi"
            },
            {
                data: "nilai_sebelum"
            },
            {
                data: "nilai_sekarang"
            },
            {
                data: "avg_pak"
            },
            {
                data: "kat_pak"
            },
            {
                data: "kat_hasil"
            },
            {
                data: "kat_talentuk"
            },
            {
                data: "tgl_pensiun"
            },
        ],

    });
    table.on('order.dt search.dt', function() {
        table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

};

$('#import_form').on('submit', function(event) {
    event.preventDefault();
    console.log('upload')

    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "uji_kelayakan/import_excel",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",

        success: function(data) {

        }
    })
});

function box1(pak, kki) {
    $('#data_talent').modal('show');
    var tabel = $('#tabelDetailNilai').DataTable({
        "processing": true,
        "destroy": true,
        "searching": true,
        "responsive": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "uji_kelayakan/list_data", // URL file untuk proses select datanya
            "type": "POST",
            "data": {
                pak: pak,
                kki: kki,
                grade: $('#grade').val(),
                sub_grade: $('#sub_grade').val()
            }
        },
        "columns": [{
                "data": "no"
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "hasil"
            },

        ],
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

$("#grade").change(function() {
    var grade = $(this).val();
    if (grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "uji_kelayakan/get_sub_grade", // the method we are calling
            data: {
                grade: grade
            },
            dataType: "json",
            beforeSend: function() {
                $('#sub_grade').html('<option value="">--Pilih Sub Grade--</option>');
            },
            success: function(data) {
                // get_data();

                $('#sub_grade').html(data);
            },
            error: function(xhr, status, error) {}

        });
    } else {}
});

$('#btn_cari').click(function() {
    get_data();
});


function get_data() {
    var consistent_star = 0;
    var utility_hi_pro = 0;
    var high_contri = 0;
    var future_start = 0;
    var experienced_pro = 0;
    var contributor = 0;
    var diamond = 0;
    var incosistent = 0;
    var iceberg = 0;
    jQuery.ajax({
        type: "POST",
        url: "uji_kelayakan/get_data", // the method we are calling
        data: {
            grade: $('#grade').val(),
            sub_grade: $('#sub_grade').val(),
            tahun: $('#txt_tahun').val(),
        },
        dataType: "json",
        success: function(data) {
            tabelUjiKelayakan(data);
            for (var i = 0; i < data.length; i++) {
                if (data[i].hasil == 3) {
                    consistent_star = parseInt(consistent_star) + 1;
                } else if (data[i].hasil == 2) {
                    utility_hi_pro = parseInt(utility_hi_pro) + 1;
                } else if (data[i].hasil == 1) {
                    high_contri = parseInt(high_contri) + 1;
                } else if (data[i].hasil == 6) {
                    future_start = parseInt(future_start) + 1;
                } else if (data[i].hasil == 5) {
                    experienced_pro = parseInt(experienced_pro) + 1;
                } else if (data[i].hasil == 4) {
                    contributor = parseInt(contributor) + 1;
                } else if (data[i].hasil == 9) {
                    diamond = parseInt(diamond) + 1;
                } else if (data[i].hasil == 8) {
                    incosistent = parseInt(incosistent) + 1;
                } else if (data[i].hasil == 7) {
                    iceberg = parseInt(iceberg) + 1;
                }
            }

            document.getElementById('consistent_star').innerHTML = "CONSISTENT STAR <br/>" + consistent_star;
            document.getElementById('utility_hi_pro').innerHTML = "UTILITY HI-PRO <br/>" + utility_hi_pro;
            document.getElementById('high_contri').innerHTML = "HIGH CONTRIBUTION <br/>" + high_contri;
            document.getElementById('future_start').innerHTML = "FUTURE STAR <br/>" + future_start;
            document.getElementById('experienced_pro').innerHTML = "EXPERIENCED PRO <br/>" + experienced_pro;
            document.getElementById('contributor').innerHTML = "CONTRIBUTOR <br/>" + contributor;
            document.getElementById('diamond').innerHTML = "DIAMOND IN THE ROUGH <br/>" + diamond;
            document.getElementById('incosistent').innerHTML = "INCONSISTENT PERFORMER <br/>" + incosistent;
            document.getElementById('iceberg').innerHTML = "ICEBERG <br/>" + iceberg;



        },
        error: function(xhr, status, error) {}

    });
}

// Class initialization on page load
jQuery(document).ready(function() {
    dataKaryawan.init();
});