"use strict";

// Class definition

var jobFitFamily = (function() {
    return {
        // Init demos
        init: function() {
            getPdf();
        }
    };
})();

function getPdf() {
    $.ajax({
        type: "GET",
        url: "laporan_minat_bakat/getPdf", // the method we are calling
        data: {
            nik: $('#nikUser').val()
        },
        dataType: "json",
        success: function(data) {
            var pathPdf = '';
            if (data[0]) {
                console.log('tidak kosong');
            } else {
                console.log('kosong');
            }
            pathPdf = $('#baseUrl').val() + '' + data[0].file_path;
            console.log(pathPdf);
            $('#viewPdf').attr('data', pathPdf);
        },
        error: function(xhr, status, error) {
            console.log(error);
        }
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    jobFitFamily.init();
});