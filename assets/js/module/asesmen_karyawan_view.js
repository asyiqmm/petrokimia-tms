"use strict";

// Class definition

var jobFitFamily = (function() {
    return {
        // Init demos
        init: function() {
            // jobFamily();
            tabelNilai();
        }
    };
})();

function modalPDF(path) {
    $('#viewPdf').remove();
    $('#canvas_father').append('<object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>');
    var pathPdf = $('#baseUrl').val() + '' + path;
    $('#viewPdf').attr('data', pathPdf);
    $('#nilaiPotensi').modal('show');
}


function tabelNilai() {
    var tabel = $('#tabelAsesmen').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "asesmen_karyawan/get_listAsesmen", // URL file untuk proses select datanya
            "type": "POST",
            "dataType": "json",
            // "data": {
            //     nik: $('#nik').val(),
            //     kd_dept: $('#id_dept').val()
            // }

        }, // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "grade"
            },
            {
                "data": "jabatan"
            },
            {
                "data": "total"
            },
            {
                "data": "rekomendasi"
            },
            {
                "data": "pdf"
            },
        ],
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(0)
                .addClass("text-center");
            $("td", row)
                .eq(3)
                .addClass("text-center");
            $("td", row)
                .eq(5)
                .addClass("text-center");
        }
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}
// Class initialization on page load
jQuery(document).ready(function() {
    jobFitFamily.init();
});