"use strict";

// Class definition

var dataKaryawan = (function() {
    return {
        // Init demos
        init: function() {
            tabelKaryawan();
        }
    };
})();
$('#file').on("change", function() {
    $('#labelFile').text('Choose file');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});


function hapusUser(nik) {
    swal
        .fire({
            title: "Perhatian",
            text: "Apakah anda ingin menghapus data?",
            type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
        })
        .then(function(result) {
            if (result.value) {
                $.ajax({
                    // url: "nilaiAsesmen/import",
                    url: "karyawan_talenta/hapus",
                    type: "POST",
                    data: { nik: nik },
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        $("#tabelKaryawan")
                            .DataTable()
                            .ajax.reload();
                    }
                })
            } else {}
        });

}

function tabelKaryawan() {
    var table = $('#tabelKaryawan').DataTable({
        // "orderCellsTop": true,
        "processing": true,
        "destroy": true,
        "searching": true,
        "responsive": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [1, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "karyawan_talenta/getListKaryawan", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": null,
                "orderable": false
            },
            {
                "data": "nama"
            },
            {
                "data": "nik_tetap"
            },
            {
                "data": "nik"
            },
            {
                "data": "grade"
            },
            {
                "data": "nm_direktorat"
            },
            {
                "data": "departemen"
            },
            {
                "data": "jabatan"
            },
            {
                "data": "job_family"
            },

            {
                "data": "aksi"
            },

        ],
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(0)
                .addClass("text-center");
            $("td", row)
                .eq(7)
                .addClass("text-center");
        },

    });
    table.on('order.dt search.dt', function() {
        table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    // $('#tabelKaryawan tfoot th').not(":eq(0),:eq(7)") //Exclude columns 3, 4, and 5
    //     .each(function() {
    //         var title = $('#tabelKaryawan thead td').eq($(this).index()).text();
    //         $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
    //     });
    // table.columns().eq(0).each(function(colIdx) {
    //     if (colIdx == 0 || colIdx == 7) return; //Do not add event handlers for these columns

    //     $('input', table.column(colIdx).footer()).on('keyup change', function() {
    //         table
    //             .column(colIdx)
    //             .search(this.value)
    //             .draw();
    //     });
    // });

};

// function ubahUser(nik) {

// }

$('#import_form').on('submit', function(event) {
    event.preventDefault();
    console.log('upload')
    var berhasil = document.getElementById('listBerhasil');
    var gagal = document.getElementById('listGagal');
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "karyawan_talenta/import_excel",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            berhasil.innerHTML = '';
            gagal.innerHTML = '';
            $("#btnsubmit").attr("disabled", true);
            $('#file').attr("disabled", true);
        },
        success: function(data) {
            $("#btnsubmit").attr("disabled", false);
            $('#file').attr("disabled", false);
            var dataBerhasil = "";
            var dataGagal = "";
            if (data.status) {
                swal.fire('Berhasil', 'berhasil mengunggah file', 'success');
                var j = 1;
                for (var i = 0; i < data.sukses.length; ++i) {
                    dataBerhasil += '<li>' + j + '. ' + data.sukses[i] + '</li>';
                    j++;
                }
                var j = 1;
                for (var i = 0; i < data.gagal.length; ++i) {
                    dataGagal += '<li>' + j + '. ' + data.gagal[i] + '</li>';
                    j++;
                }
                berhasil.innerHTML = '<ul>' + dataBerhasil + '</ul>';
                gagal.innerHTML = '<ul>' + dataGagal + '</ul>';
            } else {
                swal.fire('Gagal', 'gagal mengunggah file', 'error');
            }
            $("#tabelKaryawan")
                .DataTable()
                .ajax.reload();
        }
    })
});
$('#formUbah').on('submit', function(event) {
    event.preventDefault();

    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "karyawan_talenta/ubah",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {},
        success: function(data) {
            if (data.status == true) {
                $('#modalUbah').modal('hide');
                swal.fire('Berhasil', 'Berhasil mengubah data', 'success')
                $("#tabelKaryawan")
                    .DataTable()
                    .ajax.reload();
            } else {
                swal.fire('Gagal', 'Kode mengubah data', 'error')
            }
        }
    })
});


function ubahUser(nik) {
    console.log(nik);
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "karyawan_talenta/get_karyawan",
        type: "POST",
        data: { nik: nik },
        dataType: "json",
        success: function(data) {
            console.log(data);

            $('#nik_karyawan').val(data[0].nik);
            $('#nama_karyawan').val(data[0].nama);
            $('#grade_karyawan').val(data[0].grade);
            $('#direktorat_karyawan').val(data[0].kd_direktorat);
            $('#job_karyawan').val(data[0].id);
            $("#modalUbah").modal("show");
        }
    })
}

// Class initialization on page load
jQuery(document).ready(function() {
    dataKaryawan.init();
});