"use strict";

// Class definition

var dataKaryawan = (function() {
    return {
        // Init demos
        init: function() {
            $('#grade').select2({
                placeholder: "Grade",
            });
            $('#tipe').select2({
                placeholder: "",
            });
            $('#tabelUjiKelayakan').DataTable({});
            $('#tabelIdentifikasi').DataTable({});
            $('#tabelEksisting').DataTable({});

            grade();
        }
    };
})();

function grade() {
    jQuery.ajax({
        type: "POST",
        url: "rekap_talent/get_grade", // the method we are calling
        dataType: "json",
        success: function(data) {

            $('#grade').append(data)
            $("#btnCari").click(function() {
                cari_hitung_talent();
            });
        }
    });
}

$("#btnCariJumlah").click(function() {
    identifikasi_jumlah();
});

var intVal = function(i) {
    return typeof i === 'string' ?
        i.replace(/[\$,]/g, '') * 1 :
        typeof i === 'number' ?
        i : 0;
};

function cari_hitung_talent() {
    var tabel = $('#tabelUjiKelayakan').DataTable({
        "processing": true,
        "destroy": true,
        "paging": false,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [1, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "rekap_talent/get_data_hitung_talent", // URL file untuk proses select datanya
            "type": "POST",
            "dataType": "json",
            "data": {
                grade: $("#grade").val()
            },
        }, // Combobox Limit
        "columns": [{
                "data": null,
                orderable: false
            },
            {
                "data": "nm_direktorat"
            },
            {
                "data": "non_talent"
            },
            {
                "data": "talent"
            },
            {
                "data": "grand_total"
            }
        ],
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            var non_talent = 0;

            var non_talent = api
                .column(2)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var talent = api
                .column(3)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var total = api
                .column(4)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(non_talent);
            $(api.column(3).footer()).html(talent);
            $(api.column(4).footer()).html(total);
        },
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(1)
                .addClass("text-left");
        },
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

function identifikasi_jumlah() {
    var tabel = $('#tabelIdentifikasi').DataTable({
        "processing": true,
        "destroy": true,
        "paging": false,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [1, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "rekap_talent/get_data_hitung_talent", // URL file untuk proses select datanya
            "type": "POST",
            "dataType": "json",
            "data": {
                tipe: $('#tipe').val()
            },
        }, // Combobox Limit
        "columns": [{
                "data": null,
                orderable: false
            },
            {
                "data": "nm_direktorat"
            },
            {
                "data": "grade1"
            },
            {
                "data": "grade2"
            },
            {
                "data": "grade3"
            },
            {
                "data": "grade4"
            },
            {
                "data": "grade5"
            },
            {
                "data": "grade6"
            },
            {
                "data": "grade7"
            }
        ],
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            var grade1 = 0;
            var grade1 = api
                .column(2)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var grade2 = api
                .column(3)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var grade3 = api
                .column(4)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var grade4 = api
                .column(5)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var grade5 = api
                .column(6)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var grade6 = api
                .column(7)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var grade7 = api
                .column(8)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(grade1);
            $(api.column(3).footer()).html(grade2);
            $(api.column(4).footer()).html(grade3);
            $(api.column(5).footer()).html(grade4);
            $(api.column(6).footer()).html(grade5);
            $(api.column(7).footer()).html(grade6);
            $(api.column(8).footer()).html(grade7);
        },
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(1)
                .addClass("text-left");
        },
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}


// Class initialization on page load
jQuery(document).ready(function() {
    dataKaryawan.init();
});