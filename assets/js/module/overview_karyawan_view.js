"use strict";

// Class definition

var overview_karyawan = (function() {
    return {
        // Init demos
        init: function() {
            grafikPenilaian();
        }
    };
})();

function grafikPenilaian() {
    $('#grafikPenilaian').remove();
    $('#canvas_father').append('<canvas id="grafikPenilaian" height="100" width="420"></canvas>');
    var label = [];
    var nilai = [];
    var standar = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "POST",
        url: "overview_individu/get_grafikAsesmen",
        data: {
            nik: $('#nik').val(),
            grade: $('#grade').val(),
            job_family: $('#id_job_fit').val(),
            jabatan: $('#jabatan').val(),
        },
        dataType: "json",
        success: function(data) {
            for (var i in data) {
                nilai.push(data[i].nilai_asesmen);
                label.push(data[i].kkj);
                standar.push(data[i].nilai);
            }
            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Pencapaian Individu',
                        borderColor: "rgba(0, 123, 255, 0.9)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        fontFamily: "Poppins",
                        data: nilai
                    },
                    {
                        label: 'Standar Kompetensi Jabatan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(0,0,0,0.09)",
                        borderWidth: "0",
                        backgroundColor: "rgba(192, 57, 43,1.0)",
                        data: standar
                    },
                ]
            };

            var ctx = document.getElementById("grafikPenilaian");
            // ctx.height = 110;
            // ctx.width = 420;

            barGraph = new Chart(ctx, {
                type: 'bar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    "animation": {
                        "duration": 2000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function(dataset, i) {
                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 1,
                                min: 0,
                                max: 6,
                                display: true,

                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {
            console.log(data);
        }
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    overview_karyawan.init();
});