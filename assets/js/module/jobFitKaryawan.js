"use strict";

// Class definition

var jobFitKaryawan = (function() {
    return {
        // Init demos
        init: function() {
            jobFitFamily();
            $("#grade").select2({
                placeholder: "--Pilih Grade--",
                allowClear: true
            });
            $("#sub_grade").select2({
                placeholder: "--Pilih Sub Grade--",
                allowClear: true
            });
            $("#nama").select2({
                placeholder: "--Pilih Nama Pegawai--",
                allowClear: true
            });
            $("#rekomendasi").select2({
                placeholder: "--Pilih Jenis Rekomendasi--",
                allowClear: true
            });
            $("#jobFitFamily").select2({
                placeholder: "--Pilih Job Family--",
                width: "100%",
            });
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });

        }
    };
})();
$("#txt_tahun").change(function() {
    var tahun = $(this).val();
    if (sub_grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "job_fit_karyawan/get_grade", // the method we are calling
            dataType: "json",
            beforeSend: function() {
                $('#grade').html('<option value="">--Pilih Grade--</option>');
            },
            success: function(data) {
                $('#grade').html(data);
            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#grade').html('<option value="">--Pilih Grade--</option>');
    }
});
$("#sub_grade").change(function() {
    var sub_grade = $(this).val();
    var tahun = $('#txt_tahun').val();
    if (sub_grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "job_fit_karyawan/get_karyawan", // the method we are calling
            data: {
                sub_grade: sub_grade,
                tahun: tahun
            },
            dataType: "json",
            beforeSend: function() {
                $('#nama').html('<option value="">--Pilih Nama Pegawai--</option>');
                $('#departemen').val('');
            },
            success: function(data) {
                $('#nama').html(data);
            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#nama').html('<option value="">--Pilih Nama Pegawai--</option>');
    }
});
$("#rekomendasi").change(function() {
    if ($(this).val() == 55 || $(this).val() == 66) {
        $("#jobFitFamily").attr("required", false);
    } else {
        $("#jobFitFamily").attr("required", true);
    }
});

$("#grade").change(function() {
    if ($(this).val() == 1) {
        $("#jobFitFamily").attr("required", false);
        $('#infograde').addClass('d-none');
        $('#infograde1').removeClass('d-none');
    } else {
        $("#jobFitFamily").attr("required", true);
        $('#infograde1').addClass('d-none');
        $('#infograde').removeClass('d-none');
    }
    var grade = $(this).val();
    if (grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "job_fit_karyawan/get_nama", // the method we are calling
            data: {
                grade: grade,
                tahun: $('#txt_tahun').val()
            },
            dataType: "json",
            beforeSend: function() {
                $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--</option>');
                $('#sub_grade').html('<option value="">--Pilih Sub Grade--</option>');
                $('#nama').html('<option value="">--Pilih Nama Pegawai--</option>');
                $('#departemen').val('');
            },
            success: function(data) {
                $('#sub_grade').html(data.sub_grade);
                $('#nama').html(data.karyawan);
                $('#rekomendasi').html(data.rekomendasi);
            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#nama').html('<option value="">--Pilih Nama Pegawai--</option>');
        $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--\</option>');
    }
});

$("#nama").change(function() {
    var nama = $(this).val();
    if (nama !== '') {

        jQuery.ajax({
            type: "POST",
            url: "job_fit_karyawan/get_rekomendasi", // the method we are calling
            data: {
                nama: nama,
                tahun: $('#txt_tahun').val()
            },
            dataType: "json",
            success: function(data) {
                $('#departemen').val(data.departemen[0].nm_dept);
                $('#jabatan').val(data.departemen[0].jabatan);

            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--\</option>');

    }
});

function jobFitFamily() {
    jQuery.ajax({
        type: "POST",
        url: "job_fit_karyawan/get_jobFit", // the method we are calling
        dataType: "json",
        success: function(data) {
            $('#jobFitFamily').html(data);

        },
        error: function(xhr, status, error) {}

    });
}
// $('#jobFitFamily').change(function name(params) {
//     grafikPenilaian();
// });

// $('#rekomendasi').change(function name(params) {
//     grafikPenilaian();
//     grafikJobFamily();
// });
$('#btnCari').on('submit', function(event) {
    event.preventDefault();
    grafikPenilaian();
    grafikJobFamily();
});

function grafikPenilaian() {
    $('#grafikPenilaian').remove();
    $('#canvas_father').append('<canvas id="grafikPenilaian" height="100" width="420"></canvas>');
    var label = [];
    var nilai = [];
    var standar = [];

    var chartdata = null;
    var barGraph = null;
    var total_hasil = null;
    var tipe = $("#rekomendasi option:selected").attr("tipe");
    $.ajax({
        type: "POST",
        url: "job_fit_karyawan/get_grafikAsesmen",
        data: {
            nik: $('#nama').val(),
            grade: $('#rekomendasi').val(),
            job_family: $('#jobFitFamily').val(),
            tahun: $('#txt_tahun').val(),
            jabatan: $('#jabatan').val(),
            tipe: tipe
        },
        dataType: "json",
        success: function(data) {
            console.log(data);
            for (var i in data['grafik']) {
                nilai.push(data['grafik'][i].nilai_asesmen);
                label.push(data['grafik'][i].kkj);
                standar.push(data['grafik'][i].nilai);
            }
            total_hasil = data['total'];

            if ($('#rekomendasi').val() == 55 || $('#rekomendasi').val() == 66) {
                if (data['hasil_akhir'] == 1) {
                    $('#hasil').text('Disarankan');
                    $('#job_fit').attr('class', 'bg-primary');
                } else if (data['hasil_akhir'] == 2) {
                    $('#hasil').text('Disarankan Dengan Beberapa Pengembangan');
                    $('#job_fit').attr('class', 'bg-warning');
                } else if (data['hasil_akhir'] == 3) {
                    $('#hasil').text('Tidak Disarankan');
                    $('#job_fit').attr('class', 'bg-danger');
                } else {
                    $('#hasil').text('Nilai Asesmen Tidak Tersedia');
                    $('#job_fit').attr('class', 'bg-success');
                }
                $('#presentase').text(total_hasil);

            } else {
                if (data['hasil_akhir'] == 1) {
                    $('#hasil').text('Disarankan');
                    $('#job_fit').attr('class', 'bg-primary');
                } else if (data['hasil_akhir'] == 2) {
                    $('#hasil').text('Disarankan Dengan Beberapa Pengembangan');
                    $('#job_fit').attr('class', 'bg-warning');
                } else if (data['hasil_akhir'] == 3) {
                    $('#hasil').text('Tidak Disarankan');
                    $('#job_fit').attr('class', 'bg-danger');
                } else {
                    $('#hasil').text('Nilai Asesmen Tidak Tersedia');
                    $('#job_fit').attr('class', 'bg-success');
                }
                $('#presentase').text(total_hasil + '%');

            }


            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Pencapaian Individu',
                        borderColor: "rgba(0, 123, 255, 0.9)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        fontFamily: "Poppins",
                        data: nilai
                    },
                    {
                        label: 'Standar Kompetensi Jabatan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(0,0,0,0.09)",
                        borderWidth: "0",
                        backgroundColor: "rgba(192, 57, 43,1.0)",
                        data: standar
                    },
                ]
            };

            var ctx = document.getElementById("grafikPenilaian");
            // ctx.height = 110;
            // ctx.width = 420;

            barGraph = new Chart(ctx, {
                type: 'bar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    "hover": {
                        "animationDuration": 0
                    },
                    "animation": {
                        "duration": 2000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function(dataset, i) {
                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 1,
                                min: 0,
                                max: 6,
                                display: true,

                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {
            console.log(data);
        }
    });
}

$('#btnPerJob').click(function() {
    var tipe = $("#rekomendasi option:selected").attr("tipe");

    $.ajax({
        type: "GET",
        url: 'job_fit_karyawan/export_perJobFamily',
        data: {
            grade: $('#grade').val(),
            tahun: $('#txt_tahun').val(),
            tipe: tipe,
        },
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
});
$('#btnPerNilai').click(function() {
    $.ajax({
        type: "GET",
        url: 'job_fit_karyawan/export_perNilai',
        data: {
            grade: $('#grade').val(),
            tahun: $('#txt_tahun').val(),
        },
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
});

function grafikJobFamily() {
    $('#grafikJobFamily').remove();
    $('#canvas_father2').append('<canvas id="grafikJobFamily" height="300" width="420"></canvas>');
    var label = [];
    var nilai = [];
    var warna = [];

    var chartdata = null;
    var barGraph = null;
    var tipe = $("#rekomendasi option:selected").attr("tipe");

    $.ajax({
        type: "POST",
        url: "job_fit_karyawan/get_grafikJobFamily",
        data: {
            nik: $('#nama').val(),
            grade: $('#rekomendasi').val(),
            tahun: $('#txt_tahun').val(),
            tipe: tipe,
            jabatan: $('#jabatan').val(),
        },
        dataType: "json",
        success: function(data) {

            for (var i in data) {
                label.push(data[i].job_family);
                nilai.push(data[i].nilai_total);
                warna.push(data[i].warna);
            }

            chartdata = {
                labels: label,
                datasets: [{
                    label: 'Pencapaian Individu',
                    borderColor: "rgba(0, 123, 255, 0.9)",
                    borderWidth: "0",
                    backgroundColor: warna,
                    fontFamily: "Poppins",
                    data: nilai
                }, ]
            };

            var ctx = document.getElementById("grafikJobFamily");
            // ctx.height = 110;
            // ctx.width = 420;

            barGraph = new Chart(ctx, {
                type: 'horizontalBar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 2
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    "animation": {
                        "duration": 2,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            this.data.datasets.forEach(function(dataset, i) {
                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);
                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x + 15, bar._model.y);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'end',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                                min: 0,
                                max: 200,
                                callback: function(value) {
                                    return value + "%"
                                }
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                                display: true
                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {
            console.log(data);
        }
    });
}
// Class initialization on page load
jQuery(document).ready(function() {
    jobFitKaryawan.init();
});