"use strict";

// Class definition

var unit_job_family_view = (function() {
    return {
        // Init demos
        init: function() {
            tabelKKJ();
            $('.type2').hide();
            $('#job_family').select2({
                placeholder: "Job Family",
                dropdownParent: $("#modalUbah")
            })
        }
    };
})();

function ubah(id_unit, nm_unit, nm_job_family, id_job_family) {
    $('#modalUbah').modal('show');
    $('#id_unit').val(id_unit);
    $('#nm_unit').val(nm_unit);
    $('#job_family').val(id_job_family);
    $('#job_family').trigger('change');
}

$('#ubah_job_family').on('submit', function(event) {
    event.preventDefault();
    var form_data = $('form').serialize();
    swal
        .fire({
            title: "Perhatian",
            text: "Apakah anda ingin mengubah data?",
            type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: "Ubah",
            cancelButtonText: "Tidak"
        })
        .then(function(result) {
            if (result.value) {
                $.ajax({
                    url: "unit_job_family/update",
                    method: "POST",
                    data: form_data,
                    dataType: "json",
                    success: function(data) {
                        if (data.status) {
                            swal.fire('Berhasil', 'berhasil mengubah data', 'success');
                            $('#modalUbah').modal('hide');
                            $("#data_kkj")
                                .DataTable()
                                .ajax.reload();
                        } else {
                            swal.fire('Gagal', 'gagal mengubah data', 'error');
                        }
                    }
                })
            } else {}
        })
});

function hapus(id) {
    swal
        .fire({
            title: "Perhatian",
            text: "Apakah anda ingin menghapus data?",
            type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
        })
        .then(function(result) {
            if (result.value) {
                jQuery.ajax({
                    type: "POST",
                    url: "unit_job_family/post_hapusKkj", // the method we are calling
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.status == true) {
                            swal.fire("Dihapus", "Data berhasil dihapus.", "success");
                            $("#data_kkj")
                                .DataTable()
                                .ajax.reload();
                        } else {
                            swal.fire("Gagal", "Gagal menghapus data.", "error");
                        }
                    },
                    error: function(error) {}
                });
            } else {}
        });
}

$('#add_paket').click(function() {
    $('input').val('');
    $('.tag_type').text('Tambah');

    $('.type1').fadeOut(1);
    $('.type2').fadeIn(1);
    // $('.type2').show();
    // $('.type1').hide();
});
$('#update_unit').click(function() {
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "unit_job_family/update_unit_kerja",
        method: "POST",
        dataType: "json",

        success: function(data) {
            if (data.status == true) {
                swal.fire('Berhasil', 'Berhasil Memperbarui Data Unit Kerja', 'success');
            } else {
                swal.fire('Gagal', 'Gagal Memperbarui Data Unit Kerja', 'error');
            }
        }
    });
})
$('#back_list').click(function() {
    $('.type2').fadeOut(1);
    $('.type1').fadeIn(1);
});
$('#file').on("change", function() {
    $('#labelFile').text('Choose file');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});
$('#insert_unit').on('submit', function(event) {
    event.preventDefault();
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "unit_job_family/insert_unit",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmit").attr("disabled", true);
        },
        success: function(data) {
            if (data.status == true) {

                $('.inputan').val('');
                swal.fire('Berhasil', 'Berhasil Mengelompokkan data unit kerja', 'success');
                $("#data_kkj")
                    .DataTable()
                    .ajax.reload();
            } else {
                swal.fire('Gagal', 'Gagal Mengelompokkan Data', 'error');
            }
        }
    })
});
$('#ubah_kkj').on('submit', function(event) {
    event.preventDefault();
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "kkj/ubah_kkj",
        method: "POST",
        data: $('form').serialize(),
        dataType: "json",
        success: function(data) {
            if (data.status == true) {
                $('#modalUbah').modal('hide');
                swal.fire('Berhasil', 'Berhasil mengubah data', 'success')
                $("#data_kkj")
                    .DataTable()
                    .ajax.reload();
            } else if (data.error.code == '23505/7') {
                swal.fire('Gagal', 'Kode KKJ sudah ada', 'error')

            } else {
                swal.fire('Gagal', 'Kode mengubah data', 'error')
            }
        }
    })
});

function tabelKKJ() {
    var tabel = $('#data_kkj').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "unit_job_family/get_unit", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [10, 50],
            [10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "id_unit"
            },
            {
                "data": "nm_unit"
            },
            {
                "data": "job_family"
            },
            {
                "data": "btn"
            }

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

// Class initialization on page load
jQuery(document).ready(function() {
    unit_job_family_view.init();
});