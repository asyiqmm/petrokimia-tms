"use strict";

// Class definition

var nilaiAsesmen = (function() {
    return {
        // Init demos
        init: function() {
            tabel_kinerja();
            tabel_potensi();
        }
    };
})();

function tabel_kinerja() {
    var tabel = $('#tabel_kinerja').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "penilaian_talent/get_kinerja", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [10, 50],
            [10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "kategori"
            },
            {
                "data": "rentang_nilai"
            },
            {
                "data": "aksi"
            }

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

$('#ubah_potensi').on('submit', function(event) {
    event.preventDefault();

    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "penilaian_talent/ubah_potensi",
        method: "POST",
        data: $('form').serialize(),
        dataType: "json",
        success: function(data) {
            if (data.status == true) {
                alert(data.status)
                $("#tabel_potensi")
                    .DataTable()
                    .ajax.reload();
            } else if (data.error.code == '23505/7') {
                alert('Kode KKJ sudah ada.');
            } else {
                alert('gagal menambahkan');
            }
        }
    })
});

$('#ubah_kinerja').on('submit', function(event) {
    event.preventDefault();

    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "penilaian_talent/ubah_kinerja",
        method: "POST",
        data: $('form').serialize(),
        dataType: "json",
        success: function(data) {
            if (data.status == true) {
                alert(data.status)
                $("#tabel_kinerja")
                    .DataTable()
                    .ajax.reload();
            } else if (data.error.code == '23505/7') {
                alert('Kode KKJ sudah ada.');
            } else {
                alert('gagal menambahkan');
            }
        }
    })
});

function ubah_potensi(id, min, max, grade) {
    $('#modal_ubah_potensi').modal('show');
    $('#id_potensi').val(id);
    $('#minimum').val(min);
    $('#grade').val(grade);
    $('#medium').val(min + ' - ' + max);
    $('#maksimum').val(max);
}

function ubah_kinerja(id, min, max, kategori) {
    $('#modal_ubah_kinerja').modal('show');
    $('#id_kinerja').val(id);
    $('#kategori').val(kategori);
    $('#rentang_awal').val(min);
    $('#rentang_akhir').val(max);
}

$('#minimum').change(function() {
    $('#medium').val($(this).val() + ' - ' + $('#maksimum').val());
});

$('#maksimum').change(function() {
    $('#medium').val($('#minimum').val() + ' - ' + $(this).val());
});

function tabel_potensi() {
    var tabel = $('#tabel_potensi').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "penilaian_talent/get_potensi", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [10, 50],
            [10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "grade"
            },
            {
                "data": "miminum"
            },
            {
                "data": "medium"
            },
            {
                "data": "maksimum"
            },
            {
                "data": "aksi"
            }
        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

// Class initialization on page load
jQuery(document).ready(function() {
    nilaiAsesmen.init();
});