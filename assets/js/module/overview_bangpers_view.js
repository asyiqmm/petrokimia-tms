"use strict";

// Class definition

var overview_karyawan = (function() {
    return {
        // Init demos
        init: function() {


            grafik_perJobFamilyPromosi();

            $('.departemen').select2({
                placeholder: 'Pilih Departemen',
                maximumSelectionLength: 4
            });
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });
        }
    };
})();

$('#btnCari').click(function() {

    var tahun = $("#txt_tahun").val();

    grafik_keikutsertaan_asesmen(tahun);
    grafik_perGrade(tahun);
    grafik_perGradeIni(tahun);
    grafik_keikutsertaan(tahun);
    grafik_perJobFamily(tahun);
});

function grafik_keikutsertaan(tahun) {
    $('#grafik_keikutsertaan').remove();
    $('#keikutsertaan_father').append('<canvas id="grafik_keikutsertaan"></canvas>');
    var backgroundColor = 'white';
    Chart.plugins.register({
        beforeDraw: function(c) {
            var ctx = c.chart.ctx;
            ctx.fillStyle = backgroundColor;
            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
        }
    });
    var getTotal = function(myChart) {
        var sum = myChart.config.data.datasets[0].data.reduce((a, b) => a + b, 0);
        return `Total: ${sum}`;
    }
    $.ajax({
        type: "GET",
        url: "overview/get_keikutsertaan",
        dataType: "json",
        data: { tahun: tahun },
        success: function(datas) {
            var chartdata = {
                datasets: [{
                    label: "My First dataset",
                    data: [datas[0].ikut, datas[0].tidak_ikut],
                    backgroundColor: [
                        '#00b5e9',
                        '#fa4251'
                    ],
                    hoverBackgroundColor: [
                        '#00b5e9',
                        '#fa4251'
                    ],
                    borderWidth: [
                        0, 0
                    ],
                    hoverBorderColor: [
                        'transparent',
                        'transparent'
                    ]
                }],
                labels: [
                    'Ikut: ' + datas[0].ikut,
                    'Tidak Ikut: ' + datas[0].tidak_ikut
                ]
            };
            var ctx = document.getElementById("grafik_keikutsertaan");
            ctx.height = 280;
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: chartdata,
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    cutoutPercentage: 55,
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    legend: {
                        display: true,
                        position: 'left',
                        labels: {
                            "fontSize": 14,
                        }
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.labels[tooltipItems.index];
                            }
                        },
                        titleFontFamily: "Poppins",
                        xPadding: 15,
                        yPadding: 10,
                        caretPadding: 0,
                        bodyFontSize: 16
                    },
                    plugins: {
                        doughnutlabel: {
                            labels: [{
                                text: getTotal,
                                font: {
                                    size: '25',
                                    family: 'Arial, Helvetica, sans-serif',
                                    style: 'italic',
                                    weight: 'bold'
                                },
                                color: '#1abc9c'
                            }]
                        },
                        datalabels: {
                            formatter: (value, ctx) => {
                                let datasets = ctx.chart.data.datasets;
                                if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                                    let sum = datasets[0].data.reduce((a, b) => a + b, 0);
                                    let percentage = Math.round((value / sum) * 100) + '%';
                                    return percentage;
                                } else {
                                    return percentage;
                                }
                            },
                            color: '#fff',
                            font: {
                                weight: 'bold',
                                size: 14,
                            }
                        }
                    }
                },

            });
        }
    })

}

function grafik_keikutsertaan_asesmen(tahun) {
    $('#grafik_keikutsertaan_asesmen').remove();
    $('#keikutsertaan_asesmen_father').append('<canvas id="grafik_keikutsertaan_asesmen"></canvas>');
    var backgroundColor = 'white';
    Chart.plugins.register({
        beforeDraw: function(c) {
            var ctx = c.chart.ctx;
            ctx.fillStyle = backgroundColor;
            ctx.fillRect(0, 0, c.chart.width, c.chart.height);
        }
    });
    var getTotal = function(myChart) {
        var sum = myChart.config.data.datasets[0].data.reduce((a, b) => a + b, 0);
        return `Total: ${sum}`;
    }
    $.ajax({
        type: "GET",
        url: "overview/get_keikutsertaan_asesmen",
        dataType: "json",
        data: { tahun: tahun },
        success: function(datas) {
            var chartdata = {
                datasets: [{
                    label: "My First dataset",
                    data: [datas[0].ikut, datas[0].tidak_ikut],
                    backgroundColor: [
                        '#00b5e9',
                        '#fa4251'
                    ],
                    hoverBackgroundColor: [
                        '#00b5e9',
                        '#fa4251'
                    ],
                    borderWidth: [
                        0, 0
                    ],
                    hoverBorderColor: [
                        'transparent',
                        'transparent'
                    ]
                }],
                labels: [
                    'Ikut: ' + datas[0].ikut,
                    'Tidak Ikut: ' + datas[0].tidak_ikut
                ]
            };
            var ctx = document.getElementById("grafik_keikutsertaan_asesmen");
            ctx.height = 280;
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: chartdata,
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    cutoutPercentage: 55,
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    legend: {
                        display: true,
                        position: 'left',
                        labels: {
                            "fontSize": 14,
                        }
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.labels[tooltipItems.index];
                            }
                        },
                        titleFontFamily: "Poppins",
                        xPadding: 15,
                        yPadding: 10,
                        caretPadding: 0,
                        bodyFontSize: 16
                    },
                    plugins: {
                        doughnutlabel: {
                            labels: [{
                                text: getTotal,
                                font: {
                                    size: '25',
                                    family: 'Arial, Helvetica, sans-serif',
                                    style: 'italic',
                                    weight: 'bold'
                                },
                                color: '#1abc9c'
                            }]
                        },
                        datalabels: {
                            formatter: (value, ctx) => {
                                let datasets = ctx.chart.data.datasets;
                                if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
                                    let sum = datasets[0].data.reduce((a, b) => a + b, 0);
                                    let percentage = Math.round((value / sum) * 100) + '%';
                                    return percentage;
                                } else {
                                    return percentage;
                                }
                            },
                            color: '#fff',
                            font: {
                                weight: 'bold',
                                size: 14,
                            }
                        }
                    }
                },

            });
        }
    })

}

function grafik_perGrade(tahun) {
    $('#grafikGrade').remove();
    $('#canvas_fatherGrade').append('<canvas id="grafikGrade" height="200" width="620"></canvas>');
    var TD = [];
    var DS = [];
    var D = [];
    var label = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "GET",
        url: "overview/grafik_grade",
        dataType: "json",
        data: { tahun: tahun },
        success: function(data) {
            console.log(data);
            for (var i in data) {
                TD.push(data[i].td);
                DS.push(data[i].ds);
                D.push(data[i].d);
                label.push(data[i].nm_grade);
                // for (let index = 0; index < 3; index++) {
                //     const element = array[index];

                // }
            }

            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Disarankan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(52, 152, 219,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(52, 152, 219,1.0)",
                        data: D
                    },
                    {
                        label: 'Disarankan dengan pertimbangan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(241, 196, 15,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        data: DS
                    },
                    {
                        label: 'Tidak Disarankan',
                        borderColor: "rgba(231, 76, 60,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(231, 76, 60,1.0)",
                        fontFamily: "Poppins",
                        data: TD
                    },
                ]
            };

            var ctx = document.getElementById("grafikGrade");
            // ctx.height = 110;
            // ctx.width = 420;
            barGraph = new Chart(ctx, {
                type: 'bar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    showTooltips: false,
                    "animation": {
                        "duration": 1000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            // Math.max(chartdata.datasets.data)

                            this.data.datasets.forEach(function(dataset, i) {

                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);

                                    meta.data.forEach(function(bar, index) {
                                        // console.log(bar);
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: true
                            }
                        },
                        // series: {
                        //     dataLabels: {
                        //         enabled: true,
                        //         formatter: function() {
                        //             var mychart = $('#container').highcharts();
                        //             var mytotal = 0;

                        //             for (i = 0; i < mychart.series.length; i++) {
                        //                 if (mychart.series[i].visible) {
                        //                     mytotal += parseInt(mychart.series[i].yData[0]);
                        //                 }
                        //             }

                        //             console.log(mychart.series[i].yData[0]);
                        //             var pcnt = (this.y / mytotal) * 100;
                        //             return Highcharts.numberFormat(pcnt) + '%';
                        //         }
                        //     }
                        // }
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{

                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 50,
                                display: true,
                            }
                        }]
                    }
                },



            });
        },
        error: function(data) {}
    });
}

function grafik_perGradeIni(tahun) {
    $('#grafikGradeSaatIni').remove();
    $('#canvas_fatherGradeSaatIni').append('<canvas id="grafikGradeSaatIni" height="200" width="620"></canvas>');
    var TD = [];
    var DS = [];
    var D = [];
    var label = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "GET",
        url: "overview/grafik_gradeIni",
        dataType: "json",
        data: { tahun: tahun },
        success: function(data) {
            for (var i in data) {
                TD.push(data[i].td);
                DS.push(data[i].ds);
                D.push(data[i].d);
                label.push(data[i].nm_grade);
                // for (let index = 0; index < 3; index++) {
                //     const element = array[index];

                // }
            }

            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Disarankan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(52, 152, 219,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(52, 152, 219,1.0)",
                        data: D
                    },
                    {
                        label: 'Disarankan dengan pertimbangan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(241, 196, 15,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        data: DS
                    },
                    {
                        label: 'Tidak Disarankan',
                        borderColor: "rgba(231, 76, 60,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(231, 76, 60,1.0)",
                        fontFamily: "Poppins",
                        data: TD
                    },
                ]
            };

            var ctx = document.getElementById("grafikGradeSaatIni");
            // ctx.height = 110;
            // ctx.width = 420;
            barGraph = new Chart(ctx, {
                type: 'bar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    showTooltips: false,
                    "animation": {
                        "duration": 1000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            // Math.max(chartdata.datasets.data)

                            this.data.datasets.forEach(function(dataset, i) {

                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);

                                    meta.data.forEach(function(bar, index) {
                                        // console.log(bar);
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    plotOptions: {
                        column: {
                            dataLabels: {
                                enabled: true
                            }
                        },
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{

                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 50,
                                display: true,
                            }
                        }]
                    }
                },



            });
        },
        error: function(data) {}
    });
}

function grafik_perJobFamily(tahun) {
    $('#grafikJobFamily').remove();
    $('#canvas_fatherGrade2').append('<canvas id="grafikJobFamily" height="400" width="420"></canvas>');
    var TD = [];
    var DS = [];
    var D = [];
    var label = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "GET",
        url: "overview/grafik_jobFamily",
        dataType: "json",
        success: function(data) {

            for (var i in data) {
                TD.push(data[i].td);
                DS.push(data[i].ds);
                D.push(data[i].d);
                label.push(data[i].nm_fitjob_fam);
            }
            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Disarankan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(52, 152, 219,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(52, 152, 219,1.0)",
                        data: D
                    },
                    {
                        label: 'Disarankan dengan pertimbangan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(241, 196, 15,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        data: DS
                    },
                    {
                        label: 'Tidak Disarankan',
                        borderColor: "rgba(231, 76, 60,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(231, 76, 60,1.0)",
                        fontFamily: "Poppins",
                        data: TD
                    },
                ]
            };

            var ctx = document.getElementById("grafikJobFamily");
            // ctx.height = 110;
            // ctx.width = 420;
            barGraph = new Chart(ctx, {
                type: 'horizontalBar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    showTooltips: false,
                    "animation": {
                        "duration": 1000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            this.data.datasets.forEach(function(dataset, i) {

                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);

                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x + 7, bar._model.y);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 50,
                                display: true,
                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {}
    });
}

function grafik_perJobFamilyPromosi() {
    $('#grafikJobFamilyPromosi').remove();
    $('#canvas_fatherFamJobPromosi').append('<canvas id="grafikJobFamilyPromosi" height="400" width="420"></canvas>');
    var TD = [];
    var DS = [];
    var D = [];
    var label = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "GET",
        url: "overview/grafik_jobFamily_promosi",
        dataType: "json",
        data: { opsi: $('#pilihanJobFam').val() },
        success: function(data) {

            for (var i in data) {
                TD.push(data[i].td);
                DS.push(data[i].ds);
                D.push(data[i].d);
                label.push(data[i].nm_fitjob_fam);
            }
            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Disarankan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(52, 152, 219,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(52, 152, 219,1.0)",
                        data: D
                    },
                    {
                        label: 'Disarankan dengan pertimbangan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(241, 196, 15,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        data: DS
                    },
                    {
                        label: 'Tidak Disarankan',
                        borderColor: "rgba(231, 76, 60,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(231, 76, 60,1.0)",
                        fontFamily: "Poppins",
                        data: TD
                    },
                ]
            };

            var ctx = document.getElementById("grafikJobFamilyPromosi");
            // ctx.height = 110;
            // ctx.width = 420;
            barGraph = new Chart(ctx, {
                type: 'horizontalBar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    showTooltips: false,
                    "animation": {
                        "duration": 1000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';

                            this.data.datasets.forEach(function(dataset, i) {

                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);

                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x + 7, bar._model.y);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 50,
                                display: true,
                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {
            console.log(data);
        }
    });
}

$('#pilihanJobFam').change(function() {
    grafik_perJobFamilyPromosi();
});
$('#departemen').change(function() {

    $('#grafikDepartemen').remove();
    $('#canvas_fatherGrade3').append('<canvas id="grafikDepartemen" height="100" width="250"></canvas>');
    var TD = [];
    var DS = [];
    var D = [];
    var label = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "GET",
        url: "overview/grafik_departemen",
        data: { departemen: $(this).val() },
        dataType: "json",
        success: function(data) {

            for (var i in data) {
                TD.push(data[i].td);
                DS.push(data[i].ds);
                D.push(data[i].d);
                label.push(data[i].unit_name);
            }

            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Disarankan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(52, 152, 219,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(52, 152, 219,1.0)",
                        data: D
                    },
                    {
                        label: 'Disarankan dengan pertimbangan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(241, 196, 15,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        data: DS
                    },
                    {
                        label: 'Tidak Disarankan',
                        borderColor: "rgba(231, 76, 60,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(231, 76, 60,1.0)",
                        fontFamily: "Poppins",
                        data: TD
                    },
                ]
            };

            var ctx = document.getElementById("grafikDepartemen");
            // ctx.height = 110;
            // ctx.width = 420;
            barGraph = new Chart(ctx, {
                type: 'bar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    showTooltips: false,
                    "animation": {
                        "duration": 1000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            // Math.max(chartdata.datasets.data)

                            this.data.datasets.forEach(function(dataset, i) {

                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);

                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{

                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 20,
                                display: true,
                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {
            console.log(data);
        }
    });
})
$('#departemenPromosi').change(function() {
    $('#grafikDepartemenPromosi').remove();
    $('#canvas_fatherDepPromosi').append('<canvas id="grafikDepartemenPromosi" height="100" width="250"></canvas>');
    var TD = [];
    var DS = [];
    var D = [];
    var label = [];

    var chartdata = null;
    var barGraph = null;
    $.ajax({
        type: "GET",
        url: "overview/grafik_departemen_promosi",
        data: {
            departemen: $(this).val(),
            opsi: $('#pilihanDepartemen').val()
        },
        dataType: "json",
        success: function(data) {

            for (var i in data) {
                TD.push(data[i].td);
                DS.push(data[i].ds);
                D.push(data[i].d);
                label.push(data[i].unit_name);
            }

            chartdata = {
                labels: label,
                datasets: [{
                        label: 'Disarankan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(52, 152, 219,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(52, 152, 219,1.0)",
                        data: D
                    },
                    {
                        label: 'Disarankan dengan pertimbangan',
                        data: [28, 48, 40, 19, 86, 27, 90],
                        borderColor: "rgba(241, 196, 15,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(241, 196, 15,1.0)",
                        data: DS
                    },
                    {
                        label: 'Tidak Disarankan',
                        borderColor: "rgba(231, 76, 60,1.0)",
                        borderWidth: "0",
                        backgroundColor: "rgba(231, 76, 60,1.0)",
                        fontFamily: "Poppins",
                        data: TD
                    },
                ]
            };

            var ctx = document.getElementById("grafikDepartemenPromosi");
            // ctx.height = 110;
            // ctx.width = 420;
            barGraph = new Chart(ctx, {
                type: 'bar',

                data: chartdata,
                defaultFontFamily: 'Poppins',
                options: {
                    "hover": {
                        "animationDuration": 0
                    },
                    plugins: {
                        datalabels: {
                            display: false
                        },
                        outlabels: {
                            display: true
                        }
                    },
                    showTooltips: false,
                    "animation": {
                        "duration": 1000,
                        "onComplete": function() {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';
                            // Math.max(chartdata.datasets.data)

                            this.data.datasets.forEach(function(dataset, i) {

                                var isHidden = dataset._meta[Object.keys(dataset._meta)[0]].hidden; //'hidden' property of dataset
                                if (!isHidden) { //if dataset is not hidden
                                    var meta = chartInstance.controller.getDatasetMeta(i);

                                    meta.data.forEach(function(bar, index) {
                                        var data = dataset.data[index];
                                        ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                    });

                                }
                            });
                        }
                    },
                    legend: {
                        position: 'top',
                        labels: {
                            fontFamily: 'Poppins'
                        }

                    },
                    tooltips: {
                        "enabled": false
                    },
                    scales: {
                        xAxes: [{
                            ticks: {
                                fontFamily: "Poppins",
                            }
                        }],
                        yAxes: [{

                            ticks: {
                                beginAtZero: true,
                                fontFamily: "Poppins",
                                stepSize: 20,
                                display: true,
                            }
                        }]
                    }
                }


            });
        },
        error: function(data) {
            console.log(data);
        }
    });
})

function gambar1() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('master_gambar1'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}

function gambar2() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('canvas_fatherGradeSaatIni'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}

function gambar3() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('canvas_fatherGrade'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}

function gambar4() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('canvas_fatherGrade3'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}

function gambar5() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('canvas_fatherDepPromosi'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}

function gambar6() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('canvas_fatherGrade2'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}



function gambar7() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('canvas_fatherFamJobPromosi'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}

function gambar8() {
    // if (!isChartRendered) return; // return if chart not rendered
    html2canvas(document.getElementById('grafik_keikutsertaan_asesmen'), {
        onrendered: function(canvas) {
            var link = document.createElement('a');
            link.href = canvas.toDataURL('image/jpeg');
            link.download = 'myChart.jpeg';
            link.click();
        }
    })
}
// Class initialization on page load
jQuery(document).ready(function() {
    overview_karyawan.init();
});