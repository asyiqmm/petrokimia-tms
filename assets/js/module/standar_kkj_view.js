"use strict";

// Class definition

var nilaiAsesmen = (function() {
    return {
        // Init demos
        init: function() {
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });
            $(window).on('load', function() {
                setTimeout(removeLoader, 2000); //wait for page load PLUS two seconds.
            });

        }
    };
})();
$("#btnCari").click(function() {
    get_field();
});
$("#btnCopy").click(function() {
    duplikat_standar_kkj();
});

function duplikat_standar_kkj() {
    jQuery.ajax({
        type: "POST",
        url: "standar_kkj/copy_standar_kkj", // the method we are calling
        data: {
            tahun: $('#txt_tahun').val()
        },
        dataType: "json",
        beforeSend: function() {},
        success: function(data) {},
        error: function(xhr, status, error) {}

    });
}

function removeLoader() {
    $("#loadingDiv").fadeOut(500, function() {
        // fadeOut complete. Remove the loading div
        $("#loadingDiv").remove(); //makes page more lightweight 
    });
}

function get_field() {
    jQuery.ajax({
        type: "POST",
        url: "standar_kkj/get_standar_kkj", // the method we are calling
        data: {
            grade: $('#grade').val(),
            job_family: $('#job_family').val(),
            tahun: $('#txt_tahun').val(),
            kategori: $('#kategori').val(),
        },
        dataType: "json",
        beforeSend: function() {
            $("#response").html('<span class="text-info">Memuat Data...</span>');
            $('#data_checkbox tbody').html('');
        },
        success: function(data) {
            $("#response").fadeIn().html('');
            $('#data_checkbox tbody').append(data);
            checkbox();
        },
        error: function(xhr, status, error) {}

    });
}

function checkbox() {
    $('input[type=checkbox]').change(function() {
        if (this.checked) {
            $(this).closest('tr').find('input[type=number]').prop('disabled', false);
            $(this).closest('tr').find('input[type=number]').prop('required', true);
        } else {
            $(this).closest('tr').find('input[type=number]').prop('disabled', true);
            $(this).closest('tr').find('input[type=number]').prop('required', false);
            $(this).closest('tr').find('input[type=number]').val('');
        }
    });
}

$('#insert_standar_kkj').on('submit', function(event) {
    event.preventDefault();
    var form_data = $('form').serialize();
    // form_data['grade'] = $('#grade').val();

    $.ajax({
        url: "standar_kkj/insert",
        method: "POST",
        data: form_data,
        dataType: "json",

        success: function(data) {
            if (data.status) {
                swal.fire('Berhasil', 'berhasil menambahkan data', 'success');
            } else {
                swal.fire('Gagal', 'Gagal menambahkan data', 'error');
            }
        }
    })
});

// Class initialization on page load
jQuery(document).ready(function() {
    nilaiAsesmen.init();

});