"use strict";

// Class definition

var jobFitFamily = (function() {
    return {
        // Init demos
        init: function() {
            jobFamily();
            $('#tabelAsesmen').DataTable({});
            $("#grade").select2({
                placeholder: "--Pilih Grade--",
            });
            $("#sub_grade").select2({
                placeholder: "--Pilih Sub Grade--",
            });

            $("#rekomendasi").select2({
                placeholder: "--Pilih Jenis Rekomendasi--",
            });
            $("#jobFitFamily").select2({
                placeholder: "--Pilih Job Family--",
                width: "100%",
            });
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });
        }
    };
})();
$('#btnCari').on('submit', function(event) {
    event.preventDefault();
    tabelNilai();
});
$('#btnSaatIni').click(function() {
    $.ajax({
        type: "GET",
        url: 'job_fit_family/export',
        data: {
            grade: $('#grade').val(),
            job_family: $('#jobFitFamily').val(),
            rekomendasi: 1,
            tahun: $('#txt_tahun').val(),
        },
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
});
$('#btnPromosi').click(function() {
    $.ajax({
        type: "GET",
        url: 'job_fit_family/export',
        data: {
            grade: $('#grade').val(),
            job_family: $('#jobFitFamily').val(),
            tahun: $('#txt_tahun').val(),
            rekomendasi: 2,
        },
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
});

$("#grade").change(function() {
    var grade = $(this).val();
    if (grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "job_fit_karyawan/get_nama", // the method we are calling
            data: {
                grade: grade,
                tahun: $('#txt_tahun').val()
            },
            dataType: "json",
            beforeSend: function() {
                $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--</option>');
                $('#sub_grade').html('<option value="">--Pilih Sub Grade--</option>');
            },
            success: function(data) {
                $('#sub_grade').html(data.sub_grade);
                $('#rekomendasi').html(data.rekomendasi);
            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--\</option>');
    }
});


function jobFamily() {
    jQuery.ajax({
        type: "POST",
        url: "job_fit_karyawan/get_jobFit", // the method we are calling
        dataType: "json",
        success: function(data) {
            $('#jobFitFamily').html(data);
        },
        error: function(xhr, status, error) {}

    });
}


function tabelNilai() {
    var tipe = $("#rekomendasi option:selected").attr("tipe");
    var tabel = $('#tabelAsesmen').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "job_fit_family/get_listAsesmen", // URL file untuk proses select datanya
            "type": "POST",
            "dataType": "json",
            "data": {
                grade: $('#grade').val(),
                sub_grade: $('#sub_grade').val(),
                rekomendasi: $('#rekomendasi').val(),
                job_family: $('#jobFitFamily').val(),
                tahun: $('#txt_tahun').val(),
                tipe: tipe
            },
        }, // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "nama"
            },
            {
                "data": "nik"
            },
            {
                "data": "total"
            },
            {
                "data": "rekomendasi"
            },
        ]
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}
// Class initialization on page load
jQuery(document).ready(function() {
    jobFitFamily.init();
});