"use strict";

// Class definition

var nilaiAsesmen = (function() {
    return {
        // Init demos
        init: function() {

            $('.type2').hide();
        }
    };
})();

function ubah(id, kkj, nm_kkj) {
    $('#modalUbah').modal('show');
    $('#kd_kkj').val(kkj);
    $('#nm_kkj').val(nm_kkj);
    $('#id_kkj').val(id);
}



function hapus(id) {
    swal
        .fire({
            title: "Perhatian",
            text: "Apakah anda ingin menghapus data?",
            type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
        })
        .then(function(result) {
            if (result.value) {
                jQuery.ajax({
                    type: "POST",
                    url: "kkj/post_hapusKkj", // the method we are calling
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.status == true) {
                            swal.fire("Dihapus", "Data berhasil dihapus.", "success");
                            $("#data_kkj")
                                .DataTable()
                                .ajax.reload();
                        } else {
                            swal.fire("Gagal", "Gagal menghapus data.", "error");
                        }
                    },
                    error: function(error) {}
                });
            } else {}
        });
}
$('#btncari').click(function() {
    tabelKKJ();
})
$('#add_paket').click(function() {
    $('input').val('');
    $('.tag_type').text('Tambah');

    $('.type1').fadeOut(1);
    $('.type2').fadeIn(1);
    // $('.type2').show();
    // $('.type1').hide();
});
$('#back_list').click(function() {
    $('.type2').fadeOut(1);
    $('.type1').fadeIn(1);
});
$('#insert_kkj').on('submit', function(event) {
    event.preventDefault();

    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "kkj/insert_kkj",
        method: "POST",
        data: {
            kkj: $('#kkj').val(),
            nama: $('#nama').val()
        },
        dataType: "json",

        success: function(data) {
            if (data.status == true) {

                $('.inputan').val('');
                alert(data.status)
                $("#data_kkj")
                    .DataTable()
                    .ajax.reload();
            } else if (data.error.code == '23505/7') {
                alert('Kode KKJ sudah ada.');
            } else {
                alert('gagal menambahkan');
            }
        }
    })
});
$('#ubah_kkj').on('submit', function(event) {
    event.preventDefault();
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "kkj/ubah_kkj",
        method: "POST",
        data: $('form').serialize(),
        dataType: "json",
        success: function(data) {
            if (data.status == true) {
                $('#modalUbah').modal('hide');
                swal.fire('Berhasil', 'Berhasil mengubah data', 'success')
                $("#data_kkj")
                    .DataTable()
                    .ajax.reload();
            } else if (data.error.code == '23505/7') {
                swal.fire('Gagal', 'Kode KKJ sudah ada', 'error')

            } else {
                swal.fire('Gagal', 'Kode mengubah data', 'error')
            }
        }
    })
});

function tabelKKJ() {
    var tabel = $('#data_kkj').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "kkj/get_kkj_g1", // URL file untuk proses select datanya
            "type": "GET",
            data: { rekomendasi: $('#rekomendasi').val() }
        },
        "aLengthMenu": [
            [10, 50],
            [10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "kkj"
            },
            {
                "data": "nm_kkj"
            },
            {
                "data": "btn"
            }

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

// Class initialization on page load
jQuery(document).ready(function() {
    nilaiAsesmen.init();
});