"use strict";

// Class definition

var nilaiAsesmen = (function() {
    return {
        // Init demos
        init: function() {
            tabelNilai();
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });
        }
    };
})();

$('#file').on("change", function() {
    $('#labelFile').text('Choose file');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});
$('#filegrade1').on("change", function() {
    $('#labelFileG1').text('Choose file');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#filegrade1')[0].files[0].name;
    $('#labelFileG1').text(file);

});
import_formg1
$('#import_formg1').on('submit', function(event) {
    event.preventDefault();

    $.ajax({
        url: "nilai_asesmen/import_excelg1",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmitg1").attr("disabled", true);
        },
        success: function(data) {
            $("#btnsubmitg1").attr("disabled", false);
            if (data.status == true) {
                $('#filegrade1').val('');
                $('#labelFileG1').text('Choose file');

                swal.fire('Berhasil', data.sukses + ' data berhasil  ditambahkan\n' + data.gagal + ' data gagal ditambahkan', 'success');
                $("#tabelNilai")
                    .DataTable()
                    .ajax.reload();
            } else {
                swal.fire('Gagal', 'Gagal mengunggah data', 'error');
            }
        },
        error: function(xhr, status, error) {
            // alert(error);
            $("#btnsubmitg1").attr("disabled", false);
        }
    })
});
$('#btn_contoh_asesmen').click(function() {
    $.ajax({
        type: "GET",
        url: 'nilai_asesmen/eksport_asesmen',
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
});
// $('#btn_contoh_g1').click(function() {
//     $.ajax({
//         type: "GET",
//         url: 'nilai_asesmen/eksport_asesmen_g1',
//         cache: false,
//         success: function() {
//             window.open(this.url, '_blank');
//         }
//     });
// })

$('#import_form').on('submit', function(event) {
    event.preventDefault();

    $.ajax({
        url: "nilai_asesmen/import_excel",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmit").attr("disabled", true);
        },
        success: function(data) {
            $("#btnsubmit").attr("disabled", false);
            if (data.status == true) {
                $('#file').val('');
                $('#labelFile').text('Choose file');

                swal.fire('Berhasil', data.sukses + ' data berhasil  ditambahkan\n' + data.gagal + ' data gagal ditambahkan', 'success');
                $("#tabelNilai")
                    .DataTable()
                    .ajax.reload();
            } else {
                swal.fire('Gagal', 'Gagal mengunggah data', 'error');
            }
        },
        error: function(xhr, status, error) {
            alert(error);
            $("#btnsubmit").attr("disabled", false);
        }
    })
});

function tampilGrafik(id) {
    console.log(id);
    $('#grafikNilai').modal('show');
}

var tabel = null;

function tabelNilai() {
    tabel = $('#tabelNilai').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "nilai_asesmen/fetch", // URL file untuk proses select datanya
            "type": "GET",
            "data": {
                tahun: $('#txt_tahun').val()
            }
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [
            { data: null, "orderable": false },

            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "unit_name"
            },
            {
                "data": "nm_grade"
            },
            {
                "data": "job_fit"
            },
            {
                "data": "aksi"
            }

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

function detail(nik) {
    var tabel = $('#tabelDetailNilai').DataTable({
        "processing": true,
        "destroy": true,
        "searching": false,
        "paging": false,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "nilai_asesmen/get_nilai", // URL file untuk proses select datanya
            "type": "POST",
            "dataType": "json",
            "data": {
                nik: nik,
                tahun: $('#txt_tahun').val(),
            },
        }, // Combobox Limit
        "columns": [{
                "data": "nomor"
            },
            {
                "data": "kkj"
            },
            {
                "data": "nilai"
            }

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    $("#nilaiPotensi").modal("show");

}
// Class initialization on page load
jQuery(document).ready(function() {
    nilaiAsesmen.init();
});