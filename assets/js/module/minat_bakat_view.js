"use strict";

// Class definition

var MinatBakat = (function() {
    return {
        // Init demos
        init: function() {
            $("#grade").select2({
                placeholder: "--Pilih Grade--",
                allowClear: true
            });
            $("#sub_grade").select2({
                placeholder: "--Pilih Sub Grade--",
                allowClear: true
            });
            $("#job_family").select2({
                placeholder: "--Pilih Job Family--",
                width: "100%",
            });
            $("#job_title").select2({
                placeholder: "--Pilih Job Title--",
                width: "100%",
            });
        }
    };
})();

$('#btnCari').on('submit', function(event) {
    event.preventDefault();
    var form_data = $('form').serialize();
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "minat_bakat/get_tabel",
        method: "POST",
        data: form_data,
        dataType: "json",
        beforeSend: function() {},
        success: function(data) {
            tabel_psp_pss(data);
            minat_bakat(data);
        }
    })
});

function tabel_psp_pss(data) {
    var table = $('#tabelNilai').DataTable({
        "order": [
            [0, 'desc']
        ],
        responsive: true,
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis'],
        destroy: true,
        columns: [{
                data: null,
                "orderable": false
            },
            { data: 'nik_tetap' },
            { data: 'nama' },
            {
                data: 'grade_personal',
                "orderable": false
            },
            { data: 'x' },
            { data: 'y' },
            { data: 'nilai_pss' },
            { data: 'btn' },
        ],

        data: data,
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(0)
                .addClass("text-center");
            $("td", row)
                .eq(3)
                .addClass("text-center");
            $("td", row)
                .eq(4)
                .addClass("text-center");
            $("td", row)
                .eq(5)
                .addClass("text-center");
            $("td", row)
                .eq(6)
                .addClass("text-center");
        },
        // Localization

    });
    table.on('order.dt search.dt', function() {
        table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

function detail(nik) {
    var tabel = $('#tabelDetail').DataTable({
        "processing": true,
        "destroy": true,
        "searching": false,
        "paging": false,
        "ordering": false, // Set true agar bisa di sorting
        "ajax": {
            "url": "minat_bakat/get_nilai", // URL file untuk proses select datanya
            "type": "POST",
            "dataType": "json",
            "data": {
                nik: nik
            },
        }, // Combobox Limit
        "columns": [{
                data: 'nomor',
            },
            {
                "data": "nm_fitjob_fam"
            },
            {
                "data": "nilai_psp"
            }

        ],
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(0)
                .addClass("text-center");
            $("td", row)
                .eq(2)
                .addClass("text-center");
        },
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    $("#nilaiPotensi").modal("show");

}



function minat_bakat(data) {
    $('#chartku').remove();
    $('#canvas_father').append('<canvas id="chartku"></canvas>');
    var label = [];
    var nik = [];
    for (var i in data) {
        label.push(data[i].nama);
        nik.push(data[i].nik);
    }
    var color = ["rgba(0,121, 217, 127)", "rgba(0,161, 186, 172)", "rgba(0,125, 74, 12)"];
    var ctx = document.getElementById("chartku");
    if (ctx) {
        ctx.height = 50;
        ctx.width = 100;
        var myChart = new Chart(ctx, {
            type: 'scatter',
            defaultFontFamily: 'Poppins',
            data: {
                labels: label,
                datasets: [{
                    label: 'Legend',
                    data: data,
                    // borderColor: "rgba(0, 123, 255, 0.9)",
                    // borderWidth: "0",
                    // backgroundColor: coloR,
                    backgroundColor: color,
                    // hoverBackgroundColor: hoverColor,
                    // borderColor: graphOutlines,
                    // backgroundColor: "rgba(0, 123, 255, 0.5)",
                    fontFamily: "Poppins",
                    pointRadius: 6,
                    pointHoverRadius: 6
                }]
            },
            options: {
                onClick: function(mouseEvent, chart) {
                    detail(nik[chart[0]._index]);
                },
                plugins: {
                    datalabels: {
                        display: false
                    },
                    outlabels: {
                        display: true
                    }
                },
                responsive: true,
                legend: {
                    position: 'start',
                    labels: {
                        fontFamily: 'Poppins'
                    }

                },
                tooltips: {
                    // mode: 'single',
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return 'Nama: ' + data['labels'][tooltipItem[0]['index']];
                        },
                        beforeLabel: function(tooltipItem, data) {
                            var dataset = data['datasets'][0];
                            return 'Kompetensi: ' + data['datasets'][0]['data'][tooltipItem['index']].x;

                            // return 'Strength Statement: ' + data['datasets'][0]['data'][tooltipItem['index']].pss;
                        },
                        label: function(tooltipItem, data) {
                            console.log(data['datasets'][0].data[tooltipItem['index']].x);
                            return 'Strength Potential: ' + data['datasets'][0]['data'][tooltipItem['index']].y;


                            // return 'Kompetensi: ' + data['datasets'][0]['data'][tooltipItem['index']].x;
                        },
                        afterLabel: function(tooltipItem, data) {
                            var dataset = data['datasets'][0];
                            return 'Strength Statement: ' + data['datasets'][0]['data'][tooltipItem['index']].nilai_pss;
                            // return 'Strength Potential: ' + data['datasets'][0]['data'][tooltipItem['index']].y;
                        },

                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            fontFamily: "Poppins",
                            min: 0,
                            max: 110,
                            stepSize: 5
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Kompetensi'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontFamily: "Poppins",
                            min: 0,
                            max: 110,
                            stepSize: 5
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Strength Potential'
                        }
                    }]
                }
            }
        });
    }
}


// Class initialization on page load
jQuery(document).ready(function() {
    MinatBakat.init();
});