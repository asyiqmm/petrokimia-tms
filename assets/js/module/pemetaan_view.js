"use strict";

// Class definition

var pemetaan = (function() {
    return {
        // Init demos
        init: function() {
            $('#grade').select2({
                placeholder: "Grade",
            });
            $('#sub_grade').select2({
                placeholder: "Sub Grade",
            });
            $('#tabelPemetaan').DataTable({

            });
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });
        }
    };
})();
$("#grade").change(function() {
    var grade = $(this).val();
    if (grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "pemetaan/get_sub_grade", // the method we are calling
            data: {
                grade: grade
            },
            dataType: "json",
            beforeSend: function() {
                $('#sub_grade').html('<option value="">--Pilih Sub Grade--</option>');
            },
            success: function(data) {
                // tabelUjiKelayakan();
                $('#sub_grade').html(data);
            },
            error: function(xhr, status, error) {}

        });
    } else {}
});

$('#btn_cari').click(function() {
    get_data();
    // tabelUjiKelayakan();
});
var globaldata;

function get_data() {
    var consistent_star = 0;
    var utility_hi_pro = 0;
    var high_contri = 0;
    var future_start = 0;
    var experienced_pro = 0;
    var contributor = 0;
    var diamond = 0;
    var incosistent = 0;
    var iceberg = 0;
    jQuery.ajax({
        type: "POST",
        url: "pemetaan/get_data", // the method we are calling
        data: {
            grade: $('#grade').val(),
            sub_grade: $('#sub_grade').val(),
            tahun: $('#txt_tahun').val(),
        },
        dataType: "json",
        success: function(data) {
            globaldata = data;
            tabelPemetaan(data);
            for (var i = 0; i < data.length; i++) {
                if (data[i].hasil == 3) {
                    consistent_star = parseInt(consistent_star) + 1;
                } else if (data[i].hasil == 2) {
                    utility_hi_pro = parseInt(utility_hi_pro) + 1;
                } else if (data[i].hasil == 1) {
                    high_contri = parseInt(high_contri) + 1;
                } else if (data[i].hasil == 6) {
                    future_start = parseInt(future_start) + 1;
                } else if (data[i].hasil == 5) {
                    experienced_pro = parseInt(experienced_pro) + 1;
                } else if (data[i].hasil == 4) {
                    contributor = parseInt(contributor) + 1;
                } else if (data[i].hasil == 9) {
                    diamond = parseInt(diamond) + 1;
                } else if (data[i].hasil == 8) {
                    incosistent = parseInt(incosistent) + 1;
                } else if (data[i].hasil == 7) {
                    iceberg = parseInt(iceberg) + 1;
                }
            }

            document.getElementById('consistent_star').innerHTML = "CONSISTENT STAR <br/>" + consistent_star;
            document.getElementById('utility_hi_pro').innerHTML = "UTILITY HI-PRO <br/>" + utility_hi_pro;
            document.getElementById('high_contri').innerHTML = "HIGH CONTRIBUTION <br/>" + high_contri;
            document.getElementById('future_start').innerHTML = "FUTURE STAR <br/>" + future_start;
            document.getElementById('experienced_pro').innerHTML = "EXPERIENCED PRO <br/>" + experienced_pro;
            document.getElementById('contributor').innerHTML = "CONTRIBUTOR <br/>" + contributor;
            document.getElementById('diamond').innerHTML = "DIAMOND IN THE ROUGH <br/>" + diamond;
            document.getElementById('incosistent').innerHTML = "INCONSISTENT PERFORMER <br/>" + incosistent;
            document.getElementById('iceberg').innerHTML = "ICEBERG <br/>" + iceberg;



        },
        error: function(xhr, status, error) {}

    });
}

function box1(pak, kki) {
    $('#data_talent').modal('show');
    var tabel = $('#tabelDetailNilai').DataTable({
        "processing": true,
        "destroy": true,
        "searching": true,
        "responsive": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "pemetaan/list_data", // URL file untuk proses select datanya
            "type": "POST",
            "data": {
                pak: pak,
                kki: kki,
                grade: $('#grade').val(),
                sub_grade: $('#sub_grade').val()
            }
        },
        "columns": [{
                "data": "no"
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "hasil"
            },

        ],
        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

$('#btnEksport').click(function() {
    $.ajax({
        type: "GET",
        url: 'pemetaan/eksport_exel',
        data: {
            grade: $('#grade').val(),
            sub_grade: $('#sub_grade').val(),
            tahun: $('#txt_tahun').val(),
        },
        cache: false,
        success: function() {
            window.open(this.url, '_blank');
        }
    });
})

function tabelPemetaan(data) {
    var table = $('#tabelPemetaan').DataTable({
        "order": [
            [2, 'desc']
        ],
        responsive: true,
        lengthChange: true,
        buttons: ['copy', 'excel', 'pdf', 'colvis'],
        destroy: true,
        columns: [
            { data: null, "orderable": false },
            { data: 'nik' },
            { data: 'nama' },
            { data: 'grade' },
            { data: 'departemen' },
            { data: 'direktorat' },
            { data: 'asesmen' },
            { data: 'kat_asesmen' },
            // { data: null, "defaultContent": "-" },
            // { data: null, "defaultContent": "-" },
            // { data: null, "defaultContent": "-" },
            { data: 'nilai_sebelum' },
            { data: 'nilai_sekarang' },
            { data: 'avg_pak' },
            { data: 'kat_pak' },
            { data: 'kat_hasil' },
            { data: 'kat_talent' },
            { data: 'tgl_pensiun' },
        ],

        data: data,
        createdRow: function(row, data, index) {
            $("td", row)
                .eq(0)
                .addClass("text-center");
            $("td", row)
                .eq(3)
                .addClass("text-center");
            $("td", row)
                .eq(6)
                .addClass("text-center");
            $("td", row)
                .eq(8)
                .addClass("text-center");
            $("td", row)
                .eq(9)
                .addClass("text-center");
            $("td", row)
                .eq(10)
                .addClass("text-center");
            $("td", row)
                .eq(11)
                .addClass("text-center");
            $("td", row)
                .eq(12)
                .addClass("text-center");
            $("td", row)
                .eq(13)
                .addClass("text-center");
        },
        // Localization

    });

    table.on('order.dt search.dt', function() {
        table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    $('#tabelPemetaan').removeClass('table-responsive');
    $('#tabelPemetaan').addClass('table-responsive');
}

// Class initialization on page load
jQuery(document).ready(function() {
    pemetaan.init();
});