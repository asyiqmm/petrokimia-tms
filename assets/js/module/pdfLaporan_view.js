"use strict";

// Class definition
var pdfLaporan_view = (function() {
    return {
        // Init demos
        init: function() {
            tabelPdf();
        }
    };
})();

/* function submitForm(el) {
  // block ajax ui swal 
  // https://codepen.io/izuno/pen/jOOeBPG
  Swal.fire({
    title: 'Konfirmasi',
    showLoaderOnConfirm: true,
    showCancelButton: true,
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya',
    cancelButtonText: 'Batal',
    confirmButtonColor: '#3085d6',
    html: `
    <i class="fa fa-5 fa-info-circle" style='font-size:48px;color:rgb(48, 133, 214)'></i>
      <br>
      Yakin melanjutkan unggah PDF ??:
    `,
    preConfirm: () => {
      let datax = new FormData(el)
      $.ajax({
        url: "pdfLaporan/upload",
        method: "POST",
        data: datax,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        error: function (xhr, status, error) {
          var err = eval("(" + xhr.responseText + ")");
          Swal.showValidationMessage(
            `Request failed: ${err.Message}`
          )
        },
        success: function (data) {
          if (!data.status) {
            alert(data.error.message)
          } else {
            console.log(data);
            $("#tabelPdf")
              .DataTable()
              .ajax.reload();
          }
        },
      })
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    console.log('result', result)

    if (result.value) {
      $("#tabelPdf").DataTable().ajax.reload();
      Swal.fire({
        title: 'Berhasil',
        html: `
        <i class="fa fa-5 fa-check-circle" style='font-size:48px;color:green'></i>
        <br>
        Berhasil unggah data:
        `,
      })
    }
  })
} */

function deletePdf(id_user_info, file_path, nama) {
    Swal.fire({
        title: 'Hapus Data',
        // text: "Yakin menghapus file PDF, untuk karyawan ~" + nama + "~ ??",
        showLoaderOnConfirm: true,
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonText: 'Delete',
        confirmButtonColor: '#3085d6',
        html: `
      <i class="fa fa-5 fa-info-circle" style='font-size:48px;color:rgb(48, 133, 214)'></i>
      <br>
      Yakin menghapus file PDF, dari <b>${nama}</b> ??:
    `,
        preConfirm: () => {
            let paramx = JSON.stringify({
                'id': id_user_info,
                'file_path': file_path,
            })
            return fetch('pdf_laporan/delete/', {
                    method: 'POST',
                    mode: 'cors',
                    cache: 'no-cache',
                    headers: {
                        'Content-Type': 'application/json'
                            // 'Content-Type': 'application/x-www-form-urlencoded'
                            // https://stackoverflow.com/questions/35091757/parse-javascript-fetch-in-php
                    },
                    body: paramx
                })
                .then((response) => response.json())
                .then((data) => {
                    console.log('data', data)
                })
                .catch((error) => {
                    console.log('error', error)
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        console.log('result', result)

        if (result.value) {
            $("#tabelPdf").DataTable().ajax.reload();
            Swal.fire({
                title: 'Berhasil',
                html: `
          <i class="fa fa-5 fa-check-circle" style='font-size:48px;color:green'></i>
          <br>
          Berhasil menghapus data:
        `,
                // imageUrl: result.value.avatar_url
            })
        }
    })
}

function submitForm(el) {
    // block ajax ui swal 
    // https://codepen.io/izuno/pen/jOOeBPG
    let res = {}
    console.log('masuk submit')
    Swal.fire({
        title: 'Konfirmasi',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        confirmButtonColor: '#3085d6',
        html: `
    <i class="fa fa-5 fa-info-circle" style='font-size:48px;color:rgb(48, 133, 214)'></i>
      <br>
      Yakin melanjutkan unggah PDF ??
    `,
        preConfirm: () => {
            console.log(el)
            let fData = new FormData(el)

            return fetch('pdf_laporan/upload', {
                    method: 'POST',
                    body: fData
                })
                .then((response) => response.json())
                .then((data) => {
                    res = data
                    console.log('data', data)
                })
                .catch((error) => {
                    console.log('error', error)
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        console.log('result', result)

        if (result.value) {
            console.log('res', res)
            if (!res.status) {
                Swal.fire({
                    title: 'Gagal',
                    html: `
          <i class="fa fa-5 fa-times-circle" style='font-size:48px;color:red'></i>
          <br>
          ${res.msg}:
          `,
                })
            } else {
                $("#tabelPdf").DataTable().ajax.reload();
                let resList = ''
                for (let i = 0; i < res.detail.length; i++) {
                    let ico = res.detail[i].status ? '<i class="fa fa-5 fa-check-circle" style=\'color:green\'></i>' : '<i class="fa fa-5 fa-times-circle" style=\'color:red\'></i>'
                    resList += '<li style="list-style-type:none" class="text-left">' + ico + ' NIK <b>' + res.detail[i].nik + '</b>, ' + res.detail[i].msg + ' </li>'
                }
                Swal.fire({
                    title: 'Hasil Unggah PDF',
                    html: `
          <i  class="fa fa-5 fa-info-circle" style='font-size:48px;color:rgb(48, 133, 214)'></i>
          <br>
          ${resList}
          `,
                    // Berhasil unggah PDF:
                })
            }
        }
    })
}

// function submitForm(el) {
//   console.log(el)
//   let datax = new FormData(el)

//   $.ajax({
//     url: "pdfLaporan/upload",
//     method: "POST",
//     data: datax,
//     contentType: false,
//     cache: false,
//     processData: false,
//     dataType: "json",

//     success: function (data) {
//       if (!data.status) {
//         alert(data.error.message)
//       } else {
//         console.log(data);
//         $("#tabelPdf")
//           .DataTable()
//           .ajax.reload();
//       }
//       // if (data.status == true) {
//       //   $('#file').val('');
//       //   alert(data);
//       //   $("#tabelNilai")
//       //     .DataTable()
//       //     .ajax.reload();
//       // }
//     }
//   })
// }


function tabelPdf() {
    // alert('hah')
    var tabel = $('#tabelPdf').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            // "url": "nilaiAsesmen/fetch", // URL file untuk proses select datanya
            "url": "pdf_laporan/fetch", // URL file untuk proses select datanya
            "type": "GET",
            dataType: "json"
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": null
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "nm_grade"
            },
            {
                "data": "nm_dept"
            },
            {
                "data": "pdf_asesmen_karyawan"
            },
            {
                "data": "pdf_asesmen_sdm"
            },

        ],
    });

    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

function hapusPdf1(nik, id) {
    swal
        .fire({
            title: "Perhatian",
            text: "Apakah anda ingin menghapus pdf?",
            type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
        })
        .then(function(result) {
            if (result.value) {
                $.ajax({
                    // url: "nilaiAsesmen/import",
                    url: "pdf_laporan/hapus_asesmen",
                    type: "POST",
                    data: {
                        nik: nik,
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        $("#tabelPdf")
                            .DataTable()
                            .ajax.reload();
                        swal.fire('Berhasil', 'berhasil Menghapus pdf', 'success');

                    }
                })
            } else {}
        });

}

function hapusPdf2(nik, id) {
    swal
        .fire({
            title: "Perhatian",
            text: "Apakah anda ingin menghapus pdf?",
            type: "question",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
            cancelButtonText: "Tidak"
        })
        .then(function(result) {
            if (result.value) {
                $.ajax({
                    // url: "nilaiAsesmen/import",
                    url: "pdf_laporan/hapus_mb",
                    type: "POST",
                    data: {
                        nik: nik,
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {

                        swal.fire('Berhasil', 'berhasil Menghapus pdf', 'success');

                        $("#tabelPdf")
                            .DataTable()
                            .ajax.reload();
                    }
                })
            } else {}
        });

}

function modalPDF(path) {
    $('#viewPdf').remove();
    $('#canvas_father').append('<object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>');
    var pathPdf = '';
    // $('#viewPdf').attr('data', pathPdf);
    pathPdf = $('#baseUrl').val() + '' + path;
    $('#viewPdf').attr('data', pathPdf);
    $('#nilaiPotensi').modal('show');
}

var arry = [];
var formData = new FormData();
var arry2 = [];
var formData2 = new FormData();

function ListAsesmen(e) {
    $('.msg').hide();
    $('.msg').removeClass('alert-danger');
    var input = document.getElementById('pdfAsesmen');
    var output = document.getElementById('AsesmenList');

    var filelist;
    for (var i = 0; i < input.files.length; ++i) {
        if (arry.includes(input.files[i].name) == false && arry.length < 100) {
            formData.append("Asesmenfiles[]", input.files[i]);
            filelist = input.files[i].name;
            arry.push(filelist);
        } else {
            $('.msg').show();
            $('.msg').addClass("alert-danger");
            $('.msg').html("Maksimum upload 100 file");
        }
    }

    var children = "";
    var j = 1;
    for (var i = 0; i < arry.length; ++i) {
        children += '<li>' + j + '. ' + arry[i] + '</li>';
        j++;
    }
    output.innerHTML = '<ul>' + children + '</ul>';
    $(e).val(null);
    return false;
}

function ListMB() {
    $('.msg2').hide();
    $('.msg2').removeClass('alert-danger');
    var input = document.getElementById('pdfMinatBakat');
    var output = document.getElementById('MBList');

    var filelist;
    for (var i = 0; i < input.files.length; ++i) {
        if (arry2.includes(input.files[i].name) == false && arry.length < 100) {
            formData2.append("MinatBakatfiles[]", input.files[i]);
            filelist = input.files[i].name;
            arry2.push(filelist);
        } else {
            $('.msg2').show();
            $('.msg2').addClass("alert-danger");
            $('.msg2').html("Maksimum upload 100 file");
        }
    }

    var children = "";
    var j = 1;
    for (var i = 0; i < arry2.length; ++i) {
        children += '<li>' + j + '. ' + arry2[i] + '</li>';
        j++;
    }
    output.innerHTML = '<ul>' + children + '</ul>';
    // $(e).val(null);
    return false;

    // var input = document.getElementById('pdfMinatBakat');
    // var output = document.getElementById('MBList');
    // var children = "";
    // var j = 1;
    // for (var i = 0; i < input.files.length; ++i) {
    //     children += '<li>' + j + '. ' + input.files.item(i).name + '</li>';
    //     j++;
    // }
    // output.innerHTML = '<ul>' + children + '</ul>';
}

const showLoading = function() {
    swal({
        title: 'Now loading',
        allowEscapeKey: false,
        allowOutsideClick: false,
        timer: 2000,
        onOpen: () => {
            swal.showLoading();
        }
    }).then(
        () => {},
        (dismiss) => {
            if (dismiss === 'timer') {
                console.log('closed by timer!!!!');
                swal({
                    title: 'Finished!',
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: false
                })
            }
        }
    )
};
//showLoading();

// function deletePdf(id_user_info, nama) {
//   Swal.fire({
//     title: 'Hapus file PDF',
//     text: "Yakin menghapus file PDF, untuk karyawan ~" + nama + "~ ??",
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     confirmButtonText: 'Ya',
//     cancelButtonText: 'Tidak',


//   }).then((result) => {
//     if (result.value) {
//       // preConfirm: () => {
//         $.ajax({
//           url: "pdfLaporan/delete",
//           method: "POST",
//           data: { id: id_user_info },
//           cache: false,
//           dataType: "json",
//           error: function (xhr, status, error) {
//             var err = eval("(" + xhr.responseText + ")");
//             Swal.showValidationMessage(
//               `Request failed: ${err.Message}`
//             )
//           },
//           success: function (data) {
//             if (!data.status) {
//               alert(data.error.message)
//             } else {
//               console.log(data);
//               $("#tabelPdf")
//                 .DataTable()
//                 .ajax.reload();

//               Swal.fire(
//                 'Berhasil!',
//                 'File PDF ' + nama + ' berhasil terhapus',
//                 'success'
//               )
//             }
//           }
//         })
//       // allowOutsideClick: () => !Swal.isLoading()

//       // Swal.fire(
//       //   'Berhasil!',
//       //   'File PDF ' + nama + ' berhasil terhapus',
//       //   'success'
//       // )
//     }
//   })
// }

function submitForm() {
    // console.log(arry);
    var formData = new FormData($('#insert_pdf_asesmen'));

    // formData.append(arry);
    // console.log(formData);

    $.ajax({
        url: 'pdf_laporan/cobaupload',
        type: 'POST',
        data: formData,
        processData: false, // tell jQuery not to process the data
        contentType: false, // tell jQuery not to set contentType
        success: function(data) {
            console.log(data);
            $("#tabelPdf").DataTable().ajax.reload();

            alert(data);

        },
        error: function(error) {
            alert(error);
        }
    });


}

jQuery(document).ready(function() {
    pdfLaporan_view.init();

    $("#insert_pdf_asesmen").submit(function(event) {
        if (arry.length == 0) {

            $('.msg').show();
            $('.msg').addClass("alert-danger");
            $('.msg').html("Pilih file terlebih dahulu");

        } else {

            $("#btnsubmitAsesmen").attr("disabled", true);
            $("#button-pilihfile").attr("disabled", true);
            event.preventDefault();

            $('.msg').hide();
            $('.msg').removeClass('alert-danger');

            $('.progress').show();

            var progression = 0;
            var progress = setInterval(function() {
                $('.progress #progressBar').text(progression + '%');
                $('.progress #progressBar').css({ 'width': progression + '%' });
                if (progression <= 90) {
                    if (arry.length < 20) {
                        progression += 5;
                    }

                    if (arry.length == 20 && arry.length <= 50) {
                        progression += 3;
                    }

                    if (arry.length > 50) {
                        progression += 2;
                    }
                }
            }, 1000);

            $.ajax({
                type: 'POST',
                url: 'pdf_laporan/cobaupload',
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('.progress #progressBar').text(0 + '%');
                    $('.progress #progressBar').css({ 'width': 0 + '%' });
                    formData.delete("Asesmenfiles[]");
                    clearInterval(progress);
                    $('.progress').hide();
                    $("#btnsubmitAsesmen").attr("disabled", false);
                    $('.msg').show();
                    if (response == "") {
                        var msg = response;
                        $('.msg').addClass("alert-success");
                        $('.msg').html("Gagal upload");
                    } else {
                        var msg = response;
                        $('.msg').addClass("alert-success");
                        $('.msg').html(msg);
                    }
                    $('#AsesmenList').empty();
                    arry.splice(0, arry.length)
                    $("#tabelPdf").DataTable().ajax.reload();

                }
            });
        }

        return false;
    });

    $("#insert_pdf_bakat").submit(function(event) {
        if (arry2.length == 0) {

            $('.msg2').show();
            $('.msg2').addClass("alert-danger");
            $('.msg2').html("Pilih file terlebih dahulu");

        } else {

            $("#btnsubmitMB").attr("disabled", true);
            $("#button-pilihfile2").attr("disabled", true);
            event.preventDefault();

            $('.msg2').hide();
            $('.msg2').removeClass('alert-danger');

            $('.progress2').show();

            var progression = 0;
            var progress = setInterval(function() {
                $('.progress2 #progressBar2').text(progression + '%');
                $('.progress2 #progressBar2').css({ 'width': progression + '%' });
                if (progression <= 90) {
                    if (arry.length < 20) {
                        progression += 5;
                    }

                    if (arry.length == 20 && arry.length <= 50) {
                        progression += 3;
                    }

                    if (arry.length > 50) {
                        progression += 2;
                    }
                }
            }, 1000);

            $.ajax({
                type: 'POST',
                url: 'pdf_laporan/uploadMB',
                data: formData2,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('.progress2 #progressBar2').text(0 + '%');
                    $('.progress2 #progressBar2').css({ 'width': 0 + '%' });
                    formData2.delete("MinatBakatfiles[]");
                    clearInterval(progress);
                    $('.progress2').hide();
                    $("#btnsubmitMB").attr("disabled", false);
                    $('.msg2').show();
                    if (response == "") {
                        var msg = response;
                        $('.msg2').addClass("alert-success");
                        $('.msg2').html("Gagal upload");
                    } else {
                        var msg = response;
                        $('.msg2').addClass("alert-success");
                        $('.msg2').html(msg);
                    }
                    $('#MBList').empty();
                    arry.splice(0, arry.length)
                    $("#tabelPdf").DataTable().ajax.reload();

                }
            });
        }

        return false;
    });
});