"use strict";

// Class definition

var dataKaryawan = (function() {
    return {
        // Init demos
        init: function() {
            tabelKaryawan();

        }
    };
})();
$('#file').on("change", function() {
    $('#labelFile').text('Choose file');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});

function tabelKaryawan() {
    var table = $('#tabelKaryawan').DataTable({
        "orderCellsTop": true,
        "processing": true,
        "destroy": true,
        responsive: true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "data_karyawan/getListKaryawan", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "nama"
            },
            {
                "data": "nama"
            },
            {
                "data": "nik"
            },
            {
                "data": "departemen"
            },
            {
                "data": "jabatan"
            },
            {
                "data": "peran"
            },

            {
                "data": "aksi"
            },

        ],

    });
    table.on('order.dt search.dt', function() {
        table.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
    $('#tabelKaryawan tfoot th').not(":eq(0),:eq(7)") //Exclude columns 3, 4, and 5
        .each(function() {
            var title = $('#tabelKaryawan thead td').eq($(this).index()).text();
            $(this).html('<input type="text" class="form-control" placeholder="Cari ' + title + '" />');
        });
    table.columns().eq(0).each(function(colIdx) {
        if (colIdx == 0 || colIdx == 7) return; //Do not add event handlers for these columns

        $('input', table.column(colIdx).footer()).on('keyup change', function() {
            table
                .column(colIdx)
                .search(this.value)
                .draw();
        });
    });

};

function ubahUser(nik) {

}

$('#update_karyawan').click(function() {
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "data_karyawan/update_dataKaryawan",
        method: "POST",
        dataType: "json",

        success: function(data) {
            if (data.status == true) {
                swal.fire('Berhasil', 'Berhasil Memperbarui Data Karyawan', 'success');
            } else {
                swal.fire('Gagal', 'Gagal Memperbarui Data Karyawan', 'error');
            }
        }
    });
})

$('#formUbah').on('submit', function(event) {
    event.preventDefault();
    console.log('upload')

    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "data_karyawan/import_excel",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",

        success: function(data) {
            $("#tabelKaryawan")
                .DataTable()
                .ajax.reload();
        }
    })
});

function ubahUser(nik, nama, role) {


    document.getElementById('nama').value = nama;
    document.getElementById('nik').value = nik;

    if (role) {
        document.getElementById('role').value = 'true';
    } else {
        document.getElementById('role').value = 'false';
    }

    $("#modalUbah").modal("show");

}


$('#formUbah').on('submit', function(event) {
    event.preventDefault();
    var form_data = $('form').serialize();
    $.ajax({
        url: "data_karyawan/ubahKaryawan",
        method: "POST",
        data: form_data,
        dataType: "json",
        success: function(data) {


            $("#modalUbah").modal("hide");
            console.log(data);
            if (data.status) {
                swal.fire('Berhasil', 'berhasil mengubah data', 'success');
            } else {
                swal.fire('Gagal', 'Gagal mengubah data', 'error');
            }
            $("#tabelKaryawan")
                .DataTable()
                .ajax.reload();
        }
    })
});
// Class initialization on page load
jQuery(document).ready(function() {
    dataKaryawan.init();
});