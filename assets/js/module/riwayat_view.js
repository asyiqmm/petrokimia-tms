"use strict";

// Class definition

var riwayat = (function() {
    return {
        // Init demos
        init: function() {
            $("#grade").select2({
                placeholder: "--Pilih Grade--",
            });
            $("#sub_grade").select2({
                placeholder: "--Pilih Sub Grade--",
            });
            $("#departemen").select2({
                placeholder: "--Pilih Departemen--",
            });
            $("#job_family").select2({
                placeholder: "--Pilih Job Family--",
                width: "100%",

            });
            $("#txt_tahun").datepicker({
                minViewMode: 2,
                format: 'yyyy',
                autoclose: true
            });

            $('#tabelPemetaan').DataTable({});
        }
    };
})();
$("#grade").change(function() {
    var grade = $(this).val();
    if (grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "job_fit_karyawan/get_nama", // the method we are calling
            data: {
                grade: grade
            },
            dataType: "json",
            beforeSend: function() {
                $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--</option>');
                $('#sub_grade').html('<option value="">--Pilih Sub Grade--</option>');
            },
            success: function(data) {
                $('#sub_grade').html(data.sub_grade);
                $('#rekomendasi').html(data.rekomendasi);
            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#nama').html('<option value="">--Pilih Nama Pegawai--</option>');
        $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--\</option>');
    }
});
$("#grade").change(function() {
    var grade = $(this).val();
    if (grade !== '') {
        jQuery.ajax({
            type: "GET",
            url: "riwayat/getSubGrade", // the method we are calling
            data: {
                grade: grade
            },
            dataType: "json",
            success: function(data) {
                $('#sub_grade').html(data);
            },
            error: function(xhr, status, error) {}

        });
    } else {
        $('#nama').html('<option value="">--Pilih Nama Pegawai--</option>');
        $('#rekomendasi').html('<option value="">--Pilih Jenis Rekomendasi--\</option>');
    }
});

function format(d) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        '<td>Status TM:</td>' +
        '<td>' + d.status_tm + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Status Asesmen:</td>' +
        '<td>' + d.status_asesmen + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Extra info:</td>' +
        '<td>And any further details here (images etc)...</td>' +
        '</tr>' +
        '</table>';
}


function get_riwayat_asesmen(tahun) {
    var tabel = $('#tabel_riwayat_asesmen').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "riwayat/getRiwayat_asesmen", // URL file untuk proses select datanya
            "type": "POST",
            data: {
                tahun: tahun,
                type: $('option:selected', '#rekomendasi').attr('type'),
                grade: $('#grade').val()
            },
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": null
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "nm_grade"
            },
            {
                "data": "nm_departemen"
            },
            {
                "data": "nm_direktorat"
            },
            {
                "data": "rekomendasi"
            },
            {
                "data": "job_fit"
            },
            {
                "data": "grafik"
            },
        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

function get_riwayat_manajemen_talent(tahun) {
    var tabel = $('#tabel_riwayat_manajemen_talenta').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "riwayat/getRiwayat_manajemen_talenta", // URL file untuk proses select datanya
            "type": "POST",
            data: {
                tahun: tahun,
                grade: $('#grade').val()
            },
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": null
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "nm_grade"
            },
            {
                "data": "nm_departemen"
            },
            {
                "data": "nm_direktorat"
            },
            {
                "data": "asesmen"
            },
            {
                "data": "kat_asesmen"
            },
            {
                "data": "uji_komite"
            },
            {
                "data": "satu"
            },
            {
                "data": "dua"
            },
            {
                "data": "tiga"
            },
            {
                "data": "potensi"
            },
            {
                "data": "kat_potensi"
            },
            {
                "data": "pak_sebelum"
            },
            {
                "data": "pak_setelah"
            },
            {
                "data": "rata_rata"
            },
            {
                "data": "kat_pak"
            },
            {
                "data": "matrix"
            },
            {
                "data": "kat_talent"
            },
            {
                "data": "pensiun"
            },
        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

function get_riwayat_minat_bakat(tahun) {
    console.log(tahun);
    var tabel = $('#tabel_riwayat_minat_bakat').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "riwayat/getRiwayat_minat_bakat", // URL file untuk proses select datanya
            "type": "POST",
            data: {
                tahun: tahun,
                grade: $('#grade').val()
            },
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": null
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "grade_personal"
            },
            {
                "data": "job_fit"
            },
            {
                "data": "x"
            },
            {
                "data": "btn"
            },
        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
$('#btnUpdate').click(function() {
    updateData();
});

function updateData() {
    jQuery.ajax({
        type: "GET",
        url: "riwayat/updateData", // the method we are calling
        dataType: "json",
        success: function(data) {},
        error: function(xhr, status, error) {}
    });
}

function modalPDF(path) {
    $('#viewPdf').remove();
    $('#canvas_father').append('<object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>');
    var pathPdf = '';
    // $('#viewPdf').attr('data', pathPdf);
    pathPdf = $('#baseUrl').val() + '' + path;
    $('#viewPdf').attr('data', pathPdf);
    $('#nilaiPotensi').modal('show');
}

function get_riwayat_pdf(tahun) {
    // alert('hah')
    var tabel = $('#tabelPdf').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            // "url": "nilaiAsesmen/fetch", // URL file untuk proses select datanya
            "url": "riwayat/getRiwayat_pdf", // URL file untuk proses select datanya
            "type": "POST",
            data: {
                tahun: tahun,
                grade: $('#grade').val()
            },
            dataType: "json"
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": null
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "nm_grade"
            },
            {
                "data": "nm_dept"
            },
            {
                "data": "nm_fitjob_fam"
            },
            {
                "data": "pdf_asesmen"
            },
            {
                "data": "pdf_minat_bakat"
            },

        ],
    });

    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};
$('#btnCari').click(function() {
    get_riwayat_asesmen($('#txt_tahun').val());
    get_riwayat_manajemen_talent($('#txt_tahun').val());
    get_riwayat_minat_bakat($('#txt_tahun').val());
    get_riwayat_pdf($('#txt_tahun').val());
});

function detailPSP(id) {
    $('#nilaiPSP').modal('show');
    var tabel = $('#tabelPSP').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "psp_pss/get_psp", // URL file untuk proses select datanya
            "type": "GET",
            "data": { nik: id },
            "dataType": "json"
        },
        "paging": false,
        "searching": false,
        "columns": [{
                "data": "no"
            },
            {
                "data": "nm_fitjob_fam"
            },
            {
                "data": "nilai_psp"
            },
            {
                "data": "nilai_pss"
            },

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
// Class initialization on page load
jQuery(document).ready(function() {
    riwayat.init();
});