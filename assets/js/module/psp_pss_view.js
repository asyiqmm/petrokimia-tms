"use strict";

// Class definition

var nilaiAsesmen = (function() {
    return {
        // Init demos
        init: function() {
            tabelPSP_PSS();
        }
    };
})();

// $('#import_form').on('submit', function (event) {

// });
$('#file').on("change", function() {
    $('#labelFile').text('');
    console.log("change fire");
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});
$('#import_form').on('submit', function(event) {
    event.preventDefault();
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "psp_pss/import_excel",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmit").attr("disabled", true);
            $('#file').attr("disabled", true);
        },
        success: function(data) {
            $("#btnsubmit").attr("disabled", false);
            $('#file').attr("disabled", true);
            // console.log(data.status);
            if (data.status == true) {
                $('#file').val('');
                swal.fire('Berhasil', data.sukses + ' data PSP & PSS berhasil ditambahkan<br>' + data.gagal + ' data gagal ditambahkan', 'success');
                $("#tabelPSP_PSS")
                    .DataTable()
                    .ajax.reload();
            } else {
                $("#btnsubmit").attr("disabled", false);
            }
        }
    })
});


var tabel = null;

function tabelPSP_PSS() {
    tabel = $('#tabelPSP_PSS').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "psp_pss/fetch", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "nm_grade"
            },
            {
                "data": "unit_name"
            },
            {
                "data": "jabatan"
            },
            {
                "data": "job_fit"
            },
            {
                "data": "nilai_psp"
            },

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

function detailPSP(id) {
    $('#nilaiPSP').modal('show');
    tabel = $('#tabelPSP').DataTable({
        "processing": true,
        "destroy": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "psp_pss/get_psp", // URL file untuk proses select datanya
            "type": "GET",
            "data": { nik: id },
            "dataType": "json"
        },
        "paging": false,
        "searching": false,
        "columns": [{
                "data": "no"
            },
            {
                "data": "nm_fitjob_fam"
            },
            {
                "data": "nilai_psp"
            },
            {
                "data": "nilai_pss"
            },

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}
// Class initialization on page load
jQuery(document).ready(function() {
    nilaiAsesmen.init();
});