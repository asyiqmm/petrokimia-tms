"use strict";

// Class definition

var overviewDashboard = (function() {
    return {
        // Init demos
        init: function() {
            loading();
        }
    };
})();

// Class initialization on page load
jQuery(document).ready(function() {
    overviewDashboard.init();
});