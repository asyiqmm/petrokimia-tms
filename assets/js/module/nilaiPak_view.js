"use strict";

// Class definition

var nilaiAsesmen = (function() {
    return {
        // Init demos
        init: function() {
            tabelNilai();
        }
    };
})();

$('#file').on("change", function() {
    $('#labelFile').text('Choose file');
    var i = $(this).prev('label').clone();
    var file = $('#file')[0].files[0].name;
    $('#labelFile').text(file);

});

$('#fileUjiKelayakan').on("change", function() {
    $('#labelFileUK').text('Choose file');
    var i = $(this).prev('label').clone();
    var file = $('#fileUjiKelayakan')[0].files[0].name;
    $('#labelFileUK').text(file);
});

$('#fileUjiKomite').on("change", function() {
    $('#labelFileKomite').text('Choose file');
    var i = $(this).prev('label').clone();
    var file = $('#fileUjiKomite')[0].files[0].name;
    $('#labelFileKomite').text(file);
});

$('#import_form').on('submit', function(event) {
    event.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "nilai_pak/import_excel",
        method: "POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmitPAK").attr("disabled", true);
            $("#file").attr("disabled", true);

        },
        success: function(data) {
            $("#file").attr("disabled", false);
            $("#btnsubmitPAK").attr("disabled", false);
            if (data.status == true) {
                $('#file').val('');
                $('#labelFile').text('Choose file');
                swal.fire('Berhasil', data.total + ' data PAK berhasil ditambahkan', 'success');
                $("#tabelNilai")
                    .DataTable()
                    .ajax.reload();
                formData.delete("file");
            } else {
                $("#btnsubmitPAK").attr("disabled", false);
                $("#file").attr("disabled", false);
                swal.fire('Gagal', 'Gagal mengunggah data PAK', 'error');
            }
        }
    })
});

$('#import_uji_kelayakan').on('submit', function(event) {
    event.preventDefault();
    console.log('upload');
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "nilai_pak/import_excel_uk",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmit").attr("disabled", true);
        },
        success: function(data) {
            $("#btnsubmit").attr("disabled", false);
            if (data.status == true) {
                $('#fileUjiKelayakan').val('');
                $('#labelFileUK').text('Choose file');
                swal.fire('Berhasil', data.total + ' data Uji Kelayakan ditambahkan<br>' + data.gagal + ' data gagal ditambahkan', 'success');
                $("#tabelNilai")
                    .DataTable()
                    .ajax.reload();
            } else {
                swal.fire('Gagal', 'Gagal mengunggah data Uji Kelayakan', 'success');
            }
        }
    })
});

$('#import_uji_komite').on('submit', function(event) {
    event.preventDefault();
    console.log('upload');
    $.ajax({
        // url: "nilaiAsesmen/import",
        url: "nilai_pak/import_excel_komite",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            $("#btnsubmitKomite").attr("disabled", true);
        },
        success: function(data) {
            $("#btnsubmitKomite").attr("disabled", false);
            if (data.status == true) {
                $('#fileUjiKomite').val('');
                $('#labelFileKomite').text('Choose file');
                swal.fire('Berhasil', data.total + ' data Uji Komite ditambahkan<br>' + data.gagal + ' data gagal ditambahkan', 'success');
                $("#tabelNilai")
                    .DataTable()
                    .ajax.reload();
            } else {
                swal.fire('Gagal', 'Gagal mengunggah data Uji Kelayakan', 'success');
            }
        }
    })
});

function tabelNilai() {
    var tabel = $('#tabelNilai').DataTable({
        "processing": true,
        "destroy": true,
        "responsive": true,
        "ordering": true, // Set true agar bisa di sorting
        "order": [
            [0, 'asc']
        ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
        "ajax": {
            "url": "nilai_pak/fetch", // URL file untuk proses select datanya
            "type": "GET"
        },
        "aLengthMenu": [
            [5, 10, 50],
            [5, 10, 50]
        ], // Combobox Limit
        "columns": [{
                "data": "no"
            },
            {
                "data": "nik"
            },
            {
                "data": "nama"
            },
            {
                "data": "grade"
            },
            {
                "data": "dept"
            },
            {
                "data": "job_family"
            },
            {
                "data": "pak_sebelum"
            },
            {
                "data": "pak_sekarang"
            },
            {
                "data": "rata-rata"
            },
            {
                "data": "uk"
            },
            {
                "data": "nilai_maks"
            },
            {
                "data": "nilai_uji_komite"
            }

        ],
    });
    tabel.on('order.dt search.dt', function() {
        tabel.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
};

// Class initialization on page load
jQuery(document).ready(function() {
    nilaiAsesmen.init();
});