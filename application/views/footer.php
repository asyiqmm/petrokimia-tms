
</div>
</div>


<!-- Jquery JS-->
<!-- Bootstrap JS-->
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="<?php echo base_url() ?>assets/vendor/slick/slick.min.js">
</script>
<script src="<?php echo base_url() ?>assets/vendor/wow/wow.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/animsition/animsition.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="<?php echo base_url() ?>assets/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="<?php echo base_url() ?>assets/vendor/circle-progress/circle-progress.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="<?php echo base_url() ?>assets/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
<script src="<?php echo base_url() ?>assets/vendor/select2/select2.min.js"></script>
<script src="<?php echo base_url()?>assets/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/chartjs-plugin-doughnutlabel.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<!-- Main JS-->
<script src="<?php echo base_url() ?>assets/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
</body>

</html>