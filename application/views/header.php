<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <link rel="icon" href="<?php echo base_url() ?>assets/images/favicon.gif">
    <!-- Title Page-->


    <title> <?php echo $title ?></title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url() ?>assets/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url() ?>assets/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url() ?>assets/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <script src="<?php echo base_url() ?>assets/vendor/jquery-3.2.1.min.js"></script>
    <link href="<?php echo base_url() ?>assets/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />

    <!-- Main CSS-->
    <link href="<?php echo base_url() ?>assets/css/theme.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url() ?>assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" media="all">

    <link href="<?php echo base_url() ?>assets/datatables/datatables.bundle.css" rel="stylesheet" media="all">
    <script src="<?php echo base_url() ?>assets/datatables/datatables.bundle.js" type="text/javascript"></script>



</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner" style="background-color: #fff;">
                        <a class="logo" href="index.html">
                            <img src="<?php echo base_url() ?>assets/images/pg.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">

                        <?php foreach ($_SESSION['session_header'] as $mobile) { ?>

                            <li class="has-sub <?php echo $mobile['id'] == $parent_active ? "active" : "" ?>">
                                <a class="js-arrow" href="<?php echo $mobile['url'] != null ? base_url() . $mobile['url'] : '#' ?>">
                                    <i class="fas <?php echo $mobile['icon'] ?>"></i><?php echo $mobile['name'] ?></a>
                                <?php if (array_key_exists('child', $mobile)) { ?>

                                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                        <?php foreach ($mobile['child'] as $mchild) {  ?>
                                            <li>
                                                <a href="<?php echo $mchild['url'] ?>"><?php echo $mchild['name'] ?></a>
                                            </li>
                                        <?php } ?>

                                    </ul>
                                <?php } ?>

                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#" style="display: block;margin-left: auto;margin-right: auto;width: 70%;">
                    <!-- <img src="<?php echo base_url() ?>assets/images/logo.png" alt="Petrokimia Gresik" /> -->
                    <img src="<?php echo base_url() ?>assets/images/pg(normal).png" alt="Petrokimia Gresik" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php foreach ($_SESSION['session_header'] as $menu) { ?>

                            <li class=" <?php echo $menu['id'] == $parent_active ? "active" : "" ?> ">
                                <a class="js-arrow" href="<?php echo $menu['url'] != null ? base_url() . $menu['url'] : '#' ?>">
                                    <i class="fas <?php echo $menu['icon'] ?>"></i><?php echo $menu['name'] ?></a>
                                <?php if (array_key_exists('child', $menu)) { ?>

                                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                                        <?php foreach ($menu['child'] as $child) {  ?>
                                            <li class="<?php echo $child['id'] == $child_active ? "active" : "" ?>">
                                                <a href="<?php echo $child['url'] ?>"><?php echo $child['name'] ?></a>
                                            </li>
                                        <?php } ?>

                                    </ul>
                                <?php } ?>

                            </li>
                        <?php } ?>

                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            <p class="title-5">SISTEM MANAJEMEN TALENTA</p>


                            </form>
                            <div class="header-button">
                                <div class="noti-wrap text-left">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <?php foreach ($_SESSION['session_user'] as $user) { ?>
                                            <div class="image">

                                                <?php
                                                if (empty($user->foto)) {
                                                    $avatar =  base_url() . 'assets/images/icon/avatar-05.jpg';
                                                } else {
                                                    $avatar = $user->foto;
                                                }
                                                ?>
                                                <img src="<?php echo $avatar ?>" alt="" />

                                            </div>

                                            <div class="content">
                                                <a class="js-acc-btn" href="#">

                                                    <?php echo $user->nama ?>

                                                </a>
                                            </div>
                                            <div class="account-dropdown js-dropdown">
                                                <div class="info clearfix">
                                                    <div class="image">
                                                        <a href="#">
                                                            <?php
                                                            if (empty($user->foto)) {
                                                                $avatar = base_url() . 'assets/images/icon/avatar-05.jpg';
                                                            } else {
                                                                $avatar = $user->foto;
                                                            }
                                                            ?>
                                                            <img src="<?php echo $avatar ?>" alt="" />
                                                        </a>
                                                    </div>
                                                    <div class="content">

                                                        <h5 class="name">
                                                            <a href="#">
                                                                <?php echo $user->nama ?>
                                                            </a>
                                                        </h5>
                                                        <input type="hidden" id="nikUser" value="<?php echo $user->nik ?>">
                                                        <span class="email">NIK :
                                                            <?php echo $user->nik ?>
                                                        </span>

                                                    </div>
                                                </div>
                                                <!-- <div class="account-dropdown__body">
                                                    <div class="account-dropdown__item">
                                                        <a href="#">
                                                            <i class="zmdi zmdi-account"></i>Account</a>
                                                    </div>
                                                    <div class="account-dropdown__item">
                                                        <a href="#">
                                                            <i class="zmdi zmdi-settings"></i>Setting</a>
                                                    </div>
                                                    <div class="account-dropdown__item">
                                                        <a href="#">
                                                            <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                    </div>
                                                </div> -->
                                                <div class="account-dropdown__footer">
                                                    <a href="<?php echo base_url() ?>auth/logout">
                                                        <i class="zmdi zmdi-power"></i>Logout</a>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->