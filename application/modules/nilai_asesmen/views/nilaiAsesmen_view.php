<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <h2 class="title-5 m-b-35">Nilai Asesmen</h2>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Unggah Data Nilai Asesmen</strong>
                        </div>
                        <form method="POST" id="import_form" enctype="multipart/form-data">
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-10">
                                        <label for="file">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <!-- <input type="file"  name="file" id="file" required accept=".xls, .xlsx">| -->
                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx" />
                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <button type="button" id="btn_contoh_asesmen" class="btn btn-warning btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Contoh template
                                </button>
                            </div>
                        </form>

                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Unggah Data Nilai Asesmen Grade 1</strong>
                        </div>
                        <form method="POST" id="import_formg1" enctype="multipart/form-data">
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-10">
                                        <label for="filegrade1">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <!-- <input type="file"  name="file" id="file" required accept=".xls, .xlsx">| -->
                                                <input type="file" class="custom-file-input" name="filegrade1" id="filegrade1" required accept=".xls, .xlsx" />
                                                <label class="custom-file-label" id="labelFileG1" for="filegrade1">Choose file</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmitg1" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <a type="button" href="<?php echo base_url() . 'index.php/nilai_asesmen/eksport_asesmen_g1' ?>" id="btn_contoh_g1" class="btn btn-warning btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Contoh template
                                </a>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Data Nilai Asesmen Karyawan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">

                                <div class="col col-md-1">
                                    <label for="grade" class=" form-control-label">Tahun</label>
                                </div>
                                <div class="col-12 col-md-2">
                                    <input type="text" required placeholder="Tahun" value="<?php echo date('Y') ?>" class="form-control" autocomplete="off" id="txt_tahun">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 form-group">

                            <!-- DATA TABLE-->
                            <div class="table-responsive">
                                <table class="table  table-borderless table-data3" width="100%" id="tabelNilai">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIK</th>
                                            <th>Nama</th>
                                            <th>Departemen</th>
                                            <th>Grade</th>
                                            <th>Job Family</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Grafik -->
<div class="modal fade" id="grafikNilai" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Grafik</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <h3 class="title-2 m-b-40">Hasil Asemen</h3>
                    <canvas id="barChart"></canvas>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->


<!-- Modal Nilai Potensi -->
<div class="modal fade" id="nilaiPotensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-borderless table-data3" width="100%" id="tabelDetailNilai">
                    <thead>
                        <tr style="background:#2860a8">
                            <th>No</th>
                            <th>KKJ</th>
                            <th>Nilai</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<script src="<?php echo base_url() ?>assets/js/module/nilaiAsesmen_view.js"></script>