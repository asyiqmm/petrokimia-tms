<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
// use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Nilai_asesmen extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('nilai_asesmen_model');
		$this->load->helper(array('url', 'download'));
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Unggah Nilai Asesmen | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function import_excelg1()
	{

		if (isset($_FILES["filegrade1"]["name"])) {
			$extension = pathinfo($_FILES['filegrade1']['name'], PATHINFO_EXTENSION);
			if ($extension == 'csv') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif ($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['filegrade1']['tmp_name']);
			$worksheet = $spreadsheet->getActiveSheet();
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			// $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

			$m_kkj_d1 = $this->nilai_asesmen_model->get_kkj_d_g(5);

			$i = 8;
			$arrDomestic = array();
			foreach ($m_kkj_d1 as $key => $value) {
				$arrDomestic[$i] = $value->id_kkj_d_g;
				$i++;
			}
			$m_kkj_d2 = $this->nilai_asesmen_model->get_kkj_d_g(6);
			$j = $i + 4;
			$arrGlobal = array();
			foreach ($m_kkj_d2 as $key => $value) {
				$arrGlobal[$j] = $value->id_kkj_d_g;
				$j++;
			}

			$arrNik = [];
			$global = [];
			$domestic = [];
			$gagal = 0;
			$sukses = 0;
			for ($row = 5; $row <= $highestRow; ++$row) {
				$nik_sap = $worksheet->getCellByColumnAndRow(4, $row)->getCalculatedValue();
				if (!in_array($nik_sap, $arrNik) && is_numeric($nik_sap)) {
					$arrNik[] = $nik_sap;
					foreach ($arrDomestic as $k => $v) {
						$value = $worksheet->getCellByColumnAndRow($k, $row)->getCalculatedValue();
						if (is_numeric($value) && is_numeric($nik_sap)) {
							$domestic[] = array(
								'nik' => intval($nik_sap),
								'id_kkj_d_g' => intval($v),
								'nilai_asesmen' => floatval($value),
								'created_at' => date('Y-m-d H:i:s')
							);
						}
					}
					foreach ($arrGlobal as $k => $v) {
						$value = $worksheet->getCellByColumnAndRow($k, $row)->getCalculatedValue();
						if (is_numeric($value) && is_numeric($nik_sap)) {
							$global[] = array(
								'nik' => intval($nik_sap),
								'id_kkj_d_g' => intval($v),
								'nilai_asesmen' => floatval($value),
								'created_at' => date('Y-m-d H:i:s')
							);
						}
					}
					$sukses++;
				} else {
					$gagal++;
				}
			}
			$query = $this->nilai_asesmen_model->post_asesmeng1($domestic, $global, $arrNik, $sukses, $gagal);
			echo json_encode($query);
		} else {
		}
	}
	public function import_excel()
	{
		if (isset($_FILES["file"]["name"])) {
			$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			if ($extension == 'csv') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif ($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
			$worksheet = $spreadsheet->getActiveSheet();
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			// $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

			$m_kkj = $this->nilai_asesmen_model->get_kkj();

			$i = 11;
			$arrMap = array();
			foreach ($m_kkj as $key => $value) {
				$arrMap[$i] = $value->id_kkj;
				$i++;
			}
			// var_dump($arrMap);die();
			$arrNik = [];
			$file_data = [];
			$file_ing = [];
			$gagal = 0;
			$sukses = 0;
			$vv[] = array();
			$arrGagal[] = array();
			for ($row = 2; $row <= $highestRow; ++$row) {
				$nik = $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
				$nik_sap = $worksheet->getCellByColumnAndRow(4, $row)->getCalculatedValue();

				if (!in_array($nik_sap, $arrNik) && is_numeric($nik_sap)) {
					$arrNik[] = $nik_sap;
					foreach ($arrMap as $k => $v) {

						$value = $worksheet->getCellByColumnAndRow($k, $row)->getCalculatedValue();
						if (is_numeric($value) && is_numeric($nik_sap)) {
							if ($v == 3) {
								$file_ing[] = array(
									'nilai_ing' => intval($value),
									'nik'		=> intval($nik_sap),
									'tahun'		=> date('Y')
								);
							} else {
							}
							$file_data[] = array(
								'nik_tetap' => strval($nik),
								'nik' => intval($nik_sap),
								'id_kkj' => $v,
								'nilai_asesmen' => intval($value),
								'created_at' => date('Y-m-d H:i:s')
							);
						} else {
							// $arrGagal[] = $nik;
						}
					}
					$sukses++;
				} else {
					// $arrGagal[] = $nik;
					$gagal++;
				}
			}
			$query = $this->nilai_asesmen_model->post_asesmen($file_data, $arrNik, $sukses, $gagal,$file_ing);
			echo json_encode($query);
		} else {
		}
	}

	public function eksport_asesmen()
	{
		force_download('resources/Template_Nilai_Asesmen.xlsx', NULL);

		//make a new spreadsheet object
		// $spreadsheet = new Spreadsheet();
		// $spreadsheet->getProperties()->setCreator('Pengembangan Personal')
		// 	->setLastModifiedBy('Bangpesr')
		// 	->setTitle('Office 2007 XLSX Test Document')
		// 	->setSubject('Office 2007 XLSX Test Document')
		// 	->setDescription('HAV MATRIX')
		// 	->setKeywords('HAV MATRIX')
		// 	->setCategory('HAV');
		// //Retrieve Highest Column (e.g AE)
		// $activesheet = 0;

		// $styleArrayFirstRow = [
		// 	'font' => [
		// 		'bold' => true,
		// 	]
		// ];
		// // foreach ($grade as $key => $value) {
		// 	$spreadsheet->setActiveSheetIndex($activesheet);
		// 	$sheet = $spreadsheet->getActiveSheet();
		// 	$sheet->setTitle('Nilai Asesmen');

		// 	$sheet->getStyle('A:B')->getAlignment()->setHorizontal('center');
		// 	$sheet->getStyle('D:F')->getAlignment()->setHorizontal('center');
		// 	$sheet->getStyle('H:I')->getAlignment()->setHorizontal('center');
		// 	$sheet->getStyle('J')->getAlignment()->setHorizontal('center');
		// 	//set first row bold
		// 	$sheet->getStyle('A1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('B1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('C1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('D1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('E1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('F1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('G1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('H1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('I1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('J1')->applyFromArray($styleArrayFirstRow);
		// 	$sheet->getStyle('K1')->applyFromArray($styleArrayFirstRow);

		// 	//Memberikan Nilai diawal
		// 	$sheet->setCellValue('A1', 'NO')
		// 		->setCellValue('B1', 'NAMA')
		// 		->setCellValue('C1', 'NIK')
		// 		->setCellValue('D1', 'NIK SAP')
		// 		->setCellValue('E1', 'UNIT KERJA SAAT ASESMEN')
		// 		->setCellValue('F1', 'UNIT KERJA SAAT INI')
		// 		->setCellValue('G1', '')
		// 		->setCellValue('H1', 'GRADE SAAT ASESMEN')
		// 		->setCellValue('I1', 'GRADE SAAT INI')
		// 		->setCellValue('J1', '')
		// 		->setCellValue('K1', 'JOB FAMILY')
		// 		->setCellValue('L1', 'STAKEHOLDER SATISFACTION')
		// 		->setCellValue('M1', 'INNOVATION & TOTAL VALUE ADDED')
		// 		->setCellValue('N1', 'INTEGRITY & ETHICS')
		// 		->setCellValue('O1', 'TEAMWORK & SYNERGY')
		// 		->setCellValue('P1', 'BUSINESS ACUMEN')
		// 		->setCellValue('Q1', 'STRATEGIC THINKING')
		// 		->setCellValue('R1', 'DEVELOPING OTHERS')
		// 		->setCellValue('S1', 'DRIVING EXECUTION')
		// 		->setCellValue('T1', 'CHANGE LEADERSHIP')
		// 		->setCellValue('U1', 'ANALITICAL THINKING')
		// 		->setCellValue('V1', 'CONCEPTUAL THINKING')
		// 		->setCellValue('W1', 'TECHNICAL EXPETISE')
		// 		->setCellValue('X1', 'BUSINESS PROCESS ORIENTATION')
		// 		->setCellValue('Y1', 'INFORMATION SEEKING')
		// 		->setCellValue('Z1', 'CONCERN FOR ORDER')
		// 		->setCellValue('AA1', 'SELF CONFIDENCE')
		// 		->setCellValue('AB1', 'SELF CONTROL')
		// 		->setCellValue('AC1', 'RELATIONSHIP BUILDING')
		// 		->setCellValue('AD1', 'FLEXIBILITY')
		// 		->setCellValue('AE1', 'INTERPERSONAL UNDERSTANDING')
		// 		->setCellValue('AF1', 'ORGANIZATIONAL AWARENESS')
		// 		;



		// 	//fit kolom
		// 	foreach (range('A', 'K') as $columnID) {
		// 		$sheet->getColumnDimension($columnID)->setAutoSize(true);
		// 	}
		// 	$activesheet++;
		// 	// $spreadsheet->createSheet();
		// // }
		// $spreadsheet->setActiveSheetIndex(0);


		// $filename = 'Template Nilai Asesmen';
		// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		// header('Cache-Control: max-age=0');
		// // If you're serving to IE 9, then the following may be needed
		// header('Cache-Control: max-age=1');
		// // If you're serving to IE over SSL, then the following may be needed
		// header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		// header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		// header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		// header('Pragma: public'); // HTTP/1.0
		// $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		// $writer->save('php://output');
		// exit;
	}

	public function eksport_asesmen_g1()
	{
		// echo json_encode('asda');
		force_download('resources/Nilai_Asesmen_Grade_1.xlsx', NULL);
	}

	public function preview_excel()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=\"filename.xlsx\"");
		// header("Cache-Control: max-age=0");

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		$writer = new Xlsx($spreadsheet);
		$writer->save('hello world.xlsx');
	}


	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(9, $_SESSION['session_menu'])) {
			$data['parent_active'] = 8;
			$data['child_active'] = 9;
			$this->_render('nilaiAsesmen_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function data()
	{
	}

	function fetch()
	{
		$tahun = $this->input->get('tahun');
		$query = $this->nilai_asesmen_model->listAsesmen($tahun);
		$data 			= array();
		foreach ($query as $index => $item) {
			$btn = '<button  type="button" onclick="detail(\'' . $item->nik . '\')" class="btn btn-primary btn-sm"  title="Lihat"> <i class="fa fa-eye"></i> Lihat </button>';
			$data[] 	= array(
				"no" => '',
				"nama" => $item->nama,
				"nik" 			=> $item->nik,
				"unit_name"			=> $item->unit_name,
				"nm_grade"			=> $item->nm_grade,
				"job_fit"			=> $item->nm_fitjob_fam,
				"aksi" => ($item->nilai_kkj == true ? $btn : '-')
			);
		}
		// var_dump($data);die();
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function get_nilai()
	{
		$nik = $this->input->post('nik');
		$tahun = $this->input->post('tahun');
		$query = $this->nilai_asesmen_model->get_nilai($nik, $tahun);
		$no = 1;
		$data = array();
		foreach ($query as $item) {
			$data[] 	= array(
				"nomor"			=> $no,
				"kkj"		=> $item->kkj,
				"nilai" => $item->nilai

			);
			$no++;
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}
}
