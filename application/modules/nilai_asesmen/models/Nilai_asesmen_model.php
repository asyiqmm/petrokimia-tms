<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Nilai_asesmen_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}
	
	function listAsesmen($tahun)
	{
		$query = $this->db->query("select
				a.nik,a.nama,c.unit_name,e.nm_grade,g.nm_fitjob_fam,
				CASE WHEN h.nik IS NOT NULL
				THEN true
				ELSE false
				END as nilai_kkj 
			from
				m_users_karyawan a
			join d_list_karyawan_talenta b on
				b.nik = a.nik
			left join m_unit c on
				c.unit_id = b.kd_departemen
			left join m_direktorat d on
				d.kd_direktorat = b.kd_direktorat
			join m_grades e on
				e.nm_grade = b.grade
			left join d_unit_job_family f on
				f.unit_id = c.unit_id
			left join m_fitjob_fam g on
				g.id = f.id_job_family
			left join t_nilai_potensi_kkj h on
				h.nik = b.nik
				and date_part('year', h.created_at) = '$tahun'
			where
				date_part('year', b.created_date) = '$tahun'
			group by
				a.nik,a.nama,c.unit_name,e.nm_grade,g.nm_fitjob_fam,h.nik
		");
		return $query->result();
	}

	function post_asesmen($data, $nik, $sukses, $gagal,$file_ing)
	{
		
		$this->db->trans_begin();
		$ids = implode("','", $nik);
		// $this->db->query("delete from t_nilai_potensi_kkj where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$this->db->query("delete from t_ing_kkj where tahun = date_part('year', CURRENT_DATE)");
		$this->db->query("delete from t_nilai_potensi_kkj where date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		
		$this->db->insert_batch('t_ing_kkj', $file_ing);
		$this->db->insert_batch('t_nilai_potensi_kkj', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'sukses' => $sukses,
				'gagal'  => $gagal
			];
		}
	}

	function post_asesmeng1($domestic,$global, $nik, $sukses, $gagal)
	{
		$this->db->trans_begin();
		$ids = implode("','", $nik);
		$this->db->query("delete from t_nilai_potensi_g_d where date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$this->db->insert_batch('t_nilai_potensi_g_d', $domestic);
		$this->db->insert_batch('t_nilai_potensi_g_d', $global);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'sukses' => $sukses,
				'gagal'  => $gagal
			];
		}
	}

	public function get_kkj()
	{
		$query = $this->db->query("SELECT * from m_kkj where active = true
		and date_part('year', created_at) = date_part('year', CURRENT_DATE)
		order by id_kkj");
		return $query->result();
	}
	public function get_kkj_d_g($kat)
	{
		$query = $this->db->order_by('id_kkj_d_g')->where('kat_kompetensi',$kat)->get('m_kkj_domestic_global');
		return $query->result();
	}
	
	public function get_nilai($nik,$tahun)
	{
		$query = $this->db->query("select b.nm_kkj kkj,a.nik,a.nilai_asesmen nilai from t_nilai_potensi_kkj a
		join m_kkj b on b.id_kkj = a.id_kkj
		where nik = '$nik' 
		and date_part('year', b.created_at) = '$tahun'
		and date_part('year', a.created_at) = '$tahun'
		");
		// var_dump($this->db->last_query());die();
		return $query->result();
	}
}
