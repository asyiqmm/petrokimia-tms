<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Overview_individu extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('overview_individu_model');

	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Overview Individu | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		// var_dump('masuk');die();
		//load library
		$uri = &load_class('URI', 'core');
		if (in_array(15, $_SESSION['session_menu'])) {
			$data['parent_active'] = 15;
			$data['child_active'] = 15;
			$this->_render('overviewIndividu_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_grafikAsesmen()
	{
		$n = $this->input->post('nik');
		$g = $this->input->post('grade');
		$j = $this->input->post('job_family');
		$jabatan = $this->input->post('jabatan');

		$query = $this->overview_individu_model->get_grafikAsesmen($n, $g, $j, $jabatan);
		echo json_encode($query);
	}
}
