<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Overview_individu_model extends CI_Model{
	public function __construct()
	{
	      // Call the CI_Model constructor
	    parent::__construct();
	    $this->load->database();
	}
	public function get_grafikAsesmen($nik, $grade, $job_family,$jabatan)
	{
		$kriterias = $this->db->query("SELECT * FROM m_jabatan WHERE jabatan = '$jabatan'")->result();
		$kategori  = $kriterias[0]->kategori;
		
		$tahun = date('Y');
		$qq = "SELECT distinct a.id_kkj,g.nilai_asesmen,b.kkj,a.nilai
		from standar_kkj a
		join m_kkj b on a.id_kkj = b.id_kkj and date_part('year', b.created_at) = '2020'
		join m_fitjob_fam c on a.id_fitjob_fam = c.id
		join m_grade_cat d on d.id_grad_cat = a.id_grade
		join m_grades e on e.id_cat = d.id_grad_cat
		join d_list_karyawan_talenta f on f.grade = e.nm_grade
		left join t_nilai_potensi_kkj g on g.nik = '$nik' and g.id_kkj = a.id_kkj 
		and date_part('year', g.created_at) = '$tahun'
		where d.id_grad_cat = '$grade' and c.id = '$job_family' and a.kategori = e.id_promosi and date_part('year', a.created_at) = '$tahun'
		group by a.id_kkj,a.nilai,g.nilai_asesmen,b.kkj,a.nilai 
		order by id_kkj asc";
		
		$qqq = "SELECT
					DISTINCT A.ID_KKJ,
					G.NILAI_ASESMEN,
					B.KKJ,
					A.NILAI
				FROM
					STANDAR_KKJ A
				JOIN M_KKJ B ON
					A.ID_KKJ = B.ID_KKJ
					AND DATE_PART('YEAR', B.CREATED_AT) = '$tahun'
				JOIN M_FITJOB_FAM C ON
					A.ID_FITJOB_FAM = C.ID
				JOIN M_GRADES E ON
					E.id_promosi = A.id_grade
				JOIN D_LIST_KARYAWAN_TALENTA F ON
					F.GRADE = E.NM_GRADE
				LEFT JOIN T_NILAI_POTENSI_KKJ G ON
					G.NIK = '$nik'
					AND G.ID_KKJ = A.ID_KKJ
					AND DATE_PART('YEAR', G.CREATED_AT) = '$tahun'
				WHERE
					E.id_cat = '$grade'
					AND C.ID = '$job_family'
					AND A.KATEGORI = 1
					AND DATE_PART('YEAR', A.CREATED_AT) = '$tahun'
				GROUP BY
					A.ID_KKJ,
					A.NILAI,
					G.NILAI_ASESMEN,
					B.KKJ,
					A.NILAI
				ORDER BY
					ID_KKJ ASC";
				// var_dump($qq);die();
		return $this->db->query($qqq)->result();
	}
}