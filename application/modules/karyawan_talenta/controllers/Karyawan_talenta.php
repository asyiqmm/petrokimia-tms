<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Karyawan_talenta extends CI_Controller
{
        public function __construct()
        {
                parent::__construct();
                if (!$this->ion_auth->logged_in()) {
                        redirect('auth/login', 'refresh');
                }
                $this->load->library('session');
                $this->load->model('karyawan_talenta_model');
                $this->load->model('unit_job_family/unit_job_family_model');
                set_time_limit(0);
        }
        private function _render($view, $data = array())
        {
                $data['title'] = "Data Keikutsertaan Karyawan | PT Petrokimia Gresik";
                $this->load->view('header', $data);
                $this->load->view($view, $data);
                $this->load->view('footer');
        }

        public function index()
        {
                $uri = &load_class('URI', 'core');
                if (in_array(30, $_SESSION['session_menu'])) {
                        $data['job_family'] = $this->unit_job_family_model->getJobFam();
                        $data['unit_kerja'] = $this->unit_job_family_model->getUnit();
                        $data['direktorat'] = $this->unit_job_family_model->getDirektorat();
                        $data['grade'] = $this->unit_job_family_model->getGrade();
                        
                        
                        $data['parent_active'] = 8;
                        $data['child_active'] = 30;
                        $this->_render('karyawan_talenta_view', $data);
                } else {
                        redirect(base_url() . 'auth/login');
                }
        }

        public function ubah()
        {
                $nik = $this->input->post('nik_karyawan');
                $direktorat = $this->input->post('direktorat_karyawan');
                $unit = $this->input->post('unit_karyawan');
                $job = $this->input->post('job_karyawan');
                $grade = $this->input->post('grade_karyawan');
                
                $query = $this->karyawan_talenta_model->ubahData($nik,$direktorat,$unit,$job,$grade);
		echo json_encode($query);

        }

        public function hapus()
        {
                $nik = $this->input->post('nik');
                $query = $this->karyawan_talenta_model->hapus($nik);
		echo json_encode($query);

        }
        public function getListKaryawan()
        {
                $query = $this->karyawan_talenta_model->getListKaryawan();
                $data                         = array();
                foreach ($query as $index => $item) {
                        $btn = '<button type="button" onclick="ubahUser(\'' . $item->nik . '\')" class="btn btn-primary btn-sm"  title="Ubah"> <i class="fa fa-edit"></i></button>
                        <button type="button" onclick="hapusUser(\'' . $item->nik . '\')" class="btn btn-danger btn-sm"  title="Hapus"> <i class="fa fa-trash"></i></button>';
                        $data[]         = array(
                                "nik"                           => $item->nik,
                                "nik_tetap"                           => $item->nik_tetap,
                                "nama"                          => $item->nama,
                                "grade"                         => $item->nm_grade,
                                "departemen"                    => $item->unit_name,
                                "jabatan"                       => $item->nm_jabt,
                                "job_family"                    => $item->nm_fitjob_fam,
                                "nm_direktorat"                    => $item->nm_direktorat,
                                "aksi" => $btn
                        );
                }
                
                $obj = array("data" => $data);

                echo json_encode($obj);
        }

        public function import_excel()
        {
                if ($_FILES['file']['name']) {
                        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                        if ($extension == 'csv') {
                                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                        } elseif ($extension == 'xlsx') {
                                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                        } else {
                                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                        }

                        $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

                        // file path
                        $worksheet = $spreadsheet->getActiveSheet();
                        $highestRow = $worksheet->getHighestRow(); // e.g. 10
                        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'

                        $arrNik = [];
                        $nikGagal = [];
                        $data = [];
                        for ($row = 2; $row <= $highestRow; $row++) {
                                $nik_tetap            = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                                $nik            = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                                $grade          = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                                $kd_direktorat  = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                                $departemen     = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                                $jabatan     = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                                $kd_jabatan     = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                                $tgl_pensiun  = $worksheet->getCellByColumnAndRow(65, $row)->getValue();
                                $date = date('Y-m-d',\PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($tgl_pensiun));
                                if (!in_array($nik, $arrNik) && (is_numeric($nik) || is_int($nik))) {
                                        $arrNik[] = $nik;
                                        $data[]         = array(
                                                'nik_tetap' => strval($nik_tetap),
                                                'nik' => intval($nik),
                                                'grade' => strval($grade),
                                                'kd_direktorat' => $kd_direktorat,
                                                'tgl_pensiun' => $date,
                                                'created_date' => date('Y-m-d H:i:s'),
                                                'active' => true,
                                                'kd_departemen' => $departemen,
                                                'nm_jabatan' => $jabatan,
                                                'jabatan' => $kd_jabatan,
                                                'tahun' => date('Y'),
                                        );
                                } else {
                                        $nikGagal[] = $nik;
                                }
                        }
// var_dump($data);die();
                        $data = $this->karyawan_talenta_model->insertGrade($data, $arrNik, $nikGagal);
                        echo json_encode($data);
                } else {
                        var_dump('keluar');
                        die();
                }
        }

        public function get_karyawan()
        {
                $nik = $this->input->post('nik');
                $query = $this->karyawan_talenta_model->get_info($nik);
                
                echo json_encode($query);
        }
}
