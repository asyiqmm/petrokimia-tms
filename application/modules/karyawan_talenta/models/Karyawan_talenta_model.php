<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Karyawan_talenta_model extends CI_Model
{

	public function getListKaryawan()
	{
		$sql = "SELECT a.nik,a.nama,e.id_grad,b.nik_tetap, e.id_cat,e.nm_grade,c.unit_id,c.unit_name,d.id,d.nm_fitjob_fam,b.nm_jabatan nm_jabt,g.nm_direktorat
		from m_users_karyawan a
		join d_list_karyawan_talenta b on b.nik = a.nik
		left join m_unit c on c.unit_id = b.kd_departemen
		left join m_fitjob_fam d on d.id = b.job_family
		join m_grades e on e.nm_grade = b.grade
		left join m_direktorat g on g.kd_direktorat = b.kd_direktorat
		where date_part('year', b.created_date) = date_part('year', CURRENT_DATE) and b.active = true";
		$query = $this->db->query($sql);
		
		return $query->result();
	}

	public function hapus($nik)
	{
		$this->db->trans_begin();
		$query = $this->db->query("delete from d_list_karyawan_talenta where date_part('year', created_date) = date_part('year', CURRENT_DATE)");
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
			];
		}
	}
	
	public function insertGrade($data, $nikSukses, $nikGagal)
	{
		// var_dump($data);
		// die();
		$this->db->trans_begin();
		$NIK = implode("','", $nikSukses);
		//hapus semua data yg ada
		$this->db->query("delete from d_list_karyawan_talenta where date_part('year', created_date) = date_part('year', CURRENT_DATE)");

		//menginputkan data yg ada
		$this->db->insert_batch('d_list_karyawan_talenta', $data);

		// Mendapatkan list user
		$sql = "SELECT a.nik,c.unit_id,c.unit_name,d.id id_job_family,d.nm_fitjob_fam nm_job_family 
		from d_list_karyawan_talenta a
		left join d_unit_job_family b on b.unit_id = a.kd_departemen
		left join m_unit c on c.unit_id = a.kd_departemen
		left join m_fitjob_fam d on d.id = b.id_job_family
		where date_part('year', a.created_date) = date_part('year', CURRENT_DATE)
		and a.nik in ('" . $NIK . "')";
		$listKaryawan = $this->db->query($sql)->result();
		
		
		// //looping untuk menentukan data yg diupdate
		foreach ($listKaryawan as $index => $item) {
			$update_data[]         = array(
				"nik"   => $item->nik,
				"job_family" => $item->id_job_family,
				"updated_date" => date('Y-m-d H:i:s')
			);
		}
		$this->db->where('tahun',2020)
		->update_batch('d_list_karyawan_talenta', $update_data, 'nik');
		$data = $this->db->last_query();
		$sss = $data." and tahun='2020'";
		// var_dump($sss);die();
		$this->db->query($data .' and tahun = 2020');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'sukses' => $nikSukses,
				'gagal' => $nikGagal
			];
		}
	}
	public function ubahData($nik, $direktorat, $unit, $job,$grade)
	{
		$this->db->trans_begin();
		$this->db
			->set('job_family', $job)
			->set('kd_direktorat', $direktorat)
			->set('updated_date', date('Y-m-d H:i:s'))
			->set('grade',$grade)
			->where('nik', $nik)
			->update('d_list_karyawan_talenta');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
	public function get_info($nik)
	{
		$query = "select	a.nik,a.nama,b.grade,c.unit_id,c.unit_name,d.id,d.nm_fitjob_fam,g.kd_direktorat,g.nm_direktorat,g.id_direktorat
		from m_users_karyawan a
		join d_list_karyawan_talenta b on b.nik = a.nik
		left join m_unit c on c.unit_id = a.unit_id
		left join m_fitjob_fam d on d.id = b.job_family
--		join m_jabatans f on f.kd_jabatan = a.grade
		left join m_direktorat g on g.kd_direktorat = b.kd_direktorat
		where date_part('year', b.created_date) = date_part('year', CURRENT_DATE) and b.active = true and b.nik = '$nik'";
		// var_dump($query);die();
		return $this->db->query($query)->result();
	}
}
