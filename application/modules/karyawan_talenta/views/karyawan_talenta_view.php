<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <h2 class="title-5 m-b-35">Data Keikutsertaan Karyawan</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form method="POST" id="import_form" enctype="multipart/form-data">
                            <div class="card-header">
                                <strong>Unggah Data Keikutsertaan Karyawan</strong>
                            </div>
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-4">
                                        <label for="file">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx">
                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12 text-center">
                                            Berhasil Upload
                                        </div>
                                        <div>
                                            <p id="listBerhasil" class="alert alert-success text-left" style="max-height: 97px; overflow-y:auto"></p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-12 text-center">
                                            Gagal Upload
                                        </div>
                                        <div>
                                            <p id="listGagal" class="alert alert-danger text-left" style="max-height: 97px; overflow-y:auto"></p>
                                        </div>
                                    </div>
                                </div>
                                <div id="response"></div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Data Karyawan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="table-responsive">
                                        <table class="table  table-borderless table-data3" width="100%" id="tabelKaryawan">
                                            <thead style="background-color: cadetblue;color: aliceblue;">
                                                <tr>
                                                    <td width="1%">
                                                        No
                                                    </td>
                                                    <td width="19%">Nama</td>
                                                    <td width="0%">Nik</td>
                                                    <td width="10%">Nik SAP</td>
                                                    <td width="5%">Grade</td>
                                                    <td width="5%">Direktorat</td>
                                                    <td width="10%">Departemen</td>
                                                    <td width="12.5%">Jabatan</td>
                                                    <td width="14.5%">Job Family</td>
                                                    <td width="12%"> </td>
                                                </tr>

                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="" method="post" id="formUbah" class="form-horizontal">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Edit data karyawan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class=" form-control-label">NIK: </label>
                        </div>
                        <div class="col col-sm-8">
                            <input type="text" id="nik_karyawan" readonly name="nik_karyawan" placeholder="Normal" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class=" form-control-label">Nama: </label>
                        </div>
                        <div class="col col-sm-8">
                            <input type="text" id="nama_karyawan" readonly name="nama_karyawan" placeholder="Normal" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class=" form-control-label">Grade: </label>
                        </div>
                        <div class="col col-sm-8">
                            <select name="grade_karyawan" id="grade_karyawan" class="form-control">
                                <option value=''>--Pilih Grade--</option>
                                <?php foreach ($grade as $g) {
                                    echo '<option value="' . $g->nm_grade . '">' . $g->nm_grade . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class=" form-control-label">Direktorat: </label>
                        </div>
                        <div class="col col-sm-8">
                            <select name="direktorat_karyawan" id="direktorat_karyawan" class="form-control">
                                <option value=''>--Pilih Direktorat--</option>
                                <?php foreach ($direktorat as $d) {
                                    echo '<option value="' . $d->kd_direktorat . '">' . $d->nm_direktorat . '</option>';
                                }
                                ?>
                            </select>
                            <!-- <input type="text" id="direktorat_karyawan" readonly name="input-normal" placeholder="Normal" class="form-control"> -->
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class=" form-control-label">Job Family: </label>
                        </div>
                        <div class="col col-sm-8">
                            <select name="job_karyawan" id="job_karyawan" class="form-control">
                                <option value=''>--Pilih Unit Kerja--</option>
                                <?php foreach ($job_family as $d) {
                                    echo '<option value="' . $d->id . '">' . $d->nm_fitjob_fam . '</option>';
                                }
                                ?>
                            </select>
                            <!-- <input type="text" id="job_karyawan" readonly name="input-normal" placeholder="Normal" class="form-control"> -->
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/karyawan_talenta_view.js"></script>