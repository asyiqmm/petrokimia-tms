<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{

	public function getListKaryawan()
	{
		$query = $this->db->query('select a.nik,a.nama,b.unit_id,b.unit_name,c.id_grad id_grade,c.nm_grade,d.nm_jabt jabatan,
		EXISTS(select nik from d_users_manajer z where z.nik = a.nik and z.active = true) as peran
		from m_users_karyawan a
		left join m_unit b on b.unit_id = a.unit_id
		left join m_grades c on c.nm_grade = a.grade_personal
		left join m_jabatans d on d.kd_jabatan = a.grade');
		return $query->result();
	}
	public function ubahKaryawan($nik, $role)
	{
		$this->db->trans_begin();
		$cek_info = $this->db->query("select * from d_users_manajer where nik = '$nik'");
		
		if ($cek_info->num_rows() > 0) {
			$this->db->query("UPDATE d_users_manajer
			SET active=$role
			WHERE nik='$nik'");
		}else{
			$this->db->query("INSERT INTO d_users_manajer (nik, active)
			VALUES ($nik, $role);");
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}


	public function update_karyawan($data)
	{
		$this->db->trans_begin();
		//hapus semua data yg ada
		$this->db->query("delete from m_users_karyawan");

		//menginputkan data yg ada
		$this->db->insert_batch('m_users_karyawan', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true
			];
		}
	}
	public function insertGrade($data)
	{

		$this->db->trans_begin();

		$this->db->update_batch('m_users_karyawan', $data, 'nik');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function get_info($nik)
	{
		$query = $this->db->query("select *,exists(select * from d_users_manajer dum where dum.nik = '$nik' and dum.active = true) from m_users_karyawan a
		where a.nik = '$nik' limit 1")->result();
		return $query;
	}
}
