<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form method="POST" id="import_form" enctype="multipart/form-data">
                            <div class="card-header">
                                <strong>Unggah Data Karyawan</strong>
                            </div>
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-4">
                                        <label for="file">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx">
                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="response"></div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>

                            </div>
                        </form>

                    </div>

                </div>
            </div> -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Data Karyawan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="">
                                        <button class="btn btn-primary col-md-3 m-b-25" type="button" id="update_karyawan" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-sync-alt"></i> Update Data Karyawan</button>
                                    
                                        <table class="table display table-responsive responsive nowrap" width="100%" id="tabelKaryawan">
                                            <thead style="background-color: cadetblue;color: aliceblue;">
                                                <tr>
                                                    <td width="1%">
                                                        No
                                                    </td>
                                                    <td width="19%">Nama</td>
                                                    <td width="12.5%">Nik</td>
                                                    <!-- <td width="12.5%">Grade</td> -->
                                                    <td width="12.5%">Departemen</td>
                                                    <td width="12.5%">Jabatan</td>
                                                    <td width="12%">Peran</td>
                                                    <td width="12%"> </td>
                                                </tr>

                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <!-- <th></th> -->
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="" method="post" id="formUbah" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Edit data karyawan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="nik" name="nik">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class=" form-control-label">Nama: </label>
                        </div>
                        <div class="col col-sm-8">
                            <input type="text" id="nama" readonly name="nama" placeholder="Normal" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-sm-3">
                            <label for="input-normal" class="form-control-label">Peran: </label>
                        </div>
                        <div class="col col-sm-8">
                            <select name="role" id="role" class="form-control">
                                <option value="false">Karyawan</option>
                                <option value="true">Manajer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/dataKaryawan_view.js"></script>