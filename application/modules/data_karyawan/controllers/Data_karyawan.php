<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Data_karyawan extends CI_Controller
{
        public function __construct()
        {
                parent::__construct();
                if (!$this->ion_auth->logged_in()) {
                        redirect('auth/login', 'refresh');
                }
                $this->load->library('session');
                $this->load->model('karyawan_model');
                set_time_limit(0);
        }
        private function _render($view, $data = array())
        {
                $data['title'] = "Data Karyawan | PT Petrokimia Gresik";
                $this->load->view('header', $data);
                $this->load->view($view, $data);
                $this->load->view('footer');
        }

        public function index()
        {
                $uri = &load_class('URI', 'core');
                if (in_array(11, $_SESSION['session_menu'])) {
                        $data['parent_active'] = 11;
                        $data['child_active'] = 11;
                        $this->_render('dataKaryawan_view', $data);
                } else {
                        redirect(base_url() . 'auth/login');
                }
        }

        public function getListKaryawan()
        {
                $query = $this->karyawan_model->getListKaryawan();

                $data                         = array();
                foreach ($query as $index => $item) {
                        $btn = '<button  type="button" onclick="ubahUser(\'' . $item->nik . '\',\'' . $item->nama . '\',\'' . $item->peran . '\')" class="btn btn-primary btn-sm"  title="Ubah"> <i class="fa fa-edit"></i> Ubah </button>';
                        $manajer = '<span class="role admin">Manajer</span>';
                        $karyawan = '<span class="role user">Karyawan</span>';
                        $data[]         = array(
                                "nama" => $item->nama,
                                "nik"                           => '<span class="block-email">' . $item->nik . '</span>',
                                // "grade"                         => $item->nm_grade,
                                "departemen"                    => $item->unit_name,
                                "jabatan"                       => $item->jabatan,
                                "peran"                         => $item->peran,
                                "peran"                         => $item->peran == true ? $manajer :  $karyawan,

                                "aksi" => $btn
                        );
                }
                $obj = array("data" => $data);

                echo json_encode($obj);
        }

        public function import_excel()
        {
                if ($_FILES['file']['name']) {
                        $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                        if ($extension == 'csv') {
                                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                        } elseif ($extension == 'xlsx') {
                                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                        } else {
                                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                        }

                        $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

                        // file path
                        $worksheet = $spreadsheet->getActiveSheet();
                        $highestRow = $worksheet->getHighestRow(); // e.g. 10
                        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'

                        for ($row = 2; $row <= $highestRow; $row++) {
                                $nik_tetap      = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                                $nik            = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                                $grade          = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                                $data[]         = array(
                                        'nik' => $nik,
                                        'nik_tetap' => strval($nik_tetap),
                                        'grade_personal' => $grade
                                );
                        }

                        $data = $this->karyawan_model->insertGrade($data);
                        $obj = array("data" => $data);
                        // echo json_encode($obj);die
                } else {
                        var_dump('keluar');
                        die();
                }
        }
        public function ubahKaryawan()
        {
                $nik = $this->input->post('nik');
                $role = $this->input->post('role');
                $query = $this->karyawan_model->ubahKaryawan($nik,$role);
                echo json_encode($query);
        }

        public function update_dataKaryawan()
        {
                $data = array(
			'grant_type' 		=> 'password',
			'username' 		=> 'app_vendor',
			'password' 	=> 'apP@v3nd0r123!!'
		);
		$url = 'https://sso.petrokimia-gresik.net/token';
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded",
				'method'  => 'POST',
				'content' => http_build_query($data),
				"ignore_errors" => true
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, true, $context);
		$datas = json_decode($result, true);

		$url2 = 'https://sso.petrokimia-gresik.net/api/Employee/List';
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded",
				'method'  => 'GET',
				// 'content' => http_build_query($data),
				"ignore_errors" => true,
				"header" => 'Authorization: Bearer ' . $datas['access_token']
			)
		);
		$context  = stream_context_create($options);
		$result2 = file_get_contents($url2, true, $context);
                $datas2 = json_decode($result2, true);
                // var_dump($datas2[1]['nik']);die();
		foreach ($datas2 as $key => $value) {
			$unit_data[] = array(
				'nik' 	=> $datas2[$key]['nik'],
				'nama' => $datas2[$key]['nama'],
				'foto' 	=> $datas2[$key]['foto'],
				'grade' 	=> $datas2[$key]['grade'],
				'gradename' 	=> $datas2[$key]['gradeName'],
				'unit_id' 	=> $datas2[$key]['unitId'],
				'created_at' => date('Y-m-d H:i:s')
			);
		}
		$data = $this->karyawan_model->update_karyawan($unit_data);
		echo json_encode($data);
        }
        public function get_karyawan()
        {
                $nik = $this->input->post('nik');
                $query = $this->karyawan_model->get_info($nik);
                echo json_encode($query);
        }
}
