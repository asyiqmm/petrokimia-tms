<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <h2 class="title-5 m-b-25">Rekap Hasil Minat Bakat Karyawan</h2>

            <!-- <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <form method="POST" id="btnCari" enctype="multipart/form-data">

                                <div class="row">
                                    <div class=" col-md-12">
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Grade</label>
                                            </div>
                                            <div class="col-12 col-md-4">
                                                <select name="grade" required data-live-search="true" data-size="1" id="grade" class="form-control">
                                                    <option value="">--Pilih Grade--</option>

                                                    <?php foreach ($grades_cat as $cat) {
                                                        echo '<option value="' . $cat->id_grad_cat . '">' . $cat->nm_grade_cat . '</option>';
                                                    } ?>
                                                </select>

                                            </div>
                                            <div class="col-12 col-md-2">
                                                <select name="grade" data-live-search="true" data-size="1" id="sub_grade" class="form-control">
                                                    <option value="">--Pilih Sub Grade--</option>

                                                </select>

                                            </div>
                                            <div class="col-12 col-md-2 text-right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-search"></i> Cari
                                                </button>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Rekomendasi</label>
                                            </div>
                                            <div class="col-12 col-md-8">
                                                <select name="rekomendasi" required id="rekomendasi" class="form-control">
                                                    <option value=''>--Pilih Jenis Rekomendasi--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Job Family</label>
                                            </div>
                                            <div class="col-12 col-md-8">
                                                <select name="jobFitFamily" required data-size="5" id="jobFitFamily" class="form-control">

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <!-- <div class="table-responsive table-data"> -->
                            <table class="table" id="tabelAsesmen">
                                <thead>
                                    <tr>
                                        <td width="10%">No</td>
                                        <td width="20%">NIK</td>
                                        <td width="30%">Name</td>
                                        <td width="10%">File PDF</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-body {
        /* 100% = dialog height, 120px = header + footer */
        max-height: calc(100% - 120px);
        overflow-y: scroll;
    }
</style>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="nilaiPotensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Hasil Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                    <input type="hidden" value="<?php echo base_url() ?>" id="baseUrl">
                    <object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/laporan_minat_bakat_view.js"></script>