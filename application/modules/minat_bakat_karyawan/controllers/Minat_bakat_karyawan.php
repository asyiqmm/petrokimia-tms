<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Minat_bakat_karyawan extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('minat_bakat_karyawan_model');
		$this->load->model('job_fit_karyawan/job_fit_karyawan_model');
		$this->load->library('session');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Rekap Hasil Asesmen Karyawan | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		if (in_array(20, $_SESSION['session_menu'])) {
			$data['grades_cat'] = $this->job_fit_karyawan_model->get_grade();
			$data['parent_active'] = 20;
			$data['child_active'] = 20;
			$this->_render('TM_karyawan_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_rekomendasi()
	{
		$grade = $this->input->post('grade');
		if ($grade) {
			$query = $this->minat_bakat_karyawan_model->get_rekomendasi($grade);
			echo json_encode($query);
		}
	}

	public function get_listAsesmen()
	{
		$id_unit 			= $_SESSION['session_user'][0]->id_unit;
		$nik 			= $_SESSION['session_user'][0]->nik;

		$query = $this->minat_bakat_karyawan_model->get_listKaryawan($id_unit,$nik);
		$datas 			= array();


		foreach ($query as $index => $item) {
			// var_dump($item->nik);die();
			$pdf = '<button class="btn btn-primary" onclick="modalPDF(\'' . $item->file_path . '\')"><i class="far fa-file-pdf"></i> pdf</button>';

			$datas[] 	= array(
				"no" => "",
				"nama" => $item->nama,
				"nik" 			=> $item->nik,
				"pdf" => ($item->file_path !== null ? $pdf : 'null'),
			);
		}
		$obj = array("data" => $datas);
		echo json_encode($obj);
	}

	public function get_rek($nik, $grade, $job)
	{

		$query = $this->asesmen_karyawan_model->get_grafikAsesmen($nik, $grade, $job);

		$data = array();
		foreach ($query as $key => $value) {
			if ($query[$key]->nilai == 0) {
				$data[] = array(
					"perhitungan" => 0,
					"nik" => $query[$key]->nik
				);
			} else {
				$data[] = array(
					"perhitungan" => $query[$key]->nilai_asesmen / $query[$key]->nilai,
					"nik" => $query[$key]->nik
				);
			}
		}
		$total = 0;
		foreach ($data as $key => $value) {
			$total = $data[$key]['perhitungan'] + $total;
		}
		$hasil = intval(($total / count($data)) * 100);
		return $hasil;
	}
}
