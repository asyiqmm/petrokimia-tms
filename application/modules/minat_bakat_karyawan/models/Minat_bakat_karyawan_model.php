<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Minat_bakat_karyawan_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade')
			->get('m_grades');
		return $query->result();
	}
	public function get_rekomendasi($grade)
	{
		$query = $this->db->query("SELECT a.id_rek_grade,b.id_grad,(select nm_grade from m_grades where id_grad = a.id_rek_grade limit 1),a.keterangan from d_rekomendasi_grade a
		join m_grades b on b.id_grad = a.id_grade
		where b.id_grad = '$grade'");
		$output = "<option value=''>--Pilih Jenis Rekomendasi--</option>";
		foreach ($query->result() as $rek) {
			$output .= '<option value="' . $rek->id_rek_grade . '">' . $rek->keterangan . ' (' . $rek->nm_grade . ')</option>';
		}
		return $output;
	}

	public function get_listKaryawan($unit_id, $nik)
	{
		$baseSql = "select a.nik,a.nama,c.file_path
		from m_users_karyawan a
		join d_list_karyawan_talenta b on a.nik = b.nik
		left join t_pdf_minat_bakat c on c.nik = b.nik
		where a.unit_id = '$unit_id' and a.nik not in ('$nik')";
		return $this->db->query($baseSql)->result();
		
	}
	public function karyawan_by_grade($grade)
	{
		$query = $this->db->select('b.nama,b.nik_tetap')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->join('m_grade_cat as c', 'c.id_grad_cat = a.id_cat')
			->where('c.id_grad_cat', $grade)
			->order_by('b.nama', 'ASC')
			->get();
		return $query->result();
	}

	public function karyawan_by_sub_Grade($sub_grade)
	{
		$query = $this->db->select('b.nama,b.nik')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->where('a.id_grad', $sub_grade)
			->order_by('b.nama', 'ASC')
			->get();
		return $query->result();
	}
}
