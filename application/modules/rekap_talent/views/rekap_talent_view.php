<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">Rekap Talent</h3>

                    <!-- END DATA TABLE-->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Hitung Talent</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-md-2 text-right">
                                    <label for="grade">Grade :</label>
                                </div>
                                <div class="col-md-4">
                                    <select name="grade" class="form-control" id="grade"></select>
                                </div>
                                <div class="col col-md-6 text-right">
                                    <button class="btn btn-primary btn-md m-b-20" id="btnCari"><i class="fas fa-search"></i> Cari</button>
                                </div>
                            </div>
                            <div class="row form-group">

                                <div class="m-b-25"></div>
                                <div class="col-md-12 form-group">
                                    <table class="table text-nowrap text-center table-responsive table-data3" width="100%" id="tabelUjiKelayakan">
                                        <thead style="background-color: cadetblue;color: aliceblue;">
                                            <tr>
                                                <th width="1%" class="text-center">No</th>
                                                <th class="text-left">Direktorat</th>
                                                <th>Non Talent</th>
                                                <th>Talent</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table-bordered">

                                        </tbody>
                                        <tfoot style="background-color: #95a1a1;color: aliceblue;">
                                            <tr>
                                                <th></th>
                                                <th class="text-right">Grand Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Identifikasi Jumlah</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-md-2 text-right">
                                    <label for="tipe">Filter :</label>
                                </div>
                                <div class="col-md-4">
                                    <select name="tipe" class="form-control" id="tipe">
                                        <option value="1">Semua</option>
                                        <option value="2">20%</option>
                                    </select>
                                </div>
                                <div class="col col-md-6 text-right">
                                    <button class="btn btn-primary btn-md m-b-20" id="btnCariJumlah"><i class="fas fa-search"></i> Cari</button>
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <!-- <div class="col col-md-12 text-right">
                                    <button class="btn btn-primary btn-sm m-b-20" id="btnEksport"><i class="fas fa-file-excel"></i> Eksport Excel</button>
                                </div> -->
                                <div class="m-b-25"></div>
                                <div class="col-md-12 form-group">
                                    <table class="table text-nowrap text-center table-responsive table-data3" width="100%" id="tabelIdentifikasi">
                                        <thead style="background-color: cadetblue;color: aliceblue;">
                                            <tr>
                                                <th width="1%" class="text-center">No</th>
                                                <th class="text-left">Direktorat</th>
                                                <th width="5%">Grade 1</th>
                                                <th width="5%">Grade 2</th>
                                                <th width="5%">Grade 3</th>
                                                <th width="5%">Grade 4</th>
                                                <th width="5%">Grade 5</th>
                                                <th width="5%">Grade 6</th>
                                                <th width="5%">Grade 7</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot style="background-color: #95a1a1;color: aliceblue;">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>
            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Data Eksisting</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                
                                <div class="m-b-25"></div>
                                <div class="col-md-12 form-group">
                                    <table class="table text-nowrap table-responsive table-data3" width="100%" id="tabelEksisting">
                                        <thead style="background-color: cadetblue;color: aliceblue;">
                                            <tr>
                                                <th width="1%" class="text-center">No</th>
                                                <th>Direktorat</th>
                                                <th width="5%">Grade 1</th>
                                                <th width="5%">Grade 2</th>
                                                <th width="5%">Grade 3</th>
                                                <th width="5%">Grade 4</th>
                                                <th width="5%">Grade 5</th>
                                                <th width="5%">Grade 6</th>
                                                <th width="5%">Grade 7</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot style="background-color: cadetblue;color: aliceblue;"></tfoot>
                                    </table>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div> -->
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="data_talent" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table  table-borderless table-data3" id="tabelDetailNilai">
                    <thead>
                        <tr style="background:#2860a8">
                            <th width="5%">No</th>
                            <th>Nik</th>
                            <th>Nama</th>
                            <th width="20%">Hasil</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/rekap_talent_view.js"></script>