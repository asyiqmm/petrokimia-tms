<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Rekap_talent extends CI_Controller
{
        public function __construct()
        {
                parent::__construct();
                if (!$this->ion_auth->logged_in()) {
                        redirect('auth/login', 'refresh');
                }
                $this->load->library('session');
                $this->load->model('rekap_talent_model');
        }
        private function _render($view, $data = array())
        {
                $data['title'] = "Rekap Talent | PT Petrokimia Gresik";
                $this->load->view('header', $data);
                $this->load->view($view, $data);
                $this->load->view('footer');
        }

        public function index()
        {
                $uri = &load_class('URI', 'core');
                if (in_array(31, $_SESSION['session_menu'])) {
                        $data['parent_active'] = 4;
                        $data['child_active'] = 31;
                        $this->_render('rekap_talent_view', $data);
                } else {
                        redirect(base_url() . 'auth/login');
                }
        }

        public function get_grade()
        {
                $grade = $this->rekap_talent_model->get_grade();
                echo json_encode($grade);
        }
        public function get_data_hitung_talent()
        {
                $grade = $this->input->post('grade');
                $tipe = $this->input->post('tipe');
                $query = $this->rekap_talent_model->get_data_talent($grade);
                if (!$grade) {
                        if ($tipe != 2) {
                                $data = array();
                                foreach ($query as $item) {
                                        $data[]         = array(
                                                "nm_direktorat" => $item->nm_direktorat,
                                                "grade1"         => $item->grade1,
                                                "grade2"         => $item->grade2,
                                                "grade3"         => $item->grade3,
                                                "grade4"         => $item->grade4,
                                                "grade5"         => $item->grade5,
                                                "grade6"         => $item->grade6,
                                                "grade7"         => $item->grade7,

                                        );
                                }
                        } else {
                                $data = array();
                                foreach ($query as $item) {
                                        $data[]         = array(
                                                "nm_direktorat" => $item->nm_direktorat,
                                                "grade1"         => intVal($item->grade1 * 0.2),
                                                "grade2"         => intVal($item->grade2 * 0.2),
                                                "grade3"         => intVal($item->grade3 * 0.2),
                                                "grade4"         => intVal($item->grade4 * 0.2),
                                                "grade5"         => intVal($item->grade5 * 0.2),
                                                "grade6"         => intVal($item->grade6 * 0.2),
                                                "grade7"         => intVal($item->grade7 * 0.2),

                                        );
                                }
                        }
                        $obj = array("data" => $data);
                }else{
                        $obj = array("data" => $query);
                }
                echo json_encode($obj);
        }
}
