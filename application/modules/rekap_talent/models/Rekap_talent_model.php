<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rekap_talent_model extends CI_Model
{
	public function get_grade()
	{
		$query = $this->db
			->order_by('id_grad_cat')
			->get('m_grade_cat');
		$output = "";
		foreach ($query->result() as $rek) {
			$output .= '<option value="' . $rek->id_grad_cat . '">' . $rek->nm_grade_cat . '</option>';
		}
		return $output;
	}

	public function get_data_talent($grade)
	{
		if ($grade) {
			$select = "SELECT sum(case when kat_talent3 = 'TALENT' then 1 else 0 end) TALENT, 
			sum(case when kat_talent3 = 'NON TALENT' then 1 else 0 end) NON_TALENT,";
			$filter = "where a.id_cat = '" . $grade . "'";
		} else {
			$select = "SELECT sum(case when id_cat = 1 then 1 else 0 end) grade1,
			sum(case when id_cat = 2 then 1 else 0 end) grade2,
			sum(case when id_cat = 3 then 1 else 0 end) grade3,
			sum(case when id_cat = 4 then 1 else 0 end) grade4,
			sum(case when id_cat = 5 then 1 else 0 end) grade5,
			sum(case when id_cat = 6 then 1 else 0 end) grade6,
			sum(case when id_cat = 7 then 1 else 0 end) grade7,";
			$filter = "";
		}
		$sql = "" . $select . "
		count(kat_talent3) grand_total, bb.kd_direktorat,bb.nm_direktorat from (
			SELECT 
			  *,
			  case
						when kat_potensi2 = 'HIGH'and kat_pak = 'HIGH' then '3'
						when kat_potensi2 = 'MEDIUM'and kat_pak = 'HIGH' then '2'
						when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then '1'
						when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then '6'
						when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then '5'
						when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then '4'
						when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then '9'
						when kat_potensi2 = 'MEDIUM' and kat_pak = 'LOW' then '8'
						when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then '7'
					end as hasil,
					case when kat_asesmen = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
						when kat_asesmen = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
						when kat_asesmen = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
						when kat_asesmen = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
						when kat_asesmen = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
						when kat_asesmen = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
						when kat_asesmen = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
						when kat_asesmen = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
					end as kat_hasil1,
					case when kat_potensi = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
						when kat_potensi = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
						when kat_potensi = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
						when kat_potensi = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
						when kat_potensi = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
						when kat_potensi = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
						when kat_potensi = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
						when kat_potensi = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
					end as kat_hasil2,
					case when kat_potensi2 = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
						when kat_potensi2 = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
						when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
						when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
						when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
						when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
						when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
						when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
					end as kat_hasil3,
				  CASE WHEN kat_asesmen = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_asesmen = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
				  WHEN kat_asesmen = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				  WHEN kat_asesmen = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
			  ELSE 'NON TALENT' 
			  end AS kat_talent1,
			  CASE WHEN kat_potensi = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' 
				  WHEN kat_potensi = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
				  WHEN kat_potensi = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				  WHEN kat_potensi = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				  ELSE 'NON TALENT' 
				  end AS kat_talent2, 
			  CASE WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' 
				  WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
				  WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				  WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				  ELSE 'NON TALENT' 
				  end AS kat_talent3
			FROM 
			  (
				SELECT 
				  a.nik, 
				  a.nik_tetap,
				  a.nama, 
				  b.grade, 
				  c.id_grad, 
				  To_char(b.tgl_pensiun, 'dd/mm/yyyy') tgl_pensiun, 
				  c.id_cat,
				  i.unit_name,
				  h.kd_direktorat, 
				  h.nm_direktorat, 
				  Round(Avg(d.nilai_asesmen), 3) asesmen, 
				  (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat), 
				  CASE 
					  WHEN Round(Avg(d.nilai_asesmen), 5) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
					WHEN Round(Avg(d.nilai_asesmen), 5) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
					WHEN Round(       Avg(d.nilai_asesmen), 5) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
					ELSE 'LOW' 
				  end AS kat_asesmen, 
				  f.nilai_uji_kelayakan,
				  j.nilai_uji_komite,
				  Round(Avg(d.nilai_asesmen) * 0.6, 2) satu, 
				  Round(Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150, 2) dua, 
				  Round(Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite, 2) tiga, 
				  Round(((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)),3) potensi1,
				  CASE 
					  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
					WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
					WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
					ELSE 'LOW' 
				  end AS kat_potensi, 
				  Round(((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)),3) potensi2,
				  CASE 
					  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
					WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite) ) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
					WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
					ELSE 'LOW' 
				  end AS kat_potensi2, 
				  e.nilai_sebelum, 
				  e.nilai_sekarang, 
				  f.nilai_uji_kelayakan, 
				  Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) AS avg_pak, 
				  (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1),
				  CASE WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 3) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 3) THEN 'HIGH' 
					  WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 2) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 2) THEN 'MEDIUM' 
					  WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 1) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1) THEN 'LOW' 
					  ELSE 'LOW' 
				  end AS kat_pak 
				FROM 
				  m_users_karyawan a 
				  JOIN d_list_karyawan_talenta b ON b.nik = a.nik 
				  JOIN m_grades c ON c.nm_grade = b.grade 
				  LEFT JOIN t_nilai_potensi_kkj d ON d.nik = b.nik AND d.id_kkj IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) AND Date_part('year', d.created_at) = Date_part('year', CURRENT_DATE) 
				  LEFT JOIN t_nilai_kinerja_pak e ON e.nik = b.nik AND Date_part('year', e.created_date) = Date_part('year', CURRENT_DATE) 
				  left JOIN t_uji_kelayakan f ON f.nik = b.nik AND Date_part('year', f.created_date) = Date_part('year', CURRENT_DATE) 
				  JOIN m_direktorat h ON h.kd_direktorat = b.kd_direktorat
				  left join m_unit i on i.unit_id = a.unit_id 
				  left join t_uji_komite j on j.nik = b.nik AND Date_part('year', j.created_date) = Date_part('year', CURRENT_DATE)
				WHERE 
				  Date_part('year', b.created_date) = Date_part('year', CURRENT_DATE) 
				GROUP BY 
				  a.nik, 
				  a.nik_tetap,
				  i.unit_name,
				  a.nama, 
				  b.grade, 
				  c.id_grad, 
				  f.nilai_uji_kelayakan, 
				  b.tgl_pensiun, 
				  c.id_cat, 
				  e.nilai_sebelum, 
				  h.nm_direktorat, 
				  h.kd_direktorat, 
				  e.nilai_sekarang,
				  j.nilai_uji_komite
			  ) a " . $filter . "
			ORDER BY 
			  a.nama
			) zz
			right join m_direktorat bb on bb.kd_direktorat = zz.kd_direktorat
			group by bb.kd_direktorat,bb.nm_direktorat";
			
		return $this->db->query($sql)->result();
	}

}
