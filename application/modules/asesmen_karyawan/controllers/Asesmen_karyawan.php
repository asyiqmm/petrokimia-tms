<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

class Asesmen_karyawan extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('asesmen_karyawan_model');
		$this->load->model('job_fit_karyawan/job_fit_karyawan_model');
		$this->load->library('session');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Rekap Hasil Asesmen Karyawan | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		if (in_array(19, $_SESSION['session_menu'])) {
			$data['grades_cat'] = $this->job_fit_karyawan_model->get_grade();
			$data['parent_active'] = 19;
			$data['child_active'] = 19;
			$this->_render('asesmen_karyawan_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_rekomendasi()
	{
		$grade = $this->input->post('grade');
		if ($grade) {
			$query = $this->asesmen_karyawan_model->get_rekomendasi($grade);
			echo json_encode($query);
		}
	}

	public function get_listAsesmen()
	{
		$id_unit 			= $_SESSION['session_user'][0]->id_unit;
		$nik 				= $_SESSION['session_user'][0]->nik;

		$query = $this->asesmen_karyawan_model->get_listKaryawan($id_unit, $nik);
		$datas 			= array();
		// foreach ($query['data1'] as $key => $value) {
		// 	$array1[] 	= array(
		// 		"nik" 			=> $value->nik,
		// 		"saat_ini" 		=> $value->saat_ini
		// 	);
		// }
		foreach ($query['data2'] as $key => $value) {
			if ($value->job_fit >= $query['kriteria'][2]->nilai_angka) {
				if ($value->nilai_ki >= $value->nilai_standar) {
					$rekomendasi = 'Disarankan';
				} else {
					$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
				}
			} elseif ($value->job_fit >= $query['kriteria'][0]->nilai_angka && $value->job_fit <= $query['kriteria'][1]->nilai_angka) {
				if (($value->nilai_standar - $value->nilai_ki) <= 1) {
					$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
				} else {
					$rekomendasi = 'Tidak Disarankan
							';
				}
			} elseif ($value->job_fit < $query['kriteria'][0]->nilai_angka) {
				$rekomendasi = 'Tidak Disarankan';
			}
			$pdf = '<button class="btn btn-primary" onclick="modalPDF(\'' . $value->file_path . '\')"><i class="far fa-file-pdf"></i> pdf</button>';
			$datas[] 	= array(
				"no" 		=> "",
				"nik" 			=> $value->nik_karyawan,
				"nama" 			=> $value->nama,
				"total" 		=> ($value->job_fit != NULL ? $value->job_fit."%" : '-'),
				"nilai_asesmen"	=> $value->nilai_ki,
				"nilai_standar"	=> $value->nilai_standar,
				"grade" 		=> $value->grade,
				"jabatan" 		=> $value->gradename,
				"rekomendasi" 	=> $rekomendasi,
				"pdf" 			=> ($value->file_path != NULL ? $pdf : '-'),
			);
		}
		// $keys = array_flip(array_column($array1, 'nik'));
		// $newArr = array_map(function ($item) use ($keys, $array1) {
		// 	$item['saat_ini'] = $array1[$keys[$item['nik']]]['saat_ini'];
		// 	return $item;
		// }, $array2);
		// foreach ($newArr as $index => $item) {
		// 	// var_dump($itemsaat_ini);die();
		// 	if ($item['saat_ini'] >= $query['kriteria'][2]->nilai_angka) {
		// 		if ($item['nilai_asesmen'] >= $item['nilai_standar']) {
		// 			$rekomendasi = 'Disarankan';
		// 		} else {
		// 			$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
		// 		}
		// 	} elseif ($item['saat_ini'] >= $query['kriteria'][0]->nilai_angka && $item['saat_ini'] <= $query['kriteria'][1]->nilai_angka) {
		// 		if (($item['nilai_standar'] - $item['nilai_asesmen']) <= 1) {
		// 			$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
		// 		} else {
		// 			$rekomendasi = 'Tidak Disarankan
		// 			';
		// 		}
		// 	} elseif ($item['saat_ini'] < $query['kriteria'][0]->nilai_angka) {
		// 		$rekomendasi = 'Tidak Disarankan';
		// 	}
		// 	$pdf = '<button class="btn btn-primary" onclick="modalPDF(\'' . $item['pdf'] . '\')"><i class="far fa-file-pdf"></i> pdf</button>';
		// 	$datas[] 	= array(
		// 		"no" 		=> "",
		// 		"nik" 		=> $item['nik'],
		// 		"nama" 		=> $item['nama'],
		// 		"grade" 	=> $item['grade'],
		// 		"jabatan"	=> $item['jabatan'],
		// 		"total" 	=> ($item['saat_ini'] != NULL ? $item['saat_ini'] . "%" : '-'),
		// 		"rekomendasi" => ($item['saat_ini'] != NULL ? $rekomendasi : '-'),
		// 		"pdf" 		=> ($item['pdf'] != NULL ? $pdf : '-'),
		// 	);
		// }
		$obj = array("data" => $datas);
		echo json_encode($obj);
	}

	public function get_rek($nik, $grade, $job)
	{

		$query = $this->asesmen_karyawan_model->get_grafikAsesmen($nik, $grade, $job);

		$data = array();
		foreach ($query as $key => $value) {
			if ($query[$key]->nilai == 0) {
				$data[] = array(
					"perhitungan" => 0,
					"nik" => $query[$key]->nik
				);
			} else {
				$data[] = array(
					"perhitungan" => $query[$key]->nilai_asesmen / $query[$key]->nilai,
					"nik" => $query[$key]->nik
				);
			}
		}
		$total = 0;
		foreach ($data as $key => $value) {
			$total = $data[$key]['perhitungan'] + $total;
		}
		$hasil = intval(($total / count($data)) * 100);
		return $hasil;
	}
}
