<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Asesmen_karyawan_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade')
			->get('m_grades');
		return $query->result();
	}
	public function get_rekomendasi($grade)
	{
		$query = $this->db->query("SELECT a.id_rek_grade,b.id_grad,(select nm_grade from m_grades where id_grad = a.id_rek_grade limit 1),a.keterangan from d_rekomendasi_grade a
		join m_grades b on b.id_grad = a.id_grade
		where b.id_grad = '$grade'");
		$output = "<option value=''>--Pilih Jenis Rekomendasi--</option>";
		foreach ($query->result() as $rek) {
			$output .= '<option value="' . $rek->id_rek_grade . '">' . $rek->keterangan . ' (' . $rek->nm_grade . ')</option>';
		}
		return $output;
	}

	public function get_listKaryawan($unit_id,$nik)
	{
		$tahun = date('Y');
		$th = substr($tahun, -2);
		$baseSql1 = "SELECT
						B.NIK,
						A.SAAT_INI
					FROM
						(
						SELECT
							AA.NIK,
							ROUND(100 *(SUM(BB.NILAI_ASESMEN / NULLIF(FF.NILAI, 0)) / COUNT(BB.NILAI_ASESMEN)), 0) AS SAAT_INI
						FROM
							D_LIST_KARYAWAN_TALENTA AA
						JOIN T_NILAI_POTENSI_KKJ BB ON
							BB.NIK = AA.NIK
							AND DATE_PART('YEAR', BB.CREATED_AT) = '$tahun'
						JOIN M_KKJ CC ON
							CC.ID_KKJ = BB.ID_KKJ
							AND CC.TAHUN = '$th'
						JOIN M_GRADES DD ON
							DD.NM_GRADE = AA.GRADE
						JOIN M_JABATAN EE ON
							EE.JABATAN = AA.JABATAN
						JOIN STANDAR_KKJ FF ON
							FF.ID_KKJ = BB.ID_KKJ
							AND FF.KATEGORI = EE.KATEGORI
							AND FF.ID_GRADE = DD.ID_CAT
							AND FF.ID_FITJOB_FAM = AA.JOB_FAMILY
							AND DATE_PART('YEAR', FF.CREATED_AT) = '$tahun'
							AND FF.ACTIVE = TRUE
						WHERE
							AA.TAHUN = '$tahun'
							AND AA.KD_DEPARTEMEN = '$unit_id'
						GROUP BY
							AA.NIK) A
					RIGHT JOIN D_LIST_KARYAWAN_TALENTA B ON
						B.NIK = A.NIK
					WHERE
						B.KD_DEPARTEMEN = '$unit_id'
						AND DATE_PART('YEAR', B.CREATED_DATE) = '$tahun'";

		$baseSql2 = "SELECT
						AAA.*,BB.grade,CC.nama, CC.gradename, CC.nik nik_karyawan, DD.file_path
					FROM
						(
						SELECT
							ROUND(100 *(SUM(A.NILAI_ASESMEN / NULLIF(F.NILAI, 0)) / COUNT(A.NILAI_ASESMEN)) , 0) JOB_FIT,
							B.NIK,
							G.NILAI_ASESMEN NILAI_KI,
							H.NILAI NILAI_STANDAR
						FROM
							T_NILAI_POTENSI_KKJ A
						JOIN M_USERS_KARYAWAN B ON
							B.NIK = A.NIK
						JOIN D_LIST_KARYAWAN_TALENTA C ON
							C.NIK = B.NIK
							AND C.TAHUN = '$tahun'
						JOIN M_GRADES E ON
							E.NM_GRADE = C.GRADE
						JOIN STANDAR_KKJ F ON
							F.ID_FITJOB_FAM = C.JOB_FAMILY
							AND F.ID_GRADE = E.ID_PROMOSI
							AND F.ID_KKJ = A.ID_KKJ
							AND F.ACTIVE = TRUE
							AND F.KATEGORI = 1
							AND DATE_PART('YEAR', F.CREATED_AT) = '$tahun'
						JOIN T_NILAI_POTENSI_KKJ G ON
							G.NIK = A.NIK
							AND G.ID_KKJ = 3
							AND DATE_PART('YEAR', G.CREATED_AT) = '$tahun'
						JOIN STANDAR_KKJ H ON
							H.ID_KKJ = 3
							AND H.ACTIVE = TRUE
							AND H.ID_FITJOB_FAM = C.JOB_FAMILY
							AND H.ID_GRADE = E.ID_PROMOSI
							AND H.KATEGORI = 1
							AND DATE_PART('YEAR', H.CREATED_AT) = '$tahun'
							--JOIN M_DEPARTEMENS I ON I.UNIT_ID = B.UNIT_ID
							WHERE DATE_PART('YEAR', A.CREATED_AT) = '$tahun'
							--	AND E.ID_CAT = 2
							AND B.UNIT_ID = '$unit_id'
						GROUP BY
							B.NIK,
							B.NAMA,
							G.NILAI_ASESMEN,
							H.NILAI
						ORDER BY
							B.NIK ) AAA
					RIGHT JOIN D_LIST_KARYAWAN_TALENTA BB ON
						BB.NIK = AAA.NIK
					right join m_users_karyawan CC on 
						CC.nik = BB.nik
					left join t_pdf_laporan_sdm DD on DD.nik = CC.nik
						AND DATE_PART('YEAR', DD.created_at) = '$tahun'
					WHERE
						CC.unit_id = '$unit_id'
						AND DATE_PART('YEAR', BB.CREATED_DATE) = '$tahun'
						and BB.nik not in ('$nik')";
			
		// $data['data1'] = $this->db->query($baseSql1)->result();
		// var_dump($baseSql2);die();
		$data['data2'] = $this->db->query($baseSql2)->result();
		$data['kriteria'] = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		return $data;
	}
	public function karyawan_by_grade($grade)
	{
		$query = $this->db->select('b.nama,b.nik_tetap')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->join('m_grade_cat as c', 'c.id_grad_cat = a.id_cat')
			->where('c.id_grad_cat', $grade)
			->order_by('b.nama', 'ASC')
			->get();
		return $query->result();
	}

	public function karyawan_by_sub_Grade($sub_grade)
	{
		$query = $this->db->select('b.nama,b.nik')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->where('a.id_grad', $sub_grade)
			->order_by('b.nama', 'ASC')
			->get();
		return $query->result();
	}
}
