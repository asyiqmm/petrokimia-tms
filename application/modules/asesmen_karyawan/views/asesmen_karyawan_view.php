<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <?php foreach ($_SESSION['session_user'] as $user) { ?>
        <input type="hidden" value="<?php echo $user->nik ?>" id="nik">
        <input type="hidden" value="<?php echo $user->id_unit ?>" id="id_dept">
    <?php } ?>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <h3 class="title-2 m-b-40">Rekap Hasil Asesmen Karyawan</h3>
                            <table class="table table-data3 table-responsive text-nowrap" width="100%" id="tabelAsesmen">
                                <thead class="table-borderless text-center" style="background-color: cadetblue;color: aliceblue;">
                                    <tr>
                                        <td width="1%">NO</td>
                                        <td width="10%">NIK</td>
                                        <td width="10%">NAMA</td>
                                        <td width="10%">GRADE</td>
                                        <td width="10%">JABATAN</td>
                                        <td width="10%">NEXT LEVEL SCORE</td>
                                        <td width="10%">REKOMENDASI</td>
                                        <td width="10%">REPORT ASESMEN</td>
                                    </tr>
                                </thead>
                                <tbody class="table-bordered">
                                </tbody>
                            </table>
                            <!-- </div> -->
                            <p class="text-danger">Catatan:
                                <br>

                            </p>
                            <div class="p-r-5">
                                <ul class=" text-danger">
                                    <li>Next Level Score merupakan skor rekomendasi karyawan untuk level jabatan selanjutnya di job family saat ini.</li>
                                    <li>Laporan Rekomendasi Karyawan merupakan report yang didapatkan saat karyawan tersebut berada di unit kerja/job family pada saat melakukan asesmen.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2020 PT Petrokimia Gresik. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-body {
        /* 100% = dialog height, 120px = header + footer */
        max-height: calc(100% - 120px);
        overflow-y: scroll;
    }
</style>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="nilaiPotensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Hasil Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="canvas_father">
                    <input type="hidden" value="<?php echo base_url() ?>" id="baseUrl">
                    <object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/asesmen_karyawan_view.js"></script>