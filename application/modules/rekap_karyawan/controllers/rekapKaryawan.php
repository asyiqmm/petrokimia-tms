<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class rekapKaryawan extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Rekap Karyawan | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		//load library
		// var_dump($this->session->all_userdata());die();
		$uri = &load_class('URI', 'core');
		redirect(base_url() . 'rekapKaryawan/data');
		// redirect(base_url().'index.php/anmaag_view/data',$data);
	}

	public function data()
	{

		if (in_array(5, $_SESSION['session_menu'])) {
			$data['parent_active'] = 5;
			$data['child_active'] = 5;
			$this->_render('rekapKaryawan_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}
}
