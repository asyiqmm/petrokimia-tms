<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
        <h2 class="title-1 m-b-25">Rekap Karyawan </h2>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <div class="row">
                                <div class=" col-md-8">
                                    <div class="row form-group">
                                        <div class="col col-md-2">
                                            <label for="select" class=" form-control-label">Grade</label>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <select name="select" id="select" class="form-control">
                                                <option value="0">Please select</option>
                                                <option value="1">Option #1</option>
                                                <option value="2">Option #2</option>
                                                <option value="3">Option #3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-2">
                                            <label for="select" class=" form-control-label">Nama</label>
                                        </div>
                                        <div class="col-12 col-md-8">
                                            <select name="select" id="select" class="form-control">
                                                <option value="0">Please select</option>
                                                <option value="1">Option #1</option>
                                                <option value="2">Option #2</option>
                                                <option value="3">Option #3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-2">
                                            <label for="select" class=" form-control-label">Rekomendasi</label>
                                        </div>
                                        <div class="col-12 col-md-8">
                                            <select name="select" id="select" class="form-control">
                                                <option value="0">Please select</option>
                                                <option value="1">Option #1</option>
                                                <option value="2">Option #2</option>
                                                <option value="3">Option #3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-2">
                                            <label for="select" class=" form-control-label">Job Family</label>
                                        </div>
                                        <div class="col-12 col-md-8">
                                            <select name="select" id="select" class="form-control">
                                                <option value="0">Please select</option>
                                                <option value="1">Option #1</option>
                                                <option value="2">Option #2</option>
                                                <option value="3">Option #3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col col-md-12">
                                            <label for="select"  class=" form-control-label">Departemen saat ini</label>
                                        </div>
                                        <div class="col-12 col-md-12">
                                            <input type="text" readonly class="form-control" placeholder="Departemen">

                                        </div>
                                    </div>
                                    <div class=".col-md-6 border text-center">
                                        <div class="col col-md-12">
                                            <label for="select" class=" form-control-label">Job Fit</label>
                                            <h3 class="text-center title-2">90%</h3>
                                            <p class="text-success">Disarankan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <h3 class="title-2 m-b-40">Fit Job Family</h3>
                            <canvas id="barChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/overviewDashboard_view.js"></script>