<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Overview extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->lang->load('auth');
		$this->load->library('session');
		$this->load->model('overview_model');
		$this->load->model('job_fit_karyawan/job_fit_karyawan_model');
		// if (!$this->ion_auth->is_admin()) {
		// 	redirect('auth/login', 'refresh');
		// }
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Overview | PT Petrokimia Gresik";

		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');


		if (in_array(1, $_SESSION['session_menu'])) {
			$data['parent_active'] = 1;
			$data['child_active'] = 1;

			if ($_SESSION['session_role'] == 2) {
				$data['departemens'] = $this->overview_model->get_departemen();
				$this->_render('overviewBangpers_view', $data);
			} else {
				$this->_render('overviewKaryawan_view', $data);
			}
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_grafikAsesmen()
	{
		$n = $this->input->post('nik');
		$g = $this->input->post('grade');
		$j = $this->input->post('job_family');
		$jabatan = $this->input->post('jabatan');

		$query = $this->overview_model->get_grafikAsesmen($n, $g, $j, $jabatan);
		echo json_encode($query);
	}

	public function get_keikutsertaan()
	{
		$tahun = $this->input->get('tahun');
		$query = $this->overview_model->get_keikutsertaan($tahun);
		echo json_encode($query);
	}
	public function get_keikutsertaan_asesmen()
	{
		$tahun = $this->input->get('tahun');
		$query = $this->overview_model->get_keikutsertaan_asesmen($tahun);
		echo json_encode($query);
	}


	public function grafik_grade()
	{
		$tahun = $this->input->get('tahun');
		$g1 = $this->overview_model->get_grade1(5, $tahun);
		$cek = $this->cek_kriteria($g1);
		$data = array();
		$data[]         = array(
			"d"			=> $cek['d'],
			"ds"		=> $cek['dp'],
			"td"		=> $cek['td'],
			"nm_grade"	=> "DOMESTIC S",
		);
		$g2 = $this->overview_model->get_grade1(6, $tahun);
		$cek = $this->cek_kriteria($g2);
		$data[]         = array(
			"d"		=> $cek['d'],
			"ds"	=> $cek['dp'],
			"td"	=> $cek['td'],
			"nm_grade"	=> "GLOBAL S",
		);
		$query = $this->overview_model->get_grafikGrade($tahun);
		foreach ($query as $index => $item) {
			$data[]         = array(
				"d"		=> $item->d,
				"ds"	=> $item->ds,
				"td"	=> $item->td,
				"nm_grade"	=> $item->nm_grade,
			);
		}
		echo json_encode($data);
	}

	public function cek_kriteria($datas)
	{
		// var_dump($datas);die();
		$data = array();
		$d = 0;
		$dp = 0;
		$td = 0;

		foreach ($datas as $key => $value) {
			if ($value->satu > 0) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "TIDAK DISARANKAN",
					"cek"	=> 1
				);
				$td++;
			} elseif ($value->dua >= 6) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "TIDAK DISARANKAN",
					"cek"	=> 2
				);
				$td++;
			} elseif ($value->lima <= 6) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "TIDAK DISARANKAN",
					"cek"	=> 3
				);
				$td++;
			} elseif ($value->dua <= 2) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN",
					"cek"	=> 4
				);
				$d++;
			} elseif ($value->dua <= 5) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN DENGAN PERTIMBANGAN",
					"cek"	=> 5
				);
				$dp++;
			} elseif ($value->lima >= 7 && 9 <= $value->lima) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN DENGAN PERTIMBANGAN",
					"cek"	=> 6
				);
				$dp++;
			} elseif ($value->lima >= 10 && 12 <= $value->lima) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN",
					"cek"	=> 8
				);
				$d++;
			} else {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "Tidak terdefinisi",
					"cek"	=> 9
				);
			}
		}
		$hasil = array(
			"td" => $td,
			"dp" => $dp,
			"d" => $d,
		);
		return $hasil;
	}
	public function grafik_gradeIni()
	{
		$tahun = $this->input->get('tahun');
		$query = $this->overview_model->get_grafikGradeIni($tahun);
		echo json_encode($query);
	}

	public function grafik_jobFamily()
	{
		$query = $this->overview_model->get_grafikJobFamily();
		echo json_encode($query);
	}

	public function grafik_jobFamily_promosi()
	{
		$opsi = $this->input->get('opsi');

		$query = $this->overview_model->get_grafikJobFamilyPromosi($opsi);
		echo json_encode($query);
	}

	public function grafik_departemen()
	{
		$departemen = $this->input->get('departemen');

		$query = $this->overview_model->get_grafikDepartemen($departemen);
		echo json_encode($query);
	}
	public function grafik_departemen_promosi()
	{
		$departemen = $this->input->get('departemen');
		$opsi = $this->input->get('opsi');
		$query = $this->overview_model->get_grafikDepartemen_promosi($departemen, $opsi);
		echo json_encode($query);
	}
}
