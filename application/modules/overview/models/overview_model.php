<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Overview_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}


	public function get_grafikAsesmen($nik, $grade, $job_family,$jabatan)
	{
		$kriterias = $this->db->query("SELECT * FROM m_jabatan WHERE jabatan = '$jabatan'")->result();
		$kategori  = $kriterias[0]->kategori;
		
		$tahun = date('Y');
		$qq = "SELECT distinct a.id_kkj,g.nilai_asesmen,b.kkj,a.nilai
		from standar_kkj a
		join m_kkj b on a.id_kkj = b.id_kkj and date_part('year', b.created_at) = '2020'
		join m_fitjob_fam c on a.id_fitjob_fam = c.id
		join m_grade_cat d on d.id_grad_cat = a.id_grade
		join m_grades e on e.id_cat = d.id_grad_cat
		join d_list_karyawan_talenta f on f.grade = e.nm_grade
		left join t_nilai_potensi_kkj g on g.nik = '$nik' and g.id_kkj = a.id_kkj 
		and date_part('year', g.created_at) = '$tahun'
		where d.id_grad_cat = '$grade' and c.id = '$job_family' and a.kategori = '$kategori' and date_part('year', a.created_at) = '$tahun'
		group by a.id_kkj,a.nilai,g.nilai_asesmen,b.kkj,a.nilai 
		order by id_kkj asc";
				// var_dump($qq);die();
		return $this->db->query($qq)->result();
	}

	public function get_keikutsertaan($tahun)
	{
		$query = "
		WITH ikut AS (
			SELECT count(x) as ikut
			from (select a.nik
			from m_users_karyawan a 
			join d_list_karyawan_talenta c on c.nik = a.nik and date_part('year', c.created_date) = '$tahun'
			where 
				EXISTS(
					SELECT nik 
					FROM t_nilai_potensi_kkj zzz 
					where zzz.nik = a.nik and date_part('year', zzz.created_at) = '$tahun'  
					group by nik)
				and EXISTS(SELECT nik FROM t_nilai_kinerja_pak yyy where yyy.nik = a.nik and date_part('year', yyy.created_date) = '$tahun' group by nik)
			) x
			),
			tidak_ikut as (
			select count(t) as tidak_ikut
			from 
			(select a.nik
			from m_users_karyawan a 
			join d_list_karyawan_talenta c on c.nik = a.nik and date_part('year', c.created_date) = '$tahun'
			where 
				not EXISTS(
					SELECT nik 
					FROM t_nilai_potensi_kkj zzz 
					where zzz.nik = a.nik and date_part('year', zzz.created_at) = '$tahun'  
					group by nik)
			or
				not EXISTS(
					SELECT nik 
					FROM t_nilai_kinerja_pak yyy 
					where yyy.nik = a.nik 
					and date_part('year', yyy.created_date) = '$tahun' group by nik)
			) t
			)
		SELECT (SELECT tidak_ikut FROM tidak_ikut) as tidak_ikut,(SELECT ikut FROM ikut)
		";

		return $this->db->query($query)->result();
	}

	public function get_keikutsertaan_asesmen($tahun)
	{
		$query = $this->db->query("
		WITH ikut AS (
			select count(x) as ikut
			from (select a.nik
			from m_users_karyawan a 
			join d_list_karyawan_talenta c on c.nik = a.nik and date_part('year', c.created_date) = '$tahun'
			where 
				EXISTS(
					SELECT nik 
					FROM t_nilai_potensi_kkj zzz 
					where zzz.nik = a.nik and date_part('year', zzz.created_at) = '$tahun'  
					group by nik)
			) x
			),
			tidak_ikut as (
			select count(t) as tidak_ikut
			from 
			(select a.nik
			from m_users_karyawan a 
			join d_list_karyawan_talenta c on c.nik = a.nik and date_part('year', c.created_date) = '$tahun'
			where 
				not EXISTS(
					SELECT nik 
					FROM t_nilai_potensi_kkj tnpk 
					WHERE tnpk.nik = a.nik and date_part('year', tnpk.created_at) = '$tahun'
					group by nik)
			) t
			)
		SELECT (SELECT tidak_ikut FROM tidak_ikut) as tidak_ikut,(SELECT ikut FROM ikut)
		")->result();
		return $query;
	}

	public function get_grafikGrade($tahun)
	{
		$kriteria = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		$th = substr($tahun, -2);
		$promosi = "SELECT count(total) total,
		sum(case when total < " . $kriteria[0]->nilai_angka . " then 1 else 0 end) td,
		sum(case when total >= " . $kriteria[0]->nilai_angka . " and total <= " . $kriteria[1]->nilai_angka . " then 1 else 0 end) ds,
		sum(case when total >= " . $kriteria[2]->nilai_angka . " then 1 else 0 end) d,
		z.id_grad,z.nm_grade
						from (
						  select a.nik, b.nama,e.id_grad,e.nm_grade,
						  sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
						  round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total,b.grade_personal
						  from t_nilai_potensi_kkj a
							join m_users_karyawan b on b.nik = a.nik
							join d_list_karyawan_talenta h on h.nik = a.nik
							join d_unit_job_family c on b.unit_id = c.unit_id
							join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
							join m_grades e on e.nm_grade = h.grade
							join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
							join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_promosi
						  where date_part('year', a.created_at) = date_part('year', CURRENT_DATE)
						  group by a.nik,b.nama,b.grade_personal, e.id_grad,e.nm_grade
						  order by b.nama
						) t
						right join m_grades z on z.id_grad = t.id_grad
						where z.id_grad not in (1,2)
				group by z.id_grad,z.nm_grade order by z.id_grad asc";

		$query = "SELECT count(total) total,
				sum(case when total < " . $kriteria[0]->nilai_angka . " then 1 else 0 end) td,
				sum(case when total >= " . $kriteria[0]->nilai_angka . " and total <= " . $kriteria[1]->nilai_angka . " then 1 else 0 end) ds,
				sum(case when total >= " . $kriteria[2]->nilai_angka . " then 1 else 0 end) d,
				z.id_grad,z.nm_grade
								from (
									select a.nik, b.nama,e.id_grad,e.nm_grade,
									sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
									round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total,b.grade_personal
									from t_nilai_potensi_kkj a
									  join m_users_karyawan b on b.nik = a.nik
									  join d_list_karyawan_talenta h on h.nik = a.nik
									  join d_unit_job_family c on b.unit_id = c.unit_id
									  join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
									  join m_grades e on e.nm_grade = h.grade
									  join standar_kkj g on g.id_kkj = a.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_promosi
									  right join m_kkj f on f.id_kkj = a.id_kkj 
									where date_part('year', a.created_at) = '$tahun' 
										and date_part('year', h.created_date) = '$tahun'
										and f.tahun = '$th'
										and date_part('year', g.created_at) = '$tahun'
									group by a.nik,b.nama,b.grade_personal, e.id_grad,e.nm_grade
								) t
								right join m_grades z on z.id_grad = t.id_grad
								where z.id_grad not in (1,2)
						group by z.id_grad,z.nm_grade order by z.id_grad asc";
		return  $this->db->query($query)->result();
	}

	public function get_grafikGradeIni($tahun)
	{
		$kriteria = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		$th = substr($tahun, -2);
		$data = "SELECT count(total) total,
		sum(case when total < " . $kriteria[0]->nilai_angka . " then 1 else 0 end) td,
		sum(case when total >= " . $kriteria[0]->nilai_angka . " and total <= " . $kriteria[1]->nilai_angka . " then 1 else 0 end) ds,
		sum(case when total >= " . $kriteria[2]->nilai_angka . " then 1 else 0 end) d,
			z.id_grad,z.nm_grade
			from (
				select a.nik, b.nama,e.id_grad,e.nm_grade,
									sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
									round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total,b.grade_personal
									from t_nilai_potensi_kkj a
									  join m_users_karyawan b on b.nik = a.nik
									  join d_list_karyawan_talenta h on h.nik = a.nik
									  join d_unit_job_family c on b.unit_id = c.unit_id
									  join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
									  join m_grades e on e.nm_grade = h.grade
									  join standar_kkj g on g.id_kkj = a.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_cat
									  right join m_kkj f on f.id_kkj = a.id_kkj 
									where date_part('year', a.created_at) = '$tahun' 
										and date_part('year', h.created_date) = '$tahun'
										and f.tahun = '$th'
										and date_part('year', g.created_at) = '$tahun'
									group by a.nik,b.nama,b.grade_personal, e.id_grad,e.nm_grade
			  ) t
			  right join m_grades z on z.id_grad = t.id_grad
		  group by z.id_grad,z.nm_grade order by z.id_grad asc";
		return $this->db->query($data)->result();
	}

	public function get_grafikJobFamily()
	{
		$kriteria = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();

		$query = "SELECT count(total) total,
		sum(case when total < " . $kriteria[0]->nilai_angka . " then 1 else 0 end) td,
		sum(case when total >= " . $kriteria[0]->nilai_angka . " and total <= " . $kriteria[1]->nilai_angka . " then 1 else 0 end) ds,
		sum(case when total >= " . $kriteria[2]->nilai_angka . " then 1 else 0 end) d,
		z.id,z.nm_fitjob_fam
		from (
			select a.nik_tetap, b.nama,d.id,
			sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
			round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total
						  from t_nilai_potensi_kkj a
							join m_users_karyawan b on b.nik = a.nik
							join d_list_karyawan_talenta h on h.nik = a.nik
							join d_unit_job_family c on b.unit_id = c.unit_id
							join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
							join m_grades e on e.nm_grade = h.grade
							join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
							join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_cat
						  where date_part('year', a.created_at) = date_part('year', CURRENT_DATE)
						  group by a.nik_tetap,b.nama,d.id
						  order by b.nama
			) t
			right join m_fitjob_fam z on z.id = t.id
			group by z.id,z.nm_fitjob_fam order by z.nm_fitjob_fam asc";
		//   var_dump($query);die();

		$data = $this->db->query($query)->result();
		return $data;
	}



	public function get_grafikJobFamilyPromosi($opsi)
	{
		if ($opsi == 1) {
			$kriteria = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
			$query = "SELECT count(total) total,
		sum(case when total < " . $kriteria[0]->nilai_angka . " then 1 else 0 end) td,
		sum(case when total >= " . $kriteria[0]->nilai_angka . " and total <= " . $kriteria[1]->nilai_angka . " then 1 else 0 end) ds,
		sum(case when total >= " . $kriteria[2]->nilai_angka . " then 1 else 0 end) d,
		z.id,z.nm_fitjob_fam
		from (
			select a.nik_tetap, b.nama,d.id,
			sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
			round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total
						  from t_nilai_potensi_kkj a
							join m_users_karyawan b on b.nik = a.nik
							join d_list_karyawan_talenta h on h.nik = a.nik
							join d_unit_job_family c on b.unit_id = c.unit_id
							join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
							join m_grades e on e.nm_grade = h.grade
							join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
							join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_promosi
						  where date_part('year', a.created_at) = date_part('year', CURRENT_DATE)
						  group by a.nik_tetap,b.nama,d.id
						  order by b.nama
			) t
			right join m_fitjob_fam z on z.id = t.id
			group by z.id,z.nm_fitjob_fam order by z.nm_fitjob_fam asc";

			$data = $this->db->query($query)->result();
		} elseif ($opsi == 2) {
			$data = $this->jobFamilyG1(5);
		} elseif ($opsi == 3) {
			$data = $this->jobFamilyG1(6);
		}

		return $data;
	}

	public function jobFamilyG1($kategori)
	{
		$query = "SELECT 
		sum(case when a.tiga_lima between 10 and 12 and a.dua <= 2 and a.satu = 0 then 1 else 0 end) d,
		sum(case when a.tiga_lima between 7 and 9 and a.dua <= 5 and a.satu = 0 then 1 else 0 end) ds,
		sum(case when a.tiga_lima <= 6 and a.dua >= 6 and a.satu >= 0 then 1 else 0 end) td,	
		z.id,z.nm_fitjob_fam
		from 
			(SELECT round(avg(c.nilai_asesmen),2) total, b.nik,a.nama,e.id_job_family,
			sum(case when c.nilai_asesmen between 3 and 5 then 1 else 0 end) tiga_lima,
			sum(case when c.nilai_asesmen = 2 then 1 else 0 end) dua,
			sum(case when c.nilai_asesmen = 1 then 1 else 0 end) satu
			from m_users_karyawan a
			join d_list_karyawan_talenta b on a.nik = b.nik
			join t_nilai_potensi_g_d c on c.nik = b.nik
			join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g
			join d_unit_job_family e on e.unit_id = a.unit_id
			where d.kat_kompetensi = '$kategori' and date_part('year', c.created_at) = date_part('year', CURRENT_DATE) 
			group by b.nik,a.nama,e.id_job_family) a
			right join m_fitjob_fam z on z.id = a.id_job_family
			group by z.id,z.nm_fitjob_fam";
		return $this->db->query($query)->result();
	}

	public function get_departemen()
	{
		$query = $this->db->where('active', true)
			->order_by('unit_name', 'asc')
			->get('m_unit')->result();
		return $query;
	}

	public function get_grafikDepartemen($d)
	{
		$ids = implode("','", $d);
		$query = "SELECT count(total) total,
			sum(case when total < 65 then 1 else 0 end) td,
			sum(case when total >= 65 and total <= 79 then 1 else 0 end) ds,
			sum(case when total >= 80 then 1 else 0 end) d,
				z.unit_id,z.unit_name
					from (
					select a.nik_tetap, b.nama,c.unit_id,
					sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
					round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total
					from t_nilai_potensi_kkj a
						join m_users_karyawan b on b.nik_tetap = a.nik_tetap
						join d_unit_job_family c on b.unit_id = c.unit_id
						join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
						join m_grades e on e.nm_grade = b.grade_personal
						join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
						join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_cat
					where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) 
					group by a.nik_tetap,b.nama,c.unit_id
					order by b.nama
					) t
					right join m_unit z on z.unit_id = t.unit_id
			where z.unit_id in ('" . $ids . "')
			group by z.unit_id,z.unit_name";

		return $this->db->query($query)->result();
	}
	public function get_grafikDepartemen_promosi($d, $opsi)
	{
		$ids = implode("','", $d);
		if ($opsi == 1) {
			$query = "SELECT count(total) total,
			sum(case when total < 65 then 1 else 0 end) td,
			sum(case when total >= 65 and total <= 79 then 1 else 0 end) ds,
			sum(case when total >= 80 then 1 else 0 end) d,
				z.unit_id,z.unit_name
					from (
					select a.nik_tetap, b.nama,c.unit_id,
					sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
					round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total
					from t_nilai_potensi_kkj a
						join m_users_karyawan b on b.nik_tetap = a.nik_tetap
						join d_unit_job_family c on b.unit_id = c.unit_id
						join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
						join m_grades e on e.nm_grade = b.grade_personal
						join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
						join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_promosi
					where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) 
					group by a.nik_tetap,b.nama,c.unit_id
					order by b.nama
					) t
					right join m_unit z on z.unit_id = t.unit_id
			where z.unit_id in ('" . $ids . "')
			group by z.unit_id,z.unit_name";
		} elseif ($opsi == 2) {
			$query = $this->departemenG1(5, $ids);
		} else {
			$query = $this->departemenG1(6, $ids);
		}
		return $this->db->query($query)->result();
	}

	public function departemenG1($kat, $ids)
	{
		$query = "SELECT 
		sum(case when a.tiga_lima between 10 and 12 and a.dua <= 2 and a.satu = 0 then 1 else 0 end) d,
		sum(case when a.tiga_lima between 7 and 9 and a.dua <= 5 and a.satu = 0 then 1 else 0 end) ds,
		sum(case when a.tiga_lima <= 6 and a.dua >= 6 and a.satu >= 0 then 1 else 0 end) td,	
		z.unit_id,z.unit_name
		from 
			(SELECT round(avg(c.nilai_asesmen),2) total, b.nik,a.nama,a.unit_id,
			sum(case when c.nilai_asesmen between 3 and 5 then 1 else 0 end) tiga_lima,
			sum(case when c.nilai_asesmen = 2 then 1 else 0 end) dua,
			sum(case when c.nilai_asesmen = 1 then 1 else 0 end) satu
			from m_users_karyawan a
			join d_list_karyawan_talenta b on a.nik = b.nik
			join t_nilai_potensi_g_d c on c.nik = b.nik
			join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g
			where d.kat_kompetensi = '$kat' and date_part('year', c.created_at) = date_part('year', CURRENT_DATE) 
			group by b.nik,a.nama,a.unit_id) a
			right join m_unit z on z.unit_id = a.unit_id
			where z.unit_id in ('" . $ids . "')
			group by z.unit_id,z.unit_name";

		return $query;
	}


	public function get_grade1($kategori, $tahun)
	{
		$query = "SELECT
		round(avg(c.nilai_asesmen),2) total, 
		b.nik,a.nama,
		sum(case when c.nilai_asesmen between 3 and 5 then 1 else 0 end) lima,
		sum(case when c.nilai_asesmen = 2 then 1 else 0 end) dua,
		sum(case when c.nilai_asesmen = 1 then 1 else 0 end) satu
		from m_users_karyawan a
		join d_list_karyawan_talenta b on a.nik = b.nik
		join t_nilai_potensi_g_d c on c.nik = b.nik
		join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g and d.kat_kompetensi = '$kategori'
		where date_part('year', c.created_at) = '$tahun' and date_part('year', b.created_date) = '$tahun'
		group by b.nik,a.nama
		order by nama";

		return $this->db->query($query)->result();
	}
}
