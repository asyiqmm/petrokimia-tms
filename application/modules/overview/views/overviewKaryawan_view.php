<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="au-card m-b-30">
                                <div class="au-card-inner">
                                
                                    <?php foreach ($_SESSION['session_user'] as $user) { ?>
                                        <input type="hidden" value="<?php echo $user->id_cat ?>" id="grade">
                                        <input type="hidden" value="<?php echo $user->nik ?>" id="nik">
                                        <input type="hidden" value="<?php echo $user->kd_jabatan ?>" id="jabatan">
                                        <input type="hidden" value="<?php echo $user->id_job_fit ?>" id="id_job_fit">
                                        <h3 class="title-2 m-b-40">Profil Karyawan</h3>
                                        <div class="row">
                                            <div class="col-md-4 font-weight-bold">
                                                <label>Nama</label>
                                            </div>
                                            <div class="col-md-8 text-primary">
                                                <p><?php echo ucwords($user->nama) ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 font-weight-bold">
                                                <label>NIK</label>
                                            </div>
                                            <div class="col-md-8 text-primary">
                                                <p><?php echo $user->nik ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 font-weight-bold">
                                                <label>Grade</label>
                                            </div>
                                            <div class="col-md-8 text-primary">
                                                <p><?php echo $user->grade ?></p>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 font-weight-bold">
                                                <label>Jabatan</label>
                                            </div>
                                            <div class="col-md-8 text-primary">
                                                <p><?php echo $user->jabatan ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 font-weight-bold">
                                                <label>Departemen</label>
                                            </div>
                                            <div class="col-md-8 text-primary">
                                                <p> <?php echo $user->nama_unit ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="au-card m-b-30">
                                <div class="au-card-inner" id="canvas_father">
                                    <h3 class="title-2 m-b-40">Grafik Hasil Penilaian</h3>
                                    <canvas id="grafikPenilaian"></canvas>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2020 PT Petrokimia Gresik. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/overview_karyawan_view.js"></script>