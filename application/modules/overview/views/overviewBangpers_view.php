<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row m-b-30">
                <div class="col-md-4">
                    <input type="text" placeholder="Tahun" class="form-control" autocomplete="off" id="txt_tahun">
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary" id="btnCari">Cari</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <div id="master_gambar1" class="bg-white">
                                <h3 class="title-2 tm-b-5">Grafik Keikutsertaan Peserta Talent Program</h3>
                                <div class="row no-gutters">
                                    <div class="col-xl-12">
                                        <div class="percent-chart" id="keikutsertaan_father">
                                            <canvas id="grafik_keikutsertaan"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" title="Download grafik" onclick="gambar1()"><i class="fas fa-download"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <div id="master_gambar1" class="bg-white">
                                <h3 class="title-2 tm-b-5">Grafik Keikutsertaan Peserta Asesmen</h3>
                                <div class="row no-gutters">
                                    <div class="col-xl-12">
                                        <div class="percent-chart" id="keikutsertaan_asesmen_father">
                                            <canvas id="grafik_keikutsertaan_asesmen"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" title="Download grafik" onclick="gambar8()"><i class="fas fa-download"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner bg-white" id="canvas_fatherGradeSaatIni">
                            <h3 class="title-2 m-b-40">Grafik Rekomendasi Berdasarkan Grade Saat Ini</h3>
                            <canvas id="grafikGradeSaatIni" height="280" width="420"></canvas>
                        </div>
                        <button class="btn btn-primary" title="Download grafik" onclick="gambar2()"><i class="fas fa-download"></i></button>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner bg-white" id="canvas_fatherGrade">
                            <h3 class="title-2 m-b-40">Grafik Rekomendasi Berdasarkan Grade Saat Promosi</h3>
                            <canvas id="grafikGrade" height="280" width="420"></canvas>
                        </div>
                        <button class="btn btn-primary" title="Download grafik" onclick="gambar3()"><i class="fas fa-download"></i></button>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner bg-white" id="canvas_fatherGrade3">
                            <h3 class="title-2 m-b-40">Grafik Rekomendasi Departemen Saat Ini</h3>
                            <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border" style="width: -webkit-fill-available;">
                                <select class="js-select2 departemen" name="departemen[]" multiple="multiple" id="departemen">
                                    <?php foreach ($departemens as $departemen) {
                                        echo '<option value="' . $departemen->unit_id . '">' . $departemen->unit_name . '</option>';
                                    }
                                    ?>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>

                            <canvas id="grafikDepartemen" height="100" width="420"></canvas>
                        </div>
                        <button class="btn btn-primary" title="Download grafik" onclick="gambar4()"><i class="fas fa-download"></i></button>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner bg-white" id="canvas_fatherDepPromosi">
                            <h3 class="title-2 m-b-40">Grafik Rekomendasi Departemen Saat Promosi</h3>
                            <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border" style="width: -webkit-fill-available;">
                                <select class=" form-control departemen" name="departemenPromosi[]" multiple="multiple" id="departemenPromosi">
                                    <?php foreach ($departemens as $departemen) {
                                        echo '<option value="' . $departemen->unit_id . '">' . $departemen->unit_name . '</option>';
                                    }
                                    ?>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>

                            <select class="m-t-20 form-control col-md-5" name="pilihanDepartemen" id="pilihanDepartemen">
                                <option value="1">Grade 2-7</option>
                                <option value="2">Grade 1 (Domestic Standard)</option>
                                <option value="3">Grade 1 (Global Standard)</option>
                            </select>
                            <canvas id="grafikDepartemenPromosi" height="100" width="420"></canvas>
                        </div>
                        <button class="btn btn-primary" title="Download grafik" onclick="gambar5()"><i class="fas fa-download"></i></button>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner bg-white" id="canvas_fatherGrade2">
                            <h3 class="title-2 m-b-40">Grafik Rekomendasi Job Family Saat Ini</h3>
                            <canvas id="grafikJobFamily" height="300" width="420"></canvas>
                        </div>
                        <button class="btn btn-primary" title="Download grafik" onclick="gambar6()"><i class="fas fa-download"></i></button>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="au-card m-b-30">

                        <div class="au-card-inner bg-white" id="canvas_fatherFamJobPromosi">
                            <h3 class="title-2 m-b-40">Grafik Rekomendasi Job Family Saat Promosi</h3>
                            <select class="form-control col-md-5" name="pilihanJobFam" id="pilihanJobFam">
                                <option value="1">Grade 2-7</option>
                                <option value="2">Grade 1 (Domestic Standard)</option>
                                <option value="3">Grade 1 (Global Standard)</option>
                            </select>
                            <canvas id="grafikJobFamilyPromosi" height="300" width="420"></canvas>
                        </div>
                        <button class="btn btn-primary" title="Download grafik" onclick="gambar7()"><i class="fas fa-download"></i></button>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/overview_bangpers_view.js"></script>