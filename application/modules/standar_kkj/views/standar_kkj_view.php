<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">

            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card m-b-30">
                                <div class="au-card-inner">
                                    <!-- DATA TABLE -->
                                    <h2 class="title-5 m-b-25">Standar KKJ</h2>
                                    <form action="" method="post" id="grade_job" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="kt-portlet__body">
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label for="grade" class=" form-control-label">Tahun</label>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <input type="text" class="form-control" autocomplete="off" id="txt_tahun">
                                                </div>
                                                <div class="col-md-2">
                                                    <button class="btn btn-primary" type="button" id="btnCari">Cari</button>
                                                </div>
                                                <div class="col-md-2">
                                                    <button class="btn btn-warning" type="button" id="btnCopy">Duplikat Standar KKJ</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label for="grade" class=" form-control-label">Grade</label>
                                                </div>
                                                <div class="col-12 col-md-6">

                                                    <select name="grade" id="grade" class="form-control">
                                                        <option value=''>--Pilih Grade--</option>
                                                        <?php foreach ($grades as $grade) {
                                                            echo '<option value="' . $grade->id_grad_cat . '">' . $grade->nm_grade_cat . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label for="kategori" class=" form-control-label">Kategori</label>
                                                </div>
                                                <div class="col-12 col-md-6">

                                                    <select name="kategori" id="kategori" class="form-control">
                                                        <option value='1'>Struktural</option>
                                                        <option value='2'>Fungsional</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label for="job_family" class=" form-control-label">Job Family</label>
                                                </div>
                                                <div class="col-12 col-md-6">

                                                    <select name="job_family" id="job_family" class="form-control">
                                                        <?php echo $job_familys; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="POST" id="insert_kkj" enctype="multipart/form-data">
                                                <div class="row" id="data_checkbox">
                                                </div>
                                            </form>

                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Standar KKJ</strong>
                        </div>

                        <form action="" method="post" id="insert_standar_kkj" enctype="multipart/form-data" class="form-horizontal">

                            <div class="card-body card-block">
                                <div class="row form-group">
                                    <div class="col col-md-12">
                                        <table class="table table-borderless" id="data_checkbox">
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="response"></div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Grafik</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <h3 class="title-2 m-b-40">Hasil Asemen</h3>
                    <canvas id="barChart"></canvas>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/standar_kkj_view.js"></script>