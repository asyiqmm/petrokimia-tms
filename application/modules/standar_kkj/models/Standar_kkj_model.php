<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Standar_kkj_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_dataKKj($tahun)
	{
		$query = $this->db->query("SELECT * from m_kkj where date_part('year', created_at) = '$tahun' order by id_kkj asc");
		return $query->result();
	}
	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade_cat')
			->get('m_grade_cat');

		return $query->result();
	}
	public function get_standarKKj($grade, $job_fam, $tahun, $kategori)
	{
		$query = $this->db->query("SELECT a.id_kkj id, b.nm_kkj, a.nilai
		from standar_kkj a
		join m_kkj b on b.id_kkj = a.id_kkj
		join m_fitjob_fam c on c.id = a.id_fitjob_fam
		join m_grades d on d.id_grad = a.id_grade
		where a.id_grade = '$grade' and a.id_fitjob_fam = '$job_fam' and a.active = true and date_part('year', a.created_at) = '$tahun' and a.kategori = '$kategori'
		order by a.id_kkj asc");
		return $query->result();
	}

	public function insert_standar_kkj($data, $data_ing)
	{

		$this->db->trans_begin();
		$this->db->query("UPDATE standar_kkj a 
							set active = false
							where id_grade = '" . $data[0]['id_grade'] . "' 
							and id_fitjob_fam = '" . $data[0]['id_fitjob_fam'] . "'
							and kategori = '" . $data[0]['kategori'] . "'
							");
		$this->db->query("UPDATE t_ing_standar a 
							set active = false
							where id_grade = '" . $data_ing[0]['id_grade'] . "' 
							and id_fitjob_fam = '" . $data_ing[0]['id_fitjob_fam'] . "'
							and kategori = '" . $data_ing[0]['kategori'] . "'
							");
							// var_dump($this->db->last_query());die();
		$this->db->insert_batch('standar_kkj', $data);
		$this->db->insert_batch('t_ing_standar', $data_ing);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function get_standar_kkj($tahun)
	{
		$data = $this->db->query("select * from standar_kkj where date_part('year', created_at) = '$tahun'")->result();
		return $data;
	}

	public function insert_duplikat($data)
	{
		$this->db->trans_begin();
		$this->db->query("DELETE from standar_kkj where date_part('year', created_at) = date_part('year', current_date)");
		$this->db->insert_batch('standar_kkj', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
}
