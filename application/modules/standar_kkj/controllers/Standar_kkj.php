<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

class Standar_kkj extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('standar_kkj_model');
		$this->load->model('job_fit_karyawan/job_fit_karyawan_model');
		set_time_limit(0);
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Standar KKJ | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(23, $_SESSION['session_menu'])) {
			$data['grades'] = $this->standar_kkj_model->get_grade();
			$data['job_familys'] = $this->job_fit_karyawan_model->get_jobFit();
			$data['parent_active'] = 21;
			$data['child_active'] = 23;
			$this->_render('standar_kkj_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_standar_kkj()
	{
		$grade = $this->input->post('grade');
		$job_family = $this->input->post('job_family');
		$tahun = $this->input->post('tahun');
		$kategori = $this->input->post('kategori');

		$data_kkj = $this->standar_kkj_model->get_dataKKj($tahun);

		$standar_kkj = $this->standar_kkj_model->get_standarKKj($grade, $job_family, $tahun, $kategori);
		$output = null;
		foreach ($data_kkj as $key => $kkj) {
			$kkjID = $kkj->id_kkj;
			$checked = null;
			$enable = null;
			$nilai = null;
			foreach ($standar_kkj as $key => $standar) {
				if ($kkjID == $standar->id) {
					$checked = ' checked="checked"';
					$nilai = $standar->nilai;
					$enable = 1;
					break;
				}
			}
			// $output .= '<tr><td><label for="' . $kkj->kkj . '" class="form-check-label"><input name="idkkj[]" type="checkbox" value="' . $kkj->id_kkj . '" id="' . $kkj->kkj . '" ' . $checked . '/>' . $kkj->nm_kkj . '</label></td></tr>';
			$output .= '<tr><td><label for="' . $kkj->kkj . '" class="form-check-label"><input name="idkkj[]" type="checkbox" value="' . $kkj->id_kkj . '" id="' . $kkj->kkj . '" ' . $checked . '/>(' . $kkj->kkj . ') ' . $kkj->nm_kkj . '</label></td><td><input type="number" name="nilai[]" class="form-control" ' . ($enable == 1 ? "required" : "disabled") . ' id="' . $kkj->id_kkj . '"  value="' . $nilai . '"/></td></tr>';
			// $output .= "<input type='checkbox' name='groups[]' value='" . $kkj->id_kkj . "' $checked> $kkj->nm_kkj";
			# code...
		}
		echo json_encode($output);
	}

	public function insert()
	{
		$data = array(
			'idkkj' => $this->input->post('idkkj'),
			'nilai' => $this->input->post('nilai'),
			'grade' => $this->input->post('grade'),
			'job_family' => $this->input->post('job_family'),
			'kategori' => $this->input->post('kategori'),
		);
		$ing_standar[] = array(
			'nilai_ing' 	=> intval($data['nilai'][2]),
			'id_grade'		=> intval($data['grade']),
			'tahun'			=> intval(date('Y')),
			'id_fitjob_fam' => intval($data['job_family']),
			'active'		=> true,
			'kategori' => intval($data['kategori']),
		);
		foreach ($data['idkkj'] as $i => $value) {
			$file_data[] = array(
				'id_kkj' => $value,
				'id_grade' => $data['grade'],
				'nilai' => $data['nilai'][$i],
				'active' => true,
				'keterangan' => $i + 1,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => null,
				'id_fitjob_fam' => $data['job_family'],
				'kategori' => $data['kategori'],
			);
		}
		
		$query = $this->standar_kkj_model->insert_standar_kkj($file_data,$ing_standar);
		echo json_encode($query);
	}

	public function copy_standar_kkj()
	{
		$tahun = $this->input->post('tahun');

		$data = $this->standar_kkj_model->get_standar_kkj($tahun);

		foreach ($data as $key => $value) {
			$file_data[] = array(
				'id_kkj' => $value->id_kkj,
				'id_grade' => $value->id_grade,
				'nilai' => $value->nilai,
				'active' => $value->active,
				'keterangan' => $value->keterangan,
				'created_at' => date('Y-m-d H:i:s'),
				'id_fitjob_fam' => $value->id_fitjob_fam
			);
		}
		$insert = $this->standar_kkj_model->insert_duplikat($file_data);
		echo json_encode($insert);
	}
}
