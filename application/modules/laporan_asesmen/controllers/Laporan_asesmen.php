<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Laporan_asesmen extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('laporan_asesmen_model');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Laporan Hasil Asesmen | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		if ($_SESSION['session_role'] == 4) {
			if (in_array(17, $_SESSION['session_menu'])) {
				$data['parent_active'] = 16;
				$data['child_active'] = 17;
				$this->_render('laporanAsesmen_view', $data);
			} else {
				redirect(base_url() . 'auth/login');
			}
		}elseif($_SESSION['session_role'] == 3){
			if (in_array(13, $_SESSION['session_menu'])) {
				$data['parent_active'] = 13;
				$data['child_active'] = 13;
				$this->_render('laporanAsesmen_view', $data);
			} else {
				redirect(base_url() . 'auth/login');
			}
		}

	}

	public function getPdf()
	{
		$nik = $this->input->get('nik');
		$query = $this->laporan_asesmen_model->getPdf($nik);
		
		echo json_encode($query);
	}
}
