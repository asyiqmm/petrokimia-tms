<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

class Unit_job_family extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('unit_job_family_model');
		set_time_limit(0);
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Data KKJ | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(29, $_SESSION['session_menu'])) {
			$data['job_family'] = $this->unit_job_family_model->getJobFam();
			$data['parent_active'] = 21;
			$data['child_active'] = 29;
			$this->_render('unit_job_family_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function insert_unit()
	{
		// $query = $this->unit_job_family_model->insert_kkj($id_unit, $nm_unit);

		if (isset($_FILES["file"]["name"])) {
			$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			if ($extension == 'csv') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif ($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
			$worksheet = $spreadsheet->getActiveSheet();
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			// $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

			for ($row = 3; $row <= $highestRow; ++$row) {
				$unit_id = $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
				$job_id = $worksheet->getCellByColumnAndRow(4, $row)->getCalculatedValue();
				if (is_int($job_id)) {
					$data[] = array(
						'unit_id'	=> $unit_id,
						'id_job_family'	=> intval($job_id),
						'active'	=> true,
						'created_at'	=> date('Y-m-d H:i:s'),
					);
				}
			}
			$query = $this->unit_job_family_model->post_unit_jobFamily($data);
			echo json_encode($query);
		} else {
		}
	}

	public function get_unit()
	{
		$query = $this->unit_job_family_model->get_unit_job_family();
		// var_dump($query);die();
		$data 			= array();
		foreach ($query as $index => $item) {
			$btn 		= '<button name="btnproses" onclick="ubah(\'' . $item->unit_id . '\',\'' . $item->unit_name . '\',\'' . $item->nm_fitjob_fam . '\',\'' . $item->id . '\')" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Ubah"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" onclick="hapus(\'' . $item->unit_id . '\')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Hapus"> <i class="fa fa-times"></i> </button>';
			$data[] 	= array(
				"no" 				=> '',
				"id_unit" 			=> $item->unit_id,
				"nm_unit"			=> $item->unit_name,
				"job_family"		=> $item->nm_fitjob_fam,
				"btn" => $btn
			);
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function update()
	{
		$data = array(
			'job' =>  $this->input->post('job_family'),
			'id_unit' =>  $this->input->post('id_unit'),
			'nm_unit' =>  $this->input->post('nm_unit'),
		);
		$query = $this->unit_job_family_model->update($data);
		echo json_encode($query);
	}
	public function post_hapusKkj()
	{
		$id_unit = $this->input->post('id');
		$query = $this->unit_job_family_model->post_hapusKkj($id_unit);
		echo json_encode($query);
	}

	public function ubah_kkj()
	{
		$id_kkj = $this->input->post('id_kkj');
		$kd_kkj = $this->input->post('kd_kkj');
		$nm_kkj = $this->input->post('nm_kkj');
		$query = $this->kkj_model->post_ubahKkj($id_kkj, $kd_kkj, $nm_kkj);
		echo json_encode($query);
		die();
	}

	public function update_unit_kerja()
	{
		$data = array(
			'grant_type' 		=> 'password',
			'username' 		=> 'app_vendor',
			'password' 	=> 'apP@v3nd0r123!!'
		);
		$url = 'https://sso.petrokimia-gresik.net/token';
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded",
				'method'  => 'POST',
				'content' => http_build_query($data),
				"ignore_errors" => true
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, true, $context);
		$datas = json_decode($result, true);

		$url2 = 'https://sso.petrokimia-gresik.net/api/Unit/List';
		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded",
				'method'  => 'GET',
				// 'content' => http_build_query($data),
				"ignore_errors" => true,
				"header" => 'Authorization: Bearer ' . $datas['access_token']
			)
		);
		$context  = stream_context_create($options);
		$result2 = file_get_contents($url2, true, $context);
		$datas2 = json_decode($result2, true);
		foreach ($datas2 as $key => $value) {
			$unit_data[] = array(
				'unit_id' 	=> $datas2[$key]['unitId'],
				'unit_name' => $datas2[$key]['unitName'],
				'active' 	=> $datas2[$key]['isActive'],
				'created_at' => date('Y-m-d H:i:s')
			);
		}
		$data = $this->unit_job_family_model->update_unit($unit_data);
		echo json_encode($data);
		// $status_line = $http_response_header[0];
		// preg_match('{HTTP\/\S*\s(\d{3})}', $status_line, $match);

		// $status = $match[1];
	}
}
