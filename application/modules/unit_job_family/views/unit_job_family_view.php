<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <h2 class="title-5 m-b-25">Master data konversi Unit Kerja ke Job Family</h2>
            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card m-b-30">
                                <div class="au-card-inner">
                                    <!-- DATA TABLE -->
                                    <div class="kt-portlet__body">
                                        <button class="btn btn-info col-md-3 type1" type="button" id="add_paket" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-upload"></i> Pengelompokan Unit Kerja </button>
                                        <button class="btn btn-primary col-md-3 type1" type="button" id="update_unit" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-sync-alt"></i> Update Unit Kerja</button>
                                        <button class="btn btn-danger col-md-3 type2" type="button" id="back_list" data-toggle="kt-tooltip" data-placement="top" style="background: red;color:white"><i class="fa fa-arrow-left"></i> Kembali ke List</button>

                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 type1">
                                            <table class="table" width="100%" id="data_kkj">
                                                <thead style="background-color: cadetblue;color: aliceblue;">
                                                    <tr>
                                                        <td width="5%">No</td>
                                                        <td width="15%">KODE UNIT</td>
                                                        <td width="40%">NAMA UNIT</td>
                                                        <td width="30%">Job Family</td>
                                                        <td width="10%">Aksi</td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col-md-12 type2">
                                            <form method="POST" id="insert_unit" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="file">Pilih File Excel
                                                        </label>
                                                        <div class="form-group ">
                                                            <div class="custom-file">
                                                                <!-- <input type="file"  name="file" id="file" required accept=".xls, .xlsx">| -->
                                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx" />
                                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <button class="btn btn-info col-md-2" type="submit" id="btnsubmit" name="import" ><i class="fa fa-check"></i> simpan</button>
                                                    </div>

                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Ubah KKJ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" id="ubah_job_family" enctype="multipart/form-data">
                <div class="modal-body">

                    <input type="hidden" id="id_kkj" name="id_kkj">
                    <div class="form-group col-md-12">
                        <label>KODE UNIT</label>
                        <input type="text" class="form-control inputan" readonly required placeholder="kode unit" name="id_unit" id="id_unit">
                    </div>
                    <div class="form-group col-md-12">
                        <label> NAMA KKJ</label>
                        <input type="text" class="form-control inputan" readonly required placeholder="nama unit" id="nm_unit" name="nm_unit">
                    </div>
                    <div class="form-group col-md-12">
                        <label> JOB FAMILY</label>
                        <select name="job_family" required id="job_family" class="form-control">
                            <option value=""></option>
                            <?php foreach ($job_family as $job) {
                                echo '<option value="' . $job->id . '">' . $job->nm_fitjob_fam . '</option>';
                            } ?>
                        </select>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>Ubah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/unit_job_family_view.js"></script>