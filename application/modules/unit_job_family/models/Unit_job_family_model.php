<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Unit_job_family_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function getListKaryawan()
	{
		$query = $this->db->query('select a.nik,a.nama,b.unit_id,b.unit_name,c.id_grad id_grade,c.nm_grade,d.nm_jabt jabatan,
		EXISTS(select nik from d_users_manajer z where z.nik = a.nik) as peran
		from m_users_karyawan a
		left join m_unit b on b.unit_id = a.unit_id
		left join m_grades c on c.nm_grade = a.grade_personal
		left join m_jabatans d on d.kd_jabatan = a.grade');
		return $query->result();
	}
	public function getUnit()
	{
		$query = $this->db->order_by('unit_name', 'ASC')->get('m_unit');

		return $query->result();
	}

	public function getGrade()
	{
		$query = $this->db->order_by('id_grad','ASC')->get('m_grades');
		
		return $query->result();
	}
	public function getDirektorat()
	{
		$query = $this->db->order_by('nm_direktorat', 'ASC')->get('m_direktorat');

		return $query->result();
	}
	public function getJobFam()
	{
		$query = $this->db->order_by('id', 'ASC')->get('m_fitjob_fam');

		return $query->result();
	}
	public function post_unit_jobFamily($data)
	{
		$this->db->trans_begin();
		$date = date('Y-m-d H:i:s');
		$this->db->query("DELETE from d_unit_job_family");
		$this->db->insert_batch('d_unit_job_family', $data);


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function post_hapusKkj($id)
	{
		$this->db->trans_begin();
		$this->db->query("DELETE FROM m_unit where unit_id = '$id'");


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function update_unit($data)
	{
		$this->db->trans_begin();
		//hapus semua data yg ada
		$this->db->query("delete from m_unit");

		//menginputkan data yg ada
		$this->db->insert_batch('m_unit', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true
			];
		}
	}

	public function get_unit_job_family()
	{
		$query = $this->db->select('a.unit_id,a.unit_name,c.id,c.nm_fitjob_fam')
			->from('m_unit as a')
			->join('d_unit_job_family as b', 'a.unit_id = b.unit_id', 'left')
			->join('m_fitjob_fam as c', 'c.id = b.id_job_family', 'left')
			->where('a.active', true)
			->order_by('b.id_job_family', 'asc');

		return $query->get()->result();
	}

	public function update($data)
	{
		$this->db->trans_begin();
		$this->db->set('id_job_family', $data['job'])
			->set('updated_at', date('Y-m-d H:i:s'))
			->where('unit_id', $data['id_unit'])
			->update('d_unit_job_family');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
}
