<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <h3 class="title-2 m-b-40">Rekap Job Fit Per Karyawan</h3>
                            <form method="POST" id="btnCari" enctype="multipart/form-data">
                                <div class="row">
                                    <div class=" col-md-8">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="txt_tahun" class=" form-control-label">Tahun</label>
                                            </div>
                                            <div class="col-12 col-md-2">
                                                <input type="text" required placeholder="Tahun" class="form-control" autocomplete="off" id="txt_tahun">

                                            </div>
                                            <div class="col-12 col-md-6 text-right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-search"></i> Cari
                                                </button>
                                            </div>
                                            <div class="col-12 col-md-1 text-right">
                                                <button type="button" id="btnDownload" title="Download Exel" data-toggle="modal" data-target="#exampleModal" class="btn btn-warning">
                                                    <i class="fa fa-download"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row form-group">

                                            <div class="col col-md-3">
                                                <label for="grade" class=" form-control-label">Grade</label>
                                            </div>
                                            <div class="col-12 col-md-3">
                                                <select name="grade" required data-size="1" id="grade" class="form-control">
                                                    <option value="">--Pilih Sub Grade--</option>|
                                                    <!-- <?php foreach ($grades_cat as $cat) {
                                                                echo '<option value="' . $cat->id_grad_cat . '">' . $cat->nm_grade_cat . '</option>';
                                                            } ?> -->
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-2">
                                                <select name="grade" data-live-search="true" data-size="1" id="sub_grade" class="form-control">
                                                    <option value="">--Pilih Sub Grade--</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="nama" class=" form-control-label">Nama</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control" required name="property" id="nama">
                                                </select>
                                                <div class="dropDownSelect2"></div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="select" class="form-control-label">Rekomendasi</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="rekomendasi" required id="rekomendasi" class="form-control">
                                                    <option value=''>--Pilih Jenis Rekomendasi--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="jobFitFamily" class="form-control-label">Job Family</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select name="jobFitFamily" required data-live-search="true" data-size="5" type="text" id="jobFitFamily" class="form-control">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col col-md-12">
                                                <label for="select" class=" form-control-label">Departemen saat ini</label>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <textarea type="search" readonly class="form-control" id="departemen" placeholder="Departemen"></textarea>
                                                <input type="hidden" id="jabatan">
                                            </div>
                                        </div>
                                        <div class=".col-md-6 border text-center">
                                            <div class="col col-md-12 bg-success text-light" id="job_fit">
                                                <label for="select" class="form-control-label text-light">Job Fit</label>
                                                <h3 class="text-center title-2 text-light" id="presentase">0%</h3>

                                                <p id="hasil" class="text-light">&nbsp;</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner" id="canvas_father">
                            <h3 class="title-2 m-b-40">Grafik Kompetensi</h3>
                            <canvas id="grafikPenilaian"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner" id="canvas_father2">
                            <h3 class="title-2 m-b-40">Fit Job Family</h3>
                            <canvas id="grafikJobFamily"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="table-responsive m-b-40">
                        <table class="table table-borderless table-data3 text-center d-none" id="infograde">
                            <thead>
                                <tr>
                                    <th>KRITERIA</th>
                                    <th>REKOMENDASI</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="bg-primary text-light">Presentase Skor Kompetensi ≥ 80% dan Kompetensi Integrity & Ethic ≥ Persyaratan Minimal Kompetensi Intregity & Ethic</td>
                                    <td>Disarankan</td>
                                </tr>
                                <tr>
                                    <td class="bg-warning text-light">Presentase Skor Kompetensi 65% - 79% dan Gap Kompetensi Integrity & Ethic maksimal 1 level dari persyaratan minimal standard Kompetensi Integrity & Ethic</td>
                                    <td>Disarankan Dengan Beberapa Pengembangan</td>
                                </tr>
                                <tr>
                                    <td class="bg-danger text-light">Presentase Skor Kompetensi < 65%</td> <td>Tidak Disarankan</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless table-data3 text-center d-none" id="infograde1">
                            <thead>
                                <tr>
                                    <th>KRITERIA</th>
                                    <th>REKOMENDASI</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="bg-primary text-light">
                                        Nilai Kompetensi > 2.82; <br>
                                        Personal Quality yang mendapat rating 3-5 berjumlah 10-12 Aspek; <br>
                                        Personal Quality yang memiliki rating 2 berjumlah maksimal 2 Aspek; <br>
                                        Tidak Ada Personal Quality yang memiliki rating 1;</td>
                                    <td>Disarankan</td>
                                </tr>
                                <tr>
                                    <td class="bg-warning text-light">
                                        Nilai Kompetensi 2.58 - 2.82; <br>
                                        Personal Quality yang mendapat rating 3-5 berjumlah 7-9 Aspek; <br>
                                        Personal Quality yang memiliki rating 2 berjumlah maksimal 5 Aspek; <br>
                                        Tidak Ada Personal Quality yang memiliki rating 1;</td>
                                    <td>Disarankan Dengan Beberapa Pengembangan</td>
                                </tr>
                                <tr>
                                    <td class="bg-danger text-light">
                                        Nilai Kompetensi < 2.58; <br>
                                            Personal Quality yang mendapat rating 3-5 berjumlah sama dengan atau kurang dari 6 Aspek; <br>
                                            Personal Quality yang memiliki rating 2 berjumlah sama dengan atau lebih dari 6 aspek; <br>
                                            Terdapat Personal Quality yang memiliki rating 1</td>
                                    <td>Tidak Disarankan</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2020 PT Petrokimia Gresik. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pilih Download</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <div class="row"> -->
                <div class="text-center">

                    <div class="form-group text-center">
                        <button class="btn btn-warning" style="white-space: normal;" id="btnPerJob">
                            <i class="fas fa-download">Eksport Rekap per Job Family</i>
                        </button>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn col-md-6 btn-primary" style="white-space: normal;" id="btnPerNilai">
                            <i class="fas fa-download">Eksport Rekap Penilaian Job Fit Rekomendasi</i>
                        </button>
                    </div>
                </div>
                <!-- </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/jobFitKaryawan.js"></script>