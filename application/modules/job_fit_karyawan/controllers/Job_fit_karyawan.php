<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Job_fit_Karyawan extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$this->load->library('session');
		$this->load->model('job_fit_karyawan_model');
		$this->load->model('job_fit_family/family_model');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Job Fit per Karyawan | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		if (in_array(2, $_SESSION['session_menu'])) {
			// $data['grades_cat'] = 
			$data['parent_active'] = 2;
			$data['child_active'] = 2;
			$this->_render('jobFitKaryawan_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_grade()
	{
		$data = $this->job_fit_karyawan_model->get_grade();
		echo json_encode($data);
	}

	public function get_nama()
	{
		$grade = $this->input->get('grade');
		$tahun = $this->input->get('tahun');

		if ($grade) {
			$query = $this->job_fit_karyawan_model->get_sub_grade($grade, $tahun);
			if ($query['status']  == true) {

				echo json_encode($query['data']);
			} else {
				return 'Gagal Memuat Data';
			}
		} else {
			return 'Gagal Memuat Data';
		}
	}


	public function export_perNilai()
	{

		$grade = $this->input->get('grade');
		$tahun = $this->input->get('tahun');
		//load phpspreadsheet class using namespaces
		$date = date('Y', strtotime('-1 years'));
		

		//make a new spreadsheet object
		$spreadsheet = new Spreadsheet();

		$spreadsheet->getProperties()->setCreator('Pengembangan Personal')
			->setLastModifiedBy('Bangpers')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('HAV MATRIX')
			->setKeywords('HAV MATRIX')
			->setCategory('HAV');
		//Retrieve Highest Column (e.g AE)
		$activesheet = 0;

		$styleArrayFirstRow = [
			'font' => [
				'bold' => true,
			]
		];
		$spreadsheet->setActiveSheetIndex($activesheet);
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle($grade);
		$sheet->setCellValue('A1', 'NO')
			->setCellValue('B1', 'NIK')
			->setCellValue('C1', 'NAMA')
			->setCellValue('D1', 'GRADE')
			->setCellValue('E1', 'DEPARTEMEN')
			->setCellValue('F1', 'JOB FIT SAAT INI')
			->setCellValue('G1', 'REKOMENDASI SAAT INI');
			if ($grade != 1) {
				$sheet->setCellValue('H1', 'JOB FIT SAAT PROMOSI')
			->setCellValue('I1', 'REKOMENDASI SAAT PROMOSI');
			}
			

		$kompIndividu = $this->job_fit_karyawan_model->get_kompIndividu($tahun);

		$col = 'G';
		$data_nilai = $this->job_fit_karyawan_model->export_perKI($grade, $tahun);
		
		$row = 2;
		$i = 1;
		$no = 1;
		$kat = '';
		$kat2 = '';
		$kat3 = '';
		foreach ($data_nilai as $key => $value) {
			$sheet->setCellValue('A' . $row, $key + 1);
			$sheet->setCellValue('B' . $row, $value['nik']);
			$sheet->setCellValue('C' . $row, $value['nama']);
			$sheet->setCellValue('D' . $row, $value['grade']);
			$sheet->setCellValue('E' . $row, $value['unit_name']);
			$sheet->setCellValue('F' . $row, $value['job_fit_saat_ini'] . '%');
			$sheet->setCellValue('G' . $row, $value['rek_saat_ini']);
			if ($grade != 1) {
				$sheet->setCellValue('H' . $row, $value['job_fit_saat_promosi'] . '%');
				$sheet->setCellValue('I' . $row, $value['rek_saat_promosi']);
			}else {
			}
			$row++;
			// if ($i >= $kompIndividu->num_rows()) {
			// 	$sheet->setCellValue($col . '3', $value['kkj']);
			// 	$sheet->setCellValue($col . $row, $value['nilai_asesmen']);
			// 	$no++;
			// 	$coll = $col;
			// 	$coll++;
			// 	$sheet->setCellValue($coll . '1', 'JOB FIT SAAT INI');
			// 	$sheet->setCellValue($coll . $row, $value['saat_ini'] . '%');
			// 	$spreadsheet->getActiveSheet()->mergeCells($coll . "1:" . $coll . "3");
			// 	$coll++;
			// 	$sheet->setCellValue($coll . '1', 'REKOMENDASI SAAT INI');
			// 	$sheet->setCellValue($coll . $row, $value['rek_saat_ini']);
			// 	$spreadsheet->getActiveSheet()->mergeCells($coll . "1:" . $coll . "3");
			// 	$coll++;
			// 	$sheet->setCellValue($coll . '1', 'JOB FIT SAAT PROMOSI');
			// 	$sheet->setCellValue($coll . $row, $value['promosi'] . '%');
			// 	$spreadsheet->getActiveSheet()->mergeCells($coll . "1:" . $coll . "3");
			// 	$coll++;
			// 	$sheet->setCellValue($coll . '1', 'REKOMENDASI SAAT PROMOSI');
			// 	$sheet->setCellValue($coll . $row, $value['rekomendasi_promosi']);
			// 	$spreadsheet->getActiveSheet()->mergeCells($coll . "1:" . $coll . "3");

			// 	$col = 'G';
			// 	$i = 1;
			// 	$row++;
			// } else {
			// 	$sheet->setCellValue('B' . $row, $value['nik']);
			// 	$sheet->setCellValue('C' . $row, $value['nama']);
			// 	$sheet->setCellValue('D' . $row, $value['grade']);
			// 	$sheet->setCellValue('E' . $row, $value['jabatan']);
			// 	$sheet->setCellValue('F' . $row, $value['departemen']);
			// 	$sheet->setCellValue($col . '3', $value['kkj']);
			// 	if ($kat3 !== $value['id_kategori']) {

			// 		$kat = $col;
			// 		$sheet->setCellValue($kat . '2', $value['nm_kategori']);
			// 	}
			// 	$kat3 = $value['id_kategori'];
			// 	$sheet->setCellValue($col . $row, $value['nilai_asesmen']);
			// 	$i++;
			// 	$col++;
			// }
		}
		// die();
		$activesheet++;
		$spreadsheet->setActiveSheetIndex(0);

		$datetime = date('dmy');
		$filename = 'Rekap Penilaian Job Fit Rekomendasi Grade ' . $grade . ' ' . $datetime;
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}

	public function cek_kriteria($datas)
	{
		// var_dump($datas);die();
		$data = array();
		$d = 0;
		$dp = 0;
		$td = 0;

		foreach ($datas as $key => $value) {
			if ($value->satu > 0) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "TIDAK DISARANKAN",
					"cek"	=> 1
				);
				$td++;
			} elseif ($value->dua >= 6) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "TIDAK DISARANKAN",
					"cek"	=> 2
				);
				$td++;
			} elseif ($value->lima <= 6) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "TIDAK DISARANKAN",
					"cek"	=> 3
				);
				$td++;
			} elseif ($value->dua <= 2) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN",
					"cek"	=> 4
				);
				$d++;
			} elseif ($value->dua <= 5) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN DENGAN PERTIMBANGAN",
					"cek"	=> 5
				);
				$dp++;
			} elseif ($value->lima >= 7 && 9 <= $value->lima) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN DENGAN PERTIMBANGAN",
					"cek"	=> 6
				);
				$dp++;
			} elseif ($value->lima >= 10 && 12 <= $value->lima) {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "DISARANKAN",
					"cek"	=> 8
				);
				$d++;
			} else {
				$data[] = array(
					"nik"	=> $value->nik,
					"nama"	=> $value->nama,
					"hasil"	=> "Tidak terdefinisi",
					"cek"	=> 9
				);
			}
		}
		$hasil = array(
			"td" => $td,
			"dp" => $dp,
			"d" => $d,
		);
		return $hasil;
	}

	public function export_perJobFamily()
	{

		$grade = $this->input->get('grade');
		$tahun = $this->input->get('tahun');
		$tipe = $this->input->get('tipe');

		//load phpspreadsheet class using namespaces
		$date = date('Y', strtotime('-1 years'));

		//make a new spreadsheet object
		$spreadsheet = new Spreadsheet();

		$spreadsheet->getProperties()->setCreator('Pengembangan Personal')
			->setLastModifiedBy('Bangpers')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('HAV MATRIX')
			->setKeywords('HAV MATRIX')
			->setCategory('HAV');
		//Retrieve Highest Column (e.g AE)
		$activesheet = 0;

		$styleArrayFirstRow = [
			'font' => [
				'bold' => true,
			]
		];
		$spreadsheet->setActiveSheetIndex($activesheet);
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle($grade);

		$sheet->setCellValue('A2', 'NO')
			->setCellValue('B2', 'NIK_SAP')
			->setCellValue('C2', 'NAMA')
			->setCellValue('D2', 'GRADE')
			->setCellValue('E2', 'DEPARTEMEN');

		$job_family = $this->job_fit_karyawan_model->get_jobfamily();

		$col = 'F';
		foreach ($job_family as $key => $value) {
			$query = $this->family_model->get_listKaryawan($grade, null, $grade, $value->id, $tahun, $tipe);


			// 	var_dump($query['data']);
			// die();
			$row = 3;
			foreach ($query['data'] as $key => $value) {
				if ($value->job_fit >= $query['kriteria'][2]->nilai_angka) {
					if ($value->nilai_ki >= $value->nilai_standar == true) {
						$rekomendasi = '007bff';
					} else {
						$rekomendasi = 'ffc107';
					}
				} elseif ($value->job_fit >= $query['kriteria'][0]->nilai_angka && $value->job_fit <= $query['kriteria'][1]->nilai_angka) {
					if (($value->nilai_standar - $value->nilai_ki) <= 1) {
						$rekomendasi = 'ffc107';
					} else {
						$rekomendasi = 'dc3545';
					}
				} elseif ($value->job_fit < $query['kriteria'][0]->nilai_angka) {
					$rekomendasi = 'dc3545';
				}
				$sheet->setCellValue($col . '2', $value->nm_fitjob_fam);
				$sheet->getStyle($col . '2')->getAlignment()->setTextRotation(90);
				$sheet->setCellValue('A' . $row, $key + 1);
				$sheet->setCellValue('B' . $row, $value->nik);
				$sheet->setCellValue('C' . $row, $value->nama);
				$sheet->setCellValue('D' . $row, $value->grade);
				$sheet->setCellValue('E' . $row, $value->unit_name);
				$sheet->setCellValue($col . $row, $value->job_fit);
				$spreadsheet->getActiveSheet()->getStyle($col . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($rekomendasi);
				$row++;
			}
			$col++;
		}
		$spreadsheet->getActiveSheet()->mergeCells("F1:" . $col . "1");
		if ($tipe == 2) {
			$sheet->setCellValue('F1', 'SAAT PROMOSI');
			$jenis = 'Promosi';
		} else {
			$sheet->setCellValue('F1', 'SAAT INI');
			$jenis = 'Saat Ini';
		}
		$col++;

		// $coll = $col;
		// $new_grade = $grade - 1;
		// foreach ($job_family as $key => $value) {
		// 	$row = 3;
		// 	$query = $this->job_fit_karyawan_model->export_perJobFamily($grade, $new_grade, $value->id, $tahun, $tipe);
		// 	foreach ($query as $key => $value) {
		// 		$sheet->setCellValue($col . '2', $value->nm_fitjob_fam);
		// 		$sheet->getStyle($col . '2')->getAlignment()->setTextRotation(90);
		// 		$sheet->setCellValue('A' . $row, $key + 1);
		// 		$sheet->setCellValue('B' . $row, $value->nik);
		// 		$sheet->setCellValue('C' . $row, $value->nama);
		// 		$sheet->setCellValue('D' . $row, $value->nm_grade);
		// 		$sheet->setCellValue('E' . $row, $value->unit_name);
		// 		$sheet->setCellValue($col . $row, $value->total);
		// 		$spreadsheet->getActiveSheet()->getStyle($col . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($value->kategori);
		// 		$row++;
		// 	}
		// 	$col++;
		// }
		// $spreadsheet->getActiveSheet()->mergeCells("" . $coll . "1:" . $col . "1");
		// $sheet->setCellValue($coll . '1', 'saat promosi');

		$activesheet++;
		$spreadsheet->setActiveSheetIndex(0);

		$datetime = date('dmy');
		$filename = 'Rekap Nilai Per Job Family ' . $jenis . ' Grade ' . $grade . ' ' . $datetime;
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
	public function get_karyawan()
	{
		$sub_grade = $this->input->get('sub_grade');
		$tahun = $this->input->get('tahun');
		if ($sub_grade) {
			$query = $this->job_fit_karyawan_model->get_karyawan_by_grade($sub_grade, $tahun);
			if ($query['status']  == true) {
				echo json_encode($query['data']);
			} else {
				return 'Gagal Memuat Data';
			}
		} else {
			return 'Gagal Memuat Data';
		}
	}
	public function get_rekomendasi()
	{
		$nama = $this->input->post('nama');
		$tahun = $this->input->post('tahun');
		if ($nama) {
			$query = array();
			$query['departemen'] = $this->job_fit_karyawan_model->get_departemen($nama, $tahun);
			echo json_encode($query);
		}
	}
	public function get_jobFit()
	{

		$query = $this->job_fit_karyawan_model->get_jobFit();
		echo json_encode($query);
	}

	public function get_grafikAsesmen()
	{

		$n = $this->input->post('nik');
		$g = $this->input->post('grade');
		$j = $this->input->post('job_family');
		$tahun = $this->input->post('tahun');
		$jabatan = $this->input->post('jabatan');
		$tipe = $this->input->post('tipe');
		$query = $this->job_fit_karyawan_model->get_grafikAsesmen($n, $g, $j, $tahun, $jabatan, $tipe);
		echo json_encode($query);
	}

	public function get_grafikJobFamily()
	{
		$n = $this->input->post('nik');
		$g = $this->input->post('grade');
		$t = $this->input->post('tahun');
		$tipe = $this->input->post('tipe');
		$jabatan = $this->input->post('jabatan');

		$query = $this->job_fit_karyawan_model->fit_job_family($n, $g, $t, $tipe, $jabatan);
		echo json_encode($query);
	}
}
