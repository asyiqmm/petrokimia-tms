<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Job_fit_karyawan_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade_cat')
			->get('m_grade_cat');
		$output = "<option value=''>--Pilih Grade--</option>";
		foreach ($query->result() as $nama) {
			$output .= '<option value="' . $nama->id_grad_cat . '">' . $nama->nm_grade_cat . '</option>';
		}
		return $output;
	}



	public function get_karyawan_by_grade($sub_grade, $tahun)
	{

		$this->db->trans_begin();
		$query = $this->db->select('b.nama,b.nik')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->where('a.id_grad', $sub_grade)
			->order_by('b.nama', 'ASC')
			->get();

		$output = "<option value=''>--Pilih Nama Pegawai--</option>";
		foreach ($query->result() as $nama) {
			$output .= '<option value="' . $nama->nik . '">' . $nama->nama . ' (' . $nama->nik . ')</option>';
		}
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'data' => $output
			];
		}
	}

	public function get_sub_grade($grade, $tahun)
	{
		$this->db->trans_begin();
		$sub_grade = $this->db->where('id_cat', $grade)
			->order_by('nm_grade')
			->get('m_grades');

		$data['sub_grade'] = "<option value=''>--Pilih Sub Grade--</option>";
		foreach ($sub_grade->result() as $grade_cat) {
			$data['sub_grade'] .= '<option value="' . $grade_cat->id_grad . '">' . $grade_cat->nm_grade . '</option>';
		}

		$query = $this->db->query("SELECT b.nama,b.nik_tetap,b.nik
		from m_grades a
		join d_list_karyawan_talenta as d on d.grade = a.nm_grade
		join m_users_karyawan as b on b.nik = d.nik
		join m_grade_cat as c on c.id_grad_cat = a.id_cat
		where c.id_grad_cat = '$grade'
		and date_part('year', d.created_date) = '$tahun' 
		");
		// var_dump($query);die();
		$data['karyawan'] = "<option value=''>--Pilih Nama Pegawai--</option>";

		foreach ($query->result() as $nama) {
			$data['karyawan'] .= '<option value="' . $nama->nik . '">' . $nama->nama . ' (' . $nama->nik . ')</option>';
		}


		if ($grade == 1) {
			$rekomendasi = $this->db->select('a.id_rek_grade as id,a.keterangan,a.type')
				->from('d_rekomendasi_grade as a')
				->where('a.id_grade', $grade)
				->where('a.active', true)
				->order_by('a.id', 'ASC')
				->get();
		} else {
			$rekomendasi = $this->db->select('a.id_rek_grade as id,a.keterangan,b.nm_grade_cat as nm_grade,a.type')
				->from('d_rekomendasi_grade as a')
				->join('m_grade_cat as b', 'b.id_grad_cat = a.id_rek_grade')
				->where('a.id_grade', $grade)
				->where('a.active', true)
				->order_by('a.id', 'ASC')
				->get();
		}
		$data['rekomendasi'] = "<option value=''>--Pilih Jenis Rekomendasi--</option>";
		if ($grade == 1) {
			foreach ($rekomendasi->result() as $rek) {
				$data['rekomendasi'] .= '<option value="' . $rek->id . '" tipe=' . $rek->type . '>' . $rek->keterangan . '</option>';
			}
		} else {
			foreach ($rekomendasi->result() as $rek) {
				$data['rekomendasi'] .= '<option value="' . $rek->id . '" tipe=' . $rek->type . '>' . $rek->keterangan . ' (' . $rek->nm_grade . ')</option>';
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'data' => $data
			];
		}
	}

	public function get_rekomendasi($nik)
	{

		$query = $this->db->query("SELECT b.id_grad_cat as id,a.keterangan,b.nm_grade_cat as nama_grade
		from d_rekomendasi_grade a
		join m_grade_cat b on b.id_grad_cat = a.id_rek_grade
		join m_grades c on c.id_grad = a.id_grade
		where c.nm_grade = (select z.grade_personal from m_users_karyawan z where z.nik = '$nik')");


		$output = "<option value=''>--Pilih Jenis Rekomendasi--</option>";
		foreach ($query->result() as $rek) {
			$output .= '<option value="' . $rek->id . '">' . $rek->keterangan . ' (' . $rek->nama_grade . ')</option>';
		}
		return $output;
	}

	public function get_departemen($nik, $tahun)
	{
		$query = $this->db->query("
		select concat(a.unit_name,' (',d.nm_fitjob_fam,')') nm_dept, f.jabatan from m_unit a
		right join m_users_karyawan b on a.unit_id = b.unit_id
		right join d_list_karyawan_talenta f on f.nik = b.nik
		left join m_unit c on c.unit_id = b.unit_id
		left join d_unit_job_family e on e.unit_id = f.kd_departemen
		left join m_fitjob_fam d on d.id = f.job_family
		where b.nik = '$nik' and date_part('year', f.created_date) = '$tahun'
		");
		return $query->result();
	}

	public function get_job_family()
	{
		$query = $this->db->order_by('id', 'ASC')->get('m_fitjob_fam')->result();
		return $query;
	}
	public function get_jobFit()
	{
		$query = $this->db->order_by('id', 'ASC')->get('m_fitjob_fam');
		$output = "<option value=''>--Pilih Job Fit Family--</option>";

		foreach ($query->result() as $jobFit) {
			$output .= '<option value="' . $jobFit->id . '">' . $jobFit->nm_fitjob_fam . '</option>';
		}
		return $output;
	}

	public function get_grafikAsesmen($nik, $grade, $job, $tahun, $jabatan, $tipe)
	{
		$kategoris = $this->db->query("SELECT * FROM m_jabatan WHERE jabatan = '$jabatan'")->result();
		$kategori = $kategoris[0]->kategori;

		$qqq = "";
		if ($grade == 55) {
			$g = 5;
		} elseif ($grade == 66) {
			$g = 6;
		}
		if ($grade == 55 || $grade == 66) {

			$data['grafik'] = $this->db->query("SELECT c.id_kkj_d_g,d.nilai,c.nilai_asesmen,d.kkj_d_g kkj, b.nik,a.nama
			from m_users_karyawan a
			join d_list_karyawan_talenta b on a.nik = b.nik
			join t_nilai_potensi_g_d c on c.nik = b.nik
			join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g
			where d.kat_kompetensi = '$g' and b.nik = '$nik' 
			and date_part('year', c.created_at) = '$tahun'
			and date_part('year', b.created_date) = '$tahun'
			order by c.id_kkj_d_g
		")->result();

			$query['hasil'] = $this->get_nilaiGrafikG1($nik, $grade, $job);
		} else {
			if ($tipe == 2) {
				$qqq = "SELECT distinct a.id_kkj,g.nilai_asesmen,b.kkj,a.nilai
				from standar_kkj a
				join m_kkj b on a.id_kkj = b.id_kkj and date_part('year', b.created_at) = '$tahun'
				join m_fitjob_fam c on a.id_fitjob_fam = c.id
				join m_grade_cat d on d.id_grad_cat = a.id_grade
				join m_grades e on e.id_cat = d.id_grad_cat
				join d_list_karyawan_talenta f on f.grade = e.nm_grade
				left join t_nilai_potensi_kkj g on g.nik = '$nik' and g.id_kkj = a.id_kkj 
				and date_part('year', g.created_at) = '$tahun'
				where d.id_grad_cat = '$grade' and c.id = '$job' and a.kategori = '1' and date_part('year', a.created_at) = '$tahun'
				group by a.id_kkj,a.nilai,g.nilai_asesmen,b.kkj,a.nilai 
				order by id_kkj asc";
			} elseif ($tipe == 1) {
				$qqq = "SELECT distinct a.id_kkj,g.nilai_asesmen,b.kkj,a.nilai
				from standar_kkj a
				join m_kkj b on a.id_kkj = b.id_kkj and date_part('year', b.created_at) = '$tahun'
				join m_fitjob_fam c on a.id_fitjob_fam = c.id
				join m_grade_cat d on d.id_grad_cat = a.id_grade
				join m_grades e on e.id_cat = d.id_grad_cat
				join d_list_karyawan_talenta f on f.grade = e.nm_grade
				left join t_nilai_potensi_kkj g on g.nik = '$nik' and g.id_kkj = a.id_kkj 
				and date_part('year', g.created_at) = '$tahun'
				where d.id_grad_cat = '$grade' and c.id = '$job' and a.kategori = '$kategori' and date_part('year', a.created_at) = '$tahun'
				group by a.id_kkj,a.nilai,g.nilai_asesmen,b.kkj,a.nilai 
				order by id_kkj asc";
			}

			// }
			$data['grafik'] = $this->db->query($qqq)->result();

			$query['hasil'] = $this->get_nilaiGrafik($nik, $grade, $job, $tahun, $tipe);
		}

		if (!empty($query['hasil'])) {
			if ($grade == 55 || $grade == 66) {
				if ($query['hasil'][0]->total > 2.82) {
					$data['hasil_akhir'] = 1;
				} elseif ($query['hasil'][0]->total >= 2.58 && $query['hasil'][0]->total <= 2.82) {
					$data['hasil_akhir'] = 2;
				} elseif ($query['hasil'][0]->total < 2.58) {
					$data['hasil_akhir'] = 3;
				}
				$data['total'] = $query['hasil'][0]->total;
			} else {
				$query['kriteria'] = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
				if ($query['hasil'][0]->total >= $query['kriteria'][2]->nilai_angka) {

					if ($query['hasil'][0]->nilai_ki >= $query['hasil'][0]->nilai_standar) {
						$data['hasil_akhir'] = 1;
					} else {
						$data['hasil_akhir'] = 2;
					}
				} elseif ($query['hasil'][0]->total >= $query['kriteria'][0]->nilai_angka && $query['hasil'][0]->total <= $query['kriteria'][1]->nilai_angka) {
					if (($query['hasil'][0]->nilai_standar - $query['hasil'][0]->nilai_ki) <= 1) {
						$data['hasil_akhir'] = 2;
					} else {
						$data['hasil_akhir'] = 3;
					}
				} elseif ($query['hasil'][0]->total < $query['kriteria'][0]->nilai_angka) {
					$data['hasil_akhir'] = 3;
				}

				$data['total'] = $query['hasil'][0]->total;
			}
		} else {
			$data['hasil_akhir'] = 4;
			$data['total'] = 0;
		}

		return $data;
	}

	public function get_nilaiGrafik($nik, $grade, $job, $tahun, $tipe)
	{
		$kriteria = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		if ($tipe == 2) {
			$kategori = '1';
			$grades = $grade;
		} elseif ($tipe == 1) {
			$kategori = ' i.kategori ';
			$grades = ' i.grade_cat ';
		}
		$qqq = "SELECT
				a.nik,
				b.nama,
				h.job_family,
				i.kategori,
				i.grade_cat,
				e.id_cat,
				GG.nilai_asesmen nilai_ki,
				(
				select
					HH.nilai
				from
					standar_kkj HH
				where
					HH.id_kkj = 3
					and HH.id_fitjob_fam = $job
					and HH.id_grade = $grades
					and HH.kategori = $kategori
					and HH.active = true
					and date_part('YEAR', HH.created_at) = '$tahun') nilai_standar, 
					sum(a.nilai_asesmen / nullif(g.nilai, 0)) as nilai_asesmen,
				round(100 *(sum(a.nilai_asesmen / nullif(g.nilai, 0)) / count(a.nilai_asesmen)) , 0) as total
				from
					t_nilai_potensi_kkj a
				join m_users_karyawan b on
					b.nik = a.nik
					-- join d_unit_job_family c on b.unit_id = c.unit_id
				join d_list_karyawan_talenta h on
					b.nik = h.nik
					and date_part('year', h.created_date) = '$tahun'
				join m_grades e on
					e.nm_grade = h.grade
				join m_kkj f on
					f.id_kkj = a.id_kkj
					and f.active = true
					and date_part('year', f.created_at) = '$tahun'
				join m_jabatan i on
					i.jabatan = h.jabatan
				join standar_kkj g on
					g.id_kkj = f.id_kkj
					and g.id_grade = $grades
					and g.id_fitjob_fam = '$job'
					and g.active = true
					and date_part('year', g.created_at) = '$tahun'
					and g.kategori = $kategori
				join t_nilai_potensi_kkj GG on
					GG.id_kkj = 3
					and GG.nik = A.nik
					and date_part('YEAR', GG.created_at) = '$tahun'
				where
					date_part('year', a.created_at) = '$tahun'
					and date_part('year', g.created_at) = '$tahun'
					and date_part('year', f.created_at) = '$tahun'
					and a.nik = '$nik'
					and date_part('year', a.created_at) = '$tahun'
				group by
					a.nik,
					b.nama,
					h.job_family,
					i.kategori,
					e.id_cat,
					i.grade_cat,
					GG.nilai_asesmen";
		// var_dump($qqq);die();
		$query = $this->db->query($qqq)->result();

		return $query;
	}

	public function get_nilaiGrafikG1($nik, $grade, $job)
	{
		if ($grade == 55) {
			$kat = 5;
		} elseif ($grade == 66) {
			$kat = 6;
		}
		$query = $this->db->query("SELECT round(avg(c.nilai_asesmen),2) total, b.nik,a.nama,
		case 
			when avg(c.nilai_asesmen) > 2.82 then 'Disarankan'
			when avg(c.nilai_asesmen) between 2.58 and 2.82 then 'Disarankan dengan pertimbangan'
			when avg(c.nilai_asesmen) < 2.58 then 'Tidak disarankan'
		end
		from m_users_karyawan a
		join d_list_karyawan_talenta b on a.nik = b.nik
		join t_nilai_potensi_g_d c on c.nik = b.nik
		join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g
		where d.kat_kompetensi = '$kat' and b.nik = '$nik' and date_part('year', c.created_at) = date_part('year', CURRENT_DATE) 
		group by b.nik,a.nama");
		// $data = $this->db->last_query();
		// var_dump($data);die();
		return $query->result();
	}

	public function fit_job_family($nik, $grade, $tahun, $tipe, $jabatan)
	{
		$job_family = $this->db->where('active', true)->order_by('id')->get('m_fitjob_fam')->result();
		$query['kriteria'] = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		foreach ($job_family as $key => $value) {
			$nilai = $this->get_nilaiGrafik($nik, $grade, $value->id, $tahun, $tipe);

			if ($nilai[0]->total >= $query['kriteria'][2]->nilai_angka) {
				if ($nilai[0]->nilai_ki >= $nilai[0]->nilai_standar) {
					$rgb = 'rgba(0, 123, 255,1.0)';
				} else {
					$rgb = 'rgba(255, 193, 7,1.0)';
				}
			} elseif ($nilai[0]->total >= $query['kriteria'][0]->nilai_angka && $nilai[0]->total <= $query['kriteria'][1]->nilai_angka) {
				if (($nilai[0]->nilai_standar - $nilai[0]->nilai_ki) <= 1) {
					$rgb = 'rgba(255, 193, 7,1.0)';
				} else {
					$rgb = 'rgba(220, 53, 69,1.0)';
				}
			} elseif ($nilai[0]->total < $query['kriteria'][0]->nilai_angka) {
				$rgb = 'rgba(220, 53, 69,1.0)';
			}
			$datas[] = array(
				"job_family" => $value->nm_fitjob_fam,
				"nilai_total" => $nilai[0]->total,
				"warna" => $rgb,
			);
		}
		return $datas;
	}

	public function get_grafikKaryawan($nik, $grade, $job)
	{
		$query = $this->db->query("
		select a.nik, b.nilai_asesmen,c.id_kkj,c.kkj,c.nm_kkj,d.nilai
		from m_users_karyawan a
		join t_nilai_potensi_kkj b on a.nik = b.nik
		join m_kkj c on c.id_kkj = b.id_kkj
		join standar_kkj d on d.id_kkj = b.id_kkj
		join m_grades e on e.id_grad = d.id_grade
		join m_fitjob_fam f on f.id = d.id_fitjob_fam
		join m_grade_cat g on g.id_grad_cat = e.id_cat
		where e.id_grad = '$grade' and b.nik = '$nik' and f.id = '$job'
		group by c.id_kkj,c.kkj,d.nilai, b.nilai_asesmen,c.id_kkj,c.nm_kkj,a.nik
		order by c.id_kkj asc
		")->result();
		return $query;
	}
	public function get_kompIndividu($tahun)
	{
		$query = "SELECT * FROM m_kkj WHERE date_part('year', created_at) = '$tahun'";
		return $this->db->query($query);
	}
	public function export_perJobFamily($grade, $rekomendasi, $job_family, $tahun)
	{
		$th = substr($tahun, -2);
		$query2 = "SELECT a.nik, b.nama,e.nm_grade,b.unit_id, i.unit_name,
					case 
						when round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) >= 80 then '007bff'
						when round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) between 65 and 79 then 'ffc107'
						when round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) < 65 then 'dc3545'
					end as kategori,
						sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen, h.nm_fitjob_fam,
						round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total
					from t_nilai_potensi_kkj a
						join m_users_karyawan b on b.nik = a.nik
						join d_list_karyawan_talenta c on c.nik = a.nik
						join m_grades e on e.nm_grade = c.grade
						join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
						join standar_kkj g on g.id_kkj = f.id_kkj and g.id_grade = '$rekomendasi' and g.id_fitjob_fam = '$job_family' and g.active = true
						join m_fitjob_fam h on h.id = g.id_fitjob_fam
						left join m_unit i on i.unit_id = b.unit_id
					where date_part('year', a.created_at) = '$tahun'
						and date_part('year', g.created_at) = '$tahun'
						and f.tahun = '$th'
						and date_part('year', c.created_date) = '$tahun'
						and e.id_cat = '$grade'
					group by a.nik,b.nama,h.nm_fitjob_fam,e.nm_grade,b.unit_id,i.unit_name
					order by a.nik";

		return $this->db->query($query2)->result();
	}

	public function get_jobfamily()
	{
		$query = $this->db->order_by('id')->get('m_fitjob_fam')->result();

		return $query;
	}

	public function export_perNilai()
	{
		$query = "SELECT a.nik, b.nama,e.nm_grade,b.unit_id,
				sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen, h.nm_fitjob_fam,
				round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total
		from t_nilai_potensi_kkj a
			join m_users_karyawan b on b.nik = a.nik
			join d_list_karyawan_talenta c on b.nik = c.nik 
			left join m_grades e on e.nm_grade = c.grade
			join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
			join standar_kkj g on g.id_kkj = f.id_kkj and g.id_grade = '2' and g.id_fitjob_fam = c.job_family and g.active = true
			join m_fitjob_fam h on h.id = g.id_fitjob_fam
		--			join m_unit i on 
		where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) and e.id_cat = 2 
		group by a.nik,b.nama,h.nm_fitjob_fam,e.nm_grade,b.unit_id
		order by a.nik";
		return $this->db->query($query)->result();
	}

	public function export_perKI($grade, $tahun)
	{
		$th = substr($tahun, -2);

		if ($grade == 6 || $grade == 7) {
			$kategori = '2';
		} else {
			$kategori = ' EE.kategori ';
		}

		$query_list_ki = "SELECT a.nik,b.nama,g.unit_name, d.nm_grade,e.id_kkj,e.kkj,c.nilai_asesmen,h.nm_kat_kompetensi 
		from d_list_karyawan_talenta a
		join m_users_karyawan b on b.nik = a.nik
		join t_nilai_potensi_kkj c on c.nik = a.nik and date_part('YEAR', c.created_at) = '$tahun'
		join m_grades d on d.nm_grade = a.grade
		join m_kkj e on e.id_kkj = c.id_kkj and e.tahun = '$th'
		join m_jabatan f on f.jabatan = a.jabatan
		join m_unit g on g.unit_id = a.kd_departemen
		join kat_kompetensi h on h.id_kat_kompetensi = e.fk_kat_kompetensi
		where a.tahun = '$tahun' and d.id_cat = '$grade'
		order by b.nik, e.id_kkj";

		$query_nilai_saat_ini = "SELECT
						AA.NIK,AA.job_family,EE.kategori,
						II.nama,
						JJ.unit_name,
						AA.grade,
						(select
							A.nilai_ing
						from
							T_ING_KKJ A
						where
							A.nik = AA.nik) nilai_ki_saat_ini,
						(select
							B.nilai_ing
						from
							t_ing_standar B
						where
							B.ACTIVE = true
							and B.ID_FITJOB_FAM = AA.job_family
							and B.ID_GRADE = $grade
							and B.KATEGORI = $kategori
							and B.TAHUN = $tahun ) nilai_standar_saat_ini,
						round(100 *(sum(BB.NILAI_ASESMEN / nullif(FF.NILAI, 0)) / count(BB.NILAI_ASESMEN)), 0) as job_fit_saat_ini
					from
						D_LIST_KARYAWAN_TALENTA AA
					join T_NILAI_POTENSI_KKJ BB on
						BB.NIK = AA.NIK
						and date_part('YEAR', BB.CREATED_AT) = '$tahun'
					join M_KKJ CC on
						CC.ID_KKJ = BB.ID_KKJ
						and CC.TAHUN = '$th'
					join M_GRADES DD on
						DD.NM_GRADE = AA.GRADE
					join m_jabatan EE on
						EE.jabatan = AA.jabatan
					join STANDAR_KKJ FF on
						FF.ID_KKJ = BB.ID_KKJ
						and FF.KATEGORI = $kategori
						and FF.ID_GRADE = $grade
						and FF.ID_FITJOB_FAM = AA.job_family
						and date_part('YEAR', FF.CREATED_AT) = '$tahun'
						and FF.ACTIVE = true
					join M_USERS_KARYAWAN II on
						II.NIK = AA.NIK
					left JOIN M_UNIT JJ ON
						JJ.unit_id = AA.kd_departemen
					where
						AA.TAHUN = '$tahun'
						and DD.ID_CAT = '$grade'
					group by
						AA.NIK,
						AA.job_family,
						EE.kategori,
						II.nama,
						JJ.unit_name,
						AA.grade";

		$query_nilai_saat_promosi = "SELECT
						AA.NIK,AA.job_family,EE.kategori,
						DD.id_promosi,
						(select
							A.nilai_ing
						from
							T_ING_KKJ A
						where
							A.nik = AA.nik) nilai_ki_saat_promosi,
						(select
							B.nilai_ing
						from
							t_ing_standar B
						where
							B.ACTIVE = true
							and B.ID_FITJOB_FAM = AA.job_family
							and B.ID_GRADE = DD.id_promosi
							and B.KATEGORI = 1
							and B.TAHUN = $tahun ) nilai_standar_promosi,
						round(100 *(sum(BB.NILAI_ASESMEN / nullif(FF.NILAI, 0)) / count(BB.NILAI_ASESMEN)), 0) as job_fit_saat_promosi
					from
						D_LIST_KARYAWAN_TALENTA AA
					join T_NILAI_POTENSI_KKJ BB on
						BB.NIK = AA.NIK
						and date_part('YEAR', BB.CREATED_AT) = '$tahun'
					join M_KKJ CC on
						CC.ID_KKJ = BB.ID_KKJ
						and CC.TAHUN = '$th'
					join M_GRADES DD on
						DD.NM_GRADE = AA.GRADE
					join m_jabatan EE on
						EE.jabatan = AA.jabatan
					join STANDAR_KKJ FF on
						FF.ID_KKJ = BB.ID_KKJ
						and FF.KATEGORI = 1
						and FF.ID_GRADE = DD.id_promosi
						and FF.ID_FITJOB_FAM = AA.job_family
						and date_part('YEAR', FF.CREATED_AT) = '$tahun'
						and FF.ACTIVE = true
					join M_USERS_KARYAWAN II on
						II.NIK = AA.NIK
					where
						AA.TAHUN = '$tahun'
						and DD.ID_CAT = '$grade'
					group by
						AA.NIK,
						AA.job_family,
						EE.kategori,
						DD.id_promosi";
		$query['kriteria'] = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		// var_dump($query_nilai_saat_promosi);die();
		$list_saat_ini = $this->db->query($query_nilai_saat_ini)->result();
		
		$array_saat_ini = array();
		foreach ($list_saat_ini as $index => $item) {
			if ($item->job_fit_saat_ini >= $query['kriteria'][2]->nilai_angka) {
				if ($item->nilai_ki_saat_ini >= $item->nilai_standar_saat_ini) {
					$rekomendasi = 'Disarankan';
				} else {
					$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
				}
			} elseif ($item->job_fit_saat_ini >= $query['kriteria'][0]->nilai_angka && $item->job_fit_saat_ini <= $query['kriteria'][1]->nilai_angka) {
				if (($item->nilai_standar_saat_ini - $item->nilai_ki_saat_ini) <= 1) {
					$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
				} else {
					$rekomendasi = 'Tidak Disarankan';
				}
			} elseif ($item->job_fit_saat_ini < $query['kriteria'][0]->nilai_angka) {
				$rekomendasi = 'Tidak Disarankan';
			}

			$array_saat_ini[] 	= array(
				"nik" 				=> $item->nik,
				"job_fit_saat_ini"	=> $item->job_fit_saat_ini,
				"rek_saat_ini"		=> $rekomendasi,
				"nama"				=> $item->nama,
				"unit_name"			=> $item->unit_name,
				"grade"				=> $item->grade,
			);
		}
		if ($grade == 1) {
			return $array_saat_ini;
			# code...
		} else {

			$list_promosi = $this->db->query($query_nilai_saat_promosi)->result();
			foreach ($list_promosi as $index => $item) {
				if ($item->job_fit_saat_promosi >= $query['kriteria'][2]->nilai_angka) {
					if ($item->nilai_ki_saat_promosi >= $item->nilai_standar_promosi) {
						$rekomendasi = 'Disarankan';
					} else {
						$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
					}
				} elseif ($item->job_fit_saat_promosi >= $query['kriteria'][0]->nilai_angka && $item->job_fit_saat_promosi <= $query['kriteria'][1]->nilai_angka) {
					if (($item->nilai_standar_promosi - $item->nilai_ki_saat_promosi) <= 1) {
						$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
					} else {
						$rekomendasi = 'Tidak Disarankan';
					}
				} elseif ($item->job_fit_saat_promosi < $query['kriteria'][0]->nilai_angka) {
					$rekomendasi = 'Tidak Disarankan';
				}

				$array_saat_promosi[] 	= array(
					"nik" 					=> $item->nik,
					"job_fit_saat_promosi"	=> $item->job_fit_saat_promosi,
					"rek_saat_promosi"		=> $rekomendasi,
					// "nama"					=> $item->nama,
					// "unit_name"				=> $item->unit_name,
					// "grade"					=> $item->grade,
				);
			}

			$keys = array_flip(array_column($array_saat_promosi, 'nik'));
			$array_nilai_total = array_map(function ($item) use ($keys, $array_saat_promosi) {
				$item['job_fit_saat_promosi'] = $array_saat_promosi[$keys[$item['nik']]]['job_fit_saat_promosi'];
				$item['rek_saat_promosi'] = $array_saat_promosi[$keys[$item['nik']]]['rek_saat_promosi'];
				return $item;
			}, $array_saat_ini);
		}
		// var_dump($array_nilai_total);die();

		// $list_ki = $this->db->query($query_list_ki)->result();
		// foreach ($list_ki as $index => $item) {
		// 	$array_ki[] 	= array(
		// 		"nik" 						=> $item->nik,
		// 		"nama" 						=> $item->nama,
		// 		"unit_name" 				=> $item->unit_name,
		// 		"nm_grade" 					=> $$item->nm_grade,
		// 		"id_kkj" 					=> $$item->id_kkj,
		// 		"kkj" 						=> $$item->kkj,
		// 		"nilai_asesmen" 			=> $$item->nilai_asesmen,
		// 		"nm_kat_kompetensi" 		=> $$item->nm_kat_kompetensi,
		// 	);
		// }



		// $keys2 = array_flip(array_column($array_nilai_total, 'nik'));
		// $array_final = array_map(function ($item) use ($keys2, $array_nilai_total) {
		// 	$item['job_fit_saat_ini'] 		= $array_nilai_total[$keys2[$item['nik']]]['job_fit_saat_ini'];
		// 	$item['rek_saat_ini'] 			= $array_nilai_total[$keys2[$item['nik']]]['rek_saat_ini'];
		// 	$item['job_fit_saat_promosi'] 	= $array_nilai_total[$keys2[$item['nik']]]['job_fit_saat_promosi'];
		// 	$item['rek_saat_promosi']		= $array_nilai_total[$keys2[$item['nik']]]['rek_saat_promosi'];
		// 	return $item;
		// }, $array_ki);
		// var_dump($array_final);die();
		return $array_nilai_total;
	}

	public function get_nilaiJobFit($grade)
	{
		# code...
	}
}
