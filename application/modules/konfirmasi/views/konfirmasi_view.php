<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">

            <h3 class="title-5 m-b-35">Konfirmasi Atasan</h3>


            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Unggah Data Konfirmasi Atasan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">

                                <div class="col-md-4">
                                    <form method="POST" id="import_form" enctype="multipart/form-data">
                                        <label for="file">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx">
                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                        <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                    </form>
                                </div>

                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Konfirmasi Atasan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="table-responsive">
                                        <table class="table display responsive nowrap" width="100%" id="tabelKonfirmasi">
                                            <thead style="background-color: cadetblue;color: aliceblue;">
                                                <tr>
                                                    <td width="1%">
                                                        No
                                                    </td>
                                                    <td width="12.5%">Nik</td>
                                                    <td width="19%">Nama</td>
                                                    <td width="12.5%">Uji Kelayakan</td>
                                                    <td width="12.5%">Status</td>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/js/module/konfirmasi_view.js"></script>