<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="au-card m-b-30">
                                <div class="au-card-inner">
                                    <h3 class="title-2 m-b-40">Pemetaan Tahap III </h3>
                                    <div class="filters m-b-45">
                                        <div class="row form-group">

                                            <div class="col col-md-1">
                                                <label for="grade" class=" form-control-label">Tahun</label>
                                            </div>
                                            <div class="col-12 col-md-2">
                                                <input type="text" required placeholder="Tahun" class="form-control" autocomplete="off" id="txt_tahun">
                                            </div>

                                        </div>
                                        <div class="row form-group">

                                            <div class="col col-md-1">
                                                <label for="grade" class=" form-control-label">Grade</label>
                                            </div>
                                            <div class="col-12 col-md-2">
                                                <select name="grade" required data-size="1" id="grade" class="form-control">
                                                    <option value="">--Pilih Grade Grade--</option>|
                                                    <?php foreach ($grades_cat as $cat) {
                                                        echo '<option value="' . $cat->id_grad_cat . '">' . $cat->nm_grade_cat . '</option>';
                                                    } ?>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-3">
                                                <select name="grade" data-live-search="true" data-size="1" id="sub_grade" class="form-control">
                                                    <option value="">--Pilih Sub Grade--</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-3">
                                                <button class="btn btn-primary" id="btn_cari">cari</button>
                                            </div>

                                        </div>

                                    </div>
                                    <style>
                                        .tableku td,
                                        .tableku th {
                                            padding: .75rem;
                                            vertical-align: top;
                                            border: 5px solid #fff;
                                            border-radius: 12px;
                                        }
                                    </style>
                                    <table class="tableku text-center align-middle" width="90%">
                                        <tbody>
                                            <tr>
                                                <td class="align-middle" id="kinerja" width="5%" rowspan="3"><strong> Kinerja</strong></td>
                                                <td class="pt-5 pb-5" width="5%">High</td>
                                                <td style="width: 1%;border-left:6px solid" rowspan="3"></td>
                                                <td width="20" class="pt-5 pb-5 bg-secondary text-white klik" onclick="box1('HIGH','LOW')" id="high_contri"> HIGH CONTRIBUTION</td>
                                                <td width="20" class="pt-5 pb-5 bg-success text-white klik" id="utility_hi_pro" onclick="box1('HIGH','MEDIUM')">UTILITY HI-PRO</td>
                                                <td width="20" class="pt-5 pb-5 bg-primary text-white klik" id="consistent_star" onclick="box1('HIGH','HIGH')">CONSISTENT STAR</td>
                                            </tr>
                                            <tr>
                                                <td class="pt-5 pb-5">Medium</td>
                                                <td class="pt-5 pb-5 bg-secondary text-white klik" id="contributor" onclick="box1('MEDIUM','LOW')">CONTRIBUTOR</td>
                                                <td class="pt-5 pb-5 bg-success text-white klik" id="experienced_pro" onclick="box1('MEDIUM','MEDIUM')">EXPERIENCED PRO</td>
                                                <td class="pt-5 pb-5 bg-success text-white klik" id="future_start" onclick="box1('MEDIUM','HIGH')">FUTURE STAR</td>
                                            </tr>
                                            <tr>
                                                <td class="pt-5 pb-5">Low</td>
                                                <td class="pt-5 pb-5 text-white bg-danger klik" id="iceberg" onclick="box1('LOW','LOW')">ICEBERG</td>
                                                <td class="pt-5 pb-5 bg-warning text-dark klik" id="incosistent" onclick="box1('LOW','MEDIUM')">INCONSISTENT PERFORMER</td>
                                                <td class="pt-5 pb-5 bg-warning text-dark klik" id="diamond" onclick="box1('LOW','MEDIUM')">DIAMOND IN THE ROUGH</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td colspan="4" style="border-bottom: 6px solid; border-left:6px solid"></td>

                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Low</td>
                                                <td>Medium</td>
                                                <td>High</td>
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td colspan=4><strong> Potensi</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <style>
                .klik {
                    cursor: pointer;
                }
            </style>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Data Pemetaan Talenta Tiap Grade</strong>
                        </div>
                        <div class="m-b-25">

                        </div>
                        <div class="col col-md-12 text-right">
                            <button class="btn btn-primary btn-sm m-b-20" id="btnEksport"><i class="fas fa-file-excel"></i> Eksport Excel</button>
                        </div>
                        <div class="col-md-12 form-group">
                            <!-- DATA TABLE-->
                            <!-- <div class="table-responsive"> -->
                            <table class="table table-data3 table-responsive text-nowrap" width="100%" id="tabelKomite">
                                <thead class="table-borderless" style="background-color: cadetblue;color: aliceblue;">
                                    <tr>
                                        <th width="1%" class="text-center">No</th>
                                        <th>Nik</th>
                                        <th>Nama</th>
                                        <th class="text-center">Grade</th>
                                        <th>Departemen</th>
                                        <th>Direktorat</th>
                                        <th class="text-center">Asesmen</th>
                                        <th class="text-center">Kat Asesmen</th>
                                        <th class="text-center">Uji Komite</th>
                                        <th class="text-center">60%</th>
                                        <th class="text-center">30%</th>
                                        <th class="text-center">10%</th>
                                        <th class="text-center">Potensi</th>
                                        <th class="text-center">Kat Potensi</th>
                                        <th class="text-center">PAK <?php echo date("Y", strtotime("-2 year", time())); ?></th>
                                        <th class="text-center">PAK <?php echo date("Y", strtotime("-1 year", time())); ?></th>
                                        <th class="text-center">Rata-rata</th>
                                        <th class="text-center">Kat PAK</th>
                                        <th>Matrix</th>
                                        <th>Kat Talent</th>
                                        <th>Pensiun</th>
                                    </tr>
                                </thead>
                                <tbody class="table-bordered">
                                </tbody>
                            </table>
                            <!-- </div> -->
                            <!-- END DATA TABLE-->
                        </div>
                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>
            <style>
                td.details-control {
                    cursor: pointer;
                    text-align: center;

                }
            </style>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2020 PT Petrokimia Gresik. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="data_talent" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table  table-borderless table-data3" id="tabelDetailNilai">
                    <thead>
                        <tr style="background:#2860a8">
                            <th width="5%">No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th width="20%">Hasil</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<script src="<?php echo base_url() ?>assets/js/module/konfirmasi_view.js"></script>