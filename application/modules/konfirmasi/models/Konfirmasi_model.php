<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Konfirmasi_model extends CI_Model
{
	public function get_grade()
	{
		$grade = $this->db->order_by('id_grad_cat')
			->get('m_grade_cat');
		return $grade->result();
	}
	public function get_ujiKomite($data)
	{
		
		$grade = $data['grade'];
		$sub_grade = $data['sub_grade'];
		$tahun = $data['tahun'];

		if ($sub_grade == '') {
			$where = "id_cat = '$grade'";
		} else {
			$where = "id_grad = '$sub_grade'";
		}
		if ($data['tipe'] == 2) {
			$pak = $data['pak'];
			$asesmen = $data['asesmen'];
			$statement = "where " . $where . " and kat_pak= '$pak' and kat_potensi2 = '$asesmen'";
		} else {
			$statement = "where " . $where . "";
		}
		if ($grade == 1) {
			$sql = "SELECT 
			*,
			case
				when kat_potensi2 = 'HIGH'and kat_pak = 'HIGH' then '3'
				when kat_potensi2 = 'MEDIUM'and kat_pak = 'HIGH' then '2'
				when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then '1'
				when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then '6'
				when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then '5'
				when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then '4'
				when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then '9'
				when kat_potensi2 = 'MEDIUM' and kat_pak = 'LOW' then '8'
				when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then '7'
			end as hasil,
			case when kat_potensi2 = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
				when kat_potensi2 = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
				when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
				when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
				when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
				when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
				when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
				when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
			end as kat_hasil3,
			CASE WHEN kat_asesmen = 'HIGH' 
				AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_asesmen = 'MEDIUM' 
				AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_asesmen = 'HIGH' 
				AND kat_pak = 'MEDIUM' THEN 'TALENT' WHEN kat_asesmen = 'MEDIUM' 
				AND kat_pak = 'MEDIUM' THEN 'TALENT' ELSE 'NON TALENT' end AS kat_talent, 
			CASE WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' 
				WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
				WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				ELSE 'NON TALENT' 
			end AS kat_talent3 
		  FROM 
			(
				SELECT 
				a.nik, 
				a.nik_tetap,
				a.nama, 
				b.grade, 
				c.id_grad, 
				To_char(b.tgl_pensiun, 'dd/mm/yyyy') tgl_pensiun, 
				c.id_cat, 
				i.unit_name,
				h.kd_direktorat, 
				h.nm_direktorat, 
				Round(z.asesmen, 3) asesmen, 
				(SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat), 
				CASE 
					WHEN Round(z.asesmen, 2) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
				  WHEN Round(z.asesmen, 2) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
				  WHEN Round(z.asesmen, 2) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
				  ELSE 'LOW' 
				end AS kat_asesmen, 
				COALESCE(f.nilai_uji_kelayakan,110) nilai_uji_kelayakan,
				Round(COALESCE(z.asesmen,0) * 0.6, 2) satu,  
				Round(COALESCE(z.asesmen,0) * 0.3 * COALESCE(f.nilai_uji_kelayakan,0) / COALESCE(f.nilai_maks,1), 2) dua, 
				Round(COALESCE(z.asesmen,0) * 0.1 * 1, 2) tiga, 
				g.nilai_uji_komite, 
				Round((COALESCE(z.asesmen,0) * 0.6) + (COALESCE(z.asesmen,0) * 0.3 * COALESCE(f.nilai_uji_kelayakan,0) / COALESCE(f.nilai_maks,1)) + (COALESCE(z.asesmen,0) * 0.1 * 1),2 ) potensi2, 
				CASE 
					WHEN ((z.asesmen * 0.6) + (z.asesmen * 0.3 * f.nilai_uji_kelayakan / COALESCE(f.nilai_maks,1)) + (z.asesmen * 0.1 * COALESCE(g.nilai_uji_komite,0))) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
				  WHEN ((z.asesmen * 0.6) + (z.asesmen * 0.3 * f.nilai_uji_kelayakan / COALESCE(f.nilai_maks,1)) + (z.asesmen * 0.1 * COALESCE(g.nilai_uji_komite,0))) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
				  WHEN ((z.asesmen * 0.6) + (z.asesmen * 0.3 * f.nilai_uji_kelayakan / COALESCE(f.nilai_maks,1)) + (z.asesmen * 0.1 * COALESCE(g.nilai_uji_komite,0))) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
				  ELSE 'LOW' 
				end AS kat_potensi2, 
				round(e.nilai_sebelum,2) nilai_sebelum, 
				round(e.nilai_sekarang,2) nilai_sekarang, 
				f.nilai_uji_kelayakan, 
				Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) AS avg_pak, 
				(SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1),
				CASE 
					WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 3) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 3) THEN 'HIGH' 
					WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 2) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 2) THEN 'MEDIUM' 
					WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 1) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1) THEN 'LOW' 
					ELSE 'LOW' 
				end AS kat_pak 
			  FROM 
				  (select
					round(avg(h.nilai_asesmen), 2) asesmen,
					a.nik
				from
					m_kkj_domestic_global i
				left join t_nilai_potensi_g_d h on
					h.id_kkj_d_g = i.id_kkj_d_g
				right join m_users_karyawan a on
					a.nik = h.nik
				join d_list_karyawan_talenta c on
					c.nik = a.nik
				left join m_grades e on
					e.nm_grade = c.grade
				where
					date_part('year', h.created_at) = '$tahun'
					and i.kat_kompetensi = 5
					and date_part('year', c.created_date) = '$tahun'
				group by
					a.nik) z
				right join m_users_karyawan a on a.nik = z.nik 
				JOIN d_list_karyawan_talenta b ON b.nik = a.nik 
				JOIN m_grades c ON c.nm_grade = b.grade 
				LEFT JOIN t_nilai_potensi_kkj d ON d.nik = b.nik AND d.id_kkj IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) AND Date_part('year', d.created_at) = Date_part('year', CURRENT_DATE) 
				LEFT JOIN t_nilai_kinerja_pak e ON e.nik = b.nik AND Date_part('year', e.created_date) = Date_part('year', CURRENT_DATE) 
				left JOIN t_uji_kelayakan f ON f.nik = b.nik AND Date_part('year', f.created_date) = Date_part('year', CURRENT_DATE)
				left join t_uji_komite g on g.nik = b.nik AND Date_part('year', g.created_date) = Date_part('year', CURRENT_DATE)
				JOIN m_direktorat h ON h.kd_direktorat = b.kd_direktorat 
				left join m_unit i on i.unit_id = a.unit_id 
			  WHERE 
				Date_part('year', b.created_date) = Date_part('year', CURRENT_DATE)
			  GROUP BY 
				a.nik, 
				a.nik_tetap,
				a.nama, 
				b.grade, 
				i.unit_name,
				c.id_grad, 
				f.nilai_uji_kelayakan, 
				b.tgl_pensiun, 
				c.id_cat, 
				e.nilai_sebelum, 
				h.nm_direktorat, 
				h.kd_direktorat, 
				e.nilai_sekarang,
				g.nilai_uji_komite,
				z.asesmen,
				f.nilai_maks) a 				
				" . $statement . "
				ORDER BY a.nama";
				// var_dump($sql);die();
		} else {
			$sql = "SELECT * from (SELECT 
		*,
		case
				  when kat_potensi2 = 'HIGH'and kat_pak = 'HIGH' then '3'
				  when kat_potensi2 = 'MEDIUM'and kat_pak = 'HIGH' then '2'
				  when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then '1'
				  when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then '6'
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then '5'
				  when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then '4'
				  when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then '9'
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'LOW' then '8'
				  when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then '7'
			  end as hasil,
			  case when kat_potensi2 = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
				  when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
				  when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
				  when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
				  when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
				  when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
			  end as kat_hasil3,
			CASE WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' 
				WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
				WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
				ELSE 'NON TALENT' 
				end AS kat_talent3
	  FROM 
		(
		  SELECT 
			a.nik, 
			a.nik_tetap,
			a.nama, 
			b.grade, 
			c.id_grad, 
			To_char(b.tgl_pensiun, 'dd/mm/yyyy') tgl_pensiun, 
			c.id_cat,
			i.unit_name,
			h.kd_direktorat, 
			h.nm_direktorat, 
			Round(Avg(d.nilai_asesmen), 3) asesmen, 
			(SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat), 
			CASE 
				WHEN Round(Avg(d.nilai_asesmen), 5) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN Round(Avg(d.nilai_asesmen), 5) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN Round(       Avg(d.nilai_asesmen), 5) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_asesmen, 
			f.nilai_uji_kelayakan,
			j.nilai_uji_komite,
			Round(Avg(d.nilai_asesmen) * 0.6, 2) satu, 
			Round(Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150, 2) dua, 
			Round(Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite, 2) tiga, 
			Round(((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)),3) potensi2,
			CASE 
				WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite) ) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_potensi2, 
			e.nilai_sebelum, 
			e.nilai_sekarang, 
			f.nilai_uji_kelayakan, 
			Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) AS avg_pak, 
			(SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1),
			CASE WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 3) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 3) THEN 'HIGH' 
				WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 2) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 2) THEN 'MEDIUM' 
				WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 1) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1) THEN 'LOW' 
				ELSE 'LOW' 
			end AS kat_pak 
		  FROM 
			m_users_karyawan a 
			JOIN d_list_karyawan_talenta b ON b.nik = a.nik 
			JOIN m_grades c ON c.nm_grade = b.grade 
			LEFT JOIN t_nilai_potensi_kkj d ON d.nik = b.nik AND d.id_kkj IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) AND Date_part('year', d.created_at) = Date_part('year', CURRENT_DATE) 
			LEFT JOIN t_nilai_kinerja_pak e ON e.nik = b.nik AND Date_part('year', e.created_date) = Date_part('year', CURRENT_DATE) 
			left JOIN t_uji_kelayakan f ON f.nik = b.nik AND Date_part('year', f.created_date) = Date_part('year', CURRENT_DATE) 
			JOIN m_direktorat h ON h.kd_direktorat = b.kd_direktorat
			left join m_unit i on i.unit_id = a.unit_id 
			left join t_uji_komite j on j.nik = b.nik AND Date_part('year', j.created_date) = Date_part('year', CURRENT_DATE)
		  WHERE 
			Date_part('year', b.created_date) = Date_part('year', CURRENT_DATE) 
		  GROUP BY 
			a.nik, 
			a.nik_tetap,
			i.unit_name,
			a.nama, 
			b.grade, 
			c.id_grad, 
			f.nilai_uji_kelayakan, 
			b.tgl_pensiun, 
			c.id_cat, 
			e.nilai_sebelum, 
			h.nm_direktorat, 
			h.kd_direktorat, 
			e.nilai_sekarang,
			j.nilai_uji_komite
		) a 
	  ORDER BY 
		a.nama
	  ) zz
	  " . $statement . "
		";
		}
		// var_dump($sql);
		// die();
		return $this->db->query($sql)->result();
	}

	public function get_sub_grade($grade)
	{
		$sub_grade = $this->db->where('id_cat', $grade)
			->order_by('nm_grade')
			->get('m_grades');

		$data = "<option value=''>--Pilih Sub Grade--</option>";
		foreach ($sub_grade->result() as $grade_cat) {
			$data .= '<option value="' . $grade_cat->id_grad . '">' . $grade_cat->nm_grade . '</option>';
		}
		return $data;
	}

	public function post_ujiKelayakan($data, $nik)
	{
		$this->db->trans_begin();
		$ids = implode("','", $nik);
		$this->db->update_batch('t_uji_kelayakan', $data, 'nik');

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
}
