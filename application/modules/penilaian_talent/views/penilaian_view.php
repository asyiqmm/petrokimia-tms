<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Rentang Nilai Kinerja</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <table class="table" id="tabel_kinerja">
                                        <thead>
                                            <th width="1%">No.</th>
                                            <th>Kategori</th>
                                            <th>Rentang Nilai</th>
                                            <th width="1%">Aksi</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Rentang Nilai Potensi</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <table class="table" id="tabel_potensi">
                                        <thead>
                                            <th width="1%">No</th>
                                            <th>Grade</th>
                                            <th>Minimum</th>
                                            <th>Medium</th>
                                            <th>Maksimum</th>
                                            <th width="1%">Aksi</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_ubah_potensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Ubah Nilai Potensi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="ubah_potensi">
                <div class="modal-body">
                    <div class="col-lg-12">
                        <input type="hidden" id="id_potensi" name="id_potensi">
                        <div class="form-group">
                            <label for="">Grade</label>
                            <input type="text" readonly class="form-control" id="grade">
                        </div>
                        <div class="form-group">
                            <label for="">Minimum</label>
                            <input type="numeric" required class="form-control" id="minimum" name="minimum">
                        </div>
                        <div class="form-group">
                            <label for="">Medium</label>
                            <input type="text" readonly id="medium" name="medium" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Maksimum</label>
                            <input type="numeric" required id="maksimum" name="maksimum"  class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_ubah_kinerja" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Ubah Nilai Kinerja</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="ubah_kinerja">
                <div class="modal-body">
                    <div class="col-lg-12">
                        <input type="hidden" id="id_kinerja" name="id_kinerja">
                        <div class="form-group">
                            <label for="">Kategori</label>
                            <input type="text" readonly class="form-control" id="kategori" name="kategori">
                        </div>
                        <div class="form-group">
                            <label for="">rentang awal</label>
                            <input type="numeric" required class="form-control" id="rentang_awal" name="rentang_awal">
                        </div>
                        <div class="form-group">
                            <label for="">rentang akhir</label>
                            <input type="numeric" required id="rentang_akhir" name="rentang_akhir"  class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/penilaian_talent_view.js"></script>