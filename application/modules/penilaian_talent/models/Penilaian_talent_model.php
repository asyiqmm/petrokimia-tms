<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Penilaian_talent_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}



	public function get_kinerja()
	{
		$query = $this->db->order_by('id')
			->get('m_standar_nilai_kinerja');
		return $query->result();
	}

	public function get_potensi()
	{
		$query = $this->db->order_by('id_grade_cat')
			->get('m_standar_nilai_potensi');
		return $query->result();
	}

	public function ubah_potensi($id,$min,$max)
	{
		$this->db->trans_begin();
		$this->db->set('rentang_minimum', $min)
			->set('rentang_maksimum', $max)
			// ->set('updated_at', date('Y-m-d H:i:s'))
			->where('id', $id)
			->update('m_standar_nilai_potensi');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function ubah_kinerja($id,$min,$max)
	{
		$this->db->trans_begin();
		$this->db->set('rentang_awal', $min)
			->set('rentang_akhir', $max)
			// ->set('updated_at', date('Y-m-d H:i:s'))
			->where('id', $id)
			->update('m_standar_nilai_kinerja');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
}
