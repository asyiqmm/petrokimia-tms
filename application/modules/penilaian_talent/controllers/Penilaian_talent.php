<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

class Penilaian_talent extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('penilaian_talent_model');
		set_time_limit(0);
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Data Aspek Penilaian Talent | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(22, $_SESSION['session_menu'])) {
			$data['parent_active'] = 21;
			$data['child_active'] = 22;
			$this->_render('penilaian_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_kinerja()
	{
		$query = $this->penilaian_talent_model->get_kinerja();
		
		$data 			= array();
		foreach ($query as $index => $item) {
			$btn 		= '<button name="btnproses" onclick="ubah_kinerja(\'' . $item->id . '\',\'' . $item->rentang_awal . '\',\'' . $item->rentang_akhir . '\',\'' . $item->kategori . '\')" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Ubah"> <i class="fa fa-edit"></i> </button>';
			$data[] 	= array(
				"no" 				=> '',
				"kategori" 			=> $item->kategori,
				"rentang_nilai"		=> '≥'.$item->rentang_awal .' - '.$item->rentang_akhir,
				"aksi" 				=> $btn
			);
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}
	
	public function get_potensi()
	{
		$query = $this->penilaian_talent_model->get_potensi();
		
		$data 			= array();
		foreach ($query as $index => $item) {
			$grade		= 'Grade '.$item->id_grade_cat;
			$btn 		= '<button name="btnproses" onclick="ubah_potensi(\'' . $item->id . '\',\'' . $item->rentang_minimum . '\',\'' . $item->rentang_maksimum . '\',\'' . $grade . '\')" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Ubah"> <i class="fa fa-edit"></i> </button>';
			$data[] 	= array(
				"no" 				=> '',
				"grade" 			=> $item->id_grade_cat,
				"miminum" 			=> '<'.$item->rentang_minimum,
				"medium"			=> '≥'.$item->rentang_minimum .' - '.$item->rentang_maksimum,
				"maksimum"			=> '>'.$item->rentang_maksimum,
				"aksi" 				=> $btn
			);
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function ubah_potensi()
	{
		$id			= $this->input->post('id_potensi');
		$minimum 	= $this->input->post('minimum');
		$maksimum 	= $this->input->post('maksimum');

		$query = $this->penilaian_talent_model->ubah_potensi($id,$minimum,$maksimum);
		echo json_encode($query);
	}

	public function ubah_kinerja()
	{
		$id			= $this->input->post('id_kinerja');
		$minimum 	= $this->input->post('rentang_awal');
		$maksimum 	= $this->input->post('rentang_akhir');

		$query = $this->penilaian_talent_model->ubah_kinerja($id,$minimum,$maksimum);
		echo json_encode($query);
	}
}
