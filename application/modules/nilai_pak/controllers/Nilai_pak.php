<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Nilai_pak extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('nilai_pak_model');
		$this->load->helper(array('url', 'download'));
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Unggah Nilai PAK | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		// redirect(base_url().'index.php/anmaag_view/data',$data);
		if (in_array(25, $_SESSION['session_menu'])) {
			$data['parent_active'] = 8;
			$data['child_active'] = 25;
			$this->_render('nilaiPak_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}
	public function eksport_nilai_pak()
	{
		// echo json_encode('asda');
		force_download('resources/template_nilai_pak.xlsx', NULL);
	}
	public function eksport_nilai_uji_kelayakan()
	{
		// echo json_encode('asda');
		force_download('resources/template_nilai_uji_kelayakan.xlsx', NULL);
	}

	public function eksport_nilai_uji_komite()
	{
		// echo json_encode('asda');
		force_download('resources/template_nilai_uji_komite.xlsx', NULL);
	}
	public function import_excel()
	{

		if (isset($_FILES["file"]["name"])) {
			// $reader = IOFactory::createReader('Xlsx');
			$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			if ($extension == 'csv') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif ($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
			// $writer = IOFactory::createWriter($spreadsheet, 'Html');
			// $message = $writer->save('php://output');

			// Set cell A1 with a string value
			// $spreadsheet->getActiveSheet()->setCellValue('A1', 'PhpSpreadsheet');

			// Get the value from cell A1
			// $cellValue = $spreadsheet->getActiveSheet()->getCell('A1')->getValue();

			$worksheet = $spreadsheet->getActiveSheet();
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			// $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

			$file_data = [];
			$arrNik = [];
			$i = 0;
			$g = 0;


			for ($row = 2; $row <= $highestRow; ++$row) {
				$nik_tetap 			= $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
				$nik_sap 			= $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
				$pak_sebelum 	= $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
				$pak_sekarang 	= $worksheet->getCellByColumnAndRow(8, $row)->getCalculatedValue();
				if (!in_array($nik_sap, $arrNik) && (is_numeric($nik_sap) || is_numeric($nik_sap))) {
					$arrNik[] = $nik_sap;
					if (is_numeric($pak_sekarang) && is_numeric($pak_sebelum)) {
						$file_data[] = array(
							'nik'		=> $nik_sap,
							'nik_tetap'		=> $nik_tetap,
							'nilai_sebelum'	=> $pak_sebelum,
							'nilai_sekarang' => $pak_sekarang,
							'created_date'	=> date('Y-m-d H:i:s')
						);
						$i++;
					}
				} else {
					$g++;
				}
			}
			$query = $this->nilai_pak_model->post_asesmen($file_data, $arrNik, $i, $g);
			echo json_encode($query);
		} else {
		}
	}

	public function import_excel_uk()
	{
		if (isset($_FILES["fileUjiKelayakan"]["name"])) {
			$extension = pathinfo($_FILES['fileUjiKelayakan']['name'], PATHINFO_EXTENSION);
			if ($extension == 'csv') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif ($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['fileUjiKelayakan']['tmp_name']);
			$worksheet = $spreadsheet->getActiveSheet();
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			$arrNik = [];
			$i = 0;
			$g = 0;

			// $nikTalent = [];
			// $talent = $this->nilai_pak_model->get_dataTalent();
			// foreach ($talent as $key => $value) {
			// 	$nikTalent[] =  $value->nik;
			// }

			$file_data = [];
			for ($row = 2; $row <= $highestRow; ++$row) {
				$nik 			= $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
				$nilai_uk 	= $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
				$nilai_maks 	= $worksheet->getCellByColumnAndRow(8, $row)->getCalculatedValue();

				if (!in_array($nik, $arrNik) && (is_numeric($nik) || is_numeric($nik))) {
					$arrNik[] = $nik;
					if (is_numeric($nilai_uk) && is_numeric($nilai_maks)) {
						$file_data[] = array(
							'nik'		=> $nik,
							'nilai_uji_kelayakan' => $nilai_uk,
							'nilai_maks' => $nilai_maks,
							'created_date'	=> date('Y-m-d H:i:s')
						);
						$i++;
					}
				} else {
					$g++;
				}
			}
			$query = $this->nilai_pak_model->post_ujiKelayakan($file_data, $arrNik, $i, $g);
			echo json_encode($query);
		} else {
		}
	}
	public function import_excel_komite()
	{
		if (isset($_FILES["fileUjiKomite"]["name"])) {
			$extension = pathinfo($_FILES['fileUjiKomite']['name'], PATHINFO_EXTENSION);
			if ($extension == 'csv') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			} elseif ($extension == 'xlsx') {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			} else {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			}
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['fileUjiKomite']['tmp_name']);
			$worksheet = $spreadsheet->getActiveSheet();
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
			$arrNik = [];
			$i = 0;
			$g = 0;

			// $nikTalent = [];
			// $talent = $this->nilai_pak_model->get_dataTalentKomite();
			// foreach ($talent as $key => $value) {
			// 	$nikTalent[] =  $value->nik;
			// }

			$file_data = [];
			for ($row = 2; $row <= $highestRow; ++$row) {
				$nik 			= $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
				$nilai_uk 	= $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
				if (!in_array($nik, $arrNik) && (is_numeric($nik) || is_numeric($nik))) {
					$arrNik[] = $nik;
					if (is_numeric($nilai_uk)) {
						$file_data[] = array(
							'nik'		=> $nik,
							'nilai_uji_komite' => $nilai_uk,
							'created_date'	=> date('Y-m-d H:i:s')
						);
						$i++;
					}
				} else {
					$g++;
				}
			}
			// var_dump($file_data);die();
			$query = $this->nilai_pak_model->post_ujiKomite($file_data, $arrNik, $i, $g);
			echo json_encode($query);
		} else {
		}
	}


	public function fetch()
	{
		$query = $this->nilai_pak_model->getListPak();
		$data 	= array();
		foreach ($query as $index => $item) {
			$data[] 	= array(
				"no" => '',
				"nik"			=> $item->nik,
				"nama"			=> (!empty($item->nama) ? $item->nama : '-'),
				"grade"			=> (!empty($item->nm_grade) ? $item->nm_grade : "-"),
				"dept" 			=> (!empty($item->unit_name) ? $item->unit_name : "-"),
				"job_family" 	=> (!empty($item->nm_fitjob_fam) ? $item->nm_fitjob_fam : "-"),
				// "kki"		 	=> (!empty($item->asesmen) ? $item->asesmen : "-"),
				"pak_sebelum" 	=> (!empty($item->nilai_sebelum) ? $item->nilai_sebelum : "-"),
				"pak_sekarang"	=> (!empty($item->nilai_sekarang) ? $item->nilai_sekarang : "-"),
				"rata-rata" 	=> (!empty($item->avg_pak) ? $item->avg_pak : "-"),
				"uk" 			=> (!empty($item->nilai_uji_kelayakan) ? $item->nilai_uji_kelayakan : "-"),
				"nilai_maks" 			=> (!empty($item->nilai_uji_kelayakan) ? $item->nilai_maks : "-"),
				"nilai_uji_komite" 			=> (!empty($item->nilai_uji_kelayakan) ? $item->nilai_uji_komite : "-"),

			);
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}
}
