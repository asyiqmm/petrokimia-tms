<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <h2 class="title-1 m-b-25">Data Talent</h2>

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Unggah Data Nilai PAK</strong>
                        </div>
                        <form method="POST" id="import_form" enctype="multipart/form-data">
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-12">
                                        <label for="file">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <!-- <input type="file"  name="file" id="file" required accept=".xls, .xlsx">| -->
                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx" />
                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmitPAK" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <a type="button" href="<?php echo base_url() . 'index.php/nilai_pak/eksport_nilai_pak' ?>" id="btn_contoh_g1" class="btn btn-warning btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Contoh template
                                </a>
                            </div>
                        </form>

                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Unggah Data Nilai Uji Kelayakan</strong>
                        </div>
                        <form method="POST" id="import_uji_kelayakan" enctype="multipart/form-data">
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-12">
                                        <label for="fileUjiKelayakan">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <!-- <input type="file"  name="file" id="file" required accept=".xls, .xlsx">| -->
                                                <input type="file" class="custom-file-input" name="fileUjiKelayakan" id="fileUjiKelayakan" required accept=".xls, .xlsx" />
                                                <label class="custom-file-label" id="labelFileUK" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <a type="button" href="<?php echo base_url() . 'index.php/nilai_pak/eksport_nilai_uji_kelayakan' ?>" id="btn_contoh_g1" class="btn btn-warning btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Contoh template
                                </a>
                            </div>
                        </form>

                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Unggah Data Nilai Uji Komite</strong>
                        </div>
                        <form method="POST" id="import_uji_komite" enctype="multipart/form-data">
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-12">
                                        <label for="fileUjiKomite">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="fileUjiKomite" id="fileUjiKomite" required accept=".xls, .xlsx" />
                                                <label class="custom-file-label" id="labelFileKomite" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" id="btnsubmitKomite" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                                <a type="button" href="<?php echo base_url() . 'index.php/nilai_pak/eksport_nilai_uji_komite' ?>" id="btn_contoh_g1" class="btn btn-warning btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Contoh template
                                </a>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Data Nilai PAK Karyawan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="table-responsive">
                                <table class="table  table-borderless table-data3" width="100%" id="tabelNilai">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">No</th>
                                            <th width="14%">NIK</th>
                                            <th width="10%">Nama</th>
                                            <th width="5%">Grade</th>
                                            <th width="20%">Departemen</th>
                                            <th width="20%">Job Family</th>
                                            <th width="11%">PAK <?php echo date("Y", strtotime("-2 year", time())); ?></th>
                                            <th width="11%">PAK <?php echo date("Y", strtotime("-1 year", time())); ?></th>
                                            <th width="5%">Rata-rata</th>
                                            <th width="5%">Nilai UK</th>
                                            <th width="5%">Nilai MAKS</th>
                                            <th width="5%">Nilai Uji Komite</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/nilaiPak_view.js"></script>