<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Nilai_pak_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}
	function post_asesmen($data, $nik, $i, $g)
	{
		$this->db->trans_begin();
		$ids = implode("','", $nik);

		$this->db->query("DELETE from t_nilai_kinerja_pak WHERE date_part('year', created_date) = date_part('year', CURRENT_DATE)");
		$this->db->insert_batch('t_nilai_kinerja_pak', $data);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'total' => $i,
				'gagal' => $g
			];
		}
	}

	public function getListPak()
	{
		$query = $this->db->query("SELECT a.nik,a.nama,b.unit_id,b.unit_name,c.id_grad id_grade,c.nm_grade,d.nm_jabt jabatan,f.nm_fitjob_fam,round(g.nilai_sebelum,2) nilai_sebelum,round(g.nilai_sekarang,2) nilai_sekarang,
		round((g.nilai_sebelum + g.nilai_sekarang)/2,2) avg_pak, h.nilai_uji_kelayakan,h.nilai_maks, i.nilai_uji_komite
			from m_users_karyawan a
				left join m_unit b on b.unit_id = a.unit_id
				join m_grades c on c.nm_grade = a.grade_personal
				left join m_jabatans d on d.kd_jabatan = a.grade
				left join d_unit_job_family e on e.unit_id = b.unit_id
				left join m_fitjob_fam f on f.id = e.id_job_family
				left join t_nilai_kinerja_pak g on g.nik_tetap = a.nik_tetap and date_part('year', g.created_date) = date_part('year', CURRENT_DATE)
				left join t_uji_kelayakan h on h.nik = a.nik and date_part('year', h.created_date) = date_part('year', CURRENT_DATE)
				left join t_uji_komite i on i.nik = a.nik and date_part('year', i.created_date) = date_part('year', CURRENT_DATE)
			group by a.nik,a.nama,b.unit_id,b.unit_name,c.id_grad,c.nm_grade,d.nm_jabt,f.nm_fitjob_fam,g.nilai_sebelum,g.nilai_sekarang, h.nilai_uji_kelayakan,nilai_maks,i.nilai_uji_komite
		order by a.nama");
		return $query->result();
	}

	public function post_ujiKelayakan($data, $nik, $i, $g)
	{
		$this->db->trans_begin();
		$ids = implode("','", $nik);

		$this->db->query("delete from t_uji_kelayakan where date_part('year', created_date) = date_part('year', CURRENT_DATE)");
		$this->db->insert_batch('t_uji_kelayakan', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'total' => $i,
				'gagal' => $g
			];
		}
	}

	public function post_ujiKomite($data, $nik, $i, $g)
	{
		$this->db->trans_begin();
		$ids = implode("','", $nik);

		$this->db->query("delete from t_uji_komite where date_part('year', created_date) = date_part('year', CURRENT_DATE)");
		$this->db->insert_batch('t_uji_komite', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'total' => $i,
				'gagal' => $g
			];
		}
	}

	public function get_grade()
	{
		$query = $this->db->select('id_grad_cats')
			->order_by('id_grad_cat')
			->get('m_grade_cat');
		return $query->result();
	}

	public function get_dataTalent()
	{
		$sql = "select * from (
					SELECT
							*,
							case
								when kat_asesmen = 'HIGH'
								and kat_pak = 'HIGH' then 'TALENT'
								when kat_asesmen = 'MEDIUM'
								and kat_pak = 'HIGH' then 'TALENT'
								when kat_asesmen = 'HIGH'
								and kat_pak = 'MEDIUM' then 'TALENT'
								when kat_asesmen = 'MEDIUM'
								and kat_pak = 'MEDIUM' then 'TALENT'
								else 'NON TALENT'
						end as kat_talent
						from
							(
							select
								a.nik,
								a.nama,
								b.grade,
								c.id_grad,
								to_char(b.tgl_pensiun,'dd/mm/yyyy') tgl_pensiun,
								c.id_cat,
								round(avg(d.nilai_asesmen), 6) asesmen,
								case
									when round(avg(d.nilai_asesmen), 5) > (select rentang_maksimum from m_standar_nilai_potensi where id_grade_cat = c.id_cat) then 'HIGH'
									when round(avg(d.nilai_asesmen), 5) between (select rentang_minimum from m_standar_nilai_potensi where id_grade_cat = c.id_cat) and (select rentang_maksimum from m_standar_nilai_potensi where id_grade_cat = c.id_cat) then 'MEDIUM'
									when round(avg(d.nilai_asesmen), 5) < (select rentang_maksimum from m_standar_nilai_potensi where id_grade_cat = c.id_cat) then 'LOW'
									else 'LOW'
							end as kat_asesmen,
								e.nilai_sebelum,
								e.nilai_sekarang,
								round((e.nilai_sekarang + e.nilai_sebelum)/ 2, 2) as avg_pak,
								case
								when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between (select rentang_awal from m_standar_nilai_kinerja where id = 3) and (select rentang_akhir from m_standar_nilai_kinerja where id = 3)  then 'HIGH' 
								when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between (select rentang_awal from m_standar_nilai_kinerja where id = 2) and (select rentang_akhir from m_standar_nilai_kinerja where id = 2) then 'MEDIUM' 
								when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between (select rentang_awal from m_standar_nilai_kinerja where id = 1) and (select rentang_akhir from m_standar_nilai_kinerja where id = 1) then 'LOW'
								else 'LOW'
							end as kat_pak
							from
								m_users_karyawan a
							join d_list_karyawan_talenta b on
								b.nik = a.nik
							join m_grades c on
								c.nm_grade = b.grade
							left join t_nilai_potensi_kkj d on d.nik = b.nik and d.id_kkj in (1,2,3,4,5,6,7,8,9,10,11,12,13) and date_part('year', d.created_at) = date_part('year', CURRENT_DATE)
							left join t_nilai_kinerja_pak e on e.nik = b.nik and date_part('year', e.created_date) = date_part('year', CURRENT_DATE)
							where date_part('year', b.created_date) = date_part('year', CURRENT_DATE)
							group by a.nik,a.nama,b.grade,c.id_grad,b.tgl_pensiun,c.id_cat,e.nilai_sebelum,e.nilai_sekarang
							) a
						order by a.nama
					) z
				where kat_talent = 'TALENT'";

		return $this->db->query($sql)->result();
	}

	public function get_dataTalentKomite()
	{
		$sql = "SELECT * from (SELECT 
		*,
		case
				  when kat_potensi = 'HIGH'and kat_pak = 'HIGH' then '3'
				  when kat_potensi = 'MEDIUM'and kat_pak = 'HIGH' then '2'
				  when kat_potensi = 'LOW' and kat_pak = 'HIGH' then '1'
				  when kat_potensi = 'HIGH' and kat_pak = 'MEDIUM' then '6'
				  when kat_potensi = 'MEDIUM' and kat_pak = 'MEDIUM' then '5'
				  when kat_potensi = 'LOW' and kat_pak = 'MEDIUM' then '4'
				  when kat_potensi = 'HIGH' and kat_pak = 'LOW' then '9'
				  when kat_potensi = 'MEDIUM' and kat_pak = 'LOW' then '8'
				  when kat_potensi = 'LOW' and kat_pak = 'LOW' then '7'
			  end as hasil,
			  case when kat_potensi = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
				  when kat_potensi = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
				  when kat_potensi = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
				  when kat_potensi = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
				  when kat_potensi = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
				  when kat_potensi = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
				  when kat_potensi = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
				  when kat_potensi = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
			  end as kat_hasil,
		CASE WHEN kat_asesmen = 'HIGH' 
		AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_asesmen = 'MEDIUM' 
		AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_asesmen = 'HIGH' 
		AND kat_pak = 'MEDIUM' THEN 'TALENT' WHEN kat_asesmen = 'MEDIUM' 
		AND kat_pak = 'MEDIUM' THEN 'TALENT' ELSE 'NON TALENT' end AS kat_talent, 
		CASE WHEN kat_potensi = 'HIGH' 
		AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_potensi = 'MEDIUM' 
		AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_potensi = 'HIGH' 
		AND kat_pak = 'MEDIUM' THEN 'TALENT' WHEN kat_potensi = 'MEDIUM' 
		AND kat_pak = 'MEDIUM' THEN 'TALENT' ELSE 'NON TALENT' end AS kat_talentUK 
	  FROM 
		(
		  SELECT 
			a.nik, 
			a.nik_tetap,
			a.nama, 
			b.grade, 
			c.id_grad, 
			To_char(b.tgl_pensiun, 'dd/mm/yyyy') tgl_pensiun, 
			c.id_cat,
			i.unit_name,
			h.kd_direktorat, 
			h.nm_direktorat, 
			Round(Avg(d.nilai_asesmen), 3) asesmen, 
			(SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat), 
			CASE 
				WHEN Round(Avg(d.nilai_asesmen), 5) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN Round(Avg(d.nilai_asesmen), 5) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN Round(       Avg(d.nilai_asesmen), 5) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_asesmen, 
			f.nilai_uji_kelayakan,
			Round(Avg(d.nilai_asesmen) * 0.6, 2) satu, 
			Round(Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150, 2) dua, 
			((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) potensi, 
			CASE 
				WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_potensi, 
			e.nilai_sebelum, 
			e.nilai_sekarang, 
			f.nilai_uji_kelayakan, 
			Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) AS avg_pak, 
			(SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1),
			CASE WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 3) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 3) THEN 'HIGH' 
				WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 2) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 2) THEN 'MEDIUM' 
				WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 1) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1) THEN 'LOW' 
				ELSE 'LOW' 
			end AS kat_pak 
		  FROM 
			m_users_karyawan a 
			JOIN d_list_karyawan_talenta b ON b.nik = a.nik 
			JOIN m_grades c ON c.nm_grade = b.grade 
			LEFT JOIN t_nilai_potensi_kkj d ON d.nik = b.nik AND d.id_kkj IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) AND Date_part('year', d.created_at) = Date_part('year', CURRENT_DATE) 
			LEFT JOIN t_nilai_kinerja_pak e ON e.nik = b.nik AND Date_part('year', e.created_date) = Date_part('year', CURRENT_DATE) 
			left JOIN t_uji_kelayakan f ON f.nik = b.nik AND Date_part('year', f.created_date) = Date_part('year', CURRENT_DATE) 
			JOIN m_direktorat h ON h.kd_direktorat = b.kd_direktorat
			left join m_unit i on i.unit_id = a.unit_id 
		  WHERE 
			Date_part('year', b.created_date) = Date_part('year', CURRENT_DATE) 
		  GROUP BY 
			a.nik, 
			a.nik_tetap,
			i.unit_name,
			a.nama, 
			b.grade, 
			c.id_grad, 
			f.nilai_uji_kelayakan, 
			b.tgl_pensiun, 
			c.id_cat, 
			e.nilai_sebelum, 
			h.nm_direktorat, 
			h.kd_direktorat, 
			e.nilai_sekarang
		) a 
	  ORDER BY 
		a.nama
	  ) b
	  where kat_talentUK = 'TALENT'
		";
		return $this->db->query($sql)->result();
	}
}
