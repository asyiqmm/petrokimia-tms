<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Riwayat_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}
	public function dataSaatPromosi()
	{
		$sql = "SELECT 
		case 
		when t.total < 65 then 'Tidak Disarankan'
		when t.total >= 65 and total <= 79 then 'Disarankan Dengan Beberapa Pengembangan'
		when t.total >= 80 then 'Disarankan' end hasil,t.total,t.nik, t.nama,t.nm_fitjob_fam,t.id_grad,t.id_cat,t.kd_direktorat,t.unit_id,t.nm_direktorat, t.unit_name
								from (
								  SELECT a.nik, b.nama,d.nm_fitjob_fam,e.id_grad,e.id_cat,h.kd_direktorat,b.unit_id,j.nm_direktorat, k.unit_name,
				sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
				round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total,b.grade_personal
				from t_nilai_potensi_kkj a
				  join m_users_karyawan b on b.nik = a.nik
				  join d_unit_job_family c on b.unit_id = c.unit_id
				  join d_list_karyawan_talenta h on h.nik = a.nik
				  join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
				  join m_grades e on e.nm_grade = b.grade_personal
				  join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
				  join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_promosi
				  left join m_direktorat j on j.kd_direktorat = h.kd_direktorat
				  left join m_unit k on k.unit_id = b.unit_id
				where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) and e.id_grad not in (1,2)
				group by a.nik,d.nm_fitjob_fam,b.nama,b.grade_personal,h.kd_direktorat,b.unit_id, e.id_grad,j.nm_direktorat,e.nm_grade,e.id_grad,e.id_cat,k.unit_id
				order by b.nama
								) t
						group by t.nik, t.nama,t.nm_fitjob_fam,t.id_grad,t.id_cat,t.kd_direktorat,t.unit_id,t.nm_direktorat, t.unit_name,t.total
		";
		$r =  $this->db->query($sql)->result();
		$data 			= [];
		$nik 	= [];
		foreach ($r as $index => $item) {
			if (is_int($item->nik)) {
				$nik[] = intVal($item->nik);
				$data[] 	= array(
					"nik" 			=> intVal($item->nik),
					"grade" 			=> $item->id_grad,
					"id_departemen"	=> $item->unit_id,
					"nm_departemen"	=> $item->unit_name,
					"id_direktorat"	=> $item->kd_direktorat,
					"nm_direktorat"	=> $item->nm_direktorat,
					"job_fit"		=> $item->total,
					"rekomendasi"	=> $item->hasil,
					'created_at' => date('Y-m-d H:i:s')

				);
			}
		}
		$ids = implode("','", $nik);
		$this->db->query("delete from t_riwayat_asesmen_promosi where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$insert = $this->db->insert_batch('t_riwayat_asesmen_promosi', $data);
	}
	public function dataSaatIni()
	{
		$sql = "SELECT 
		case 
		when t.total < 65 then 'Tidak Disarankan'
		when t.total >= 65 and total <= 79 then 'Disarankan Dengan Beberapa Pengembangan'
		when t.total >= 80 then 'Disarankan' end hasil,t.total,t.nik, t.nama,t.nm_fitjob_fam,t.id_grad,t.id_cat,t.kd_direktorat,t.unit_id,t.nm_direktorat, t.unit_name
								from (
								  SELECT a.nik, b.nama,d.nm_fitjob_fam,e.id_grad,e.id_cat,h.kd_direktorat,b.unit_id,j.nm_direktorat, k.unit_name,
				sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen,
				round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total,b.grade_personal
				from t_nilai_potensi_kkj a
				  join m_users_karyawan b on b.nik = a.nik
				  join d_unit_job_family c on b.unit_id = c.unit_id
				  join d_list_karyawan_talenta h on h.nik = a.nik
				  join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
				  join m_grades e on e.nm_grade = b.grade_personal
				  join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
				  join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_promosi
				  left join m_direktorat j on j.kd_direktorat = h.kd_direktorat
				  left join m_unit k on k.unit_id = b.unit_id
				where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) and e.id_grad not in (1,2)
				group by a.nik,d.nm_fitjob_fam,b.nama,b.grade_personal,h.kd_direktorat,b.unit_id, e.id_grad,j.nm_direktorat,e.nm_grade,e.id_grad,e.id_cat,k.unit_id
				order by b.nama
								) t
						group by t.nik, t.nama,t.nm_fitjob_fam,t.id_grad,t.id_cat,t.kd_direktorat,t.unit_id,t.nm_direktorat, t.unit_name,t.total
		";
		$r =  $this->db->query($sql)->result();
		$data 			= [];
		$nik 	= [];
		foreach ($r as $index => $item) {
			if (is_int($item->nik)) {
				$nik[] = intVal($item->nik);
				$data[] 	= array(
					"nik" 			=> intVal($item->nik),
					"grade" 			=> $item->id_grad,
					"id_departemen"	=> $item->unit_id,
					"nm_departemen"	=> $item->unit_name,
					"id_direktorat"	=> $item->kd_direktorat,
					"nm_direktorat"	=> $item->nm_direktorat,
					"job_fit"		=> $item->total,
					"rekomendasi"	=> $item->hasil,
					'created_at' => date('Y-m-d H:i:s')

				);
			}
		}
		$ids = implode("','", $nik);
		$this->db->query("delete from t_riwayat_asesmen_saat_ini where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$insert = $this->db->insert_batch('t_riwayat_asesmen_saat_ini', $data);
	}
	public function dataDomestic()
	{
		$sql = "SELECT 
			case 
				when a.lima between 10 and 12 and a.dua <= 2 and a.satu = 0 then 'DISARANKAN' 
				when a.lima between 7 and 9 and a.dua <= 5 and a.satu = 0 then 'DISARANKAN DENGAN PERTIMBANGAN'
				when a.lima <= 6 and a.dua >= 6 and a.satu >= 0 then 'TIDAK DISARANKAN'
			end  hasil, a.nik,a.nama,a.total,a.lima,a.dua,a.satu, a.unit_id,a.unit_name,a.kd_direktorat,a.nm_direktorat,a.id_grad,a.nm_grade from (
			SELECT round(avg(c.nilai_asesmen),2) total, b.nik,a.nama, a.unit_id,e.unit_name,f.kd_direktorat,f.nm_direktorat,g.id_grad,g.nm_grade,
			sum(case when c.nilai_asesmen between 3 and 5 then 1 else 0 end) lima,
			sum(case when c.nilai_asesmen = 2 then 1 else 0 end) dua,
			sum(case when c.nilai_asesmen = 1 then 1 else 0 end) satu
			from m_users_karyawan a
			join d_list_karyawan_talenta b on a.nik = b.nik
			join t_nilai_potensi_g_d c on c.nik = b.nik
			join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g
			join m_unit e on e.unit_id = a.unit_id
			join m_direktorat f on f.kd_direktorat =b.kd_direktorat
			left join m_grades g on g.nm_grade = b.grade
			where d.kat_kompetensi = '5' and date_part('year', c.created_at) = date_part('year', CURRENT_DATE) 
			group by b.nik,a.nama,a.unit_id,e.unit_name,f.kd_direktorat,f.nm_direktorat,g.id_grad,g.nm_grade
			) a
			group by a.nik,a.nama,a.total,a.lima,a.dua,a.satu,a.unit_id,a.unit_name,a.kd_direktorat,a.nm_direktorat,a.id_grad,a.nm_grade";
		$query = $this->db->query($sql)->result();
		$data 			= [];
		$nik 	= [];
		foreach ($query as $index => $item) {
			if (is_int($item->nik)) {
				$nik[] = intVal($item->nik);
				$data[] 	= array(
					"nik" 			=> intVal($item->nik),
					"grade" 			=> $item->id_grad,
					"id_departemen"	=> $item->unit_id,
					"nm_departemen"	=> $item->unit_name,
					"id_direktorat"	=> $item->kd_direktorat,
					"nm_direktorat"	=> $item->nm_direktorat,
					"job_fit"		=> $item->total,
					"rekomendasi"	=> $item->hasil,
					'created_at' => date('Y-m-d H:i:s')

				);
			}
		}
		$ids = implode("','", $nik);
		$this->db->query("delete from t_riwayat_asesmen_promosi_grade1_domestic where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$insert = $this->db->insert_batch('t_riwayat_asesmen_promosi_grade1_domestic', $data);
		return $insert;
	}

	public function dataGlobal()
	{
		$sql = "SELECT 
			case 
				when a.lima between 10 and 12 and a.dua <= 2 and a.satu = 0 then 'DISARANKAN' 
				when a.lima between 7 and 9 and a.dua <= 5 and a.satu = 0 then 'DISARANKAN DENGAN PERTIMBANGAN'
				when a.lima <= 6 and a.dua >= 6 and a.satu >= 0 then 'TIDAK DISARANKAN'
			end  hasil, a.nik,a.nama,a.total,a.lima,a.dua,a.satu, a.unit_id,a.unit_name,a.kd_direktorat,a.nm_direktorat,a.id_grad,a.nm_grade from (
			SELECT round(avg(c.nilai_asesmen),2) total, b.nik,a.nama, a.unit_id,e.unit_name,f.kd_direktorat,f.nm_direktorat,g.id_grad,g.nm_grade,
			sum(case when c.nilai_asesmen between 3 and 5 then 1 else 0 end) lima,
			sum(case when c.nilai_asesmen = 2 then 1 else 0 end) dua,
			sum(case when c.nilai_asesmen = 1 then 1 else 0 end) satu
			from m_users_karyawan a
			join d_list_karyawan_talenta b on a.nik = b.nik
			join t_nilai_potensi_g_d c on c.nik = b.nik
			join m_kkj_domestic_global d on d.id_kkj_d_g = c.id_kkj_d_g
			join m_unit e on e.unit_id = a.unit_id
			join m_direktorat f on f.kd_direktorat =b.kd_direktorat
			left join m_grades g on g.nm_grade = b.grade
			where d.kat_kompetensi = '6' and date_part('year', c.created_at) = date_part('year', CURRENT_DATE) 
			group by b.nik,a.nama,a.unit_id,e.unit_name,f.kd_direktorat,f.nm_direktorat,g.id_grad,g.nm_grade
			) a
			group by a.nik,a.nama,a.total,a.lima,a.dua,a.satu,a.unit_id,a.unit_name,a.kd_direktorat,a.nm_direktorat,a.id_grad,a.nm_grade";
		$query = $this->db->query($sql)->result();
		$data 			= [];
		$nik 	= [];
		foreach ($query as $index => $item) {
			if (is_int($item->nik)) {
				$nik[] = intVal($item->nik);
				$data[] 	= array(
					"nik" 			=> intVal($item->nik),
					"grade" 			=> $item->id_grad,
					"id_departemen"	=> $item->unit_id,
					"nm_departemen"	=> $item->unit_name,
					"id_direktorat"	=> $item->kd_direktorat,
					"nm_direktorat"	=> $item->nm_direktorat,
					"job_fit"		=> $item->total,
					"rekomendasi"	=> $item->hasil,
					'created_at' => date('Y-m-d H:i:s')

				);
			}
		}
		$ids = implode("','", $nik);
		$this->db->query("delete from t_riwayat_asesmen_promosi_grade1_global where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$insert = $this->db->insert_batch('t_riwayat_asesmen_promosi_grade1_global', $data);
		return $insert;
	}

	public function updateData()
	{
		$data1  = $this->dataSaatIni();
		$data2  = $this->dataSaatPromosi();
		$data3 = $this->dataDomestic();
		$data4 = $this->dataGlobal();
	}

	public function update_talenta()
	{
		$data = $this->dataPemetaan();
	}

	public function dataPemetaan()
	{
		$sql = "SELECT * from (SELECT 
		*,
		case
				  when kat_potensi2 = 'HIGH'and kat_pak = 'HIGH' then '3'
				  when kat_potensi2 = 'MEDIUM'and kat_pak = 'HIGH' then '2'
				  when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then '1'
				  when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then '6'
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then '5'
				  when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then '4'
				  when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then '9'
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'LOW' then '8'
				  when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then '7'
			  end as hasil,
			  case when kat_asesmen = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
				  when kat_asesmen = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
				  when kat_asesmen = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
				  when kat_asesmen = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
				  when kat_asesmen = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
				  when kat_asesmen = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
				  when kat_asesmen = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
				  when kat_asesmen = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
			  end as kat_hasil1,
			  case when kat_potensi = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
				  when kat_potensi = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
				  when kat_potensi = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
				  when kat_potensi = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
				  when kat_potensi = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
				  when kat_potensi = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
				  when kat_potensi = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
				  when kat_potensi = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
			  end as kat_hasil2,
			  case when kat_potensi2 = 'HIGH' and kat_pak = 'HIGH' then 'CONSISTENT STAR' 
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'HIGH' then 'UTILITY HI_PRO' 
				  when kat_potensi2 = 'LOW' and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR' 
				  when kat_potensi2 = 'HIGH' and kat_pak = 'MEDIUM' then 'FUTURE STAR' 
				  when kat_potensi2 = 'MEDIUM' and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO' 
				  when kat_potensi2 = 'LOW' and kat_pak = 'MEDIUM' then 'CONTRIBUTOR' 
				  when kat_potensi2 = 'HIGH' and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH' when kat_asesmen = 'MEDIUM' and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER' 
				  when kat_potensi2 = 'LOW' and kat_pak = 'LOW' then 'ICEBERG' 
			  end as kat_hasil3,
			CASE WHEN kat_asesmen = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' WHEN kat_asesmen = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
			WHEN kat_asesmen = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
			WHEN kat_asesmen = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
		ELSE 'NON TALENT' 
		end AS kat_talent1,
		CASE WHEN kat_potensi = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' 
			WHEN kat_potensi = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
			WHEN kat_potensi = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
			WHEN kat_potensi = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
			ELSE 'NON TALENT' 
			end AS kat_talent2, 
		CASE WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'HIGH' THEN 'TALENT' 
			WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'HIGH' THEN 'TALENT' 
			WHEN kat_potensi2 = 'HIGH' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
			WHEN kat_potensi2 = 'MEDIUM' AND kat_pak = 'MEDIUM' THEN 'TALENT' 
			ELSE 'NON TALENT' 
			end AS kat_talent3
	  FROM 
		(
		  SELECT 
			a.nik, 
			a.nik_tetap,
			a.nama, 
			b.grade, 
			c.id_grad gradee, 
			To_char(b.tgl_pensiun, 'dd/mm/yyyy') tgl_pensiun, 
			c.id_cat,
			i.unit_name,
			h.kd_direktorat, 
			h.nm_direktorat, 
			Round(Avg(d.nilai_asesmen), 3) asesmen, 
			(SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat), 
			CASE 
				WHEN Round(Avg(d.nilai_asesmen), 5) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN Round(Avg(d.nilai_asesmen), 5) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN Round(       Avg(d.nilai_asesmen), 5) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_asesmen, 
			f.nilai_uji_kelayakan,
			j.nilai_uji_komite,
			Round(Avg(d.nilai_asesmen) * 0.6, 2) satu, 
			Round(Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150, 2) dua, 
			Round(Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite, 2) tiga, 
			Round(((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)),3) potensi1,
			CASE 
				WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150)) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_potensi, 
			Round(((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)),3) potensi2,
			CASE 
				WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)) > (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'HIGH' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite) ) BETWEEN (SELECT rentang_minimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) AND (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'MEDIUM' 
			  WHEN ((Avg(d.nilai_asesmen) * 0.6) + (Avg(d.nilai_asesmen) * 0.3 * f.nilai_uji_kelayakan / 150) + (Avg(d.nilai_asesmen) * 0.1 * j.nilai_uji_komite)) < (SELECT rentang_maksimum FROM m_standar_nilai_potensi WHERE id_grade_cat = c.id_cat) THEN 'LOW' 
			  ELSE 'LOW' 
			end AS kat_potensi2, 
			e.nilai_sebelum, 
			e.nilai_sekarang, 
			f.nilai_uji_kelayakan, 
			Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) AS avg_pak, 
			(SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1),
			CASE WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 3) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 3) THEN 'HIGH' 
				WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 2) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 2) THEN 'MEDIUM' 
				WHEN Round((e.nilai_sekarang + e.nilai_sebelum) / 2, 2) BETWEEN (SELECT rentang_awal FROM m_standar_nilai_kinerja WHERE id = 1) AND (SELECT rentang_akhir FROM m_standar_nilai_kinerja WHERE id = 1) THEN 'LOW' 
				ELSE 'LOW' 
			end AS kat_pak 
		  FROM 
			m_users_karyawan a 
			JOIN d_list_karyawan_talenta b ON b.nik = a.nik 
			JOIN m_grades c ON c.nm_grade = b.grade 
			LEFT JOIN t_nilai_potensi_kkj d ON d.nik = b.nik AND d.id_kkj IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13) AND Date_part('year', d.created_at) = Date_part('year', CURRENT_DATE) 
			LEFT JOIN t_nilai_kinerja_pak e ON e.nik = b.nik AND Date_part('year', e.created_date) = Date_part('year', CURRENT_DATE) 
			left JOIN t_uji_kelayakan f ON f.nik = b.nik AND Date_part('year', f.created_date) = Date_part('year', CURRENT_DATE) 
			JOIN m_direktorat h ON h.kd_direktorat = b.kd_direktorat
			left join m_unit i on i.unit_id = a.unit_id 
			left join t_uji_komite j on j.nik = b.nik AND Date_part('year', j.created_date) = Date_part('year', CURRENT_DATE)
		  WHERE 
			Date_part('year', b.created_date) = Date_part('year', CURRENT_DATE) 
		  GROUP BY 
			a.nik, 
			a.nik_tetap,
			i.unit_name,
			a.nama, 
			b.grade, 
			c.id_grad, 
			f.nilai_uji_kelayakan, 
			b.tgl_pensiun, 
			c.id_cat, 
			e.nilai_sebelum, 
			h.nm_direktorat, 
			h.kd_direktorat, 
			e.nilai_sekarang,
			j.nilai_uji_komite
		) a 
	  ORDER BY 
		a.nama
	  ) zz";
		$query = $this->db->query($sql)->result();
		// var_dump($query);die();
		$data 			= [];
		$nik 	= [];
		foreach ($query as $index => $item) {
			if (is_int($item->nik)) {
				$nik[] = intVal($item->nik);
				$data[] 	= array(
					"nik" 			=> intVal($item->nik),
					"grade" 		=> $item->gradee,
					// "kd_departemen"	=> $item->grade,
					"nm_departemen"	=> $item->unit_name,
					"kd_direktorat"	=> $item->kd_direktorat,
					"nm_direktorat"	=> $item->nm_direktorat,
					"asesmen"		=> $item->asesmen,
					"kat_asesmen"	=> $item->kat_asesmen,
					"uji_komite"	=> $item->nilai_uji_komite,
					"satu"			=> $item->satu,
					"dua"			=> $item->dua,
					"tiga"			=> $item->tiga,
					"potensi"		=> $item->potensi2,
					"kat_potensi"	=> $item->kat_potensi2,
					"pak_sebelum"	=> $item->nilai_sebelum,
					"pak_setelah"	=> $item->nilai_sekarang,
					"kat_pak"		=> $item->kat_pak,
					"rata_rata"		=> $item->avg_pak,
					"matrix"		=> $item->kat_hasil3,
					"kat_talent"	=> $item->kat_talent3,
					"pensiun"		=> $item->tgl_pensiun,
					'created_at' 	=> date('Y-m-d H:i:s')

				);
				
			}
		}
		$this->db->trans_begin();
		$ids = implode("','", $nik);
		$this->db->query("delete from t_riwayat_manajemen_talenta where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$this->db->insert_batch('t_riwayat_manajemen_talenta', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			var_dump($this->db->error());die();
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
	public function get_riwayat_asesmen($tahun, $type, $grade)
	{
		// var_dump($type);
		// die();
		if ($type == 1) {
			$table = "t_riwayat_asesmen_saat_ini";
		} elseif ($type == 2) {
			$table = "t_riwayat_asesmen_promosi";
		} elseif ($type == 4) {
			$table = "t_riwayat_asesmen_promosi_grade1_domestic";
		} elseif ($type == 3) {
			$table = "t_riwayat_asesmen_promosi_grade1_global";
		}

		$sql = "SELECT c.nik,a.nama,c.grade,d.nm_grade, c.id_direktorat,c.nm_direktorat,c.rekomendasi,c.job_fit,c.grafik, c.id_departemen,c.nm_departemen from m_users_karyawan a
		join d_list_karyawan_talenta b on a.nik = b.nik
		join $table c on c.nik = a.nik
		join m_grades d on d.id_grad = c.grade
		where date_part('year', c.created_at) = '$tahun' and d.id_cat = '$grade'";
		// var_dump($sql);
		// die();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade_cat')
			->get('m_grade_cat');

		return $query->result();
	}
	public function get_jobFamily()
	{
		$query = $this->db->order_by('id', 'ASC')
			->get('m_fitjob_fam')->result();

		return $query;
	}
	public function get_sub_grade($grade)
	{
		$sub_grade = $this->db->where('id_cat', $grade)
			->order_by('nm_grade')
			->get('m_grades');

		$data['sub_grade'] = "<option value=''>--Pilih Sub Grade--</option>";
		foreach ($sub_grade->result() as $grade_cat) {
			$data['sub_grade'] .= '<option value="' . $grade_cat->id_grad . '">' . $grade_cat->nm_grade . '</option>';
		}

		return $data;
	}

	public function get_riwayat_manajemen_talenta($tahun, $grade)
	{
		$sql = $this->db->query("SELECT * 
		FROM t_riwayat_manajemen_talenta a
		join d_list_karyawan_talenta b on b.nik = a.nik
		join m_users_karyawan c on c.nik = a.nik
		left join m_grades d on d.nm_grade = b.grade
		where Date_part('year', a.created_at) = '$tahun' and d.id_cat = '$grade'
		");

		return $sql->result();
	}
	public function get_riwayat_pdf($tahun, $grade)
	{
		$qry = "select a.nik,a.nama,b.unit_name,d.nm_fitjob_fam,e.nm_grade,f.file_path pdf_asesmen,g.file_path pdf_tm
		from m_users_karyawan a
		left join d_unit_job_family c on a.unit_id = c.unit_id
		left join m_unit b on b.unit_id = a.unit_id
		left join m_fitjob_fam d on d.id = c.id_job_family and d.active = true
		join m_grades e on e.nm_grade = a.grade_personal
		left join t_pdf_laporan f on f.nik = a.nik and date_part('year', f.created_at) = '$tahun'
		left join t_pdf_minat_bakat g on g.nik = a.nik and date_part('year', g.created_at) = '$tahun'
		where e.id_cat = '$grade'";
		$query = $this->db->query($qry);
		// pr($query->result_id->queryString);
		return $query->result();
	}
	public function get_riwayat_minat_bakat($tahun,$grade)
	{
		$sql = "SELECT a.nik_tetap,a.nik, b.nama,b.grade_personal,
		round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen)) ,0) as x, h.nilai_psp as y, j.nilai_pss,i.nm_fitjob_fam
		from m_users_karyawan b
			join t_nilai_potensi_kkj a on b.nik = a.nik and date_part('year', a.created_at) = '$tahun'
			join d_unit_job_family c on b.unit_id = c.unit_id 
			join m_fitjob_fam d on d.id = c.id_job_family and d.active = true 
			join m_grades e on e.nm_grade = b.grade_personal and e.id_cat = '3' 
			join m_kkj f on f.id_kkj = a.id_kkj and f.active = true 
			join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_cat
			join t_minat_bakat_psp h on h.nik = b.nik and h.id_job_family = d.id and date_part('year', h.created_at) = '$tahun'
			left join m_fitjob_fam i on i.id = h.id_job_family
			left join t_minat_bakat_pss j on j.id_job_family = i.id and j.nik = b.nik and date_part('year', h.created_at) = '$tahun'
		where e.id_cat = '$grade'
		group by a.nik_tetap,b.nama,b.grade_personal,h.nilai_psp,j.nilai_pss,i.nm_fitjob_fam,a.nik
		order by b.nama";
		return $this->db->query($sql)->result();

	}
}
