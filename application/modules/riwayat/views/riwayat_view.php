<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Riwayat Karyawan</strong>
                        </div>
                        <div class="m-t-30 col-md-12">
                            <div class="col col-md-2">
                                <label for="grade" class=" form-control-label">Tahun</label>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" autocomplete="false" id="txt_tahun">
                                    </div>
                                    <div class="col-md-4">
                                        <button id="btnCari" class="btn btn-primary">cari</button>
                                    </div>
                                    <div class="text-right col-md-4">
                                        <button id="btnUpdate" class="btn btn-success">update data tahun ini</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 form-control-label" for="">Grade</label>
                                <div class="col-md-12">
                                    <div class="row ">
                                        <div class="col-md-3">
                                            <select name="grade" required data-size="1" id="grade" class="form-control">
                                                <option value="">--Pilih Sub Grade--</option>|
                                                <?php foreach ($grades_cat as $cat) {
                                                    echo '<option value="' . $cat->id_grad_cat . '">' . $cat->nm_grade_cat . '</option>';
                                                } ?>
                                            </select>
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <select name="grade" data-live-search="true" data-size="1" id="sub_grade" class="form-control">
                                                <option value="">--Pilih Sub Grade--</option>
                                            </select>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col col-md-2">
                                    <label for="select" class="form-control-label">Rekomendasi</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <select name="rekomendasi" required id="rekomendasi" class="form-control">
                                        <option value=''>--Pilih Jenis Rekomendasi--</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="custom-tab">

                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Asesmen</a>
                                        <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Manajemen Talenta</a>
                                        <a class="nav-item nav-link" id="custom-nav-contact-tab" data-toggle="tab" href="#custom-nav-contact" role="tab" aria-controls="custom-nav-contact" aria-selected="false">Minat Bakat</a>
                                        <a class="nav-item nav-link" id="custom-nav-contact-tab" data-toggle="tab" href="#custom-nav-pdf" role="tab" aria-controls="custom-nav-pdf" aria-selected="false">PDF</a>
                                    </div>
                                </nav>
                                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="custom-nav-home" style="padding:15px;background-color: beige;" role="tabpanel" aria-labelledby="custom-nav-home-tab">

                                        <!-- <div class="row form-group">
                                            <label class="col-md-2 form-control-label" for="">Rekomendasi</label>
                                            <div class="col-md-3">
                                                <select name="" class="form-control" id="">
                                                    <option value="1">Jabatan Saat Ini</option>
                                                    <option value="2">Jabatan Saat Promosi</option>
                                                </select>
                                            </div>
                                        </div> -->
                                        <table class="table table-data3 table-responsive text-nowrap" width="100%" id="tabel_riwayat_asesmen">
                                            <thead class="table-borderless" style="background-color: cadetblue;color: aliceblue;">
                                                <tr>
                                                    <th width="1%" class="text-center">No</th>
                                                    <th>Nik</th>
                                                    <th>Nama</th>
                                                    <th class="text-center">Grade</th>
                                                    <th>Departemen</th>
                                                    <th>Direktorat</th>
                                                    <th class="text-center">Rekomendasi</th>
                                                    <th class="text-center">Job Fit</th>
                                                    <th class="text-center">Grafik</th>
                                                </tr>
                                            </thead>
                                            <tbody class="table-bordered">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                        <table class="table table-data3 table-responsive text-nowrap" width="100%" id="tabel_riwayat_manajemen_talenta">
                                            <thead class="table-borderless" style="background-color: #358f55;color: aliceblue;">
                                                <tr>
                                                    <th width="1%" class="text-center">No</th>
                                                    <th>Nik</th>
                                                    <th>Nama</th>
                                                    <th class="text-center">Grade</th>
                                                    <th>Departemen</th>
                                                    <th>Direktorat</th>
                                                    <th class="text-center">Asesmen</th>
                                                    <th class="text-center">Kat Asesmen</th>
                                                    <th class="text-center">Uji Komite</th>
                                                    <th class="text-center">60%</th>
                                                    <th class="text-center">30%</th>
                                                    <th class="text-center">10%</th>
                                                    <th class="text-center">Potensi</th>
                                                    <th class="text-center">Kat Potensi</th>
                                                    <th class="text-center">PAK <?php echo date("Y", strtotime("-1 year", time())); ?></th>
                                                    <th class="text-center">PAK <?php echo date("Y"); ?></th>
                                                    <th class="text-center">Rata-rata</th>
                                                    <th class="text-center">Kat PAK</th>
                                                    <th>Matrix</th>
                                                    <th>Kat Talent</th>
                                                    <th>Pensiun</th>
                                                </tr>
                                            </thead>
                                            <tbody class="table-bordered">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="custom-nav-contact" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                                        <table class="table table-data3 table-responsive text-nowrap" width="100%" id="tabel_riwayat_minat_bakat">
                                            <thead class="table-borderless" style="background-color: cadetblue;color: aliceblue;">
                                                <tr>
                                                    <th width="1%" class="text-center">No</th>
                                                    <th>Nik</th>
                                                    <th>Nama</th>
                                                    <th class="text-center">Grade</th>
                                                    <th class="text-center">Job Fit Family</th>
                                                    <th class="text-center">Kompetensi</th>
                                                    <th class="text-center">Lihat</th>
                                                </tr>
                                            </thead>
                                            <tbody class="table-bordered">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="custom-nav-pdf" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                                        <table class="table table-borderless table-data3" id="tabelPdf">
                                            <thead>
                                                <tr class="">
                                                    <th>NO</th>
                                                    <th>NIK</th>
                                                    <th>Nama</th>
                                                    <th>Grade</th>
                                                    <th>Departemen</th>
                                                    <th>Job Family</th>
                                                    <th>Pdf Asesmen</th>
                                                    <th>Pdf Minat Bakat</th>
                                                    <!-- <th>Aksi</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="nilaiPSP" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai PSP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-borderless table-data3" id="tabelPdf">
                    <thead>
                        <tr class="">
                            <th>NO</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Grade</th>
                            <th>Departemen</th>
                            <th>Job Family</th>
                            <th>Pdf Asesmen</th>
                            <th>Pdf Minat Bakat</th>
                            <!-- <th>Aksi</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="nilaiPotensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="canvas_father">
                    <input type="hidden" value="<?php echo base_url() ?>" id="baseUrl">
                    <object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/riwayat_view.js"></script>