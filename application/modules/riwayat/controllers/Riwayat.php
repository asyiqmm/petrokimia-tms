<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Riwayat extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('riwayat_model');
		$this->load->model('overview/overview_model');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Riwayat | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}
	public function updateData()
	{
		$data1 = $this->riwayat_model->updateData();
		$data2 = $this->riwayat_model->update_talenta();
		// echo json_encode($data);

	}
	public function index()
	{
		if (in_array(12, $_SESSION['session_menu'])) {
			$data['grades_cat'] = $this->riwayat_model->get_grade();
			$data['departemens'] = $this->overview_model->get_departemen();
			$data['job_familys'] = $this->riwayat_model->get_jobFamily();
			$data['parent_active'] = 12;
			$data['child_active'] = 12;
			$this->_render('riwayat_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function getRiwayat_asesmen()
	{
		$tahun = $this->input->post('tahun');
		$type = $this->input->post('type');
		$grade = $this->input->post('grade');

		$data = $this->riwayat_model->get_riwayat_asesmen($tahun, $type, $grade);
		$obj = array("data" => $data);


		echo json_encode($obj);
	}
	public function getRiwayat_manajemen_talenta()
	{
		$tahun = $this->input->post('tahun');
		$grade = $this->input->post('grade');

		$data = $this->riwayat_model->get_riwayat_manajemen_talenta($tahun, $grade);
		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function getRiwayat_minat_bakat()
	{
		
		$tahun = $this->input->post('tahun');
		$grade = $this->input->post('grade');

		$query = $this->riwayat_model->get_riwayat_minat_bakat($tahun,$grade);
		$data =array();
		foreach ($query as $index => $item) {
			$btn = '<button  type="button" onclick="detailPSP(\'' . $item->nik . '\')" class="btn btn-primary btn-sm"  title="Lihat"> <i class="fa fa-eye"></i> Lihat </button>';
			$data[] 	= array(
				"nama" 				=> $item->nama,
				"nik" 				=> $item->nik,
				"grade_personal"	=> $item->grade_personal,
				"job_fit"	=> $item->nm_fitjob_fam,
				"x"		=> $item->x,
				"btn"				=> $btn
			);
		}
		$obj = array("data" => $data);
		echo json_encode($obj);
	}
	public function getRiwayat_pdf()
	{
		
		$tahun = $this->input->post('tahun');
		$grade = $this->input->post('grade');
		$query = $this->riwayat_model->get_riwayat_pdf($tahun,$grade);
		$datas 			= array();


		foreach ($query as $index => $item) {
			$pdf_asesmen = '<button class="btn btn-primary" onclick="modalPDF(\'' . $item->pdf_asesmen . '\')"><i class="far fa-file-pdf"></i> pdf</button>';
			$pdf_tm = '<button class="btn btn-primary" onclick="modalPDF(\'' . $item->pdf_tm . '\')"><i class="far fa-file-pdf"></i> pdf</button>';

			$datas[] 	= array(
				"no" 			=> null,
				"nama" 			=> $item->nama,
				"nik" 			=> $item->nik,
				"nm_grade"		=> $item->nm_grade,
				"nm_dept"		=> $item->unit_name,
				"nm_fitjob_fam"	=> $item->nm_fitjob_fam,
				"pdf_asesmen" 	=> ($item->pdf_asesmen !== null ? $pdf_asesmen : 'null'),
				"pdf_minat_bakat" => ($item->pdf_tm !== null ? $pdf_tm : 'null'),
			);
		}
		$obj = array("data" => $datas);
		echo json_encode($obj);
	}
	public function getSubGrade()
	{
		$grade = $this->input->get('grade');
		$query = $this->riwayat_model->get_sub_grade($grade);
		echo json_encode($query['sub_grade']);
	}
}
