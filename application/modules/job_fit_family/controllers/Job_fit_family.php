<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Job_fit_family extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->model('family_model');
		$this->load->model('job_fit_karyawan/job_fit_karyawan_model');
		$this->load->library('session');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Rekap Job Family | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		if (in_array(3, $_SESSION['session_menu'])) {
			$data['grades_cat'] = $this->family_model->get_grade();
			$data['parent_active'] = 3;
			$data['child_active'] = 3;
			$this->_render('jobFitFamily_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}
	public function export()
	{
		$grade = $this->input->get('grade');
		$job_family = $this->input->get('job_family');
		$rekomendasi = $this->input->get('rekomendasi');
		$tahun = $this->input->get('tahun');

		$date = date('Y', strtotime('-1 years'));

		//make a new spreadsheet object
		$spreadsheet = new Spreadsheet();

		$spreadsheet->getProperties()->setCreator('Pengembangan Personal')
			->setLastModifiedBy('Bangpesr')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('HAV MATRIX')
			->setKeywords('HAV MATRIX')
			->setCategory('HAV');
		//Retrieve Highest Column (e.g AE)

		$styleArrayFirstRow = [
			'font' => [
				'bold' => true,
			]
		];
		$kriteria = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();

		$job_family = $this->db->order_by('id', 'ASC')->get('m_fitjob_fam')->result();

		$spreadsheet->createSheet();
		foreach ($job_family as $key => $value) {
			$spreadsheet->setActiveSheetIndex($key);
			$sheet = $spreadsheet->getActiveSheet();
			// $sheet->setCellValue('A1', 'Hello world');
			$invalidCharacters = $sheet->getInvalidCharacters();
			$nm_job_fam = substr($value->nm_fitjob_fam, 0, 30);
			$title = str_replace($invalidCharacters, '', $nm_job_fam);
			$sheet->setCellValue('A1', 'NO')
				->setCellValue('B1', 'NIK')
				->setCellValue('C1', 'NAMA')
				->setCellValue('D1', 'GRADE')
				->setCellValue('E1', 'JABATAN SAAT INI')
				->setCellValue('F1', 'JOB FAMILY SAAT INI')
				->setCellValue('G1', 'JOB FAMILY TUJUAN')
				->setCellValue('H1', '%JOB FIT')
				->setCellValue('I1', 'Rekomendasi');

			$data = $this->family_model->get_listKaryawan($grade, null, null, $value->id, $tahun, $rekomendasi);
			// var_dump($data);die();

			$row = 3;
			foreach ($data['data'] as $key => $value) {
				if ($value->job_fit >= $data['kriteria'][2]->nilai_angka) {
					if ($value->nilai_ki >= $value->nilai_standar) {
						$rek = 'Disarankan';
					} else {
						$rek = 'Disarankan Dengan Beberapa Pengembangan';
					}
				} elseif ($value->job_fit >= $data['kriteria'][0]->nilai_angka && $value->job_fit <= $data['kriteria'][1]->nilai_angka) {
					if (($value->nilai_standar - $value->nilai_ki) <= 1) {
						$rek = 'Disarankan Dengan Beberapa Pengembangan';
					} else {
						$rek = 'Tidak Disarankan';
					}
				} elseif ($value->job_fit < $data['kriteria'][0]->nilai_angka) {
					$rek = 'Tidak Disarankan';
				}
				$sheet->setCellValue('A' . $row, $key + 1);
				$sheet->setCellValue('B' . $row, $value->nik);
				$sheet->setCellValue('C' . $row, $value->nama);
				$sheet->setCellValue('D' . $row, $value->grade);
				$sheet->setCellValue('E' . $row, $value->nm_jabatan);
				$sheet->setCellValue('F' . $row, $value->job_family_saat_ini);
				$sheet->setCellValue('G' . $row, $value->nm_fitjob_fam);
				$sheet->setCellValue('H' . $row, $value->job_fit . '%');
				$sheet->setCellValue('I' . $row, $rek);
				$row++;
				$job_fam = $value->nm_fitjob_fam;
			}
			foreach (range('A', 'I') as $columnID) {
				$spreadsheet->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}
			$sheet->setTitle($title);
			$spreadsheet->createSheet();
		}
		$datetime = date('dmy');
		if ($rekomendasi == 2) {
			$filename = 'Longlist Job Family Grade ' . $grade . ' Saat Promosi ' . $datetime;
		} else {
			$filename = 'Longlist Job Family Grade ' . $grade . ' Saat Ini ' . $datetime;
		}

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
	public function get_rekomendasi()
	{
		$grade = $this->input->post('grade');
		if ($grade) {
			$query = $this->family_model->get_rekomendasi($grade);
			echo json_encode($query);
		}
	}

	public function get_listAsesmen()
	{
		$grade 			= $_POST['grade'];
		$sub_grade 			= $_POST['sub_grade'];
		$rekomendasi 	= $_POST['rekomendasi'];
		$job_family 	= $_POST['job_family'];
		$tahun 	= $_POST['tahun'];
		$tipe 	= $_POST['tipe'];

		$query = $this->family_model->get_listKaryawan($grade, $sub_grade, $rekomendasi, $job_family, $tahun, $tipe);
		$datas 			= array();
		foreach ($query['data'] as $index => $item) {
			// var_dump($item->nik);die();
			if ($item->job_fit >= $query['kriteria'][2]->nilai_angka) {
				if ($item->nilai_ki >= $item->nilai_standar == true) {
					$rekomendasi = 'Disarankan';
				} else {
					$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
				}
			} elseif ($item->job_fit >= $query['kriteria'][0]->nilai_angka && $item->job_fit <= $query['kriteria'][1]->nilai_angka) {
				if (($item->nilai_standar - $item->nilai_ki) <= 1) {
					$rekomendasi = 'Disarankan Dengan Beberapa Pengembangan';
				} else {
					$rekomendasi = 'Tidak Disarankan
				';
				}
			} elseif ($item->job_fit < $query['kriteria'][0]->nilai_angka) {
				$rekomendasi = 'Tidak Disarankan';
			}

			$datas[] 	= array(
				"no" => "",
				"nama" => $item->nama,
				"nik" 			=> $item->nik,
				"rekomendasi" => $rekomendasi,
				"total" => $item->job_fit . "%",
			);
		}
		$obj = array("data" => $datas);
		echo json_encode($obj);
	}

	public function get_rek($nik, $grade, $job)
	{

		$query = $this->jobFitKaryawan_model->get_grafikAsesmen($nik, $grade, $job);

		$data = array();
		foreach ($query as $key => $value) {
			if ($query[$key]->nilai == 0) {
				$data[] = array(
					"perhitungan" => 0,
					"nik" => $query[$key]->nik
				);
			} else {
				$data[] = array(
					"perhitungan" => $query[$key]->nilai_asesmen / $query[$key]->nilai,
					"nik" => $query[$key]->nik
				);
			}
		}
		$total = 0;
		foreach ($data as $key => $value) {
			$total = $data[$key]['perhitungan'] + $total;
		}
		$hasil = intval(($total / count($data)) * 100);
		return $hasil;
	}
}
