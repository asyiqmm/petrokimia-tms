<div class="main-content"  style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            

            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                        <h3 class="title-2 m-b-40">Rekap Job Fit Per Family</h3>

                            <form method="POST" id="btnCari" enctype="multipart/form-data">
                                <div class="row">
                                    <div class=" col-md-12">
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="txt_tahun" class=" form-control-label">Tahun</label>
                                            </div>
                                            <div class="col-12 col-md-2">
                                                <input type="text" required placeholder="Tahun" class="form-control" autocomplete="off" id="txt_tahun">

                                            </div>

                                            <div class="col-12 col-md-2 text-right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-search"></i> Cari
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Grade</label>
                                            </div>

                                            <div class="col-12 col-md-4">
                                                <select name="grade" required data-live-search="true" data-size="1" id="grade" class="form-control">
                                                    <option value="">--Pilih Grade--</option>

                                                    <?php
                                                    foreach ($grades_cat as $cat) {
                                                        echo '<option value="' . $cat->id_grad_cat . '">' . $cat->nm_grade_cat . '</option>';
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                            <div class="col-12 col-md-4">
                                                <select name="grade" data-live-search="true" data-size="1" id="sub_grade" class="form-control">
                                                    <option value="">--Pilih Sub Grade--</option>

                                                </select>

                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Rekomendasi</label>
                                            </div>
                                            <div class="col-12 col-md-8">
                                                <select name="rekomendasi" required id="rekomendasi" class="form-control">
                                                    <option value=''>--Pilih Jenis Rekomendasi--</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Job Family</label>
                                            </div>
                                            <div class="col-12 col-md-8">
                                                <select name="jobFitFamily" required data-size="5" id="jobFitFamily" class="form-control">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="select" class=" form-control-label">Download</label>
                                            </div>

                                            <div class="col col-md-4">

                                                <button type="button" id="btnSaatIni" title="Download Exel" data-toggle="modal" data-target="#exampleModal" class="col btn btn-warning">
                                                    <i class="fa fa-download">Download Rekomendasi Saat ini</i>
                                                </button>
                                            </div>
                                            <div class="col col-md-4">
                                                <button type="button" id="btnPromosi" title="Download Exel" data-toggle="modal" data-target="#exampleModal" class="col btn btn-warning">
                                                    <i class="fa fa-download">Download Rekomendasi Saat Promosi</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <!-- <div class="table-responsive table-data"> -->
                            <table class="table" id="tabelAsesmen">
                                <thead>
                                    <tr>
                                        <td width="10%">no</td>
                                        <td width="30%">name</td>
                                        <td width="20%">nik</td>
                                        <td width="10%">Job Fit</td>
                                        <td width="30%">rekomendasi</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/jobFitFamily.js"></script>