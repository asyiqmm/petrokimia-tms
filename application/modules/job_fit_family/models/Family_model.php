<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Family_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade_cat')
			->get('m_grade_cat');

		return $query->result();
	}
	public function get_rekomendasi($grade)
	{
		$query = $this->db->query("SELECT a.id_rek_grade,b.id_grad,(select nm_grade from m_grades where id_grad = a.id_rek_grade limit 1),a.keterangan from d_rekomendasi_grade a
		join m_grades b on b.id_grad = a.id_grade
		where b.id_grad = '$grade'");
		$output = "<option value=''>--Pilih Jenis Rekomendasi--</option>";
		foreach ($query->result() as $rek) {
			$output .= '<option value="' . $rek->id_rek_grade . '">' . $rek->keterangan . ' (' . $rek->nm_grade . ')</option>';
		}
		return $output;
	}

	public function cek_rekomendasi($grade)
	{
		$query = 'select * 
		from d_rekomendasi_grade a
		where a.id_grade = ' . $grade . '  and a."type" =2';
		var_dump($query);
		die();
		return $this->db->query($query)->result();
	}

	public function get_nilai($grade, $rekomendasi, $job_family, $kriteria, $tahun)
	{
		if ($rekomendasi == '1') {
			$rek = ' .id_cat ';
		}else {
			$rek = ' .id_promosi ';
		}
		$query = "SELECT a.nik, b.nama,e.nm_grade,b.unit_id,h.id job_family_id, j.nm_fitjob_fam job_family, i.nm_jabt,
		sum(a.nilai_asesmen / nullif(g.nilai,0)) as nilai_asesmen, h.nm_fitjob_fam,
		round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) as total,
		case 
			when round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) >= " . $kriteria[2]->nilai_angka . " then 'Disarankan'
			when round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) between " . $kriteria[0]->nilai_angka . " and " . $kriteria[1]->nilai_angka . " then 'Disarankan Dengan Beberapa Pengembangan'
			when round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen))	,0) < " . $kriteria[0]->nilai_angka . " then 'Tidak Disarankan'
		end as rekomendasi
		from t_nilai_potensi_kkj a
			join m_users_karyawan b on b.nik = a.nik
			join d_list_karyawan_talenta c on b.nik = c.nik 
			left join m_grades e on e.nm_grade = b.grade_personal
			join m_kkj f on f.id_kkj = a.id_kkj and f.active = true
			join standar_kkj g on g.id_kkj = f.id_kkj and g.id_grade = '$rekomendasi' and g.id_fitjob_fam = '$job_family' and g.active = true
			join m_fitjob_fam h on h.id = g.id_fitjob_fam
			left join m_jabatans i on b.grade = i.kd_jabatan
			left join m_fitjob_fam j on j.id = c.job_family
		where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) and e.id_cat = $grade
		group by a.nik,b.nama,h.nm_fitjob_fam,e.nm_grade,b.unit_id,h.id,j.nm_fitjob_fam, i.nm_jabt
		order by a.nik";
		var_dump($query);die();
		return $this->db->query($query)->result();
	}

	public function get_listKaryawan($grade, $sub_grade, $rekomendasi, $job_family, $tahun, $tipe)
	{
		$th = substr($tahun, -2);
		if (!empty($sub_grade)) {
			$query = "and e.id_grad = '$sub_grade'";
		} else {
			$query = "and e.id_cat = '$grade'";
		}

		if ($tipe == 2) {
			$qqq = "SELECT
							AA.NIK,
							II.NAMA,
							GG.NILAI_ING nilai_ki,
							HH.NILAI_ING nilai_standar,
							JJ.unit_name,
							AA.grade,
							KK.nm_fitjob_fam,
							EE.nm_jabatan,
							LL.nm_fitjob_fam job_family_saat_ini,
							ROUND(100 *(SUM(BB.NILAI_ASESMEN / NULLIF(FF.NILAI, 0)) / COUNT(BB.NILAI_ASESMEN)), 0) AS job_fit
						FROM
							D_LIST_KARYAWAN_TALENTA AA
						JOIN T_NILAI_POTENSI_KKJ BB ON
							BB.NIK = AA.NIK
							AND DATE_PART('YEAR', BB.CREATED_AT) = '$tahun'
						JOIN M_KKJ CC ON
							CC.ID_KKJ = BB.ID_KKJ
							AND CC.TAHUN = '$th'
						JOIN M_GRADES DD ON
							DD.NM_GRADE = AA.GRADE
							join m_jabatan EE on
				EE.jabatan = AA.jabatan
						JOIN STANDAR_KKJ FF ON
							FF.ID_KKJ = BB.ID_KKJ
							AND FF.KATEGORI = 1
							AND FF.ID_GRADE = DD.id_promosi
							AND FF.ID_FITJOB_FAM = $job_family
							AND DATE_PART('YEAR', FF.CREATED_AT) = '$tahun'
							AND FF.ACTIVE = TRUE
						JOIN T_ING_KKJ GG ON
							GG.NIK = AA.NIK
							AND GG.TAHUN = '$tahun'
						JOIN T_ING_STANDAR HH ON
							HH.ACTIVE = TRUE
							AND HH.ID_FITJOB_FAM = $job_family
							AND HH.ID_GRADE = DD.id_promosi
							AND HH.KATEGORI = 1
							AND HH.TAHUN = $tahun
						JOIN M_USERS_KARYAWAN II ON
							II.NIK = AA.NIK
						join m_unit JJ on 
							JJ.unit_id = AA.kd_departemen
						join m_fitjob_fam KK on KK.id = HH.id_fitjob_fam
						join m_fitjob_fam LL on LL.id = AA.job_family
						WHERE
							AA.TAHUN = '$tahun'
							AND DD.ID_CAT = '$grade'
						GROUP BY
							AA.NIK,
							GG.NILAI_ING,
							HH.NILAI_ING,
							II.NAMA,
							JJ.unit_name,
							AA.grade,
							KK.nm_fitjob_fam,
							EE.nm_jabatan,
							LL.nm_fitjob_fam";
		} else {
			if ($grade == 6 || $grade == 7) {
				# code...

				$qqq = "SELECT
							AA.NIK,
							II.NAMA,
							GG.NILAI_ING nilai_ki,
							HH.NILAI_ING nilai_standar,
							JJ.unit_name,
							AA.grade,
							KK.nm_fitjob_fam,
							EE.nm_jabatan,
							LL.nm_fitjob_fam job_family_saat_ini,
							ROUND(100 *(SUM(BB.NILAI_ASESMEN / NULLIF(FF.NILAI, 0)) / COUNT(BB.NILAI_ASESMEN)), 0) AS job_fit
						FROM
							D_LIST_KARYAWAN_TALENTA AA
						JOIN T_NILAI_POTENSI_KKJ BB ON
							BB.NIK = AA.NIK
							AND DATE_PART('YEAR', BB.CREATED_AT) = '$tahun'
						JOIN M_KKJ CC ON
							CC.ID_KKJ = BB.ID_KKJ
							AND CC.TAHUN = '$th'
						JOIN M_GRADES DD ON
							DD.NM_GRADE = AA.GRADE
						join m_jabatan EE on EE.jabatan = AA.jabatan
						JOIN STANDAR_KKJ FF ON
							FF.ID_KKJ = BB.ID_KKJ
							AND FF.KATEGORI = 2
							AND FF.ID_GRADE = $grade
							AND FF.ID_FITJOB_FAM = $job_family
							AND DATE_PART('YEAR', FF.CREATED_AT) = '$tahun'
							AND FF.ACTIVE = TRUE
						JOIN T_ING_KKJ GG ON
							GG.NIK = AA.NIK
							AND GG.TAHUN = '$tahun'
						JOIN T_ING_STANDAR HH ON
							HH.ACTIVE = TRUE
							AND HH.ID_FITJOB_FAM = $job_family
							AND HH.ID_GRADE = $grade
							AND HH.KATEGORI = 2
							AND HH.TAHUN = $tahun
						JOIN M_USERS_KARYAWAN II ON
							II.NIK = AA.NIK
						join m_unit JJ on 
							JJ.unit_id = AA.kd_departemen
						join m_fitjob_fam KK on KK.id = HH.id_fitjob_fam
						join m_fitjob_fam LL on LL.id = AA.job_family
						WHERE
							AA.TAHUN = '$tahun'
							AND DD.ID_CAT = '$grade'
						GROUP BY
							AA.NIK,
							GG.NILAI_ING,
							HH.NILAI_ING,
							II.NAMA,
							JJ.unit_name,
							AA.grade,
							KK.nm_fitjob_fam,
							EE.nm_jabatan,
							LL.nm_fitjob_fam";
			} else {
				$qqq = "SELECT
				AA.NIK,
				II.NAMA,
				GG.NILAI_ING nilai_ki,
				HH.NILAI_ING nilai_standar,
				JJ.unit_name,
				AA.grade,
				KK.nm_fitjob_fam,
				EE.nm_jabatan,
				LL.nm_fitjob_fam job_family_saat_ini,
				round(100 *(sum(BB.NILAI_ASESMEN / nullif(FF.NILAI, 0)) / count(BB.NILAI_ASESMEN)), 0) as job_fit
			from
				D_LIST_KARYAWAN_TALENTA AA
			join T_NILAI_POTENSI_KKJ BB on
				BB.NIK = AA.NIK
				and date_part('YEAR', BB.CREATED_AT) = '$tahun'
			join M_KKJ CC on
				CC.ID_KKJ = BB.ID_KKJ
				and CC.TAHUN = '$th'
			join M_GRADES DD on
				DD.NM_GRADE = AA.GRADE
			join m_jabatan EE on
				EE.jabatan = AA.jabatan
			join STANDAR_KKJ FF on
				FF.ID_KKJ = BB.ID_KKJ
				and FF.KATEGORI = EE.kategori
				and FF.ID_GRADE = $grade
				and FF.ID_FITJOB_FAM = $job_family
				and date_part('YEAR', FF.CREATED_AT) = '$tahun'
				and FF.ACTIVE = true
			join T_ING_KKJ GG on
				GG.NIK = AA.NIK
				and GG.TAHUN = '$tahun'
			join T_ING_STANDAR HH on
				HH.ACTIVE = true
				and HH.ID_FITJOB_FAM = $job_family
				and HH.ID_GRADE = $grade
				and HH.KATEGORI = EE.kategori
				and HH.TAHUN = '$tahun'
			join M_USERS_KARYAWAN II on
				II.NIK = AA.NIK
			join m_unit JJ on 
				JJ.unit_id = AA.kd_departemen
			join m_fitjob_fam KK on KK.id = HH.id_fitjob_fam
			join m_fitjob_fam LL on LL.id = AA.job_family
			where
				AA.TAHUN = '$tahun'
				and DD.ID_CAT = '$grade'
			group by
				AA.NIK,
				GG.NILAI_ING,
				HH.NILAI_ING,
				II.NAMA,
				JJ.unit_name,
				AA.grade,
				KK.nm_fitjob_fam,
				EE.nm_jabatan,
				LL.nm_fitjob_fam";
			}
		}
		// var_dump($qqq);die();
		$data['data'] = $this->db->query($qqq)->result();
		$data['kriteria'] = $this->db->query("SELECT * from m_tms_helper where active = true and kategori = 1 order by id")->result();
		return $data;
	}
	// public function get_listAsesmen($grade, $sub_grade)
	// {
	// 	var_dump($sub_grade == "");
	// 	die();
	// 	$query = $this->db->query("SELECT a.first_name,a.last_name,a.nik,b.id_grade,b.id_job_fam from users a
	// 	left join users_info b on a.nik = b.nik_users where b.id_grade = '$grade'");

	// 	return $query->result();
	// }

	// public function get_listKaryawan($draw, $start, $length, $search, $grade, $sub_grade, $rekomendasi, $job_family)
	// {
	// 	$query = "offset " . $start . " rows
	// 			fetch next " . $length . " rows only ";

	// 	if ($sub_grade !== '') {
	// 		$total = "from m_grades a 
	// 		join m_users_karyawan b on b.grade_personal = a.nm_grade 
	// 		where a.id_grad = '$sub_grade'";
	// 	} else if ($sub_grade == '') {
	// 		$total = "from m_grades a 
	// 		join m_users_karyawan b on b.grade_personal = a.nm_grade
	// 		join m_grade_cat c on c.id_grad_cat = a.id_cat
	// 		where c.id_grad_cat = '$grade'";
	// 	}
	// 	$baseSql = "select b.nik, b.nama " . $total . " order by b.nama ASC";

	// 	$sql = "" . $baseSql . " " . $query . "";
	// 	$sqlTotal = "select count(*) as tot " . $total;
	// 	// echo json_encode($sql);die();
	// 	$query = $this->db->query($sql);
	// 	$queryTot = $this->db->query($sqlTotal);

	// 	if ($queryTot) {
	//         $row = $queryTot->row();
	//         $tot = $row->tot;
	//     } else {
	//         $tot = 0;
	//         $res = [
	//             // 'sql'     => $sql,
	//             'success' => false,
	//             'tot'     => $tot,
	//             'data'    => null,
	//             'error'   => $this->db->error()
	//         ];
	//         return $res;
	//     }

	//     if (!$query) {
	//         $res = [
	//             'success' => false,
	//             'tot'     => $tot,
	//             'data'    => null,
	//             'error'   => $this->db->error()
	//             // 'sql'     => $sql
	//         ];
	//     } else {
	//         $res = [
	//             'success' => true,
	//             'tot'     => $tot,
	//             'data'    => $query->result()
	//         ];
	//         return $res;
	//     }
	// }

	public function karyawan_by_grade($grade)
	{
		$query = $this->db->select('b.nama,b.nik_tetap')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->join('m_grade_cat as c', 'c.id_grad_cat = a.id_cat')
			->where('c.id_grad_cat', $grade)
			->order_by('b.nama', 'ASC')
			->get();
		return $query->result();
	}

	public function karyawan_by_sub_Grade($sub_grade)
	{
		$query = $this->db->select('b.nama,b.nik')
			->from('m_grades as a')
			->join('m_users_karyawan as b', 'b.grade_personal = a.nm_grade')
			->where('a.id_grad', $sub_grade)
			->order_by('b.nama', 'ASC')
			->get();
		return $query->result();
	}
}
