<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Minat_bakat extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$this->load->library('session');
		$this->load->model('Minat_bakat_model');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Minat Bakat | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		if (in_array(28, $_SESSION['session_menu'])) {
			$data['job_family'] = $this->Minat_bakat_model->getJobFam();
			$data['parent_active'] = 28;
			$data['child_active'] = 28;
			$this->_render('minat_bakat_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_tabel()
	{
		$job_family = $this->input->post('job_family');
		$job_title = $this->input->post('job_title');

		// var_dump($job_family,$job_title);die();
		$query = $this->Minat_bakat_model->get_data($job_family, $job_title);
		$data =array();
		foreach ($query['data'] as $index => $item) {
			$btn = '<button  type="button" onclick="detail(\'' . $item->nik . '\')" class="btn btn-primary btn-sm"  title="Lihat"> <i class="fa fa-eye"></i> Lihat </button>';
			$data[] 	= array(
				"nama" 				=> $item->nama,
				"nik" 				=> $item->nik,
				"nik_tetap" 		=> $item->nik_tetap,
				"grade_personal"	=> $item->grade_personal,
				"x"		=> $item->x,
				"y"			=> $item->y,
				"nilai_pss"			=> $item->nilai_pss,
				"btn"				=> $btn
			);
		}
		$obj = $data;
		echo json_encode($obj);
		// echo json_encode($query['data']);
	}

	public function get_nilai()
	{
		$nik = $this->input->post('nik');
		$query['data'] = $this->Minat_bakat_model->get_nilai($nik);
		
		echo json_encode($query);
	}
}
