<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                        <h3 class="title-2 m-b-40">Minat Bakat</h2>
                            <form method="POST" id="btnCari" enctype="multipart/form-data">
                                <div class="row">
                                    <div class=" col-md-12">
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="job_family" class=" form-control-label">Job Family</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <select class="form-control" required name="job_family" id="job_family">
                                                    <option value=""></option>
                                                    <?php foreach ($job_family as $job2) {
                                                        echo '<option value="' . $job2->id . '">' . $job2->nm_fitjob_fam . '</option>';
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="job_title" class="form-control-label">Job Title</label>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <select name="job_title" required data-live-search="true" data-size="5" type="text" id="job_title" class="form-control">
                                                    <option value=""></option>
                                                    <?php foreach ($job_family as $job) {
                                                        echo '<option value="' . $job->id . '">' . $job->nm_fitjob_fam . '</option>';
                                                    } ?>
                                                </select>
                                            </div>
                                            <div class="col-12 col-md-2 text-right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-search"></i> Cari
                                                </button>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner" id="canvas_father">
                            <h3 class="title-2 m-b-40">Grafik Minat Bakat</h3>
                            <canvas id="chartku"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner" id="canvas_father2">
                            <h3 class="title-2 m-b-40">Tabel Minat Bakat</h3>


                            <!-- DATA TABLE-->
                            <div class="table-responsive">
                                <table class="table  table-borderless table-data3" width="100%" id="tabelNilai">
                                    <thead>
                                        <tr>
                                            <th width="1%">No</th>
                                            <th >NIK</th>
                                            <th >Nama</th>
                                            <th class="text-center">Grade</th>
                                            <th class="text-center">Kompetensi</th>
                                            <th class="text-center">Strength Potential</th>
                                            <th class="text-center">Strength Statement</th>
                                            <th width="1%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                                <!-- END DATA TABLE-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="nilaiPotensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Best Fit Berdasarkan Strength Potential</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-borderless table-data3" width="100%" id="tabelDetail">
                    <thead>
                        <tr style="background:#2860a8">
                            <th class="text-center">No</th>
                            <th>Job Title</th>
                            <th class="text-center">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/minat_bakat_view.js"></script>