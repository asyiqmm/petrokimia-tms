<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Minat_bakat_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_grade()
	{
		$query = $this->db->order_by('nm_grade_cat')
			->get('m_grade_cat');

		return $query->result();
	}


	public function getJobFam()
	{
		$query = $this->db->order_by('id', 'ASC')->get('m_fitjob_fam');

		return $query->result();
	}

	public function get_data($job_family, $job_title)
	{
		$this->db->trans_begin();
		$query = $this->db->query("SELECT a.nik_tetap,a.nik, b.nama,b.grade_personal,
		round(100*(sum(a.nilai_asesmen / nullif(g.nilai,0)) / count(a.nilai_asesmen)) ,0) as x, h.nilai_psp as y, j.nilai_pss,i.nm_fitjob_fam
		from m_users_karyawan b
			join t_nilai_potensi_kkj a on b.nik = a.nik and date_part('year', a.created_at) = date_part('year', CURRENT_DATE) 
			join d_unit_job_family c on b.unit_id = c.unit_id 
			join m_fitjob_fam d on d.id = c.id_job_family and d.active = true 
			join m_grades e on e.nm_grade = b.grade_personal and e.id_cat = '3' 
			join m_kkj f on f.id_kkj = a.id_kkj and f.active = true 
			join standar_kkj g on g.id_kkj = f.id_kkj and g.id_fitjob_fam = d.id and g.active = true and g.id_grade = e.id_cat
			join t_minat_bakat_psp h on h.nik = b.nik and h.id_job_family = '$job_title' and date_part('year', h.created_at) = date_part('year', CURRENT_DATE)
			left join m_fitjob_fam i on i.id = h.id_job_family
			left join t_minat_bakat_pss j on j.id_job_family = i.id and j.nik = b.nik and date_part('year', h.created_at) = date_part('year', CURRENT_DATE)
		where d.id = '$job_family'
		group by a.nik_tetap,b.nama,b.grade_personal,h.nilai_psp,j.nilai_pss,i.nm_fitjob_fam,a.nik
		order by b.nama ");
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'data' => $query->result()
			];
		}
	}

	function get_nilai($nik){
		$query = $this->db->select('row_number() OVER () as nomor,a.nilai_psp,b.nm_fitjob_fam')
		->from('t_minat_bakat_psp as a')
		->join('m_fitjob_fam as b','a.id_job_family = b.id')
		->where('a.nik',$nik)
		->group_by('a.nilai_psp,b.nm_fitjob_fam')
		->order_by('a.nilai_psp','desc')
		->limit(5)
		->get()->result();

		return $query;
	}
}
