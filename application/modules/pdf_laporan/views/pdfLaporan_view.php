<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Laporan Feedback Karyawan</strong>
                        </div>

                        <form id="insert_pdf_asesmen" class="form-horizontal">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <fieldset>
                                        <label id="button-pilihfile" class="btn btn-primary" for="pdfAsesmen">Pilih File..</label>
                                        <input type="file" multiple="multiple" hidden onchange="ListAsesmen(this);" accept=".pdf" name="Asesmenfiles[]" id="pdfAsesmen">
                                    </fieldset>
                                    <label>Selected files: </label>

                                    <p id="AsesmenList" style="max-height: 150px; overflow-y:auto"></p>
                                </div>
                                <div class="progress" style="display:none">
                                    <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                        <span class="sr-only">0%</span>
                                    </div>
                                </div>
                                <div class="msg alert text-left" style="display:none"></div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" id="btnsubmitAsesmen" class="btn btn-success btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <strong>Laporan Rekomendasi Karyawan</strong>
                        </div>

                        <form action="" method="post" id="insert_pdf_bakat" enctype="multipart/form-data" class="form-horizontal">

                            <div class="card-body card-block">
                                <div class="form-group">
                                    <fieldset>
                                        <label class="btn btn-primary" id="button-pilihfile2" for="pdfMinatBakat">Pilih File..</label>
                                        <input required type="file" multiple hidden onchange="ListMB()" accept=".pdf" name="MinatBakatfiles[]" id="pdfMinatBakat">
                                    </fieldset>
                                    <label>Selected files: </label>

                                    <p id="MBList" style="max-height: 150px; overflow-y:auto"></p>
                                </div>
                                <div class="progress2" style="display:none">
                                    <div id="progressBar2" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                        <span class="sr-only">0%</span>
                                    </div>
                                </div>
                                <div class="msg2 alert text-left" style="display:none"></div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" id="btnsubmitMB" name="fileSubmit" class="btn btn-success btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="au-card m-b-30">
                        <div class="au-card-inner">
                            <div class="row">
                                <div class=" col-md-12">
                                    <div class=" m-b-40">
                                        <table class="table table-responsive table-borderless table-data3" id="tabelPdf">
                                            <thead>
                                                <tr class="">
                                                    <th>NO</th>
                                                    <th>NIK</th>
                                                    <th>Nama</th>
                                                    <th>Grade</th>
                                                    <th>Departemen</th>
                                                    <th width="20%">Laporan Feedback Karyawan <button class="btn btn-danger" onclick="alert('asd')">hapus semua</button></th>
                                                    <th width="20%">Laporan Rekomendasi Karyawan</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- DATA TABLE-->

                    <!-- END DATA TABLE-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #form-container {
        padding: 20px 0;
    }

    #form-container h1 {
        font-size: 200%;
        text-align: center;
        margin-bottom: 50px;
    }
</style>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="nilaiPotensi" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai Asesmen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="canvas_father">
                    <input type="hidden" value="<?php echo base_url() ?>" id="baseUrl">
                    <object data="" id="viewPdf" width="100%" height="500px" type="application/pdf"> Hasil laporan PDF belum tersedia. </object>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/pdfLaporan_view.js"></script>