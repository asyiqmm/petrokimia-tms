<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pdf_laporan_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	function listPdfLaporan()
	{
		$qry = "SELECT
					A.NIK,
					A.NAMA,
					B.GRADE,
					C.UNIT_NAME,
					D.FILE_PATH pdf_asesmen_karyawan,
					E.FILE_PATH pdf_asesmen_sdm
				FROM
					M_USERS_KARYAWAN A
				JOIN D_LIST_KARYAWAN_TALENTA B ON
					B.NIK = A.NIK
					AND DATE_PART('YEAR', B.CREATED_DATE) = DATE_PART('YEAR', CURRENT_DATE)
				LEFT JOIN M_UNIT C ON
					C.UNIT_ID = A.UNIT_ID
				LEFT JOIN T_PDF_LAPORAN D ON
					D.NIK = A.NIK
					AND DATE_PART('YEAR', D.CREATED_AT) = DATE_PART('YEAR', CURRENT_DATE)
				LEFT JOIN T_PDF_LAPORAN_SDM E ON
					E.NIK = A.NIK
					AND DATE_PART('YEAR', E.CREATED_AT) = DATE_PART('YEAR', CURRENT_DATE)";
		$query = $this->db->query($qry);
		// pr($query->result_id->queryString);
		return $query->result();
	}
	public function deletepdfAsesmen($nik, $id)
	{
		$get = $this->db->query("SELECT file_path from t_pdf_laporan Where nik = '$nik'")->result();
		$query = "DELETE from t_pdf_laporan where nik = '$nik' and date_part('year', created_at) = date_part('year', CURRENT_DATE)";
		return $this->db->query($query);
	}
	public function deletepdfMB($nik, $id)
	{
		$query = "DELETE from t_pdf_laporan_sdm where nik = '$nik' and date_part('year', created_at) = date_part('year', CURRENT_DATE)";
		return $this->db->query($query);
	}

	function deletePdfLaporan($id)
	{
		$qry = "UPDATE users_info SET file_path ='' WHERE id = " . $id;
		// pr($qry);
		$query = $this->db->query($qry);
		return $query;
	}

	function save_upload($data)
	{
		$this->db->trans_begin();
		// $datax = [];
		// foreach ($data as $k => $v) {
		// 	$v['id_fitjob_fam'] = $jobfam;
		// 	$datax[] = $v;
		// }

		// $ids = implode("','", $nik);
		// $this->db->query("delete from nilai_potensi_kkj npk where nik IN ('" . $ids . "')");
		// $this->db->insert_batch('nilai_potensi_kkj', $data);
		// $insert = $this->db->insert_batch('t_pdf_laporan', $datax);
		$inse0rt = $this->db->insert_batch('users_info', $data);
		// $insert = $this->db->insert_batch('t_pdf_laporan', $data);
		// pr($insert);
		// pr($this->db->trans_status());

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	function updateUpload($id_fitjob_fam, $data)
	{
		// pr($data);
		// $this->db->trans_begin();
		// $this->db->where('id_fitjob_fam', $id_fitjob_fam);
		// $this->db->update_batch('users_info', $data, 'nik_users');
		$ret = array();
		$successNum = 0;
		$failedNum = 0;
		foreach ($data as $key => $val) {
			// cek 
			$qry2 = "SELECT * FROM users_info WHERE nik_users = '" . $val['nik_users'] . "' AND id_fitjob_fam='" . $id_fitjob_fam . "'";
			$query2 = $this->db->query($qry2);
			$num = $query2->num_rows();
			// pr($qry2);

			if ($num > 0) {
				// upd
				$qry = "UPDATE  users_info SET file_path='" . $val['file_path'] . "' 
						WHERE nik_users='" . $val['nik_users'] . "' AND id_fitjob_fam='" . $id_fitjob_fam . "'";
				$query = $this->db->query($qry);
				$detStatus = $query > 0 ? true : false;
				$successNum = $query > 0 ? ($successNum + 1) : ($failedNum + 1);
				$msg = 'file berhasil diunggah';
			} else {
				$detStatus = false;
				$failedNum++;
				$msg = ' tidak ditemukan ';
				unlink($val['file_path']);
			}

			$ret['detail'][] = [
				'status' => $detStatus,
				'msg' => $msg,
				'nik' => $val['nik_users'],
			];
		}
		// vd($successNum == 0 ? false : true);
		$ret['status'] = $successNum == 0 ? false : true;
		$ret['msg'] = $successNum == 0 ? 'gagal menyimpan semua data' : 'berhasil menyimpan ' . $successNum . ' data dan gagal menyimpan ' . $failedNum . ' data';
		return $ret;
	}

	public function get_job_family()
	{
		$query = $this->db->order_by('nm_fitjob_fam')
			->get('m_fitjob_fam');
		// pr($query);
		return $query->result();
	}

	//cek nik exist in database m_users_karyawan
	function isUserExist($nik)
	{
		return $this->db->where('nik', $nik)->get('m_users_karyawan');
	}

	//cek laporan exist in database t_pdf_laporan pada tahun sekarang
	function isLaporanExist($nik)
	{
		$qry = "SELECT id_pdf_laporan, file_path 
		FROM t_pdf_laporan
		WHERE nik = '$nik'
		AND date_part('year', created_at ) = date_part('year', CURRENT_DATE)";
		$query = $this->db->query($qry);
		return $query;
	}
	function isLaporanExist2($nik)
	{
		$qry = "SELECT id_pdf_minat_bakat, file_path 
		FROM t_pdf_laporan_sdm
		WHERE nik = '$nik'
		AND date_part('year', created_at ) = date_part('year', CURRENT_DATE)";
		$query = $this->db->query($qry);
		return $query;
	}
}
