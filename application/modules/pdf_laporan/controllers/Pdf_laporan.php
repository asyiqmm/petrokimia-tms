<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

class Pdf_laporan extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('pdf_laporan_model');
	}

	private function _render($view, $data = array())
	{
		$data['title'] = "Unggah Laporan PDF | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(10, $_SESSION['session_menu'])) {
			$data['job_family'] = $this->pdf_laporan_model->get_job_family();
			$data['parent_active'] = 10;
			$data['child_active'] = 10;
			$this->_render('pdfLaporan_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function upload()
	{

		// $data = file_get_contents('php://input');

		echo json_decode("hahah");
		exit();
		// pr($_FILES);
		if (!empty($_FILES['files']['name'])) {
			$filesCount = count($_FILES['files']['name']);

			for ($i = 0; $i < $filesCount; $i++) {
				$id_fitjob_fam = $_POST['id_fitjob_fam'];
				$fileName = $_FILES['files']['name'][$i];
				$nik = explode('.', $fileName)[0];
				$fileName2 = $nik . '_' . $id_fitjob_fam . '.pdf';
				// pr($fileName2);

				// $_FILES['file']['name']     = $_FILES['files']['name'][$i];
				$_FILES['file']['name']     = $fileName2;
				$_FILES['file']['type']     = $_FILES['files']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
				$_FILES['file']['error']     = $_FILES['files']['error'][$i];
				$_FILES['file']['size']     = $_FILES['files']['size'][$i];

				// File upload configuration
				$uploadPath = 'resources/pdf/asesmen/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'pdf';

				// Load and initialize upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				// Upload file to server
				// pr($uploadPath.$fileName2);
				// vd(file_exists($uploadPath . $fileName));

				if (file_exists($uploadPath . $fileName2)) {
					unlink($uploadPath . $fileName2);
				}
				if ($this->upload->do_upload('file')) {
					// Uploaded file data
					$fileData = $this->upload->data();

					// mode : insert 
					// $uploadData[$i]['nik'] = $nik;
					// $uploadData[$i]['file_path'] = $uploadPath  . $fileData['file_name'];
					// $uploadData[$i]['active'] = '1';
					// $uploadData[$i]['created_at'] = date("Y-m-d H:i:s");
					// $uploadData[$i]['id_fitjob_fam'] = $id_fitjob_fam;
					// $sess = $_SESSION['session_user'][0];
					// $uploadData[$i]['created_by'] = $sess->first_name . ' ' . $sess->last_name;

					// mode : update 
					$uploadData[$i]['nik_users'] = $nik;
					$uploadData[$i]['file_name'] = $fileData['file_name'];
					$uploadData[$i]['file_path'] = $uploadPath  . $fileData['file_name'];
				}
			}

			if (!empty($uploadData)) {
				$update = $this->pdf_laporan_model->updateUpload($id_fitjob_fam, $uploadData);
				// $update = $this->pdfLaporan_model->save_upload($uploadData);
				// $update = $this->pdfLaporan_model->save_upload($_POST['job_family'], $uploadData);
				// $statusMsg = $update ? 'Files uploaded successfully.' : 'Some problem occurred, please try again.';
				// $this->session->set_flashdata('statusMsg', $statusMsg);
				echo json_encode($update);
			} else {
				echo json_encode(['status' => 'gagal menyimpan data, file tidak ditemukan ']);
			}
		}
	}

	/**
	 * Upload function
	 * 
	 * @param string $field
	 * @param string $default Failed upload, return this
	 * @return string
	 */
	public function aksi_upload($field = 'userfile', $default = '')
	{
		$this->load->library('upload');
		$config['upload_path'] = './assets/file/';
		$config['allowed_types'] = 'gif|jpg|png|pdf|doc';
		$config['max_size'] = 10000;
		$this->upload->initialize($config, true);
		if (!$this->upload->do_upload($field)) {
			return $default;
		}
		return base_url() . "assets/images/" . $this->upload->data('file_name');
	}

	// private function listFiles()
	// {
	// 	$this->load->helper('file');
	// 	$files = get_filenames("./assets/uploads");
	// 	echo json_encode($files);
	// }


	public function fetch()
	{
		$query = $this->pdf_laporan_model->listPdfLaporan();
		$datas 			= array();
		foreach ($query as $index => $item) {
			$pdf_asesmen = '<button class="btn btn-primary" onclick="modalPDF(\'' . $item->pdf_asesmen_karyawan . '\')"><i class="far fa-file-pdf"></i> </button> <button class="btn btn-md btn-danger" onclick="hapusPdf1(\''.$item->nik.'\',1)"><i class="fas fa-trash"></i></button>';
			$pdf_tm = '<button class="btn btn-primary" onclick="modalPDF(\'' . $item->pdf_asesmen_sdm . '\')"><i class="far fa-file-pdf"></i> </button><button class="btn btn-md btn-danger" onclick="hapusPdf2(\''.$item->nik.'\')"><i class="fas fa-trash"></i></button>';
			
			$datas[] 	= array(
				"no" 			=> null,
				"nama" 			=> $item->nama,
				"nik" 			=> $item->nik,
				"nm_grade"		=> $item->grade,
				"nm_dept"		=> $item->unit_name,
				"pdf_asesmen_karyawan" 	=> ($item->pdf_asesmen_karyawan !== null ? $pdf_asesmen : 'null'),
				"pdf_asesmen_sdm" => ($item->pdf_asesmen_sdm !== null ? $pdf_tm : 'null'),
			);
		}
		// var_dump($datas);die();
		$obj = array("data" => $datas);
		echo json_encode($obj);
	}

	public function hapus_asesmen()
	{
		$nik = $this->input->post('nik');
		$id = $this->input->post('id');
		
		$query = $this->pdf_laporan_model->deletepdfAsesmen($nik,$id);
		echo json_decode($query);
	}
	public function hapus_mb()
	{
		$nik = $this->input->post('nik');
		$id = $this->input->post('id');
		$query = $this->pdf_laporan_model->deletepdfMB($nik,$id);
		echo json_decode($query);
	}
	public function delete()
	{
		$json = file_get_contents('php://input');
		$param = json_decode($json);

		if (file_exists($param->file_path)) {
			unlink($param->file_path);
		}
		$data = $this->pdf_laporan_model->deletePdfLaporan($param->id);
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function cobaupload()
	{
		$upload_berhasil = [];
		$upload_gagal = [];

		$config = array(
			'upload_path'   => 'resources/pdf/asesmen/',
			'allowed_types' => 'pdf',
			// 'overwrite'		=> 1,
		);

		$this->load->library('upload', $config);

		$filesCount = count($_FILES['Asesmenfiles']['name']);
		
		for ($i = 0; $i < $filesCount; $i++) {
			$_FILES['Asesmenfiles[]']['name'] = $_FILES['Asesmenfiles']['name'][$i];
			$_FILES['Asesmenfiles[]']['type'] = $_FILES['Asesmenfiles']['type'][$i];
			$_FILES['Asesmenfiles[]']['tmp_name'] = $_FILES['Asesmenfiles']['tmp_name'][$i];
			$_FILES['Asesmenfiles[]']['error'] = $_FILES['Asesmenfiles']['error'][$i];
			$_FILES['Asesmenfiles[]']['size'] = $_FILES['Asesmenfiles']['size'][$i];
			
			$nik = $_FILES['Asesmenfiles[]']['name'];

			$substring_nik = substr($nik,0,7);

			if (is_numeric($substring_nik)) {
				$fileName = date("Y-m-d") . "_" . $nik;

				$config['file_name'] = $fileName;

				$this->upload->initialize($config);

				$cek_nik = $this->pdf_laporan_model->isUserExist($substring_nik);
				$cek_laporan = $this->pdf_laporan_model->isLaporanExist($substring_nik);

				if ($cek_nik->num_rows() > 0) {

					date_default_timezone_set("Asia/Bangkok");

					if ($cek_laporan->num_rows() == 0) {

						$this->upload->do_upload('Asesmenfiles[]');
						$data = $this->upload->data();

						$data = array(
							"nik"	=>	$substring_nik,
							"nik_tetap"	=>	$cek_nik->row()->nik_tetap,
							"file_path"	=>	$config['upload_path'] . $data['file_name'],
							"created_at"	=>	date("Y-m-d H:i:s")
						);
						$this->db->insert('t_pdf_laporan', $data);

						array_push($upload_berhasil, $_FILES['Asesmenfiles[]']['name']);
					} else {

						unlink($cek_laporan->row()->file_path);

						$this->upload->do_upload('Asesmenfiles[]');
						$data = $this->upload->data();

						$data = array(
							"file_path" => $config['upload_path'] . $data['file_name'],
							"created_at"	=>	date("Y-m-d H:i:s")
						);

						$this->db->where('id_pdf_laporan', $cek_laporan->row()->id_pdf_laporan);
						$this->db->update('t_pdf_laporan', $data);

						array_push($upload_berhasil, $_FILES['Asesmenfiles[]']['name']);
					}
				} else {

					array_push($upload_gagal, $_FILES['Asesmenfiles[]']['name']);
				}
			} else {
				array_push($upload_gagal, $_FILES['Asesmenfiles[]']['name']);
			}
		}

		echo json_encode(" " . count($upload_berhasil) . " file berhasil diupload dan " . count($upload_gagal) . " file gagal");
	}
	public function uploadMB()
	{
		$upload_berhasil = [];
		$upload_gagal = [];

		$config = array(
			'upload_path'   => 'resources/pdf/minat_bakat/',
			'allowed_types' => 'pdf'
		);

		$this->load->library('upload', $config);

		$filesCount = count($_FILES['MinatBakatfiles']['name']);
		
		for ($i = 0; $i < $filesCount; $i++) {
			$_FILES['MinatBakatfiles[]']['name'] = $_FILES['MinatBakatfiles']['name'][$i];
			$_FILES['MinatBakatfiles[]']['type'] = $_FILES['MinatBakatfiles']['type'][$i];
			$_FILES['MinatBakatfiles[]']['tmp_name'] = $_FILES['MinatBakatfiles']['tmp_name'][$i];
			$_FILES['MinatBakatfiles[]']['error'] = $_FILES['MinatBakatfiles']['error'][$i];
			$_FILES['MinatBakatfiles[]']['size'] = $_FILES['MinatBakatfiles']['size'][$i];

			$nik = $_FILES['MinatBakatfiles[]']['name'];
			$substring_nik = substr($nik,0,7);
			if (is_numeric($substring_nik)) {
				$fileName = date("Y-m-d") . "_" . $nik;
				
				$config['file_name'] = $fileName;
				
				$this->upload->initialize($config);
				
				$cek_nik = $this->pdf_laporan_model->isUserExist($substring_nik);
				$cek_laporan = $this->pdf_laporan_model->isLaporanExist2($substring_nik);
				if ($cek_nik->num_rows() > 0) {
					
					date_default_timezone_set("Asia/Bangkok");
					
					if ($cek_laporan->num_rows() == 0) {
						
						$this->upload->do_upload('MinatBakatfiles[]');
						$data = $this->upload->data();
						
						$data = array(
							"nik"	=>	$substring_nik,
							"file_path"	=>	$config['upload_path'] . $data['file_name'],
							"created_at"	=>	date("Y-m-d H:i:s")
						);
						$q = $this->db->insert('t_pdf_laporan_sdm', $data);
						
						
						array_push($upload_berhasil, $_FILES['MinatBakatfiles[]']['name']);
					} else {

						unlink($cek_laporan->row()->file_path);

						$this->upload->do_upload('MinatBakatfiles[]');
						$data = $this->upload->data();

						$data = array(
							"file_path" => $config['upload_path'] . $data['file_name'],
							"created_at"	=>	date("Y-m-d H:i:s")
						);

						$this->db->where('id_pdf_minat_bakat', $cek_laporan->row()->id_pdf_minat_bakat);
						$this->db->update('t_pdf_minat_bakat', $data);

						array_push($upload_berhasil, $_FILES['MinatBakatfiles[]']['name']);
					}
				} else {

					array_push($upload_gagal, $_FILES['MinatBakatfiles[]']['name']);
				}
			} else {
				array_push($upload_gagal, $_FILES['MinatBakatfiles[]']['name']);
			}
		}

		echo json_encode(" " . count($upload_berhasil) . " file berhasil diupload dan " . count($upload_gagal) . " file gagal");
	}
}
