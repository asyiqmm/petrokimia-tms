<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");
class Laporan_minat_bakat extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('laporan_minat_bakat_model'); 
		$this->load->library('session');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Hasil Talent Mapping | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		// if (in_array(14, $_SESSION['session_menu'])) {
		// 	$data['parent_active'] = 14;
		// 	$data['child_active'] = 14;
		// 	$this->_render('laporanMinatBakat_view', $data);
		// } else {
		// 	redirect(base_url() . 'auth/login');
		// }
		if ($_SESSION['session_role'] == 4) {
			if (in_array(18, $_SESSION['session_menu'])) {
				$data['parent_active'] = 16;
				$data['child_active'] = 18;
				$this->_render('laporanMinatBakat_view', $data);
			} else {
				redirect(base_url() . 'auth/login');
			}
		}elseif($_SESSION['session_role'] == 3){
			if (in_array(14, $_SESSION['session_menu'])) {
				$data['parent_active'] = 14;
				$data['child_active'] = 14;
				$this->_render('laporanMinatBakat_view', $data);
			} else {
				redirect(base_url() . 'auth/login');
			}
		}
	}
	public function getPdf()
	{
		$nik = $this->input->get('nik');
		$query = $this->laporan_minat_bakat_model->getPdf($nik);
		
		echo json_encode($query);
	}

}
