<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

// require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Pemetaan extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		$this->lang->load('auth');
		$this->load->library('session');
		$this->load->model('pemetaan_model');
		$this->load->model('job_fit_karyawan/job_fit_karyawan_model');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Pemetaan Tahap I | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');

		if (in_array(5, $_SESSION['session_menu'])) {
			$data['grades_cat'] = $this->pemetaan_model->get_grade();
			$data['parent_active'] = 4;
			$data['child_active'] = 5;
			$this->_render('pemetaan_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function get_data()
	{
		$data = array(
			'grade'	=> $this->input->post('grade'),
			'tipe'	=> 1,
			'sub_grade'        => $this->input->post('sub_grade'),
			'tahun'        => $this->input->post('tahun'),
		);
		$query = array();
		$query = $this->pemetaan_model->get_dataGrade($data);

		echo json_encode($query['data']);
	}

	public function list_data()
	{
		$data = array(
			'pak'		=> strtoupper($this->input->post('pak')),
			'asesmen'	=> strtoupper($this->input->post('kki')),
			'grade'		=> $this->input->post('grade'),
			'sub_grade'	=> $this->input->post('sub_grade'),
			'tipe'		=> 2
		);

		$query = $this->pemetaan_model->get_dataGrade($data);
		$data 			= array();
		foreach ($query['data'] as $index => $item) {
			$data[] 	= array(
				"no" => '',
				"nama" => $item->nama,
				"nik" 			=> $item->nik,
				"hasil"	=> $item->kat_hasil

			);
		}
		$obj = array("data" => $data);
		echo json_encode($obj);
	}
	public function get_sub_grade()
	{
		$grade = $this->input->get('grade');
		$query = $this->pemetaan_model->get_sub_grade($grade);

		echo json_encode($query);
	}

	public function eksport_exel()
	{

		$grade = $this->input->get('grade');
		$txt_tahun = $this->input->get('tahun');
		//load phpspreadsheet class using namespaces
		$date = date('Y', strtotime('-1 years'));

		//make a new spreadsheet object
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()->setCreator('Pengembangan Personal')
			->setLastModifiedBy('Bangpesr')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('HAV MATRIX')
			->setKeywords('HAV MATRIX')
			->setCategory('HAV');
		//Retrieve Highest Column (e.g AE)
		$activesheet = 0;

		$styleArrayFirstRow = [
			'font' => [
				'bold' => true,
			]
		];
		// foreach ($grade as $key => $value) {
		$spreadsheet->setActiveSheetIndex($activesheet);
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle($grade);

		$sheet->getStyle('A:B')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('D:F')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('H:I')->getAlignment()->setHorizontal('center');
		$sheet->getStyle('J')->getAlignment()->setHorizontal('center');
		//set first row bold
		$sheet->getStyle('A1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('B1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('C1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('D1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('E1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('F1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('G1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('H1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('I1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('J1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('K1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('L1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('M1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('N1')->applyFromArray($styleArrayFirstRow);
		$sheet->getStyle('O1')->applyFromArray($styleArrayFirstRow);

		//Memberikan Nilai diawal
		$sheet->setCellValue('A1', 'NO')
			->setCellValue('B1', 'NIK')
			->setCellValue('C1', 'NAMA')
			->setCellValue('D1', 'GRADE')
			->setCellValue('E1', 'DIREKTORAT')
			->setCellValue('F1', 'DEPARTEMEN')
			->setCellValue('G1', 'ASESMEN')
			->setCellValue('H1', 'KAT_ASESMEN')
			->setCellValue('I1', 'PAK ' . $date . '')
			->setCellValue('J1', 'PAK ' . date('Y') . '')
			->setCellValue('K1', 'AVERAGE')
			->setCellValue('L1', 'KAT_PAK')
			->setCellValue('M1', 'MATRIX')
			->setCellValue('N1', 'STATUS TALENT')
			->setCellValue('O1', 'PENSIUN');

		$data = array(
			'grade'	=> $grade,
			'sub_grade'        => null,
			'tipe'	=> 1,
			'tahun'	=> $txt_tahun
		);

		$query = $this->pemetaan_model->get_dataGrade($data);
		// var_dump($query['data']); die();
		$row = 2;
		foreach ($query['data'] as $key => $value) {
			$sheet->setCellValue('A' . $row, $key + 1);
			$sheet->setCellValue('B' . $row, $value->nik);
			$sheet->setCellValue('C' . $row, $value->nama);
			$sheet->setCellValue('D' . $row, $value->grade);
			$sheet->setCellValue('E' . $row, $value->direktorat);
			$sheet->setCellValue('F' . $row, $value->departemen);
			$sheet->setCellValue('G' . $row, $value->asesmen);
			$sheet->setCellValue('H' . $row, $value->kat_asesmen);
			$sheet->setCellValue('I' . $row, $value->nilai_sebelum);
			$sheet->setCellValue('J' . $row, $value->nilai_sekarang);
			$sheet->setCellValue('K' . $row, $value->avg_pak);
			$sheet->setCellValue('L' . $row, $value->kat_pak);
			$sheet->setCellValue('M' . $row, $value->kat_hasil);
			$sheet->setCellValue('N' . $row, $value->kat_talent);
			$sheet->setCellValue('O' . $row, $value->tgl_pensiun);
			$row++;
		}

		//fit kolom
		foreach (range('A', 'N') as $columnID) {
			$sheet->getColumnDimension($columnID)->setAutoSize(true);
		}
		$activesheet++;
		$spreadsheet->createSheet();
		// }
		$spreadsheet->setActiveSheetIndex(0);

		$datetime = date('dmy');
		$filename = 'Rekap Pemetaan Tahap I Grade' . $grade . ' ' . $datetime;
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}
