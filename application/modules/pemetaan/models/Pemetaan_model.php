<?php if (!defined('BASEPATH')) exit('No direct script access alLOWed');
class Pemetaan_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function get_sub_grade($grade)
	{
		$sub_grade = $this->db->where('id_cat', $grade)
			->order_by('nm_grade')
			->get('m_grades');

		$data = "<option value=''>--Pilih Sub Grade--</option>";
		foreach ($sub_grade->result() as $grade_cat) {
			$data .= '<option value="' . $grade_cat->id_grad . '">' . $grade_cat->nm_grade . '</option>';
		}
		return $data;
	}

	public function get_grade()
	{
		$grade = $this->db->order_by('id_grad_cat')
			->get('m_grade_cat');
		return $grade->result();
	}

	public function get_dataGrade($data)
	{
		$grade = $data['grade'];
		$sub_grade = $data['sub_grade'];
		$tahun = $data['tahun'];

		if ($sub_grade == '') {
			$where = "id_cat = '$grade'";
		} else {
			$where = "id_grad = '$sub_grade'";
		}
		if ($data['tipe'] == 2) {
			$pak = $data['pak'];
			$asesmen = $data['asesmen'];
			$statement = "where " . $where . " and kat_pak= '$pak' and kat_asesmen = '$asesmen'";
		} else {
			$statement = "where " . $where . "";
		}
		$standar = $this->db->where('id_grade_cat', $grade)
			->order_by('id_grade_cat')
			->get('m_standar_nilai_potensi')->result();
		$standarkinerja = $this->db
			->order_by('id_kategori')
			->get('m_standar_nilai_kinerja')->result();
			
		if ($grade == 1) {
			// PERHITUNGAN GRADE1 DOMESTIC
			$query = "SELECT
			*,
			case
				when kat_asesmen = 'HIGH'
				and kat_pak = 'HIGH' then '3'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'HIGH' then '2'
				when kat_asesmen = 'LOW'
				and kat_pak = 'HIGH' then '1'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'MEDIUM' then '6'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'MEDIUM' then '5'
				when kat_asesmen = 'LOW'
				and kat_pak = 'MEDIUM' then '4'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'LOW' then '9'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'LOW' then '8'
				when kat_asesmen = 'LOW'
				and kat_pak = 'LOW' then '7'
		end as hasil,
			case
				when kat_asesmen = 'HIGH'
				and kat_pak = 'HIGH' then 'TALENT'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'HIGH' then 'TALENT'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'MEDIUM' then 'TALENT'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'MEDIUM' then 'TALENT'
				else 'NON TALENT'
		end as kat_talent,
			case
				when kat_asesmen = 'HIGH'
				and kat_pak = 'HIGH' then 'CONSISTENT STAR'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'HIGH' then 'UTILITY HI_PRO'
				when kat_asesmen = 'LOW'
				and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'MEDIUM' then 'FUTURE STAR'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO'
				when kat_asesmen = 'LOW'
				and kat_pak = 'MEDIUM' then 'CONTRIBUTOR'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER'
				when kat_asesmen = 'LOW'
				and kat_pak = 'LOW' then 'ICEBERG'
		end as kat_hasil
		from
			(
			select
				a.nik,
				a.nama,
				b.grade,
				c.id_grad,
				g.unit_name departemen,
				f.kd_direktorat,
				f.nm_direktorat direktorat,
				to_char(b.tgl_pensiun,'dd/mm/yyyy') tgl_pensiun,
				c.id_cat,
				round(z.asesmen, 2) asesmen,
				round(((z.asesmen) * 0.6),2) as satu,
				round(((z.asesmen) * 0.3),2) as dua,
				round(((z.asesmen) * 0.1),2) as tiga,
				case
					when round(z.asesmen,5) > " . $standar[0]->rentang_maksimum . " then 'HIGH' 
					when round(z.asesmen,5) between " . $standar[0]->rentang_minimum . " and " . $standar[0]->rentang_maksimum . " then 'MEDIUM'
					when round(z.asesmen,5) < " . $standar[0]->rentang_minimum . " then 'LOW'
					else 'LOW'
			end as kat_asesmen,
				round(e.nilai_sebelum,2) nilai_sebelum,
				round(e.nilai_sekarang,2) nilai_sekarang,
				round((e.nilai_sekarang + e.nilai_sebelum)/ 2, 2) as avg_pak,
				case
					when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between " . $standarkinerja[2]->rentang_awal . " and " . $standarkinerja[2]->rentang_akhir . " then 'HIGH' 
					when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between " . $standarkinerja[1]->rentang_awal . " and " . $standarkinerja[1]->rentang_akhir . " then 'MEDIUM' 
					when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between " . $standarkinerja[0]->rentang_awal . " and " . $standarkinerja[0]->rentang_akhir . " then 'LOW'
				else 'LOW'
			end as kat_pak
			from
			(select round(avg(h.nilai_asesmen),2) asesmen, a.nik
			from
			m_kkj_domestic_global i
			left join t_nilai_potensi_g_d h on h.id_kkj_d_g = i.id_kkj_d_g and i.kat_kompetensi = 5
			right join m_users_karyawan a on a.nik = h.nik
			right join d_list_karyawan_talenta c on c.nik = a.nik
			left join m_grades e on e.nm_grade = c.grade
			where date_part('year', h.created_at) = '$tahun' and i.kat_kompetensi = 5 and date_part('year', c.created_date) = '$tahun'
			group by a.nik) z
			right join m_users_karyawan a on z.nik = a.nik
			join d_list_karyawan_talenta b on b.nik = a.nik
			join m_grades c on c.nm_grade = b.grade
			left join t_nilai_potensi_kkj d on d.nik = b.nik and d.id_kkj in (1,2,3,4,5,6,7,8,9,10,11,12,13) and date_part('year', d.created_at) = date_part('year', CURRENT_DATE)
			left join t_nilai_kinerja_pak e on e.nik = b.nik and date_part('year', e.created_date) = date_part('year', CURRENT_DATE)
			left join m_direktorat f on f.kd_direktorat = b.kd_direktorat
			left join m_unit g on g.unit_id = a.unit_id
			where date_part('year', b.created_date) = date_part('year', CURRENT_DATE) and c.id_cat = 1 
			group by a.nik,a.nama,b.grade,c.id_grad,b.tgl_pensiun,c.id_cat,e.nilai_sebelum,g.unit_name,e.nilai_sekarang,f.nm_direktorat,f.kd_direktorat,z.asesmen
			) a 
			order by a.nama";
			// var_dump($query);die();
		} else {
			// UNTUK PERHITUNGAN PI
			$query = "SELECT
			*,
			case
				when kat_asesmen = 'HIGH'
				and kat_pak = 'HIGH' then '3'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'HIGH' then '2'
				when kat_asesmen = 'LOW'
				and kat_pak = 'HIGH' then '1'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'MEDIUM' then '6'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'MEDIUM' then '5'
				when kat_asesmen = 'LOW'
				and kat_pak = 'MEDIUM' then '4'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'LOW' then '9'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'LOW' then '8'
				when kat_asesmen = 'LOW'
				and kat_pak = 'LOW' then '7'
		end as hasil,
			case
				when kat_asesmen = 'HIGH'
				and kat_pak = 'HIGH' then 'TALENT'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'HIGH' then 'TALENT'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'MEDIUM' then 'TALENT'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'MEDIUM' then 'TALENT'
				else 'NON TALENT'
		end as kat_talent,
			case
				when kat_asesmen = 'HIGH'
				and kat_pak = 'HIGH' then 'CONSISTENT STAR'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'HIGH' then 'UTILITY HI_PRO'
				when kat_asesmen = 'LOW'
				and kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'MEDIUM' then 'FUTURE STAR'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'MEDIUM' then 'EXPERIENCED PRO'
				when kat_asesmen = 'LOW'
				and kat_pak = 'MEDIUM' then 'CONTRIBUTOR'
				when kat_asesmen = 'HIGH'
				and kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH'
				when kat_asesmen = 'MEDIUM'
				and kat_pak = 'LOW' then 'INCONSISTENT PERFORMER'
				when kat_asesmen = 'LOW'
				and kat_pak = 'LOW' then 'ICEBERG'
		end as kat_hasil
		from
			(
			select
				a.nik,
				a.nama,
				b.grade,
				c.id_grad,
				g.unit_name departemen,
				f.kd_direktorat,
				f.nm_direktorat direktorat,
				to_char(b.tgl_pensiun,'dd/mm/yyyy') tgl_pensiun,
				c.id_cat,
				round(avg(d.nilai_asesmen), 2) asesmen,
				round(((avg(d.nilai_asesmen)) * 0.6),2) as satu,
				round(((avg(d.nilai_asesmen)) * 0.3),2) as dua,
				round(((avg(d.nilai_asesmen)) * 0.1),2) as tiga,
				case
					when round(avg(d.nilai_asesmen),5) > " . $standar[0]->rentang_maksimum . " then 'HIGH' 
					when round(avg(d.nilai_asesmen),5) between " . $standar[0]->rentang_minimum . " and " . $standar[0]->rentang_maksimum . " then 'MEDIUM'
					when round(avg(d.nilai_asesmen),5) < " . $standar[0]->rentang_minimum . " then 'LOW'
					else 'LOW'
			end as kat_asesmen,
				round(e.nilai_sebelum,2) nilai_sebelum,
				round(e.nilai_sekarang,2) nilai_sekarang,
				round((e.nilai_sekarang + e.nilai_sebelum)/ 2, 2) as avg_pak,
				case
					when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between " . $standarkinerja[2]->rentang_awal . " and " . $standarkinerja[2]->rentang_akhir . " then 'HIGH' 
					when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between " . $standarkinerja[1]->rentang_awal . " and " . $standarkinerja[1]->rentang_akhir . " then 'MEDIUM' 
					when round((e.nilai_sekarang + e.nilai_sebelum)/2,2) between " . $standarkinerja[0]->rentang_awal . " and " . $standarkinerja[0]->rentang_akhir . " then 'LOW'
				else 'LOW'
			end as kat_pak
			from
				m_users_karyawan a
			join d_list_karyawan_talenta b on
				b.nik = a.nik
			join m_grades c on
				c.nm_grade = b.grade
			left join t_nilai_potensi_kkj d on d.nik = b.nik and d.id_kkj in (1,2,3,4,5,6,7,8,9,10,11,12,13) and date_part('year', d.created_at) = date_part('year', CURRENT_DATE)
			left join t_nilai_kinerja_pak e on e.nik = b.nik and date_part('year', e.created_date) = date_part('year', CURRENT_DATE)
			left join m_direktorat f on f.kd_direktorat = b.kd_direktorat
			left join m_unit g on g.unit_id = a.unit_id
			where date_part('year', b.created_date) = date_part('year', CURRENT_DATE)
			group by a.nik,a.nama,b.grade,c.id_grad,b.tgl_pensiun,c.id_cat,e.nilai_sebelum,g.unit_name,e.nilai_sekarang,f.nm_direktorat,f.kd_direktorat
			) a
			" . $statement . "
			order by a.nama";
		};
		$this->db->trans_begin();
		$data =  $this->db->query($query);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'data' => $data->result()
			];
		}
		// return $query->result();
	}

	public function get_list_data($asesmen, $pak, $grade)
	{
		$query = $this->db->query("SELECT b.nik,b.nama,b.grade_personal grade, c.unit_name,e.nm_fitjob_fam,ROUND( a.pak_awal, 2 ) pk_awal,ROUND( a.pak_sekarang, 2 ) pk_akhir, ROUND( a.rata_rata, 2 ) average,ROUND( a.asesmen, 2 ) asesmen,
				case
				when a.kat_asesmen = 'HIGH' and a.kat_pak = 'HIGH' then 'CONSISTENT STAR'
					when a.kat_asesmen = 'MEDIUM' and a.kat_pak = 'HIGH' then 'UTILITY HI_PRO'
					when a.kat_asesmen = 'LOW' and a.kat_pak = 'HIGH' then 'HIGH CONTRIBUTOR'
					when a.kat_asesmen = 'HIGH' and a.kat_pak = 'MEDIUM' then 'FUTURE STAR'
					when a.kat_asesmen = 'MEDIUM' and a.kat_pak = 'MEDIUM' then 'EXPERIENCED PRO'
					when a.kat_asesmen = 'LOW' and a.kat_pak = 'MEDIUM' then 'CONTRIBUTOR'
					when a.kat_asesmen = 'HIGH' and a.kat_pak = 'LOW' then 'DIAMOND IN THE ROUGH'
					when a.kat_asesmen = 'MEDIUM' and a.kat_pak = 'LOW' then 'INCONSISTENT PERFORMER'
					when a.kat_asesmen = 'LOW' and a.kat_pak = 'LOW' then 'ICEBERG'
				end as hasil
				from t_nilai_kinerja_pak a
				left join m_users_karyawan b on b.nik = a.nik
				left join m_unit c on c.unit_id = b.unit_id
				left join d_unit_job_family d on d.unit_id = c.unit_id
				left join m_fitjob_fam e on e.id = d.id_job_family
				join m_grades f on f.nm_grade = b.grade_personal
				where date_part('year', a.created_at) = date_part('year', CURRENT_DATE) and f.id_grad = '$grade' 
				and a.kat_asesmen = '$asesmen' and a.kat_pak = '$pak'
		")->result();

		return $query;
	}
}
