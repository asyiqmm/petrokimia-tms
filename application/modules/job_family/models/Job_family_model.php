<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Job_family_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function getListKaryawan()
	{
		$query = $this->db->query('select a.nik,a.nama,b.unit_id,b.unit_name,c.id_grad id_grade,c.nm_grade,d.nm_jabt jabatan,
		EXISTS(select nik from d_users_manajer z where z.nik = a.nik) as peran
		from m_users_karyawan a
		left join m_unit b on b.unit_id = a.unit_id
		left join m_grades c on c.nm_grade = a.grade_personal
		left join m_jabatans d on d.kd_jabatan = a.grade');
		return $query->result();
	}

	public function insert_kkj($kkj, $nama)
	{
		$this->db->trans_begin();
		$date = date('Y-m-d H:i:s');
		$this->db->query("INSERT into m_fitjob_fam (orders,nm_fitjob_fam,created_at,active) values ('$kkj','$nama','$date',true)");


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function post_hapusKkj($id)
	{
		$this->db->trans_begin();
		$this->db->query("DELETE FROM m_fitjob_fam where id = '$id'");


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function get_kkj()
	{
		$query = $this->db->query('select * from m_fitjob_fam order by orders asc');

		return $query->result();
	}

	public function post_ubahKkj($id_job_family, $order, $nm_job_family)
	{
		$this->db->trans_begin();
		$this->db->set('orders', $order)
			->set('nm_fitjob_fam', $nm_job_family)
			->set('updated_at', date('Y-m-d H:i:s'))
			->where('id', $id_job_family)
			->update('m_fitjob_fam');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
}
