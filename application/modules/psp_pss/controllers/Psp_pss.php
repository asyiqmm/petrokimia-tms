<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

require 'vendor/autoload.php';
set_time_limit(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Psp_pss extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('Psp_pss_model');
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Unggah Nilai PSP & PSS | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(9, $_SESSION['session_menu'])) {
			$data['parent_active'] = 8;
			$data['child_active'] = 9;
			$this->_render('psp_pss_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function import_excel()
	{

		if (isset($_FILES["file"]["name"])) {
			$reader = IOFactory::createReader('Xlsx');
			$reader->setReadDataOnly(true);
			$reader->setReadEmptyCells(false);
			$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
			$worksheet = $spreadsheet->getSheet(3);
			// $worksheet = $spreadsheet->getActiveSheet(0);
			// Get the highest row and column numbers referenced in the worksheet
			$highestRow = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'

			$m_jobFamily = $this->Psp_pss_model->get_jobFamily();

			$i = 4;
			$arrPsp = array();
			foreach ($m_jobFamily as $key => $value) {
				$arrPsp[$i] = $value->id;
				$i++;
			}
			// var_dump($arrPsp);die();

			$j = $i;
			$arrPss = array();
			foreach ($m_jobFamily as $key => $value) {
				$arrPss[$j] = $value->id;
				$j++;
			}

			$sukses = 0;
			$gagal = 0;
			$data_psp = [];
			$data_pss = [];
			$arrNik = [];
			for ($row = 4; $row <= $highestRow; ++$row) {
				$nik = $worksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue();
				$nik_sap = $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
				if (!in_array($nik_sap, $arrNik) && (is_numeric($nik_sap) || is_int($nik_sap))) {
					$sukses++;
					$arrNik[] = $nik_sap;
					foreach ($arrPsp as $k => $v) {
						$value = $worksheet->getCellByColumnAndRow($k, $row)->getCalculatedValue();
						$data_psp[] = array(
							'nik_tetap' => $nik,
							'nik' => $nik_sap,
							'id_job_family' => $v,
							'nilai_psp' => intval($value),
							'created_at' => date('Y-m-d H:i:s')
						);
						// var_dump($data_psp);
						// die();
					}
					foreach ($arrPss as $k => $v) {
						$value = $worksheet->getCellByColumnAndRow($k, $row)->getCalculatedValue();
						$data_pss[] = array(
							'nik_tetap' => $nik,
							'nik' => $nik_sap,
							'id_job_family' => $v,
							'nilai_pss' => intval($value),
							'created_at' => date('Y-m-d H:i:s')
						);
					}
					// var_dump($data_pss);die();
				}else{
					$gagal++;
				}
			}

			$query = $this->Psp_pss_model->post_asesmen($data_pss, $data_psp, $arrNik,$sukses,$gagal);
			echo json_encode($query);
		} else {
		}
	}


	public function preview_excel()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=\"filename.xlsx\"");
		// header("Cache-Control: max-age=0");

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');

		$writer = new Xlsx($spreadsheet);
		$writer->save('hello world.xlsx');
	}


	function fetch()
	{
		$query = $this->Psp_pss_model->listAsesmen();
		$data 			= array();
		foreach ($query as $index => $item) {
			$btnPSP = '<button  type="button" onclick="detailPSP(\'' . $item->nik_tetap . '\')" class="btn btn-primary btn-sm"  title="Lihat"> <i class="fa fa-eye"></i> Lihat </button>';
			$data[] 	= array(
				"no" => '',
				"nama" => $item->nama,
				"nik" 			=> $item->nik_tetap,
				"unit_id"	=> $item->unit_id,
				"unit_name"			=> $item->unit_name,
				"id_grade"			=> $item->id_grade,
				"nm_grade"			=> $item->nm_grade,
				"jabatan"			=> $item->jabatan,
				"job_fit"			=> $item->nm_fitjob_fam,

				"nilai_psp" => $btnPSP,
			);
		}
		// var_dump($data);die();
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function get_nilai()
	{
		$nik = $this->input->post('nik');
		$query = $this->Psp_pss_model->get_nilai($nik);
		$no = 1;
		$data = array();
		foreach ($query as $item) {
			$data[] 	= array(
				"nomor"			=> $no,
				"kkj"		=> $item->kkj,
				"nilai" => $item->nilai

			);
			$no++;
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function get_psp()
	{
		$nik = $this->input->get('nik');
		$query = $this->Psp_pss_model->get_psp($nik);
		$data = array();
		foreach ($query as $item) {
			$data[] 	= array(
				"no"			=> '',
				"nilai_psp"		=> $item->nilai_psp,
				"nilai_pss"		=> $item->nilai_pss,
				"nm_fitjob_fam" 		=> $item->nm_fitjob_fam

			);
		}

		$obj = array("data" => $data);

		echo json_encode($obj);
	}
}
