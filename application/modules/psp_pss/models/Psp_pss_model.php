<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Psp_pss_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}
	function listAsesmen()
	{
		$query = $this->db->query("SELECT a.nik_tetap,a.nama,b.unit_id,b.unit_name,c.id_grad id_grade,c.nm_grade,d.nm_jabt jabatan,f.nm_fitjob_fam
		from m_users_karyawan a
		left join m_unit b on b.unit_id = a.unit_id
		join m_grades c on c.nm_grade = a.grade_personal
		left join m_jabatans d on d.kd_jabatan = a.grade
		left join d_unit_job_family e on e.unit_id = b.unit_id
		left join m_fitjob_fam f on f.id = e.id_job_family
		");
		return $query->result();
	}

	function post_asesmen($pss, $psp, $nik, $sukses, $gagal)
	{
		$this->db->trans_begin();
		$ids = implode("','", $nik);

		$this->db->query("delete from t_minat_bakat_pss where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$this->db->query("delete from t_minat_bakat_psp where nik IN ('" . $ids . "') and date_part('year', created_at) = date_part('year', CURRENT_DATE)");
		$this->db->insert_batch('t_minat_bakat_pss', $pss);
		$this->db->insert_batch('t_minat_bakat_psp', $psp);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' => true,
				'sukses' => $sukses,
				'gagal' => $gagal
			];
		}
	}

	public function get_jobFamily()
	{
		$query = $this->db->query('select * from m_fitjob_fam order by orders asc');

		return $query->result();
	}

	public function get_nilai($nik)
	{
		$query = $this->db->query("select b.nm_kkj kkj,a.nik,a.nilai_asesmen nilai from t_nilai_potensi_kkj a
		join m_kkj b on b.id_kkj = a.id_kkj
		where nik = '$nik' 
		and date_part('year', a.created_at) = date_part('year', CURRENT_DATE)");
		return $query->result();
	}

	public function get_psp($nik)
	{

		$query = $this->db->query("select * 
		from t_minat_bakat_psp a
		join m_fitjob_fam b on b.id = a.id_job_family
		join t_minat_bakat_pss c on c.id_job_family = b.id and c.nik_tetap = a.nik_tetap and date_part('year', c.created_at) = date_part('year', CURRENT_DATE)
		where a.nik = '$nik' and date_part('year', a.created_at) = date_part('year', CURRENT_DATE)");
		return $query->result();
	}
}
