<div class="main-content" style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <h2 class="title-5 m-b-35">PSP PSS</h2>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form method="POST" id="import_form" enctype="multipart/form-data">
                            <div class="card-header">
                                <strong>Unggah Data PSP & PSS</strong>
                            </div>
                            <div class="card-body card-block">
                                <div class="row form-group">

                                    <div class="col-md-4">
                                        <label for="file">Pilih File Excel
                                        </label>
                                        <div class="form-group ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="file" id="file" required accept=".xls, .xlsx">
                                                <label class="custom-file-label" id="labelFile" for="customFile">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div id="response"></div>
                            </div>
                            
                            <div class="card-footer">
                                <button type="submit" id="btnsubmit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Submit
                                </button>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Tabel Data Nilai PSP & PSS Karyawan</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    <div class="table-responsive">
                                        <table class="table display responsive nowrap" width="100%" id="tabelPSP_PSS">
                                            <thead style="background-color: cadetblue;color: aliceblue;">
                                                <tr>
                                                    <td width="1%">
                                                        No
                                                    </td>
                                                    <td width="12.5%">NIK</td>
                                                    <td width="20%">Nama</td>
                                                    <td width="12%">Grade</td>
                                                    <td width="12%">Departemen</td>
                                                    <td width="12%">Jabatan</td>
                                                    <td width="12%">Job Family</td>
                                                    <td width="12%">Nilai PSP & PSS</td>
                                                </tr>

                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="response"></div>
                        </div>

                        <div class="card-footer">

                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Nilai Potensi -->
<div class="modal fade" id="nilaiPSP" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Nilai PSP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-borderless table-data3" width="100%" id="tabelPSP">
                    <thead>
                        <tr style="background:#2860a8">
                            <th>No</th>
                            <th>Job Family</th>
                            <th>Nilai PSP</th>
                            <th>Nilai PSS</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                <!-- <button type="button" class="btn btn-primary">Confirm</button> -->
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
<script src="<?php echo base_url() ?>assets/js/module/psp_pss_view.js"></script>