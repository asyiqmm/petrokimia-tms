<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
ini_set("memory_limit", "512M");

class Kkj extends CI_Controller
{
	public $urlws = null;
	public function __construct()
	{
		parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		$this->load->library('session');
		$this->load->model('kkj_model');
		set_time_limit(0);
	}
	private function _render($view, $data = array())
	{
		$data['title'] = "Data Kompetensi | PT Petrokimia Gresik";
		$this->load->view('header', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$uri = &load_class('URI', 'core');
		if (in_array(22, $_SESSION['session_menu'])) {
			$data['parent_active'] = 21;
			$data['child_active'] = 22;
			$this->_render('kkj_view', $data);
		} else {
			redirect(base_url() . 'auth/login');
		}
	}

	public function insert_kkj()
	{
		$kkj = $this->input->post('kkj');
		$nama = $this->input->post('nama');
		$query = $this->kkj_model->insert_kkj($kkj, $nama);

		echo json_encode($query);
	}

	public function get_kkj()
	{
		$tahun = $this->input->get('tahun');
		$query = $this->kkj_model->get_kkj($tahun);
		$data 			= array();
		foreach ($query as $index => $item) {
			$btn 		= '<button name="btnproses" onclick="ubah(\'' . $item->id . '\',\'' . $item->kkj . '\',\'' . $item->nm_kkj . '\')" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Ubah"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" onclick="hapus(\'' . $item->id . '\')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Hapus"> <i class="fa fa-times"></i> </button>';
			$data[] 	= array(
				"no" 			=> '',
				"kkj" 			=> $item->kkj,
				"nm_kkj"		=> $item->nm_kkj,
				"btn" => $btn
			);
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}
	public function get_kkj_g1()
	{
		$rekomendasi = $this->input->get('rekomendasi');
		
		$query = $this->kkj_model->get_kkj_g1($rekomendasi);
		// var_dump($query);die();
		$data 			= array();
		foreach ($query as $index => $item) {
			$btn 		= '<button name="btnproses" onclick="ubah(\'' . $item->id_kkj_d_g . '\',\'' . $item->kkj_d_g . '\',\'' . $item->nm_kkj_d_g . '\')" id="btnproses" type="button" class="btn btn-success btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Ubah"> <i class="fa fa-edit"></i> </button>
                            <button name="btnbatal" id="btnbatal" onclick="hapus(\'' . $item->id_kkj_d_g . '\')" type="button" class="btn btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="Hapus"> <i class="fa fa-times"></i> </button>';
			$data[] 	= array(
				"no" 			=> '',
				"kkj" 			=> $item->kkj_d_g,
				"nm_kkj"		=> $item->nm_kkj_d_g,
				"btn" => $btn
			);
		}
		$obj = array("data" => $data);

		echo json_encode($obj);
	}

	public function post_hapusKkj()
	{
		$id_kkj = $this->input->post('id');
		$query = $this->kkj_model->post_hapusKkj($id_kkj);
		echo json_encode($query);
	}

	public function ubah_kkj()
	{
		$id_kkj = $this->input->post('id_kkj');
		$kd_kkj = $this->input->post('kd_kkj');
		$nm_kkj = $this->input->post('nm_kkj');
		$query = $this->kkj_model->post_ubahKkj($id_kkj,$kd_kkj,$nm_kkj);
		echo json_encode($query);
	}

	public function copy_kkj()
	{
		$tahun = $this->input->post('tahun');

		$data = $this->kkj_model->get_kkj($tahun);
		
		foreach ($data as $key => $value) {
			$file_data[] = array(
				'id_kkj' => $value->id_kkj,
				'kkj' => $value->kkj,
				'nm_kkj' => $value->nm_kkj,
				'created_at' => date('Y-m-d H:i:s'),
				'active' => $value->active,
				'fk_kat_kompetensi' => $value->fk_kat_kompetensi
			);
		}
		$insert = $this->kkj_model->insert_duplikat($file_data);
		echo json_encode($insert);
	}
}
