<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Kkj_model extends CI_Model
{
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();
		$this->load->database();
	}

	public function getListKaryawan()
	{
		$query = $this->db->query('select a.nik,a.nama,b.unit_id,b.unit_name,c.id_grad id_grade,c.nm_grade,d.nm_jabt jabatan,
		EXISTS(select nik from d_users_manajer z where z.nik = a.nik) as peran
		from m_users_karyawan a
		left join m_unit b on b.unit_id = a.unit_id
		left join m_grades c on c.nm_grade = a.grade_personal
		left join m_jabatans d on d.kd_jabatan = a.grade');
		return $query->result();
	}

	public function insert_kkj($kkj, $nama)
	{
		$this->db->trans_begin();
		$date = date('Y-m-d H:i:s');
		$this->db->query("INSERT into m_kkj (kkj,nm_kkj,created_at,active) values ('$kkj','$nama','$date',true)");


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function post_hapusKkj($id)
	{
		$this->db->trans_begin();
		$this->db->query("DELETE FROM m_kkj where id = '$id'");


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function get_kkj($tahun)
	{
		$query = $this->db->query("select * from m_kkj where date_part('year', created_at) = '$tahun' order by id_kkj asc");

		return $query->result();
	}

	public function get_kkj_g1($rekomendasi)
	{
		$query = $this->db->query("select * from m_kkj_domestic_global where kat_kompetensi = '$rekomendasi'");

		return $query->result();
	}

	public function post_ubahKkj($id_kkj, $kd_kkj, $nm_kkj)
	{
		$this->db->trans_begin();
		$this->db->set('kkj', $kd_kkj)
			->set('nm_kkj', $nm_kkj)
			->set('updated_at', date('Y-m-d H:i:s'))
			->where('id', $id_kkj)
			->update('m_kkj');
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}

	public function insert_duplikat($data)
	{
		$this->db->trans_begin();
		$this->db->query("DELETE from m_kkj where date_part('year', created_at) = date_part('year', current_date)");
		$this->db->insert_batch('m_kkj', $data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'error' => $this->db->error()
			];
		} else {
			$this->db->trans_commit();
			return ['status' => true];
		}
	}
}
