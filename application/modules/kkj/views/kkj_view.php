<div class="main-content"  style="background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url(http://diklat.petrokimia-gresik.com/jurnal/img/banner/banner.png);
    background-size: cover;">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row m-t-30">
                <div class="col-md-12">
                    <!-- DATA TABLE-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-card m-b-30">
                                <div class="au-card-inner">
                                    <h2 class="title-5 m-b-25">Master data Kompetensi</h2>
                                    <!-- DATA TABLE -->
                                    <div class="kt-portlet__body">
                                        <div class="row form-group">
                                            <div class="col col-md-1">
                                                <label for="grade" class=" form-control-label">Tahun</label>
                                            </div>
                                            <div class="col-12 col-md-3">
                                                <input type="text" class="form-control" autocomplete="off" id="txt_tahun">
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-primary" type="button" id="btnCari">Cari</button>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-warning" type="button" id="btnCopy">Duplikat Kompetensi KKJ</button>
                                            </div>
                                        </div>
                                        <button class="btn btn-info col-md-2 type1" type="button" id="add_paket" data-toggle="kt-tooltip" data-placement="top"><i class="fa fa-plus"></i> Tambah</button>
                                        <button class="btn btn-danger col-md-2 type2" type="button" id="back_list" data-toggle="kt-tooltip" data-placement="top" style="background: red;color:white"><i class="fa fa-arrow-left"></i> Kembali ke List</button>

                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12 type1">
                                            <table class="table" id="data_kkj">
                                                <thead style="background-color: cadetblue;color: aliceblue;">
                                                    <tr>
                                                        <td width="5%">No</td>
                                                        <td width="25%">Kode Kompetensi</td>
                                                        <td width="60%">Nama Kompetensi</td>
                                                        <td width="10%">Aksi</td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col-md-12 type2">
                                            <form method="POST" id="insert_kkj" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>Kode Kompetensi</label>
                                                        <input type="text" class="form-control inputan" required placeholder="kkj" id="kkj">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label> Nama Kompetensi</label>
                                                        <input type="text" class="form-control inputan" required placeholder="nama" id="nama">
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <button class="btn btn-info col-md-2" type="submit" name="import" value="Import"><i class="fa fa-check"></i> simpan</button>
                                                    </div>

                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END DATA TABLE -->
                        </div>
                    </div>
                    <!-- END DATA TABLE-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p id="aku">Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Ubah KKJ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" id="ubah_kkj" enctype="multipart/form-data">
                <div class="modal-body">

                    <input type="hidden" id="id_kkj" name="id_kkj">
                    <div class="form-group col-md-12">
                        <label>KODE KKJ</label>
                        <input type="text" class="form-control inputan" required placeholder="kkj" name="kd_kkj" id="kd_kkj">
                    </div>
                    <div class="form-group col-md-12">
                        <label> NAMA KKJ</label>
                        <input type="text" class="form-control inputan" required placeholder="nama" id="nm_kkj" name="nm_kkj">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>Ubah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/module/kkj_view.js"></script>