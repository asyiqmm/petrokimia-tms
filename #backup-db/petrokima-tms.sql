/*
 Navicat Premium Data Transfer

 Source Server         : heroku_pg-asyiq
 Source Server Type    : PostgreSQL
 Source Server Version : 120002
 Source Host           : ec2-34-204-22-76.compute-1.amazonaws.com:5432
 Source Catalog        : deljg1ibpparvf
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120002
 File Encoding         : 65001

 Date: 17/03/2020 14:27:21
*/


-- ----------------------------
-- Sequence structure for d_kompetensi_job_fam_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."d_kompetensi_job_fam_id_seq";
CREATE SEQUENCE "public"."d_kompetensi_job_fam_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for d_rekomendasi_grade_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."d_rekomendasi_grade_id_seq";
CREATE SEQUENCE "public"."d_rekomendasi_grade_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."groups_id_seq";
CREATE SEQUENCE "public"."groups_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for login_attempts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."login_attempts_id_seq";
CREATE SEQUENCE "public"."login_attempts_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_departemens_id_dept_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_departemens_id_dept_seq";
CREATE SEQUENCE "public"."m_departemens_id_dept_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_fitjob_fam_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_fitjob_fam_id_seq";
CREATE SEQUENCE "public"."m_fitjob_fam_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_grade_cat_id_grad_cat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_grade_cat_id_grad_cat_seq";
CREATE SEQUENCE "public"."m_grade_cat_id_grad_cat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_grades_id_grad_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_grades_id_grad_seq";
CREATE SEQUENCE "public"."m_grades_id_grad_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_jabatans_id_jabt_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_jabatans_id_jabt_seq";
CREATE SEQUENCE "public"."m_jabatans_id_jabt_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_kkj_id_kkj_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_kkj_id_kkj_seq";
CREATE SEQUENCE "public"."m_kkj_id_kkj_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_kompartemens_id_komp_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_kompartemens_id_komp_seq";
CREATE SEQUENCE "public"."m_kompartemens_id_komp_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for m_menus_id_menus_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."m_menus_id_menus_seq";
CREATE SEQUENCE "public"."m_menus_id_menus_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for nilai_potensi_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."nilai_potensi_id_seq";
CREATE SEQUENCE "public"."nilai_potensi_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for nilai_potensi_kkj_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."nilai_potensi_kkj_id_seq";
CREATE SEQUENCE "public"."nilai_potensi_kkj_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for t_nilai_potensi_kkj_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."t_nilai_potensi_kkj_id_seq";
CREATE SEQUENCE "public"."t_nilai_potensi_kkj_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for t_pdf_laporan_id_pdf_laporan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."t_pdf_laporan_id_pdf_laporan_seq";
CREATE SEQUENCE "public"."t_pdf_laporan_id_pdf_laporan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_groups_id_seq";
CREATE SEQUENCE "public"."users_groups_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_info_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_info_id_seq";
CREATE SEQUENCE "public"."users_info_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for d_grade_cat
-- ----------------------------
DROP TABLE IF EXISTS "public"."d_grade_cat";
CREATE TABLE "public"."d_grade_cat" (
  "id_grad_cat" int4 NOT NULL DEFAULT nextval('m_grade_cat_id_grad_cat_seq'::regclass),
  "nm_grade_cat" varchar(5) COLLATE "pg_catalog"."default" NOT NULL,
  "keteranganx" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of d_grade_cat
-- ----------------------------
INSERT INTO "public"."d_grade_cat" VALUES (1, 'grade', 'gradeeeee', NULL, NULL);
INSERT INTO "public"."d_grade_cat" VALUES (2, 'grade', 'gradeeeee', NULL, NULL);
INSERT INTO "public"."d_grade_cat" VALUES (3, 'grade', 'gradeeeee', NULL, NULL);
INSERT INTO "public"."d_grade_cat" VALUES (4, 'grade', 'gradeeeee', NULL, NULL);
INSERT INTO "public"."d_grade_cat" VALUES (5, 'grade', 'gradeeeee', NULL, NULL);
INSERT INTO "public"."d_grade_cat" VALUES (6, 'grade', 'gradeeeee', NULL, NULL);

-- ----------------------------
-- Table structure for d_kompetensi_job_fam
-- ----------------------------
DROP TABLE IF EXISTS "public"."d_kompetensi_job_fam";
CREATE TABLE "public"."d_kompetensi_job_fam" (
  "id" int4 NOT NULL DEFAULT nextval('d_kompetensi_job_fam_id_seq'::regclass),
  "id_kkj" int4 NOT NULL,
  "id_grade" int4 NOT NULL,
  "id_job_fam" int4 NOT NULL,
  "active" bool NOT NULL,
  "keterangan" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of d_kompetensi_job_fam
-- ----------------------------
INSERT INTO "public"."d_kompetensi_job_fam" VALUES (1, 1, 1, 21, 't', NULL, '2020-03-08 19:18:39.988', NULL);
INSERT INTO "public"."d_kompetensi_job_fam" VALUES (2, 2, 1, 21, 't', NULL, '2020-03-08 19:19:16.298', NULL);
INSERT INTO "public"."d_kompetensi_job_fam" VALUES (3, 3, 1, 21, 't', NULL, '2020-03-08 19:19:15.644', NULL);

-- ----------------------------
-- Table structure for d_rekomendasi_grade
-- ----------------------------
DROP TABLE IF EXISTS "public"."d_rekomendasi_grade";
CREATE TABLE "public"."d_rekomendasi_grade" (
  "id" int4 NOT NULL DEFAULT nextval('d_rekomendasi_grade_id_seq'::regclass),
  "id_grade" int4 NOT NULL,
  "id_rek_grade" int4 NOT NULL,
  "active" bool NOT NULL,
  "keterangan" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of d_rekomendasi_grade
-- ----------------------------
INSERT INTO "public"."d_rekomendasi_grade" VALUES (1, 11, 10, 't', 'Rekomendasi Target Jabatan Promosi', '2020-03-09 18:19:56.624', NULL);
INSERT INTO "public"."d_rekomendasi_grade" VALUES (2, 11, 11, 't', 'Rekomendasi Target Jabatan Saat Ini', '2020-03-09 18:20:04.489', NULL);

-- ----------------------------
-- Table structure for grade_job_fam
-- ----------------------------
DROP TABLE IF EXISTS "public"."grade_job_fam";
CREATE TABLE "public"."grade_job_fam" (
  "id_job_fam" int4 NOT NULL,
  "id_kkj" int4 NOT NULL,
  "id_grade" int4 NOT NULL,
  "active" bool NOT NULL,
  "created_date" timestamp(6)
)
;

-- ----------------------------
-- Records of grade_job_fam
-- ----------------------------

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."groups";
CREATE TABLE "public"."groups" (
  "id" int4 NOT NULL DEFAULT nextval('groups_id_seq'::regclass),
  "name" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO "public"."groups" VALUES (1, 'admin', 'Administrator');
INSERT INTO "public"."groups" VALUES (2, 'bangpers', 'Pengembangan Personil');
INSERT INTO "public"."groups" VALUES (3, 'karyawan', 'Karyawan');
INSERT INTO "public"."groups" VALUES (4, 'manajer', 'Manajer');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS "public"."login_attempts";
CREATE TABLE "public"."login_attempts" (
  "id" int4 NOT NULL DEFAULT nextval('login_attempts_id_seq'::regclass),
  "ip_address" varchar(15) COLLATE "pg_catalog"."default",
  "login" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "time" int4
)
;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for m_departemens
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_departemens";
CREATE TABLE "public"."m_departemens" (
  "id_dept" int4 NOT NULL DEFAULT nextval('m_departemens_id_dept_seq'::regclass),
  "nm_dept" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "keterangan" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "id_komp" int4
)
;

-- ----------------------------
-- Records of m_departemens
-- ----------------------------
INSERT INTO "public"."m_departemens" VALUES (1, 'Audit Operasional', NULL, '2020-03-02 15:55:20.11', NULL, 1);
INSERT INTO "public"."m_departemens" VALUES (2, 'Audi Administrasi', NULL, '2020-03-02 15:55:20.695', NULL, 1);
INSERT INTO "public"."m_departemens" VALUES (4, 'Hubungan Masyarakat', NULL, '2020-03-02 15:57:09.76', NULL, 2);
INSERT INTO "public"."m_departemens" VALUES (5, 'Hukum & Sekretariat', NULL, '2020-03-02 15:57:09.76', NULL, 2);
INSERT INTO "public"."m_departemens" VALUES (6, 'Tata Kelola Perusahaan & Manajemen Resiko', NULL, '2020-03-02 15:57:09.76', NULL, 2);
INSERT INTO "public"."m_departemens" VALUES (8, 'Penjualan Retail Wilayah 1', NULL, '2020-03-02 15:57:54.752', NULL, 3);
INSERT INTO "public"."m_departemens" VALUES (9, 'Penjualan Retail Wilayah 2', NULL, '2020-03-02 15:57:54.752', NULL, 3);
INSERT INTO "public"."m_departemens" VALUES (11, 'Penjualan Pupuk Korporasi', NULL, '2020-03-02 15:59:04.471', NULL, 4);
INSERT INTO "public"."m_departemens" VALUES (12, 'Penjualan Produk Non Pupuk & Jasa', NULL, '2020-03-02 15:59:04.471', NULL, 4);
INSERT INTO "public"."m_departemens" VALUES (13, 'Penjualan Produk Pengembangan', NULL, '2020-03-02 15:59:04.471', NULL, 4);
INSERT INTO "public"."m_departemens" VALUES (24, 'Produksi 2A', NULL, '2020-03-02 16:37:09.172', NULL, 7);
INSERT INTO "public"."m_departemens" VALUES (25, 'Produksi 2B', NULL, '2020-03-02 16:37:09.172', NULL, 7);
INSERT INTO "public"."m_departemens" VALUES (26, 'Pemeliharaan 2', NULL, '2020-03-02 16:37:09.172', NULL, 7);
INSERT INTO "public"."m_departemens" VALUES (15, 'Pengelola Mitra Produksi', NULL, '2020-03-02 16:34:02.392', NULL, 5);
INSERT INTO "public"."m_departemens" VALUES (16, 'Promosi & Perencanaan Pemasaran', NULL, '2020-03-02 16:34:02.392', NULL, 5);
INSERT INTO "public"."m_departemens" VALUES (17, 'Distribusi Wilayah 1', NULL, '2020-03-02 16:34:02.392', NULL, 5);
INSERT INTO "public"."m_departemens" VALUES (18, 'Distribusi Wilayah 2', NULL, '2020-03-02 16:34:02.392', NULL, 5);
INSERT INTO "public"."m_departemens" VALUES (20, 'Produksi 1A', NULL, '2020-03-02 16:34:58.805', NULL, 6);
INSERT INTO "public"."m_departemens" VALUES (21, 'Produksi 1B', NULL, '2020-03-02 16:34:58.805', NULL, 6);
INSERT INTO "public"."m_departemens" VALUES (22, 'Pemeliharaan 1', NULL, '2020-03-02 16:34:58.805', NULL, 6);
INSERT INTO "public"."m_departemens" VALUES (28, 'Produksi 3A', NULL, '2020-03-02 17:00:54.532', NULL, 8);
INSERT INTO "public"."m_departemens" VALUES (29, 'Produksi 3B', NULL, '2020-03-02 17:00:54.532', NULL, 8);
INSERT INTO "public"."m_departemens" VALUES (30, 'Pemeliharaan 3', NULL, '2020-03-02 17:00:54.532', NULL, 8);
INSERT INTO "public"."m_departemens" VALUES (3, 'Staf Utama Muda', NULL, '2020-03-02 15:55:21.269', NULL, 1);
INSERT INTO "public"."m_departemens" VALUES (7, 'Staf Utama Muda', NULL, '2020-03-02 15:57:09.76', NULL, 2);
INSERT INTO "public"."m_departemens" VALUES (10, 'Staf Utama Muda', NULL, '2020-03-02 15:57:54.752', NULL, 3);
INSERT INTO "public"."m_departemens" VALUES (14, 'Staf Utama Muda', NULL, '2020-03-02 15:59:04.471', NULL, 4);
INSERT INTO "public"."m_departemens" VALUES (19, 'Staf Utama Muda', NULL, '2020-03-02 16:34:02.392', NULL, 5);
INSERT INTO "public"."m_departemens" VALUES (23, 'Staf Utama Muda', NULL, '2020-03-02 16:34:58.805', NULL, 6);
INSERT INTO "public"."m_departemens" VALUES (27, 'Staf Utama Muda', NULL, '2020-03-02 16:37:09.172', NULL, 7);
INSERT INTO "public"."m_departemens" VALUES (31, 'Staf Utama Muda', NULL, '2020-03-02 17:00:54.532', NULL, 8);
INSERT INTO "public"."m_departemens" VALUES (32, 'Proses & Pengelolaan Energi', NULL, '2020-03-02 17:02:28.449', NULL, 9);
INSERT INTO "public"."m_departemens" VALUES (33, 'Lingkungan', NULL, '2020-03-02 17:02:28.45', NULL, 9);
INSERT INTO "public"."m_departemens" VALUES (34, 'Keselamatan & Kesehatan Kerja', NULL, '2020-03-02 17:02:28.45', NULL, 9);
INSERT INTO "public"."m_departemens" VALUES (35, 'Inspeksi Teknik', NULL, '2020-03-02 17:02:28.45', NULL, 9);
INSERT INTO "public"."m_departemens" VALUES (36, 'Staf Utama Muda', NULL, '2020-03-02 17:02:28.45', NULL, 9);
INSERT INTO "public"."m_departemens" VALUES (39, 'Staf Utama Muda', NULL, '2020-03-02 17:03:55.596', NULL, 10);
INSERT INTO "public"."m_departemens" VALUES (37, 'Riset Pupuk & Produksi Hayati', NULL, '2020-03-02 17:03:55.596', NULL, 10);
INSERT INTO "public"."m_departemens" VALUES (38, 'Riset Pemuliaan & Pengolahan Hasil Tanaman', NULL, '2020-03-02 17:03:55.596', NULL, 10);
INSERT INTO "public"."m_departemens" VALUES (40, 'Pengembangan Usaha', NULL, '2020-03-02 17:26:33.112', NULL, 11);
INSERT INTO "public"."m_departemens" VALUES (41, 'Rancang Bangun', NULL, '2020-03-02 17:26:33.112', NULL, 11);
INSERT INTO "public"."m_departemens" VALUES (42, 'Staf Utama Muda', NULL, '2020-03-02 17:26:33.112', NULL, 11);
INSERT INTO "public"."m_departemens" VALUES (46, 'Staf Utama Muda', NULL, '2020-03-02 17:28:22.922', NULL, 12);
INSERT INTO "public"."m_departemens" VALUES (43, 'Pengolahan Air', NULL, '2020-03-02 17:27:03.61', NULL, 12);
INSERT INTO "public"."m_departemens" VALUES (44, 'Fabrikasi', NULL, '2020-03-02 17:27:03.61', NULL, 12);
INSERT INTO "public"."m_departemens" VALUES (45, 'Pengelolahan Pelabuhan', NULL, '2020-03-02 17:27:03.61', NULL, 12);
INSERT INTO "public"."m_departemens" VALUES (47, 'Pengadaan Barang', NULL, '2020-03-02 17:29:14.82', NULL, 13);
INSERT INTO "public"."m_departemens" VALUES (48, 'Pengadaan Jasa', NULL, '2020-03-02 17:29:14.82', NULL, 13);
INSERT INTO "public"."m_departemens" VALUES (49, 'Perencanaan & Pengawasan Barang/Jasa', NULL, '2020-03-02 17:29:14.82', NULL, 13);
INSERT INTO "public"."m_departemens" VALUES (50, 'Staf Utama Muda', NULL, '2020-03-02 17:29:14.82', NULL, 13);
INSERT INTO "public"."m_departemens" VALUES (51, 'Keuangan', NULL, '2020-03-02 17:29:52.197', NULL, 14);
INSERT INTO "public"."m_departemens" VALUES (52, 'Akuntansi', NULL, '2020-03-02 17:29:52.197', NULL, 14);
INSERT INTO "public"."m_departemens" VALUES (53, 'Staf Utama Muda', NULL, '2020-03-02 17:29:52.197', NULL, 14);
INSERT INTO "public"."m_departemens" VALUES (54, 'Anggaran', NULL, '2020-03-02 17:30:16.273', NULL, 15);
INSERT INTO "public"."m_departemens" VALUES (55, 'Pengelolaan Anak Perusahaan', NULL, '2020-03-02 17:30:16.273', NULL, 15);
INSERT INTO "public"."m_departemens" VALUES (56, 'Staf Utama Muda', NULL, '2020-03-02 17:30:16.273', NULL, 15);
INSERT INTO "public"."m_departemens" VALUES (57, 'Perencanaan SDM', NULL, '2020-03-02 17:37:23.714', NULL, 16);
INSERT INTO "public"."m_departemens" VALUES (58, 'Operasional SDM', NULL, '2020-03-02 17:37:23.714', NULL, 16);
INSERT INTO "public"."m_departemens" VALUES (59, 'Pengembangan SDM', NULL, '2020-03-02 17:37:23.714', NULL, 16);
INSERT INTO "public"."m_departemens" VALUES (60, 'Staf Utama Muda', NULL, '2020-03-02 17:37:23.714', NULL, 16);
INSERT INTO "public"."m_departemens" VALUES (61, 'Pelayanan Umum', NULL, '2020-03-02 17:38:17.144', NULL, 17);
INSERT INTO "public"."m_departemens" VALUES (62, 'Corporate Social Responsibility', NULL, '2020-03-02 17:38:17.144', NULL, 17);
INSERT INTO "public"."m_departemens" VALUES (63, 'Keamanan', NULL, '2020-03-02 17:38:17.144', NULL, 17);
INSERT INTO "public"."m_departemens" VALUES (64, 'Staf Utama Muda', NULL, '2020-03-02 17:38:17.144', NULL, 17);

-- ----------------------------
-- Table structure for m_fitjob_fam
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_fitjob_fam";
CREATE TABLE "public"."m_fitjob_fam" (
  "id" int4 NOT NULL DEFAULT nextval('m_fitjob_fam_id_seq'::regclass),
  "nm_fitjob_fam" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "active" bool NOT NULL,
  "keterangan" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of m_fitjob_fam
-- ----------------------------
INSERT INTO "public"."m_fitjob_fam" VALUES (1, 'Audit', 't', NULL, '2020-03-08 14:53:28.322982', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (2, 'Hukum', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (7, 'Supply Chain Management', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (8, 'Teknologi Informasi', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (13, 'Perencanaan & Pengembangan', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (14, 'Keuangan', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (19, 'Engineering/Konstruksi', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (20, 'Pemeliharaan', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (21, 'Operasi', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (15, '4. Pemasaran (Distribusi)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (16, '3. Pemasaran (Pelayanan Pelanggan)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (17, '2. Pemasaran (Non PSO/Komersil)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (18, '1. Pemasaran (PSO)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (4, '2. Sekretaris Perusahaan dan Umum (Good Corporate Governance)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (5, '3. Sekretaris Perusahaan dan Umum (Umum)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (3, '1. Sekretaris Perusahaan dan Umum (Humas)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (6, '4. Sekretaris Perusahaan dan Umum (Sekretaris Perusahaan)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (9, '1. Manajemen SDM (Ketenagakerjaan/HI)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (10, '2. Manajemen SDM (Perencanaan SDM)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (11, '3. Manajemen SDM (Sistem Prosedur)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);
INSERT INTO "public"."m_fitjob_fam" VALUES (12, '4. Manajemen SDM (Pengembangan SDM/Diklat)', 't', NULL, '2020-03-08 14:55:33.218381', NULL);

-- ----------------------------
-- Table structure for m_grades
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_grades";
CREATE TABLE "public"."m_grades" (
  "id_grad" int4 NOT NULL DEFAULT nextval('m_grades_id_grad_seq'::regclass),
  "nm_grade" varchar(5) COLLATE "pg_catalog"."default" NOT NULL,
  "golongan" varchar(25) COLLATE "pg_catalog"."default",
  "keterangan" varchar(25) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of m_grades
-- ----------------------------
INSERT INTO "public"."m_grades" VALUES (1, '1A', NULL, NULL, '2020-03-02 18:18:06.301', NULL);
INSERT INTO "public"."m_grades" VALUES (2, '1B', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (3, '2A', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (4, '2B', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (5, '3A', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (6, '3B', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (7, '4A', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (8, '4B', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (9, '5A', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (10, '5B', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (11, '6A', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (12, '6B', NULL, NULL, '2020-03-02 18:21:08.531', NULL);
INSERT INTO "public"."m_grades" VALUES (13, '7A', NULL, NULL, '2020-03-02 18:21:08.531', NULL);

-- ----------------------------
-- Table structure for m_jabatans
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_jabatans";
CREATE TABLE "public"."m_jabatans" (
  "id_jabt" int4 NOT NULL DEFAULT nextval('m_jabatans_id_jabt_seq'::regclass),
  "nm_jabt" varchar(25) COLLATE "pg_catalog"."default",
  "keterangan" varchar(50) COLLATE "pg_catalog"."default",
  "active" bool NOT NULL,
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of m_jabatans
-- ----------------------------

-- ----------------------------
-- Table structure for m_kkj
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_kkj";
CREATE TABLE "public"."m_kkj" (
  "id_kkj" int4 NOT NULL DEFAULT nextval('m_kkj_id_kkj_seq'::regclass),
  "kkj" varchar(5) COLLATE "pg_catalog"."default" NOT NULL,
  "nm_kkj" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of m_kkj
-- ----------------------------
INSERT INTO "public"."m_kkj" VALUES (1, 'SS', 'Stakeholder Satisfaction', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (2, 'Inn', 'Innovation & Total Value Added', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (3, 'Ing', 'Integrity & Ethics', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (4, 'TW', 'Teamwork & Synergy', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (5, 'BA', 'Business Acumen', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (6, 'ST', 'Strategic Thinking', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (7, 'DEV', 'Developing Others', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (8, 'DE', 'Driving Execution', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (9, 'CL', 'Change Leadership', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (10, 'AT', 'Analitical Thinking', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (11, 'CT', 'Conceptual Thinking', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (12, 'TE', 'Technical Expetise', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (13, 'BPO', 'Business Process Orientation', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (14, 'INF', 'Information Seeking', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (15, 'CO', 'Concern For Order', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (16, 'SCF', 'Self Confidence', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (17, 'SCT', 'Self Control', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (18, 'RB', 'Relationship Building', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (19, 'FLX', 'Flexibility', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (20, 'IU', 'Interpersonal Understanding', '2020-02-29 20:00:55.308786', NULL);
INSERT INTO "public"."m_kkj" VALUES (21, 'OA', 'Organizational Awareness', '2020-02-29 20:00:55.308786', NULL);

-- ----------------------------
-- Table structure for m_kompartemens
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_kompartemens";
CREATE TABLE "public"."m_kompartemens" (
  "id_komp" int4 NOT NULL DEFAULT nextval('m_kompartemens_id_komp_seq'::regclass),
  "nm_komp" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "keterangan" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of m_kompartemens
-- ----------------------------
INSERT INTO "public"."m_kompartemens" VALUES (1, 'Audit Intern', NULL, '2020-03-02 14:36:54.487', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (2, 'Sekretaris Perusahaan', NULL, '2020-03-02 14:38:08.904', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (3, 'Penjualan Retail', NULL, '2020-03-02 14:38:09.576', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (4, 'Penjualan Komersil', NULL, '2020-03-02 14:50:19.991', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (5, 'Pemasaran & Logistik', NULL, '2020-03-02 14:50:20.73', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (6, 'Pabrik 1', NULL, '2020-03-02 14:50:21.288', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (7, 'Pabrik 2', NULL, '2020-03-02 14:50:21.9', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (8, 'Pabrik 3', NULL, '2020-03-02 14:50:22.529', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (16, 'Sumber Daya Manusia', NULL, '2020-03-02 14:53:05.96', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (17, 'Umum', NULL, '2020-03-02 14:53:06.533', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (9, 'Teknologi', NULL, '2020-03-02 14:53:00.961', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (10, 'Riset', NULL, '2020-03-02 14:53:02.155', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (11, 'Pengembangan', NULL, '2020-03-02 14:53:02.721', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (12, 'Prasarana & Utilitas', NULL, '2020-03-02 14:53:03.285', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (13, 'Pengadaan', NULL, '2020-03-02 14:53:03.86', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (14, 'Administrasi Keuangan', NULL, '2020-03-02 14:53:04.43', NULL);
INSERT INTO "public"."m_kompartemens" VALUES (15, 'Perencanaan & Pengendalian Usaha', NULL, '2020-03-02 14:53:05.333', NULL);

-- ----------------------------
-- Table structure for m_menus
-- ----------------------------
DROP TABLE IF EXISTS "public"."m_menus";
CREATE TABLE "public"."m_menus" (
  "id_menus" int4 NOT NULL DEFAULT nextval('m_menus_id_menus_seq'::regclass),
  "nm_menu" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "icon" varchar(25) COLLATE "pg_catalog"."default",
  "url" varchar(50) COLLATE "pg_catalog"."default",
  "orders" int4,
  "parent_id" int4,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "active" bool NOT NULL,
  "keterangan" varchar COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of m_menus
-- ----------------------------
INSERT INTO "public"."m_menus" VALUES (16, 'Laporan Individu', 'fa-rub', NULL, 2, NULL, '2020-02-27 23:37:37.392', '2020-02-27 23:37:40.006', 't', 'manajer');
INSERT INTO "public"."m_menus" VALUES (4, 'Manajemen Talenta', 'fa-rub', NULL, 4, NULL, '2020-02-27 23:00:43.267', '2020-02-27 23:00:44.08', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (8, 'Unggah', 'fa-upload', NULL, 8, NULL, '2020-02-27 23:02:30.252', '2020-02-27 23:02:31.236', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (13, 'Laporan Hasil Asesmen', 'fa-rub', 'laporanAsesmen', 2, NULL, '2020-02-27 23:34:24.468', '2020-02-27 23:34:25.325', 't', 'karyawan');
INSERT INTO "public"."m_menus" VALUES (14, 'Laporan Hasil Minat Bakat', 'fa-rub', 'laporanMinatBakat', 3, NULL, '2020-02-27 23:37:35.925', '2020-02-27 23:37:38.672', 't', 'karyawan');
INSERT INTO "public"."m_menus" VALUES (15, 'Overview Individu', 'fa-rub', 'overviewIndividu', 1, NULL, '2020-02-27 23:37:36.719', '2020-02-27 23:37:39.372', 't', 'manajer');
INSERT INTO "public"."m_menus" VALUES (17, 'Laporan Hasil Asesmen', 'fa-rub', 'laporanAsesmen', 3, 16, '2020-02-27 23:51:04.594', '2020-02-27 23:51:05.308', 't', 'manajer');
INSERT INTO "public"."m_menus" VALUES (18, 'Laporan Hasil Minat Bakat', 'fa-rub', 'laporanMinatBakat', 4, 16, '2020-02-28 00:21:12.557', '2020-02-28 00:21:13.194', 't', 'manajer');
INSERT INTO "public"."m_menus" VALUES (19, 'Rekap Hasil Asesmen Karyawan', 'fa-rub', 'profilBakat', 5, NULL, '2020-02-28 00:26:42.995', '2020-02-28 00:26:43.764', 't', 'manajer');
INSERT INTO "public"."m_menus" VALUES (20, 'Rekap Hasil Minat Bakat Karyawan', 'fa-rub', NULL, 6, NULL, '2020-02-28 18:49:26.891', '2020-02-28 18:49:27.472', 't', 'manajer');
INSERT INTO "public"."m_menus" VALUES (1, 'Overview', 'fa-rub', 'overview', 1, NULL, '2020-02-27 22:02:43.171', '2020-02-27 22:02:52.114', 't', 'bangpers,karyawan');
INSERT INTO "public"."m_menus" VALUES (2, 'Rekap Job Fit per Karyawan', 'fa-rub', 'jobFitKaryawan', 2, NULL, '2020-02-27 22:58:33.886', '2020-02-27 22:58:38.808', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (3, 'Rekap Job Fit per Family', 'fa-rub', 'jobFitFamily', 3, NULL, '2020-02-27 22:59:25.045', '2020-02-27 22:59:25.909', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (5, 'Pemetaan Talent', 'fa-rub', 'pemetaan', 5, 4, '2020-02-27 23:01:35.63', '2020-02-27 23:01:36.344', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (6, 'Uji Kelayakan', 'fa-rub', 'ujiKelayakan', 6, 4, '2020-02-28 18:02:33.247', '2020-02-28 18:02:33.961', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (7, 'Konfirmasi Atasan', 'fa-rub', 'konfirmasi', 7, 4, '2020-02-27 23:02:25.466', '2020-02-27 23:02:26.234', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (9, 'Nilai Asesmen', 'fa-rub', 'nilaiAsesmen', 9, 8, '2020-02-28 18:05:01.744', '2020-02-28 18:05:02.37', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (10, 'PDF Laporan', 'fa-rub', 'pdfLaporan', 10, 8, '2020-02-28 18:05:26.218', '2020-02-28 18:05:26.802', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (11, 'Data Karyawan', 'fa-rub', 'dataKaryawan', 11, NULL, '2020-02-27 23:35:08.942', '2020-02-27 23:35:09.782', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (12, 'Riwayat', 'fa-rub', 'riwayat', 12, NULL, '2020-02-28 18:13:36.981', '2020-02-28 18:13:37.599', 't', 'bangpers');
INSERT INTO "public"."m_menus" VALUES (21, 'Master', NULL, NULL, 13, NULL, '2020-03-14 12:25:20.241', '2020-03-14 12:25:21.337', 't', 'bangpers');

-- ----------------------------
-- Table structure for menus_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."menus_groups";
CREATE TABLE "public"."menus_groups" (
  "id_menus" int4,
  "id_groups" int4
)
;

-- ----------------------------
-- Records of menus_groups
-- ----------------------------
INSERT INTO "public"."menus_groups" VALUES (1, 2);
INSERT INTO "public"."menus_groups" VALUES (3, 2);
INSERT INTO "public"."menus_groups" VALUES (4, 2);
INSERT INTO "public"."menus_groups" VALUES (5, 2);
INSERT INTO "public"."menus_groups" VALUES (6, 2);
INSERT INTO "public"."menus_groups" VALUES (7, 2);
INSERT INTO "public"."menus_groups" VALUES (8, 2);
INSERT INTO "public"."menus_groups" VALUES (9, 2);
INSERT INTO "public"."menus_groups" VALUES (10, 2);
INSERT INTO "public"."menus_groups" VALUES (11, 2);
INSERT INTO "public"."menus_groups" VALUES (12, 2);
INSERT INTO "public"."menus_groups" VALUES (2, 2);
INSERT INTO "public"."menus_groups" VALUES (1, 3);
INSERT INTO "public"."menus_groups" VALUES (13, 3);
INSERT INTO "public"."menus_groups" VALUES (14, 3);
INSERT INTO "public"."menus_groups" VALUES (15, 4);
INSERT INTO "public"."menus_groups" VALUES (16, 4);
INSERT INTO "public"."menus_groups" VALUES (17, 4);
INSERT INTO "public"."menus_groups" VALUES (18, 4);
INSERT INTO "public"."menus_groups" VALUES (19, 4);
INSERT INTO "public"."menus_groups" VALUES (20, 4);

-- ----------------------------
-- Table structure for nilai_potensi_kkj
-- ----------------------------
DROP TABLE IF EXISTS "public"."nilai_potensi_kkj";
CREATE TABLE "public"."nilai_potensi_kkj" (
  "id" int4 NOT NULL DEFAULT nextval('nilai_potensi_kkj_id_seq'::regclass),
  "id_kkj" int4 NOT NULL,
  "nik" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "nilai_asesmen" numeric,
  "created_at" timestamp(6)
)
;

-- ----------------------------
-- Records of nilai_potensi_kkj
-- ----------------------------
INSERT INTO "public"."nilai_potensi_kkj" VALUES (43, 1, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (44, 2, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (45, 3, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (46, 4, '2103161003', 3, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (47, 5, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (48, 6, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (49, 7, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (50, 8, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (51, 9, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (52, 10, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (53, 11, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (54, 12, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (55, 13, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (56, 14, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (57, 15, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (58, 16, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (59, 17, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (60, 18, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (61, 19, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (62, 20, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (63, 21, '2103161003', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (64, 1, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (65, 2, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (66, 3, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (67, 4, '2103161004', 3, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (68, 5, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (69, 6, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (70, 7, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (71, 8, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (72, 9, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (73, 10, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (74, 11, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (75, 12, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (76, 13, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (77, 14, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (78, 15, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (79, 16, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (80, 17, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (81, 18, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (82, 19, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (83, 20, '2103161004', 2, '2020-03-10 21:01:35');
INSERT INTO "public"."nilai_potensi_kkj" VALUES (84, 21, '2103161004', 2, '2020-03-10 21:01:35');

-- ----------------------------
-- Table structure for nilai_potensi_kkj-backup
-- ----------------------------
DROP TABLE IF EXISTS "public"."nilai_potensi_kkj-backup";
CREATE TABLE "public"."nilai_potensi_kkj-backup" (
  "id" int4 NOT NULL DEFAULT nextval('nilai_potensi_id_seq'::regclass),
  "nik" varchar(20) COLLATE "pg_catalog"."default",
  "ss" numeric,
  "inn" numeric,
  "ing" numeric,
  "tw" numeric,
  "ba" numeric,
  "st" numeric,
  "dev" numeric,
  "de" numeric,
  "cl" numeric,
  "at" numeric,
  "ct" numeric,
  "te" numeric,
  "bpo" numeric,
  "inf" numeric,
  "co" numeric,
  "scf" numeric,
  "sct" numeric,
  "rb" numeric,
  "flx" numeric,
  "iu" numeric,
  "oa" numeric,
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Records of nilai_potensi_kkj-backup
-- ----------------------------
INSERT INTO "public"."nilai_potensi_kkj-backup" VALUES (80, '2103161003', 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, NULL, NULL);
INSERT INTO "public"."nilai_potensi_kkj-backup" VALUES (81, '2103161004', 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, NULL, NULL);
INSERT INTO "public"."nilai_potensi_kkj-backup" VALUES (82, '2103161003', 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, NULL, NULL);
INSERT INTO "public"."nilai_potensi_kkj-backup" VALUES (83, '2103161004', 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, NULL, NULL);

-- ----------------------------
-- Table structure for standar_kkj
-- ----------------------------
DROP TABLE IF EXISTS "public"."standar_kkj";
CREATE TABLE "public"."standar_kkj" (
  "id_kkj" int4 NOT NULL,
  "id_grade" int4 NOT NULL,
  "nilai" numeric NOT NULL,
  "active" bool NOT NULL,
  "keterangan" varchar(25) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "id_fitjob_fam" int4
)
;

-- ----------------------------
-- Records of standar_kkj
-- ----------------------------
INSERT INTO "public"."standar_kkj" VALUES (3, 11, 2, 't', NULL, '2020-03-10 14:54:32.585', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (4, 11, 2, 't', NULL, '2020-03-10 14:54:32.585', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (14, 11, 1, 't', NULL, '2020-03-10 14:54:32.586', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (15, 11, 2, 't', NULL, '2020-03-10 14:54:32.586', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (21, 11, 1, 't', NULL, '2020-03-10 14:54:32.586', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (1, 11, 1, 't', NULL, '2020-03-06 08:46:20.012', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (2, 11, 1, 't', NULL, '2020-03-06 08:46:32.49', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (10, 11, 0, 't', NULL, '2020-03-10 14:54:32.585', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (11, 11, 0, 't', NULL, '2020-03-10 14:54:32.585', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (12, 11, 0, 't', NULL, '2020-03-10 14:54:32.585', NULL, 1);
INSERT INTO "public"."standar_kkj" VALUES (13, 11, 0, 't', NULL, '2020-03-10 14:54:32.585', NULL, 1);

-- ----------------------------
-- Table structure for t_nilai_potensi_kkj
-- ----------------------------
DROP TABLE IF EXISTS "public"."t_nilai_potensi_kkj";
CREATE TABLE "public"."t_nilai_potensi_kkj" (
  "id" int4 NOT NULL DEFAULT nextval('t_nilai_potensi_kkj_id_seq'::regclass),
  "id_kkj" int4 NOT NULL,
  "nik" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "nilai_asesmen" numeric,
  "created_at" timestamp(6)
)
;

-- ----------------------------
-- Records of t_nilai_potensi_kkj
-- ----------------------------

-- ----------------------------
-- Table structure for t_pdf_laporan
-- ----------------------------
DROP TABLE IF EXISTS "public"."t_pdf_laporan";
CREATE TABLE "public"."t_pdf_laporan" (
  "id_pdf_laporan" int4 NOT NULL DEFAULT nextval('t_pdf_laporan_id_pdf_laporan_seq'::regclass),
  "nik" varchar(20) COLLATE "pg_catalog"."default",
  "file_path" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "created_by" varchar(20) COLLATE "pg_catalog"."default",
  "active" bool
)
;

-- ----------------------------
-- Records of t_pdf_laporan
-- ----------------------------
INSERT INTO "public"."t_pdf_laporan" VALUES (1, '2103161005', 'resources/pdf/asesmen/2103161003.pdf', '2020-03-08 15:37:31.922', NULL, 't');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "ip_address" varchar(45) COLLATE "pg_catalog"."default",
  "username" varchar(100) COLLATE "pg_catalog"."default",
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "salt" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "activation_code" varchar(40) COLLATE "pg_catalog"."default",
  "forgotten_password_code" varchar(40) COLLATE "pg_catalog"."default",
  "forgotten_password_time" int4,
  "remember_code" varchar(40) COLLATE "pg_catalog"."default",
  "created_on" int4 NOT NULL,
  "last_login" int4,
  "active" int4,
  "first_name" varchar(50) COLLATE "pg_catalog"."default",
  "last_name" varchar(50) COLLATE "pg_catalog"."default",
  "company" varchar(100) COLLATE "pg_catalog"."default",
  "phone" varchar(20) COLLATE "pg_catalog"."default",
  "nik" varchar COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (18, '::1', 'karyawan@karyawan.com', '$2y$08$PwF/696pKv81TypvTwZPjebQ5T4KYwFQIc8FBMUKkFuzP0hAubyES', NULL, 'karyawan@karyawan.com', NULL, NULL, NULL, NULL, 1582813327, 1583662582, 1, 'karyawan', 'asyiq', 'karyawan', '080', '2103161005');
INSERT INTO "public"."users" VALUES (1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', '1111', NULL, NULL, 1268889823, 1583649905, 1, 'Admin', 'istrator', 'ADMIN', '0', '111111');
INSERT INTO "public"."users" VALUES (20, '::1', 'manajer@gmail.com', '$2y$08$QnNcU3BEW0NP9Xw/NctZtO/L3l.OslVulKNjsRvvuC6.79bhb4QFi', NULL, 'manajer@gmail.com', NULL, NULL, NULL, NULL, 1582813965, 1583649772, 1, 'manajer', 'asyiq', 'manajer', '123', '2103161004');
INSERT INTO "public"."users" VALUES (19, '::1', 'bangpers@gmail.com', '$2y$08$ylDT.zHrpGq2z97TI6lRV.kYTyn3OuzsVtyTrzd2yA4uz71efR7D2', NULL, 'bangpers@gmail.com', NULL, NULL, NULL, 'erfP6AJfYsSC/Qdk5p8iQe', 1582813524, 1584416613, 1, 'bangpers', 'asyiq', 'bangpers', '12', '2103161003');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_groups";
CREATE TABLE "public"."users_groups" (
  "id" int4 NOT NULL DEFAULT nextval('users_groups_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "group_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO "public"."users_groups" VALUES (1, 1, 1);
INSERT INTO "public"."users_groups" VALUES (11, 17, 2);
INSERT INTO "public"."users_groups" VALUES (16, 20, 4);
INSERT INTO "public"."users_groups" VALUES (18, 18, 3);
INSERT INTO "public"."users_groups" VALUES (19, 19, 2);

-- ----------------------------
-- Table structure for users_info
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_info";
CREATE TABLE "public"."users_info" (
  "id" int4 NOT NULL DEFAULT nextval('users_info_id_seq'::regclass),
  "keterangan" varchar(50) COLLATE "pg_catalog"."default",
  "nik_users" varchar COLLATE "pg_catalog"."default",
  "id_dept" int4,
  "id_grade" int4,
  "id_jab" int4,
  "id_fitjob_fam" int4,
  "file_path" text COLLATE "pg_catalog"."default",
  "active" bool DEFAULT true
)
;

-- ----------------------------
-- Records of users_info
-- ----------------------------
INSERT INTO "public"."users_info" VALUES (1, 'ini user bangpers', '2103161003', 51, 11, NULL, 3, 'resources/pdf/asesmen/2103161003_3.pdf', 't');
INSERT INTO "public"."users_info" VALUES (2, 'ini karyawan', '2103161005', 52, 11, NULL, 3, '', 't');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."d_kompetensi_job_fam_id_seq"
OWNED BY "public"."d_kompetensi_job_fam"."id";
SELECT setval('"public"."d_kompetensi_job_fam_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."d_rekomendasi_grade_id_seq"
OWNED BY "public"."d_rekomendasi_grade"."id";
SELECT setval('"public"."d_rekomendasi_grade_id_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."groups_id_seq"
OWNED BY "public"."groups"."id";
SELECT setval('"public"."groups_id_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."login_attempts_id_seq"
OWNED BY "public"."login_attempts"."id";
SELECT setval('"public"."login_attempts_id_seq"', 12, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_departemens_id_dept_seq"
OWNED BY "public"."m_departemens"."id_dept";
SELECT setval('"public"."m_departemens_id_dept_seq"', 65, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_fitjob_fam_id_seq"
OWNED BY "public"."m_fitjob_fam"."id";
SELECT setval('"public"."m_fitjob_fam_id_seq"', 22, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_grade_cat_id_grad_cat_seq"
OWNED BY "public"."d_grade_cat"."id_grad_cat";
SELECT setval('"public"."m_grade_cat_id_grad_cat_seq"', 7, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_grades_id_grad_seq"
OWNED BY "public"."m_grades"."id_grad";
SELECT setval('"public"."m_grades_id_grad_seq"', 14, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_jabatans_id_jabt_seq"
OWNED BY "public"."m_jabatans"."id_jabt";
SELECT setval('"public"."m_jabatans_id_jabt_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_kkj_id_kkj_seq"
OWNED BY "public"."m_kkj"."id_kkj";
SELECT setval('"public"."m_kkj_id_kkj_seq"', 25, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_kompartemens_id_komp_seq"
OWNED BY "public"."m_kompartemens"."id_komp";
SELECT setval('"public"."m_kompartemens_id_komp_seq"', 18, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."m_menus_id_menus_seq"
OWNED BY "public"."m_menus"."id_menus";
SELECT setval('"public"."m_menus_id_menus_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."nilai_potensi_id_seq"
OWNED BY "public"."nilai_potensi_kkj-backup"."id";
SELECT setval('"public"."nilai_potensi_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."nilai_potensi_kkj_id_seq"
OWNED BY "public"."nilai_potensi_kkj"."id";
SELECT setval('"public"."nilai_potensi_kkj_id_seq"', 85, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."t_nilai_potensi_kkj_id_seq"
OWNED BY "public"."t_nilai_potensi_kkj"."id";
SELECT setval('"public"."t_nilai_potensi_kkj_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."t_pdf_laporan_id_pdf_laporan_seq"
OWNED BY "public"."t_pdf_laporan"."id_pdf_laporan";
SELECT setval('"public"."t_pdf_laporan_id_pdf_laporan_seq"', 2, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_groups_id_seq"
OWNED BY "public"."users_groups"."id";
SELECT setval('"public"."users_groups_id_seq"', 20, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 23, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_info_id_seq"
OWNED BY "public"."users_info"."id";
SELECT setval('"public"."users_info_id_seq"', 5, true);

-- ----------------------------
-- Primary Key structure for table d_grade_cat
-- ----------------------------
ALTER TABLE "public"."d_grade_cat" ADD CONSTRAINT "m_grade_cat_pkey" PRIMARY KEY ("id_grad_cat");

-- ----------------------------
-- Primary Key structure for table d_kompetensi_job_fam
-- ----------------------------
ALTER TABLE "public"."d_kompetensi_job_fam" ADD CONSTRAINT "d_kompetensi_job_fam_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table d_rekomendasi_grade
-- ----------------------------
ALTER TABLE "public"."d_rekomendasi_grade" ADD CONSTRAINT "d_rekomendasi_grade_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Checks structure for table groups
-- ----------------------------
ALTER TABLE "public"."groups" ADD CONSTRAINT "check_id" CHECK (id >= 0);

-- ----------------------------
-- Primary Key structure for table groups
-- ----------------------------
ALTER TABLE "public"."groups" ADD CONSTRAINT "groups_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Checks structure for table login_attempts
-- ----------------------------
ALTER TABLE "public"."login_attempts" ADD CONSTRAINT "check_id" CHECK (id >= 0);

-- ----------------------------
-- Primary Key structure for table login_attempts
-- ----------------------------
ALTER TABLE "public"."login_attempts" ADD CONSTRAINT "login_attempts_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_departemens
-- ----------------------------
ALTER TABLE "public"."m_departemens" ADD CONSTRAINT "m_departemens_pkey" PRIMARY KEY ("id_dept");

-- ----------------------------
-- Primary Key structure for table m_fitjob_fam
-- ----------------------------
ALTER TABLE "public"."m_fitjob_fam" ADD CONSTRAINT "m_fitjob_fam_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table m_grades
-- ----------------------------
ALTER TABLE "public"."m_grades" ADD CONSTRAINT "m_grades_pkey" PRIMARY KEY ("id_grad");

-- ----------------------------
-- Primary Key structure for table m_jabatans
-- ----------------------------
ALTER TABLE "public"."m_jabatans" ADD CONSTRAINT "m_jabatans_pkey" PRIMARY KEY ("id_jabt");

-- ----------------------------
-- Uniques structure for table m_kkj
-- ----------------------------
ALTER TABLE "public"."m_kkj" ADD CONSTRAINT "m_kkj_kkj_key" UNIQUE ("kkj");

-- ----------------------------
-- Primary Key structure for table m_kkj
-- ----------------------------
ALTER TABLE "public"."m_kkj" ADD CONSTRAINT "m_kkj_pkey" PRIMARY KEY ("id_kkj");

-- ----------------------------
-- Primary Key structure for table m_kompartemens
-- ----------------------------
ALTER TABLE "public"."m_kompartemens" ADD CONSTRAINT "m_kompartemens_pkey" PRIMARY KEY ("id_komp");

-- ----------------------------
-- Primary Key structure for table m_menus
-- ----------------------------
ALTER TABLE "public"."m_menus" ADD CONSTRAINT "m_menus_pkey" PRIMARY KEY ("id_menus");

-- ----------------------------
-- Primary Key structure for table nilai_potensi_kkj
-- ----------------------------
ALTER TABLE "public"."nilai_potensi_kkj" ADD CONSTRAINT "nilai_potensi_kkj_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table nilai_potensi_kkj-backup
-- ----------------------------
ALTER TABLE "public"."nilai_potensi_kkj-backup" ADD CONSTRAINT "nilai_potensi_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table t_pdf_laporan
-- ----------------------------
ALTER TABLE "public"."t_pdf_laporan" ADD CONSTRAINT "t_pdf_laporan_pkey" PRIMARY KEY ("id_pdf_laporan");

-- ----------------------------
-- Checks structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "check_active" CHECK (active >= 0);
ALTER TABLE "public"."users" ADD CONSTRAINT "check_id" CHECK (id >= 0);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pk" PRIMARY KEY ("nik");

-- ----------------------------
-- Uniques structure for table users_groups
-- ----------------------------
ALTER TABLE "public"."users_groups" ADD CONSTRAINT "uc_users_groups" UNIQUE ("user_id", "group_id");

-- ----------------------------
-- Checks structure for table users_groups
-- ----------------------------
ALTER TABLE "public"."users_groups" ADD CONSTRAINT "users_groups_check_group_id" CHECK (group_id >= 0);
ALTER TABLE "public"."users_groups" ADD CONSTRAINT "users_groups_check_id" CHECK (id >= 0);
ALTER TABLE "public"."users_groups" ADD CONSTRAINT "users_groups_check_user_id" CHECK (user_id >= 0);

-- ----------------------------
-- Primary Key structure for table users_groups
-- ----------------------------
ALTER TABLE "public"."users_groups" ADD CONSTRAINT "users_groups_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users_info
-- ----------------------------
ALTER TABLE "public"."users_info" ADD CONSTRAINT "users_info_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table d_kompetensi_job_fam
-- ----------------------------
ALTER TABLE "public"."d_kompetensi_job_fam" ADD CONSTRAINT "d_kompetensi_job_fam_fk" FOREIGN KEY ("id_kkj") REFERENCES "public"."m_kkj" ("id_kkj") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."d_kompetensi_job_fam" ADD CONSTRAINT "d_kompetensi_job_fam_fk_1" FOREIGN KEY ("id_grade") REFERENCES "public"."m_grades" ("id_grad") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."d_kompetensi_job_fam" ADD CONSTRAINT "d_kompetensi_job_fam_fk_2" FOREIGN KEY ("id_job_fam") REFERENCES "public"."m_fitjob_fam" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table standar_kkj
-- ----------------------------
ALTER TABLE "public"."standar_kkj" ADD CONSTRAINT "standar_kkj_fk" FOREIGN KEY ("id_fitjob_fam") REFERENCES "public"."m_fitjob_fam" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."standar_kkj" ADD CONSTRAINT "standar_kkj_id_grade_fkey" FOREIGN KEY ("id_grade") REFERENCES "public"."m_grades" ("id_grad") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."standar_kkj" ADD CONSTRAINT "standar_kkj_id_kkj_fkey" FOREIGN KEY ("id_kkj") REFERENCES "public"."m_kkj" ("id_kkj") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table users_info
-- ----------------------------
ALTER TABLE "public"."users_info" ADD CONSTRAINT "users_info_fk" FOREIGN KEY ("nik_users") REFERENCES "public"."users" ("nik") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."users_info" ADD CONSTRAINT "users_info_id_dept_fkey" FOREIGN KEY ("id_dept") REFERENCES "public"."m_departemens" ("id_dept") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."users_info" ADD CONSTRAINT "users_info_id_grade_fkey" FOREIGN KEY ("id_grade") REFERENCES "public"."m_grades" ("id_grad") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."users_info" ADD CONSTRAINT "users_info_id_jab_fkey" FOREIGN KEY ("id_jab") REFERENCES "public"."m_jabatans" ("id_jabt") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."users_info" ADD CONSTRAINT "users_info_id_job_fam_fkey" FOREIGN KEY ("id_fitjob_fam") REFERENCES "public"."m_fitjob_fam" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
